iDubaiApp.controller('mainController',
    ['$scope', '$rootScope', 'LoadingService', '$timeout', '$state', '$translate', '$cordovaSocialSharing', '$DMLinks',
        '$auth', '$DbStorageService', 'underscore', '$logger', 'GuestUtils', '$dmServices', '$stopWords', '$arabicStopWords', '$cordovaCamera', 'SharedService', 'CommonUtils',
        function ($scope, $rootScope, LoadingService, $timeout, $state, $translate, $cordovaSocialSharing, $DMLinks,
                  $auth, $DbStorageService, underscore, $logger, GuestUtils, $dmServices, $stopWords, $arabicStopWords, $cordovaCamera, SharedService, CommonUtils) {

            console.log("=====================================================================================");
            console.log("=========================== ENTERING MAIN CONTROLLER ================================");
            console.log("=====================================================================================");

            // Hide splash screen
            navigator.splashscreen.hide();


            var _ = underscore;
            localStorage.setItem('welcome', 0); // init welcome to zero
            localStorage.setItem('BLIND-APP', 'normal');
            $rootScope.currentLang = localStorage.getItem('dm-lang');
            $rootScope.isLoggedIn = $auth.token ? true : false;
            $rootScope.isBack = false;
            $rootScope.back = true;
            $rootScope.colorClass = localStorage.getItem('invertColors') ? localStorage.getItem('invertColors') : 'normal';
            $rootScope.fontsFactor = localStorage.getItem('fontsFactor') ? JSON.parse(localStorage.getItem('fontsFactor')).minValue : 3;
            $rootScope.fonts = localStorage.getItem('originalFonts');
            $rootScope.geoLocation = localStorage.getItem('geoLocation') ? localStorage.getItem('geoLocation') : true;
            $rootScope.isNotif = localStorage.getItem('isNotif') ? localStorage.getItem('isNotif') : true;
            $rootScope.isWifi = localStorage.getItem('isWifi') ? localStorage.getItem('isWifi') : true;

            $scope.modal = {
                openCamera: false,
                happiness: false,
                hideHappiness: false
            };

            $auth.loginOnStartup().then(function () {
                $scope.token = $auth.token;
            });


            $scope.goTo = function (state, requireLogin, params) {
                if (requireLogin) {
                    $scope.token = $auth.token;
                    if ($rootScope.isLoggedIn || $auth.token || GuestUtils.getGuestPhoneNumber()) {
                        SharedService.setStateHistory(state, params);
                        $state.go(state, params);
                    } else {
                        SharedService.setStateHistory('personalDetails_1', {from: ''});
                        $state.go('personalDetails_1', {from: ''});

                    }
                } else {
                    SharedService.setStateHistory(state, params);
                    $state.go(state, params);
                }
            };

            $scope.goToPersonalDetails = function (params) {
                console.log('params ', params);
                var guestMode = GuestUtils.getGuestPhoneNumber();
                if ($rootScope.isLoggedIn || $auth.token || GuestUtils.getGuestPhoneNumber()) {
                    SharedService.setStateHistory('personalDetailsUpdate', params);
                    $state.go('personalDetailsUpdate', params);
                } else {
                    SharedService.setStateHistory('personalDetails_1', params);
                    $state.go('personalDetails_1', params);

                }
            };


            $scope.openCamera = function (source) {
                if (navigator.camera) {
                    var cameraOptions = {
                        //quality: 50,
                        //targetWidth: 720,
                        //targetHeight: 600,
                        encodingType: Camera.EncodingType.JPEG,
                        destinationType: Camera.DestinationType.FILE_URI,
                        correctOrientation: true,
                        sourceType: (source === 'camera') ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY
                    };
                    if (WL.Client.getEnvironment() === WL.Environment.IPHONE) {
                        navigator.camera.getPicture(function (imageData) {
                            $state.go("searchMedia", {
                                searchRequest: imageData,
                                mediaType: 'photo',
                                sourceType: source
                            });
                        }, function (message) {
                            console.log(message);
                        }, cameraOptions);
                    } else {
                        $cordovaCamera.getPicture(cameraOptions).then(function (imageData) {
                            $state.go("searchMedia", {
                                searchRequest: imageData,
                                mediaType: 'photo',
                                sourceType: source
                            });
                        }, function (message) {
                            console.log(message);
                        }, cameraOptions);
                    }
                }

            };


            /*
             destinationType: Camera.DestinationType.NATIVE_URI,
             correctOrientation: true,
             sourceType: (source === 'camera') ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY
             };
             // $scope.closePopup();
             navigator.camera.getPicture(function (imageData) {
             window.resolveLocalFileSystemURL(imageData,
             function(entry) {
             entry.file(function(file) {
             EXIF.getData(file, function() {
             var long = EXIF.getTag(this, 'GPSLongitude');
             alert(long);
             });
             // do something useful....

             }, standardErrorHandler);
             },
             function(e) {
             alert('Unexpected error obtaining image file.');
             standardErrorHandler(e);
             });
             */

            var body = angular.element(document.querySelector(".toShowMenu"));
            $scope.showMenu = function () {
                if (!body.hasClass('sidebar-open')) {
                    body.addClass('sidebar-open');
                } else {
                    body.removeClass('sidebar-open');
                }
            };

            // Share the app
            $scope.onShareApp = function () {
                var currentLang = localStorage.getItem('dm-lang');
                var text = (currentLang == 'ar') ? "دبي 24/7 يعتبر هو التطبيق الرئيسي لعملاء بلدية دبي التي تمكن المستخدمين من التقدم بطلب للحصول على الخدمة ، أو تقديم شكوى والتحقق من إشعاراتهم.هذه التجربة الذكية للعملاء ستقلل من الحاجة إلى الاتصال بخدمة العملاء أو حتى زيارة المراكز" : "Dubai 24/7 is Dubai Municipality Mobile application to enable customers to easily raise their notifications and require services without the need of calling the call center";
                var link = '';
                if (WL.Client.getEnvironment() === WL.Environment.ANDROID) {
                    link = $DMLinks.dmPlayStore;
                } else {
                    link = $DMLinks.dmItunes;
                }
                $timeout(function () {
                    try {
                        //message, subject, file, link
                        $cordovaSocialSharing.share(text, null, null, link)
                            .then(function (success) {
                                console.log("App shared successfully: ", success);
                            }, function (error) {
                                console.log("Social sharing error : ", error);
                            });

                    } catch (ex) {
                        console.log(ex);
                    }

                }, 500);

            };

            //Rate App
            $scope.onAppRate = function () {
                document.addEventListener("deviceready", function () {
                    if (WL.Client.getEnvironment() === WL.Environment.ANDROID) {
                        window.open($DMLinks.dmPlayStoreMarket, '_system');
                    } else {
                        window.open($DMLinks.dmItunesMarket, '_system');
                    }
                    /*$cordovaAppRate.navigateToAppStore().then(function (result) {
                     // success
                     console.log("success to Rate App", result);
                     });*/
                    body.removeClass('sidebar-open');
                }, false);
            }


            // Open app settings
            $scope.onOpenSettings = function () {
                window.NativeSettings.open('settings');
            };

            // Open market (PlayStore/iTunes)
            $scope.onOpenMarket = function () {
                if (WL.Client.getEnvironment() === WL.Environment.ANDROID) {
                    window.open($DMLinks.dmPlayStoreMarket, '_system');
                } else {
                    window.open($DMLinks.dmItunesMarket, '_system');
                }
            };

            // DM related applications
            $scope.onOpenRelatedApps = function () {
                if (WL.Client.getEnvironment() === WL.Environment.ANDROID) {
                    window.open($DMLinks.dmPlayStoreRelatedApp, '_system');
                } else {
                    window.open($DMLinks.dmItunesRelatedApp, '_system');
                }
            };

            $rootScope.history = [];
            $scope.colorBlindIcon = false;
            $scope.bodyClass = "";

            $scope.openEmail = function (loginMessage) {
                if (loginMessage == 'UM005') {
                    window.open('mailto:eservices@dm.gov.ae', '_system');
                }
            };

            // Check if environment is IOS
            $scope.isIos = false;
            try {
                if (WL.Client.getEnvironment() === WL.Environment.IPHONE)
                    $scope.isIos = true;
            } catch (e) {
                $logger.log(e);
            }


            /*
             * Change state
             */
            $rootScope.$on('$stateChangeStart',
                function (event, to, toParams, from, fromParams) {
                    console.log('event:', event, 'to:', to, 'toParams:', toParams, 'from:', from, 'fromParams:', fromParams);
                    var welcomeCount = localStorage.getItem('welcome');

                    localStorage.setItem('welcome', +welcomeCount + 1);
                    if (LoadingService.isOpen) {
                        LoadingService.hide();
                    }
                    closeMenu();
                    if ($rootScope.isBack) {
                        $rootScope.history.splice($rootScope.history.length - 1);
                        $rootScope.isBack = false;
                    } else {
                        var previousState = to;
                        previousState.params = toParams;
                        if ($rootScope.history.length === 0 || ($rootScope.history[$rootScope.history.length - 1].name !== previousState.name)) {
                            $rootScope.history.push(previousState);
                        }
                    }
                });


            /*
             *  Apply Font Size
             * */

            $rootScope.applyFontSizes = function () {
                console.log('==================== Applying new fonts sizes');

                if ($('.page  ')) {
                    $('.page  ').css('font-size', JSON.parse($rootScope.fonts).page * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz25-R')) {
                    $('.fz25-R').css('font-size', JSON.parse($rootScope.fonts).fz25R * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz25-B')) {
                    $('.fz25-B').css('font-size', JSON.parse($rootScope.fonts).fz25B * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz24-B')) {
                    $('.fz24-B').css('font-size', JSON.parse($rootScope.fonts).fz24B * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz24-R')) {
                    $('.fz24-R').css('font-size', JSON.parse($rootScope.fonts).fz24R * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz23-R')) {
                    $('.fz23-R').css('font-size', JSON.parse($rootScope.fonts).fz23R * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz23-B')) {
                    $('.fz23-B').css('font-size', JSON.parse($rootScope.fonts).fz23B * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz22-R')) {
                    $('.fz22-R').css('font-size', JSON.parse($rootScope.fonts).fz22R * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz22-B')) {
                    $('.fz22-B').css('font-size', JSON.parse($rootScope.fonts).fz22B * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz21-R')) {
                    $('.fz21-R').css('font-size', JSON.parse($rootScope.fonts).fz21R * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz19-R')) {
                    $('.fz19-R').css('font-size', JSON.parse($rootScope.fonts).fz19R * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz19-B')) {
                    $('.fz19-B').css('font-size', JSON.parse($rootScope.fonts).fz19B * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz15-R')) {
                    $('.fz15-R').css('font-size', JSON.parse($rootScope.fonts).fz15R * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz15-B')) {
                    $('.fz15-B').css('font-size', JSON.parse($rootScope.fonts).fz15B * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz13-R')) {
                    $('.fz13-R').css('font-size', JSON.parse($rootScope.fonts).fz13R * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz13-B')) {
                    $('.fz13-B').css('font-size', JSON.parse($rootScope.fonts).fz13B * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz21-B')) {
                    $('.fz21-B').css('font-size', JSON.parse($rootScope.fonts).fz21B * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz20-R')) {
                    $('.fz20-R').css('font-size', JSON.parse($rootScope.fonts).fz20R * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz20-B')) {
                    $('.fz20-B').css('font-size', JSON.parse($rootScope.fonts).fz20B * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz18-B')) {
                    $('.fz18-B').css('font-size', JSON.parse($rootScope.fonts).fz18B * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz18-R')) {
                    $('.fz18-R').css('font-size', JSON.parse($rootScope.fonts).fz18R * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz17-R')) {
                    $('.fz17-R').css('font-size', JSON.parse($rootScope.fonts).fz17R * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz17-B')) {
                    $('.fz17-B').css('font-size', JSON.parse($rootScope.fonts).fz17B * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz17-600')) {
                    $('.fz17-600').css('font-size', JSON.parse($rootScope.fonts).fz17600 * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz16-B')) {
                    $('.fz16-B').css('font-size', JSON.parse($rootScope.fonts).fz16B * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz16-R')) {
                    $('.fz16-R').css('font-size', JSON.parse($rootScope.fonts).fz16R * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz14-B')) {
                    $('.fz14-B').css('font-size', JSON.parse($rootScope.fonts).fz14B * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz14-R')) {
                    $('.fz14-R').css('font-size', JSON.parse($rootScope.fonts).fz14R * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz12-B')) {
                    $('.fz12-B').css('font-size', JSON.parse($rootScope.fonts).fz12B * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
                if ($('.fz12-R')) {
                    $('.fz12-R').css('font-size', JSON.parse($rootScope.fonts).fz12R * (0.4 * $rootScope.fontsFactor - 0.2) + 'px');
                }
            }


            /**
             * Loaded view content
             */
            $scope.$on('$viewContentLoaded', function (event) {
                $timeout(function () {
                    if ($scope.bodyClass) {
                        $("body").removeClass().addClass($scope.bodyClass);
                    } else {
                        $("body").removeClass();
                    }
                    if ($scope.isIos) {
                        $("body").addClass("wl_ios7");
                    }
                    $(window).scrollTop(0);
                    try {
                        //$("body").addClass($state.current.name.replace(".", "-") + "-state");
                    } catch (e) {
                    }

                    $rootScope.applyFontSizes();
                }, 10);
            });


            /*
            // Get script
            $.getMultiScripts = function(arr) {
                var _arr = $.map(arr, function(scr) {
                    return $.getScript( scr );
                });

                _arr.push($.Deferred(function( deferred ){
                    $( deferred.resolve );
                }));

                return $.when.apply($, _arr);
            }
            var script_arr = [
                'https://maps.googleapis.com/maps/api/js?libraries=places&amp&key=AIzaSyDAyPe0GL66TrDBhkzQCF9-8pIWTLoxg7o&amp&sensor=true&amp&v=3',
            ];
            $.getMultiScripts(script_arr)
                .done(function() {
                    // all scripts loaded
                    console.log('all scripts loaded');
                }).fail(function (jqxhr, settings, exception) {

            });
            */

        }
    ]
);
