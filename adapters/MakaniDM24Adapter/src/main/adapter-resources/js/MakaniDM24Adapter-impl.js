var token = "ml5v8eoqin9k1tu2squ1mkbe4j!=+=rna+Etmv1u6NdHBTbnfwSw==";

function getMakaniDetails(makaniNumber) {
	var request = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">";
	request += "<soapenv:Header/>";
	request += "<soapenv:Body>";
	request += "<tem:GetMakaniDetails>";
	request += "<tem:makani>" + makaniNumber + "</tem:makani>";
	request += "<tem:token>" + token + "</tem:token>";
	request += "<tem:remarks>DM Smart Government</tem:remarks>";
	request += "</tem:GetMakaniDetails>";
	request += "</soapenv:Body>";
	request += "</soapenv:Envelope>";

	return request;
}

function getNearestMakaniFromXY(lat, lng) {
	var request = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">";
	request += "<soapenv:Header/>";
	request += "<soapenv:Body>";
	request += "<tem:GetNearestMakaniFromXY>";
	request += "<tem:type>LATLNG</tem:type>";
	request += "<tem:coordinateX>" + lat + "</tem:coordinateX>";
	request += "<tem:coordinateY>" + lng + "</tem:coordinateY>";
	request += "<tem:token>"+ token +"</tem:token>";
	request += "<tem:remarks>DM Smart Government</tem:remarks>";
	request += "</tem:GetNearestMakaniFromXY>";
	request += "</soapenv:Body>";
	request += "</soapenv:Envelope>";

	return request;
}

function getIsValidMakaniIDRequest(makaniID){
	try {
        var request = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">";
        request += "<soapenv:Header/>";
        request += "<soapenv:Body>";
        request += "<tem:IsValidMakani>";
        request += "<tem:makani>";
        request += makaniID;
        request += "</tem:makani>";
        request += "<tem:remarks>DM Smart Government</tem:remarks>";
        request += "</tem:IsValidMakani>";
        request += "</soapenv:Body>";
        request += "</soapenv:Envelope>";

        return request;
    } catch (e) {
        return {
            isSuccessful: false,
            reference: e
        };
    }
}

function getLatLngofMakaniIDRequest(makaniID){
	try {
        var request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">';
        request += "<soapenv:Header/>";
        request += "<soapenv:Body>";
        request += "<tem:GetLatLngFromMakani>";
        request += "<tem:makani>";
        request += makaniID;
        request += "</tem:makani>";
        request += "<tem:remarks>?</tem:remarks>";
        request += "</tem:GetLatLngFromMakani>";
        request += "</soapenv:Body>";
        request += "</soapenv:Envelope>";
        return request;
    } catch (e) {
        return {
            isSuccessful: false,
            reference: e
        };
    }

}

function getMakaniIDofLatLngRequest(lat, lng){

	try {
        var request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">';
        request += "<soapenv:Header/>";
        request += "<soapenv:Body>";
        request += "<tem:GetMakanis_ParcelId>";
        request += "<tem:type>LATLNG</tem:type>";
        request += "<tem:coordinateX>"+ lat +"</tem:coordinateX>";
        request += "<tem:coordinateY>"+ lng +"</tem:coordinateY>";
        request += "<tem:remarks>Sent from DM Smart Gov</tem:remarks>";
        request += "</tem:GetMakanis_ParcelId>";
        request += "</soapenv:Body>";
        request += "</soapenv:Envelope>";
        return request;
    } catch (e) {
        return {
            isSuccessful: false,
            reference: e
        };
    }
}

function getBuildingInfoRequest(lat, lng){
	try {
        var request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">';
        request += "<soapenv:Header/>";
        request += "<soapenv:Body>";
        request += "<tem:GetBuildingInfo>";
        request += "<tem:latlng>"+ lat +","+lng+"</tem:latlng>";
        request += "<tem:remarks>Sent from DM Smart Gov</tem:remarks>";
        request += "</tem:GetBuildingInfo>";
        request += "</soapenv:Body>";
        request += "</soapenv:Envelope>";
        return request;
    } catch (e) {
        return {
            isSuccessful: false,
            reference: e
        };
    }
}

function getBuildingAddressRequest(makaniID){
	try {
        var request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">';
        request += "<soapenv:Header/>";
        request += "<soapenv:Body>";
        request += "<tem:GetBuildingAddress>";
        request += "<tem:makani>";
        request += makaniID;
        request += "</tem:makani>";
        request += "<tem:remarks>?</tem:remarks>";
        request += "</tem:GetBuildingAddress>";
        request += "</soapenv:Body>";
        request += "</soapenv:Envelope>";
        return request;
    } catch (e) {
        return {
            isSuccessful: false,
            reference: e
        };
    }
}

function getMakaniNumberDetails(makaniNumber) {
	try {
        var request = getMakaniDetails(makaniNumber);
        WL.Logger.error(request);
        var input = {
            method: 'post',
            returnedContentType: 'xml',
            path: '/GMS/Service.svc',
            headers: {SOAPAction: 'http://tempuri.org/IService/GetMakaniDetails'},
            body: {
                contentType: 'text/xml;charset=UTF-8',
                content: request
            }
        };
        WL.Logger.error("getMakaniDetails :: request  : " + JSON.stringify(input));
        var x = WL.Server.invokeHttp(input);
        WL.Logger.error("getMakaniDetails :: response : " + JSON.stringify(x));
        if (x.isSuccessful) {
            return {
                isSuccess: true,
                details: x.Envelope.Body.GetMakaniDetailsResponse["GetMakaniDetailsResult"]
            };
        }
        return {
            isSuccess: true,
            details: {"DATA": "Serivce is not availible, please try again."}
        };
    } catch (e) {
        return {
            isSuccessful: false,
            reference: e
        };
    }
}

function getNearestMakani(lat, lng) {
	var request = getNearestMakaniFromXY(lat, lng);
	WL.Logger.error(JSON.stringify(request));
	var input = {
		method: 'post',
		returnedContentType: 'xml',
		path: '/GMS/Service.svc',
		headers: {SOAPAction: 'http://tempuri.org/IService/GetNearestMakaniFromXY'},
		body: {
			contentType: 'text/xml;charset=UTF-8',
			content: request
		}
	};
	WL.Logger.error("getNearestMakani :: request  : " + JSON.stringify(input));
	var x = MFP.Server.invokeHttp(input);// WL.Server.invokeHttp(input);
	WL.Logger.error("getNearestMakani :: response : " + JSON.stringify(x));
	if (x.isSuccessful) {
		return {
			isSuccess: true,
			details: x.Envelope.Body.GetNearestMakaniFromXYResponse["GetNearestMakaniFromXYResult"]
		};
	}
	return {
		isSuccess: true,
		details: '{"DATA": "Serivce is not availible, please try again."}'
	};

}

function checkMakani(makaniID){
	var validMakani = false;
	var xResult = "";
	var yResult = "";
	var wsResult = "This is not a valid Makani ID";
	var xyResult = "";

	var request = getIsValidMakaniIDRequest(makaniID);
	var input = {
		method : 'post',
		returnedContentType : 'xml',
		path : '/dmsmartgov/Service.svc',
		headers: {SOAPAction: 'http://tempuri.org/IService/IsValidMakani', Operation:'IsValidMakani'},
		body:{
			contentType:'text/xml;charset=UTF-8',
			content:request
		}
	};

	var x = MFP.Server.invokeHttp(input);// WL.Server.invokeHttp(input);

	if(x.isSuccessful) {
		try{
			var data=x.Envelope.Body.IsValidMakaniResponse["IsValidMakaniResult"];
			if(data&&data.CDATA==="1") {

				wsResult = "Makani ID Found";

				var latlng=getLatLngfromMakani(makaniID);
				if(latlng.isSuccess) {
					xyResult =latlng.details.CDATA;
					try{
						xyResult = xyResult.substring(xyResult.indexOf("25"), xyResult.indexOf("55")+10);
						xResult = xyResult.substring(0,xyResult.indexOf(",")-6);
						yResult = xyResult.substring(xyResult.indexOf(",")+1);
					}catch(e) {
						wsResult = "Get related coordinates of Makani Number failed"+latlng.details;
					}
				}
				return {
					isSuccess : true,
					result:{
						x : xResult,
						y : yResult,
						details : wsResult
					}
				};
			}else{
				return {
					isSuccess : false,
					result:{
						x : xResult,
						y : yResult,
						details : wsResult
					}
				};
			}
		}catch(e){
			return {
				isSuccess : false,
				result:{
					x : xResult,
					y : yResult,
					details : "Serivce is not availible, please try again."+e
				}
			};
		}
	}else{
		return {
			isSuccess : false,
			result:{
				x : xResult,
				y : yResult,
				details : "Serivce is not availible, please try again."
			}
		};
	}

	return x;
}

function getLatLngfromMakani(makaniID){
	var request = getLatLngofMakaniIDRequest(makaniID);

	var input = {
		method : 'post',
		returnedContentType : 'xml',
		path : '/dmsmartgov/Service.svc',
		headers: {SOAPAction: 'http://tempuri.org/IService/GetLatLngFromMakani', Operation:'GetLatLngFromMakani'},
		body:{
			contentType:'text/xml;charset=UTF-8',
			content:request
		}
	};

	var x = MFP.Server.invokeHttp(input);// WL.Server.invokeHttp(input);

	if(x.isSuccessful){
		return {
			isSuccess: true,
			details: x.Envelope.Body.GetLatLngFromMakaniResponse["GetLatLngFromMakaniResult"]
		};
	}
	return x;
}

function getMakani(lat, lng){
	try {
        var request = getMakaniIDofLatLngRequest(lng, lat);
        var makaniResult;
        var input = {
            method : 'post',
            returnedContentType : 'xml',
            path : '/dmsmartgov/Service.svc',
            headers: {SOAPAction: 'http://tempuri.org/IService/GetMakanis_ParcelId', Operation:'GetMakanis_ParcelId'},
            body:{
                contentType:'text/xml;charset=UTF-8',
                content:request
            }
        };

        var x = MFP.Server.invokeHttp(input);// WL.Server.invokeHttp(input);
        var requestOK = true;
        var makaniData = '';
        var wsResult = '';

        if(x.isSuccessful){
            try{
                makaniResult = x.Envelope.Body.GetMakanis_ParcelIdResponse["GetMakanis_ParcelIdResult"];
                var maData=makaniResult.CDATA;
                if(maData.indexOf("No")>0){
                    requestOK = false;
                    makaniData = maData;
                }else
                    makaniData = makaniResult.CDATA;
            }catch(e) {
                requestOK = false;
                wsResult = "Get related Makani Number failed."+makaniData;
            }

            return {
                isSuccess: requestOK,
                result:{
                    makaniID : makaniData,
                    details : wsResult
                }
            };
        } else {
            return {
                isSuccess: true,
                details: {"DATA": "Serivce is not availible, please try again."}
            };
        }
        return x;
    }
    catch (e) {
        return{
            isSuccessful: false,
            reference : e
        }
    }

}

function getBuildingInfo(lat, lng){
	var request = getBuildingInfoRequest(lat, lng);
	var input = {
		method : 'post',
		returnedContentType : 'xml',
		path : '/dmsmartgov/Service.svc',
		headers: {SOAPAction: 'http://tempuri.org/IService/GetBuildingInfo', Operation:'GetBuildingInfo'},
		body:{
			contentType:'text/xml;charset=UTF-8',
			content:request
		}
	};

	var x = WL.Server.invokeHttp(input);
	var requestOK = true;
	var makaniData = '';
	var makaniResult = '';
	var wsResult = '';
	if(x.isSuccessful){
		try{
			makaniResult = x.Envelope.Body.GetBuildingInfoResponse["GetBuildingInfoResult"];
			var maData=makaniResult.CDATA;
			if(maData.indexOf("No")>0){
				requestOK = false;
				makaniData = maData;
			}else
				makaniData = makaniResult.CDATA;
//			makaniData = '{"SMARTGOV": [{"MAKANI_LATLNG": "09006 66322,25.0003715910032,55.1074917590075","BLDG_OUTLINE": "25.0004549636615,55.1073222752874;25.0004575681102,55.1073133849558;25.0004589421848,55.1073086969646;25.0004804389763,55.1073162653484;25.0007978329214,55.1074281364756;25.0006931787544,55.1077856729465;25.0003566908621,55.1076670701611;25.0003608130893,55.1076529863985;25.0003253704804,55.1076404905074;25.0004219236109,55.1073106269914;25.0004549636615,55.1073222752874;;25.000593568211,55.1074691141871;25.0004456919191,55.1074169923754;25.0004366482055,55.1074478855102;25.0005970522247,55.1075044123855;25.0006060959647,55.1074735291203;25.000593568211,55.1074691141871;;25.0005628146363,55.1076256635093;25.0005714722072,55.1075960885434;25.0004101559571,55.1075392363338;25.000401498397,55.1075688112649;25.0005484262337,55.1076205882674;25.0005628146363,55.1076256635093;;25.000642678087,55.1076539276911;25.0006817155256,55.1076676850051;25.0006905976025,55.107637327038;25.0006515691886,55.1076235697161;25.000642678087,55.1076539276911;;25.000677851992,55.1075309907838;25.0007167810525,55.1075447086373;25.0007251960998,55.1075159463554;25.0006862760642,55.1075022284936;25.000677851992,55.1075309907838;"}]}';

		}catch(e) {
			requestOK = false;
			wsResult = "Get related Makani Number failed."+makaniData;
		}

		return {
			isSuccess: requestOK,
			result:{
				makaniID : makaniData,
				details : wsResult
			}
		};
	}
	return x;
}

function getBuildingAddress(makaniID){
	var request = getBuildingAddressRequest(makaniID);

	var input = {
		method : 'post',
		returnedContentType : 'xml',
		path : '/dmsmartgov/Service.svc',
		headers: {SOAPAction: 'http://tempuri.org/IService/GetBuildingAddress', Operation:'GetBuildingAddress'},
		body:{
			contentType:'text/xml;charset=UTF-8',
			content:request
		}
	};

	var x = WL.Server.invokeHttp(input);

	if(x.isSuccessful){
		return {
			isSuccess: true,
			details: x.Envelope.Body.GetBuildingAddressResponse["GetBuildingAddressResult"]
		};
	}
	return x;
}

function checkMakaniReturnBuildingAddress(makaniID){
	var validMakani = false;
	var xResult = "";
	var yResult = "";
	var wsResult = "This is not a valid Makani ID";
	var addressResult = "";

	var request = getIsValidMakaniIDRequest(makaniID);
	var input = {
		method : 'post',
		returnedContentType : 'xml',
		path : 'dmsmartgov/Service.svc',
		headers: {SOAPAction: 'http://tempuri.org/IService/IsValidMakani', Operation:'IsValidMakani'},
		body:{
			contentType:'text/xml;charset=UTF-8',
			content:request
		}
	};

	var x = WL.Server.invokeHttp(input);

	if(x.isSuccessful) {
		try{
			var data=x.Envelope.Body.IsValidMakaniResponse["IsValidMakaniResult"];
			if(data&&data.CDATA==="1") {

				wsResult = "Makani ID Found";

				var address=getBuildingAddress(makaniID);
				if(address.isSuccess) {
					addressResult =address.details.CDATA;
				}
				return {
					isSuccess : true,
					result:{
						details : addressResult
					}
				};
			}else{
				return {
					isSuccess : false,
					result:{
						details : wsResult
					}
				};
			}
		}catch(e){
			return {
				isSuccess : false,
				result:{
					details : "Serivce is not availible, please try again."+e
				}
			};
		}
	}else{
		return {
			isSuccess : false,
			result:{
				details : "Serivce is not availible, please try again."
			}
		};
	}

	return x;
}

