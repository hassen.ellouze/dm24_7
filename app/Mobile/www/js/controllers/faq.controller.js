iDubaiApp.controller('faqController',['$scope','$filter','$WL','$notification','LoadingService', 'faqsService', '$logger', '$stateParams', 'SharedService', '$state',
    function($scope,$filter,$WL,$notification,LoadingService, faqsService, $logger, $stateParams, SharedService, $state){

        $scope.currentLang = localStorage.getItem('dm-lang');
        $scope.categories = {};
        $scope.catSelected = {};
        $scope.showFaq = false;
        var categories = [], tmpFaq;
        $stateParams =  SharedService.getStateHistory($state.current.name);

        if($stateParams && $stateParams.questions) {
            $scope.questions = $stateParams.questions;
        }else if($stateParams && $stateParams.question){
            $scope.question = $stateParams.question;
            $scope.forBack = $stateParams.forBack;
        }
        else {
            initFaqList();
        }


        function objectMap(object, mapFn) {
            return Object.keys(object).reduce(function(result, key) {
                result[key] = mapFn(object[key])
                return result
            }, {})
        }




        function initFaqList() {
            faqsService.getFAQ().then(function(data){
                console.log('==> getFAQ ', data );
                if (data) {
                    $scope.faqList = data;
                    var s =  Object.keys(data).map(
                        function(item, index){
                            console.log(JSON.stringify(data[item])+' '+index);
                            return JSON.stringify(data[item]);
                        });
                }
                var newObject = objectMap(data, function(value) {
                    return value
                });
                console.log(newObject);
            }, function(error){
                $logger.log("faq invoke adapter fail::",error);
            });
        }


        $scope.goToFaqQuestions = function (category) {
            SharedService.setStateHistory('faq-questions', {
                questions: category,
                from:'faq'
            });

            $state.go('faq-questions',{
                questions: category
            })
        }

        $scope.goToFaqQA = function (question, questions) {
            SharedService.setStateHistory('faq-question-answer', {
                question: question,
                forBack: questions,
                from:'faq-questions'
            });

            $state.go('faq-question-answer',{
                question: question,
                forBack: questions
            })
        }

        $scope.goBack = function () {
            window.history.go(-1);
        }

    }]);
