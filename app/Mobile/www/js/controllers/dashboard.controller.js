iDubaiApp.controller('dashboardController', ['$scope', '$timeout', '$state', '$auth', 'underscore', '$rootScope', '$myLocations',
    'DMService', 'DMCategories', 'GuestUtils', 'PhoneUtils', 'TrackingService', '$location', '$window', 'LoadingService', '$dmServices', 'cat', '$notification', '$filter', '$http', 'Makani', '$q', 'AnalyticsService', 'Constants', 'SearchEngine', 'SharedService', '$cordovaCamera',
    function ($scope, $timeout, $state, $auth, underscore, $rootScope, $myLocations,
              DMService, DMCategories, GuestUtils, PhoneUtils, TrackingService, $location, $window, LoadingService, $dmServices, cat, $notification, $filter, $http, Makani, $q, AnalyticsService, Constants, SearchEngine, SharedService, $cordovaCamera) {
        console.log('dashboardController');
        var _ = underscore;
        AnalyticsService.trackRoute(Constants.HIT_TYPE.SCREEN_VIEW, 'Dashboard');

        $scope.currentLang = localStorage.getItem('dm-lang');
        $rootScope.history = [];
        $scope.requestsCount = 0;
        $scope.notifications = [];
        $scope.showClickHereGuide = false;
        $scope.isShow = true;
        $rootScope.allServices = {};
        $rootScope.allCategories = [];
        $scope.LoudNoise = false;
        $scope.modal = {
            isTrending: false,
            isRecent: false,
            isFavourites: false,
            openNoise: false,
            openCamera: false
        };
        $scope.trendingServices = [];
        $scope.isScroling = false;

        localStorage.setItem('recordedAudio', '');
        localStorage.setItem('recordedNoiseAudio', '');
        if (localStorage.getItem('uploadImage')) {
            localStorage.setItem('uploadImage', '');
            localStorage.setItem('sourceType', '');
            localStorage.setItem('metaData', '');
            localStorage.setItem('orientation', '');

        }


        window.db = window.openDatabase('DM247', '1.0', 'todo list example db', 2 * 1024 * 1024);
        window.db.transaction(function (txDB) {

            //console.log('SELECT name FROM sqlite_master mostPopular');
            var invocationData = {
                adapter: "PopularServicesAdapter",
                procedure: "getPopularServices",
                parameters: []
            };
            WL.Client.invokeProcedure(invocationData, {
                onSuccess: function (data) {
                    //console.log('PopularServices payload ::', JSON.stringify(data));
                    if (data.invocationResult.isSuccessful) {
                        window.db.transaction(function (txDB) {
                            txDB.executeSql("DELETE FROM mostPopular WHERE 1 = 1", [], function (txDelPop, resDelPop) {
                                //console.log('Delete FROM mostPopular ', JSON.stringify(txDelPop), JSON.stringify(resDelPop));

                                data.invocationResult.array.map(function (item, index) {
                                    //console.log('item from payload ::  ', JSON.stringify(item));
                                    txDB.executeSql('INSERT INTO mostPopular VALUES (?)', [item.Title_en]);

                                    /*$dmServices.map(function(itemAll, indexAll){
                                        if(item.Title_en == itemAll.serviceEN) {
                                            var id = ( itemAll.service_request_id ) ? itemAll.service_request_id : itemAll.incident_id;
                                            txDB.executeSql('INSERT INTO mostPopular VALUES (?)',[id],function(tx11, res11){
                                                console.log('INSERT INTO mostPopular VALUES ', JSON.stringify(tx11), JSON.stringify(res11));
                                            });
                                        }
                                    })*/
                                });
                                txDB.executeSql("SELECT * FROM mostPopular", [], function (txpp, respp) {
                                   // console.log('selecting mostPopular ', JSON.stringify(txpp), JSON.stringify(respp));
                                    if (respp.rows.length > 0) {

                                        for (var i = 0; i < respp.rows.length; i++) {
                                            console.log(JSON.stringify(respp.rows.item(i).id));
                                            $dmServices.some(function (itemAll, indexAll) {

                                                if (itemAll.serviceEN == respp.rows.item(i).id) {
                                                    //console.log('trend service :: ', JSON.stringify(itemAll));
                                                    var category = _.find(cat, function (itemCat, indexCat) {
                                                        return itemAll.category_id == itemCat.category_id;
                                                    });
                                                    if (itemAll.category_id <= 8) {
                                                        itemAll.category = category;
                                                    } else {
                                                        itemAll.category = {
                                                            title: 'General Services',
                                                            titleAR: 'خدمات عامة',
                                                            description: 'General Services',
                                                            descriptionAR: 'خدمات عامة',
                                                            image: 'generalCategory.png',
                                                            gear: 'other-gear.png',
                                                            backgroundColor: 'generalCategory',
                                                            category_id: 99
                                                        }
                                                    }

                                                    $scope.trendingServices.push(itemAll);
                                                }
                                            })
                                        }
                                        if ($scope.currentLang == 'ar') {
                                            $scope.trendingServices = $scope.trendingServices.reverse();
                                        }
                                        //console.log('Final Trending Service :: ', JSON.stringify($scope.trendingServices));
                                        $scope.modal.isTrending = true;
                                        $scope.$apply();

                                    }
                                })

                            })

                        });


                    }
                },
                onFailure: function (err) {
                    window.db.transaction(function (txDB) {
                        txDB.executeSql("SELECT * FROM mostPopular", [], function (txpp, respp) {
                            //console.log('selecting mostPopular ', JSON.stringify(txpp), JSON.stringify(respp));
                            if (respp.rows.length > 0) {

                                for (var i = 0; i < respp.rows.length; i++) {
                                    console.log(JSON.stringify(respp.rows.item(i).id));
                                    $dmServices.some(function (itemAll, indexAll) {

                                        if (itemAll.serviceEN == respp.rows.item(i).id) {
                                            //console.log('trend service :: ', JSON.stringify(itemAll));
                                            var category = _.find(cat, function (itemCat, indexCat) {
                                                return itemAll.category_id == itemCat.category_id;
                                            });
                                            if (itemAll.category_id <= 8) {
                                                itemAll.category = category;
                                            } else {
                                                itemAll.category = {
                                                    title: 'General Services',
                                                    titleAR: 'خدمات عامة',
                                                    description: 'General Services',
                                                    descriptionAR: 'خدمات عامة',
                                                    image: 'generalCategory.png',
                                                    gear: 'other-gear.png',
                                                    backgroundColor: 'generalCategory',
                                                    category_id: 99
                                                }
                                            }

                                            $scope.trendingServices.push(itemAll);
                                        }
                                    })
                                }
                                if ($scope.currentLang == 'ar') {
                                    $scope.trendingServices = $scope.trendingServices.reverse();
                                }
                                //console.log('Final Trending Service :: ', JSON.stringify($scope.trendingServices));
                                $scope.modal.isTrending = true;
                                $scope.$apply();

                            }
                        })
                    });
                    console.log('error PopularServices ::', err);

                }
            });


        })


        /*function callTracking(car) {
                TrackingService.vehicleEnquireService(car).then(function (data) {
                    console.log('vehicle number : ' +car +'\n'+ JSON.stringify(data.invocationResult.Envelope.Body.VehicleInquiryServiceResponse.VehicleInquiryServiceResult));
                }, function (error) {
                    console.log('vehicle number : ' +car +'\n'+ JSON.stringify(error));
                });
                    }*/

        $scope.goTo = function (state, parameters) {
            AnalyticsService.trackRoute(Constants.HIT_TYPE.EVENT, '', {
                eventCategory: Constants.EVENTS.Trending_Services,
                eventAction: Constants.EVENTS.Trending_Services,
                eventLabel: parameters.service.service_title_en
            });

            SharedService.setStateHistory(state, parameters);
            $state.go(state, parameters);
        };


        /*
        *  Get trending services and check if isVisitor
        * */

        /*$scope.trendingServices = _.filter($dmServices, function(item, index) {
            return item.isPopular == true
		});*/


        /*if ( GuestUtils.getGuestPhoneNumber()) {
            var guestInfo = GuestUtils.getGuestInfo();
            if (guestInfo && guestInfo.option3 == true) {
                $scope.trendingServices = _.filter($dmServices, function(item, index) {
                    return item.isPopular && item.isVisitor == true;
                });
            }else {
                $scope.trendingServices = _.filter($dmServices, function(item, index) {
                    return item.isPopular == true
                });
            }


           /!* $scope.trendingServices = _.filter($dmServices, function(item, index) {
                return item.isPopular == true  &&
                    	item.isVisitor == true;
            });
        } else {
            $scope.trendingServices = _.filter($dmServices, function(item, index) {
                return item.isPopular == true ;
            });*!/
        } else {
            $scope.trendingServices = _.filter($dmServices, function(item, index) {
                return item.isPopular == true
            });
		}*/

        // Calculate the height of the last request box
        var deviceHeight = ($(window).height());
        var lastRequestHeight = deviceHeight - 235;
        if (WL.Client.getEnvironment() === WL.Environment.IPHONE) {
            lastRequestHeight = deviceHeight - 265;
        }
        // Number of request displayed in the dashboard
        var nbRequestToShow = Math.floor((lastRequestHeight - 40) / 80);
        var welcome = localStorage.getItem('welcome');
        if (welcome > 5) {
            $scope.showClickHereGuide = false;
            lastRequestHeight = deviceHeight - 195;
            if (WL.Client.getEnvironment() === WL.Environment.IPHONE) {
                lastRequestHeight = deviceHeight - 210;
            }
        }
        $("#lastRequest").css("height", lastRequestHeight);

        // Disappear Welcome after 5 navigation in the app
        $scope.showWelcome = (welcome <= 5);

        // Hide click here guide
        $scope.hideClickHereGuide = function () {
            $scope.showClickHereGuide = false;
            $scope.isShow = false;
        };

        /**
         * Check user authenticated or is quest
         */
        $scope.token = $auth.token;

        /*if ($rootScope.isLoggedIn || $auth.token) {
            getLatestNotifications(PhoneUtils.formatPhoneNumber($auth.dubaiUserProfile.mobile + ""));
        } else {
            if (GuestUtils.isGuest()) {
                if (PhoneUtils.isValidPhoneNumber(GuestUtils.getGuestPhoneNumber())) {
                    setTimeout(function () {
                        getLatestNotifications(GuestUtils.getGuestPhoneNumber());
                        $scope.$apply();
                    });

                }
            }
        }*/

        /**
         * Get latest requests
         * @param phone
         */
        function getLatestNotifications(phone) {
            try {
                DMService.getMyLatestNotifications(phone, false).then(function (data) {
                    var filteredData = _.filter(data, function (item) {
                        return (item.RequestStatus !== 'Closed' && item.RequestStatus !== 'Rejected');
                    });
                    $scope.notifications = _.first(filteredData, nbRequestToShow);
                    $scope.requestsCount = data.length;
                });
            } catch (e) {
                console.log('==> ERROR ', e);
            }
        }


        /**
         * Translate name of category
         */
        function getLatestNotifications(phone) {
            try {

                DMService.getMyLatestNotifications(phone, true).then(
                    function (result) {
                        console.log('==> results ', result);
                        $scope.latestNotificationsList = result;
                        var latestNotificationsList = result;
                        if ($scope.latestNotificationsList) {
                            for (var i = 0; i < $scope.latestNotificationsList.length; i++) {
                                //$rootScope.allServices
                                var allServices = $dmServices;


                                var srv= _.find(allServices, function(item , index) {
                                    return item.serviceEN == $scope.latestNotificationsList[i].ServiceEN;
                                });

                                if (srv) {
                                    var SLA = ( (parseInt(srv.sla) % 7) > 0)? (parseInt(srv.sla) / 7) +1 : (parseInt(srv.sla) / 7);
                                    $scope.latestNotificationsList[i].SLA = Math.round(SLA);$scope.latestNotificationsList[i].SLA = Math.round(SLA);
                                }else {
                                    $scope.latestNotificationsList[i].SLA = '';
                                }


                                var StatusDate = moment($scope.latestNotificationsList[i].RequestStatusDate, ['DD/MM/YYYY HH:mm', moment.ISO_8601]);

                                $scope.latestNotificationsList[i].RequestCreatedDate = moment($scope.latestNotificationsList[i].RequestCreatedDate, ['DD/MM/YYYY HH:mm', moment.ISO_8601]).format('D MMM');
                                var today = moment(moment().format('DD/MM/YYYY HH:mm'), ['DD/MM/YYYY HH:mm', moment.ISO_8601]);
                                var duration = moment.duration(StatusDate.diff(today)).asHours();
                                //duration = Math.abs(duration);
                                /*if (duration && duration < 0) {
                                    durationToDestination = 1;
                                } else {
                                    var durationToDestination = (isNaN(duration)) ? "--" : (SLA != '') ? parseInt(SLA) - parseInt(duration) : duration;
                                    durationToDestination = (!isNaN(durationToDestination) && durationToDestination <= 0) ? 1 : Math.round(durationToDestination);

                                }*/
                                //$scope.latestNotificationsList[i].durationToDestination = durationToDestination;
                                allServices.some(function (item, index) {
                                    if (item.serviceEN == $scope.latestNotificationsList[i].ServiceEN) {
                                        $scope.latestNotificationsList[i].service_title_en = item.service_title_en;
                                        $scope.latestNotificationsList[i].service_title_ar = item.service_title_ar;
                                        $scope.latestNotificationsList[i].description_ar = item.description_ar;
                                        $scope.latestNotificationsList[i].description_en = item.description_en;
                                        return;
                                    }
                                });

                                switch ($scope.latestNotificationsList[i].RequestStatus) {
                                    case 'Initiation' : {
                                        $scope.latestNotificationsList[i].ServiceENHighlighted = 'Received';
                                        $scope.latestNotificationsList[i].ServiceARHighlighted = 'تم الاستلام';

                                        $scope.latestNotificationsList[i].ServiceENDetailed = 'Initiation';
                                        $scope.latestNotificationsList[i].ServiceARDetailed = 'تم الاستلام';
                                        break;
                                    }
                                    case 'Under Processing' : {
                                        if ($scope.latestNotificationsList[i].RequestUserFullName == '' || $scope.latestNotificationsList[i].RequestUserFullName.indexOf('Group:') >= 0) {
                                            $scope.latestNotificationsList[i].ServiceENHighlighted = 'Received';
                                            $scope.latestNotificationsList[i].ServiceARHighlighted = 'تم الاستلام';

                                            $scope.latestNotificationsList[i].ServiceENDetailed = 'Initiation';
                                            $scope.latestNotificationsList[i].ServiceARDetailed = 'تم الاستلام';
                                        } else {
                                            $scope.latestNotificationsList[i].ServiceENHighlighted = 'In Progress';
                                            $scope.latestNotificationsList[i].ServiceARHighlighted = 'في تَقَدم';

                                            $scope.latestNotificationsList[i].ServiceENDetailed = 'Under Processing';
                                            $scope.latestNotificationsList[i].ServiceARDetailed = 'قيد التنفيذ';
                                        }

                                        break;
                                    }
                                    case 'Canceled' : {
                                        $scope.latestNotificationsList[i].ServiceENHighlighted = 'Completed';
                                        $scope.latestNotificationsList[i].ServiceARHighlighted = 'مكتملة';

                                        $scope.latestNotificationsList[i].ServiceENDetailed = 'Cancelled';
                                        $scope.latestNotificationsList[i].ServiceARDetailed = 'ألغيت';
                                        break;
                                    }
                                    case 'Rejected' : {
                                        $scope.latestNotificationsList[i].ServiceENHighlighted = 'Completed';
                                        $scope.latestNotificationsList[i].ServiceARHighlighted = 'مكتملة';

                                        $scope.latestNotificationsList[i].ServiceENDetailed = 'Rejected';
                                        $scope.latestNotificationsList[i].ServiceARDetailed = 'رفض';
                                        break;
                                    }
                                    case 'Approved' : {
                                        $scope.latestNotificationsList[i].ServiceENHighlighted = 'Completed';
                                        $scope.latestNotificationsList[i].ServiceARHighlighted = 'مكتملة';

                                        $scope.latestNotificationsList[i].ServiceENHighlightedView = 'Accomplished';
                                        $scope.latestNotificationsList[i].ServiceARHighlightedView = 'مكتملة';

                                        $scope.latestNotificationsList[i].ServiceENDetailed = 'Accomplished'; // or Unaccomplished
                                        $scope.latestNotificationsList[i].ServiceARDetailed = 'مكتملة';
                                        break;
                                    }
                                    case 'Closed' : {
                                        $scope.latestNotificationsList[i].ServiceENHighlighted = 'Completed';
                                        $scope.latestNotificationsList[i].ServiceARHighlighted = 'مكتملة';

                                        $scope.latestNotificationsList[i].ServiceENHighlightedView = 'Unaccomplished';
                                        $scope.latestNotificationsList[i].ServiceARHighlightedView = 'غير مكتملة';

                                        $scope.latestNotificationsList[i].ServiceENDetailed = 'Unaccomplished';
                                        $scope.latestNotificationsList[i].ServiceARDetailed = 'غير مكتملة';
                                        break;
                                    }
                                }


                            }
                            $scope.listRecentRequest = _.filter($scope.latestNotificationsList, function (item) {
                                return item.RequestStatus.toUpperCase() !== 'REJECTED'
                                    && item.RequestStatus.toUpperCase() !== 'CLOSED'
                                    && item.RequestStatus.toUpperCase() !== 'APPROVED'
                                    && item.RequestStatus.toUpperCase() !== 'CANCELED';
                            });
                            var nbRequestToShow = ($scope.listRecentRequest.length >= 4) ? 4 : $scope.listRecentRequest.length;
                            $scope.listRecentRequest = _.first($scope.listRecentRequest, nbRequestToShow);
                            if ($scope.currentLang == 'ar') {
                                $scope.listRecentRequest = $scope.listRecentRequest.reverse();
                            }
                            $scope.ready = true;
                        }
                    },
                    function (error) {
                        /*WL.SimpleDialog.show($filter('translate')('Alert'), ($filter('translate')("Can't fetch data. Please try again later")), [{
                            text: $filter('translate')('OK'), handler: function () {
                                if (GuestUtils.isGuest())
                                    $scope.openPopup();
                            }
                        }]);*/
                    });
            } catch (e) {
                console.log('===> ERROR ', e);
            }
        }


        /*if (welcome < 5) {
            $scope.$watch(function () {
                if (welcome > 5) {
                    $scope.showClickHereGuide = false;
                } else {
                    $scope.showClickHereGuide = ($scope.notifications.length === 0);
                }
            });
        }*/

        $('#dashBoard').scroll(function () {
            $scope.isScroling = true;
            var scrollTop = 0;
            scrollTop = $('#dashBoard').scrollTop();
            if (scrollTop < 70) {
                $('#inputWrapper').addClass('input-wrapper-hidden ');
                $('#heroInput').removeClass('heroinputScrolled');
            }

            if (scrollTop >= 70) {
                $('#inputHolder').addClass('sticky-itemA');
                $('#inputWrapper').removeClass('input-wrapper-hidden ');
                $('#heroInput').addClass('heroinputScrolled');
            } else if (scrollTop < 202) {
                $('#inputHolder').removeClass('sticky-itemA');
            }

            if (scrollTop >= 202) {
                $('#heroInput').addClass('sticky-input');
                $('#inputWrapper').removeClass('inputWrapperScrolled');
                $('#heroInput').addClass('heroinputScrolled');
            } else if (scrollTop <= 202) {
                $('#heroInput').removeClass('sticky-input');
            }
        });

        $('#dashBoard').on("scrollstop", function () {
            console.log('scrollstop');
            $scope.isScroling = false;
        })


        $scope.searchToggle = function () {
            //if ($scope.searchInput.length > 1) {
            $state.go('search', {
                searchRequest: $scope.searchInput,
                mediaType: 'text'
            });
            //}
        }

        var clicked = 0;

        $scope.enterServerUrl = function () {
            clicked++;

            if (clicked > 2) {
                clicked = 0;
                $rootScope.SEARCH_URL = 'http://' + prompt('Enter SmartSearch server URL:', $rootScope.SEARCH_URL ? $rootScope.SEARCH_URL : 'dubai247.proxym-it.net' + '/search');
            }
        }

        // slick sliders
        $scope.slickConfig = {
            enabled: true,
            autoplay: false,
            infinite: false,
            draggable: true,
            autoplaySpeed: 3000,
            variableWidth: false,
            centerMode: true,
            arrows: false,
            dots: true,
            rtl: ($scope.currentLang == 'ar') ? true : false,
            method: {},
            event: {
                beforeChange: function (event, slick, currentSlide, nextSlide) {
                },
                afterChange: function (event, slick, currentSlide, nextSlide) {
                }
            }
        }

        $scope.modal = {
            openCamera: false
        };

        $scope.makeSearch = function(searchRequest, mediaType, sourceType) {
            LoadingService.show('Matching your search');
            var requestFrom = 'api';
            var platform = WL.Client.getEnvironment();
            var lang = localStorage.getItem('dm-lang') == 'ar' ? 'ar-AE' : 'en-US';

            SearchEngine.search(searchRequest, mediaType, requestFrom, platform, lang).then(
                function (result) {
                console.log('Success');
                console.log('FOUND SERVICES:', result);
                $scope.foundServices = result;


                if (!$scope.foundServices.responseJSON.services || ($scope.foundServices.responseJSON.services && $scope.foundServices.responseJSON.services.length < 1)) {
                    LoadingService.hide();
                    WL.SimpleDialog.show(
                        $filter('translate')("No result matching your search"), (""),
                        [{
                            text: "OK", handler: function () {
                                WL.Logger.debug("First button pressed");
                            }
                        }]
                    )
                } else {
                    if ($scope.foundServices.responseJSON.services.length > 5) {
                        $scope.foundServices.responseJSON.services = $scope.foundServices.responseJSON.services.slice(0, 5);
                    }
                    if ($scope.recordedAudio) {
                        localStorage.setItem('recordedAudio', $scope.recordedAudio);
                    }
                    if (mediaType == 'photo') {

                        var json_metadata = $scope.json_metadata;
                        console.log('json_metadata :: ', json_metadata);
                        if (WL.Client.getEnvironment() == WL.Environment.ANDROID) {
                            var lat = (json_metadata.gpsLatitude) ? json_metadata.gpsLatitude : undefined;
                            var long = (json_metadata.gpsLongitude) ? json_metadata.gpsLongitude : undefined;
                            if (lat && long) {
                                var gpsLat = [];
                                var gpsLong = [];
                                lat = lat.split(',');
                                long = long.split(',');

                                //var dec = dms2dec(["35/1", "48/1", "568927/100"], "N", ["10/1", "35/1", "277942/100"], "E");
                                var longLat = dms2dec(lat, json_metadata.gpsLatitudeRef, long, json_metadata.gpsLongitudeRef);
                                $scope.metaData = JSON.stringify({
                                    lat: longLat[0] + '',
                                    lng: longLat[1] + ''
                                });
                                localStorage.setItem('metaData', JSON.stringify({
                                    lat: longLat[0] + '',
                                    lng: longLat[1] + ''
                                }));
                            }
                        } else {
                            var lat = (json_metadata.GPS) ? json_metadata.GPS.Latitude : undefined;
                            var long = (json_metadata.GPS) ? json_metadata.GPS.Longitude : undefined;

                            if (lat && long) {
                                $scope.metaData = JSON.stringify({
                                    lat: lat + '',
                                    lng: long + ''
                                });

                                localStorage.setItem('metaData', JSON.stringify({
                                    lat: lat + '',
                                    lng: long + ''
                                }));
                            }

                        }

                        localStorage.setItem('uploadImage', $scope.fileURL);
                        localStorage.setItem('sourceType', $scope.sourceType);
                        //LoadingService.hide();


                        if ($scope.sourceType == 'camera') {
                            $scope.checkGPS();
                        } else {
                            if ($scope.metaData) {
                                console.log('Metadata From LocalStorage :: ', $scope.metaData);
                                var meta = JSON.parse($scope.metaData);
                                prepareRequest(meta.lat, meta.lng);
                            } else {
                                $scope.checkGPS();
                            }

                        }

                    } else if (mediaType == 'text') {
                        //LoadingService.hide();
                        $scope.additionalInfo = $scope.searchInput;
                        $scope.checkGPS();
                    } else {
                        //LoadingService.hide();
                        $scope.additionalInfo = $scope.foundServices.requestText;
                        $scope.checkGPS();
                    }
                }
            },
                function (error) {
                console.log(error);
                LoadingService.hide();
                $notification.alert($filter('translate')("An error has occurred"));
                if (document.getElementById("Search")) {
                    document.getElementById("Search").blur();
                }
                $scope.noService = true;
            })
        };



        $scope.speechToText = function (searchRequest, mediaType) {
                var requestFrom = 'api'
                var platform = WL.Client.getEnvironment();
                var lang = localStorage.getItem('dm-lang') == 'ar' ? 'ar-AE' : 'en-US';
            SearchEngine.search(searchRequest, mediaType, requestFrom, platform, lang).then(
                function (result) {
                console.log('Speech:', result);

                LoadingService.hide();
                if (result) {
                    $scope.searchInput = result.responseJSON.requestText;
                }else {
                    $scope.recordedAudio = '';
                }
            },function (error) {
                    $scope.recordedAudio = '';
                    console.log(error);
                    LoadingService.hide();
            });

        }

        var toDecimal = function (number) {
            return number[0].numerator + number[1].numerator /
                (60 * number[1].denominator) + number[2].numerator / (3600 * number[2].denominator);
        };

        $scope.openCamera = function (source) {
            var options = {
                quality: 50,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 100,
                targetHeight: 100,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false,
                correctOrientation:true
            };

            if (navigator.camera) {
                var cameraOptions = {
                    encodingType: Camera.EncodingType.JPEG,
                    destinationType: Camera.DestinationType.FILE_URI,
                    correctOrientation: true,
                    targetWidth: 720,
                    targetHeight: 480,
                    quality: 75,
                    sourceType: (source === 'camera') ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY
                };
                // $scope.closePopup();
                navigator.camera.getPicture(function (imageData) {
                    LoadingService.show('Matching your search');
                    var payload = {searchRequest: imageData, mediaType: 'photo', sourceType: source};
                    $scope.searchImage(payload);
                }, function (message) {
                    console.log(message);
                }, cameraOptions);
            }
        };

        $scope.searchText = function () {
            LoadingService.show('Matching your search');
            AnalyticsService.trackRoute(Constants.HIT_TYPE.EVENT, '', {
                eventCategory: Constants.EVENTS.Search_By_Text,
                eventAction: Constants.EVENTS.Search_By_Text,

            });
            $scope.makeSearch($scope.searchInput, 'text');
        };

        $scope.searchImage = function (payload) {
            try {
                $scope.searchRequest = payload.searchRequest;
                $scope.mediaType = payload.mediaType;
                $scope.sourceType = payload.sourceType;

                var result = JSON.parse($scope.searchRequest);
                if (result.json_metadata != '') {
                    $scope.json_metadata = JSON.parse(result.json_metadata);
                } else {
                    $scope.json_metadata = {};
                }


                $scope.fileURL = result.filename;

                toDataUrl($scope.fileURL, function (myBase64) {
                    AnalyticsService.trackRoute(Constants.HIT_TYPE.EVENT, '', {
                        eventCategory: 'Search_By_Image',
                        eventAction: ($scope.sourceType == 'camera') ? 'Take picture' : 'Choose from gallery'

                    });
                    $scope.makeSearch(myBase64, $scope.mediaType);
                });
            }catch (e) {
                console.log(e);
                LoadingService.hide();
            }

        };

        $scope.searchVoice = function () {

            $scope.makeSearch($scope.searchInput, 'audio');
        };

        /*
        *  check GPS
        * */
        $scope.checkGPS = function () {
            LoadingService.show('Checking your GPS connection');
            try {
                if (typeof(diagnostic) !== "undefined" && typeof(cordova) !== "undefined") {
                    if ($rootScope.geoLocation) {
                        // In case the GPS option is ENABLED in the application settings
                        // diagnostic.isLocationEnabled(function (enabled) {
                        cordova.plugins.diagnostic.requestLocationAuthorization(function(status){

                            console.log("requestLocationAuthorization status ", status);
                            if( status===cordova.plugins.diagnostic.permissionStatus.GRANTED ){
                                cordova.plugins.diagnostic.isLocationEnabled(function (enabled) {

                                    if (!enabled) {
                                        LoadingService.hide();
                                        WL.SimpleDialog.show(
                                            $filter('translate')('Alert'), $filter('translate')("Your GPS seems to be disabled, do you want to enable it ?"), [
                                                {
                                                    text: $filter('translate')("Settings"),
                                                    handler: function () {
                                                        if (WL.Client.getEnvironment() === WL.Environment.IPHONE) {
                                                            diagnostic.switchToSettings();
                                                        }
                                                        else {
                                                            diagnostic.switchToLocationSettings();
                                                        }
                                                    }
                                                },
                                                {
                                                    text: $filter('translate')("Close"), handler: function () {
                                                        return false;
                                                    }
                                                }]
                                        );
                                    } else {
                                        setTimeout(function () {
                                            $scope.gpsFailureCount = 0;
                                            $scope.getCurrentPosition();
                                        }, 800);
                                    }
                                }, function (error) {
                                    LoadingService.hide();
                                    console.log("The following error occurred: " + error);
                                });
                            }

                            /*
                            switch(status){
                                case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
                                    console.log("Permission not requested");
                                    break;
                                case cordova.plugins.diagnostic.permissionStatus.DENIED:
                                    console.log("Permission denied");
                                    break;
                                case cordova.plugins.diagnostic.permissionStatus.GRANTED:
                                    console.log("Permission granted always");
                                    break;
                                case cordova.plugins.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE:
                                    console.log("Permission granted only when in use");
                                    break;
                            }
                            */
                        }, function(error){
                            console.error(error);
                        }, cordova.plugins.diagnostic.locationAuthorizationMode.ALWAYS);
                    } else {
                        LoadingService.hide();
                        // In case the GPS option is DISABLED in the application settings
                        WL.SimpleDialog.show(
                            $filter('translate')('Alert'), $filter('translate')("Please enable location service from application settings"), [
                                {
                                    text: $filter('translate')("App Settings"),
                                    handler: function () {
                                        $state.go('settings');
                                    }
                                },
                                {
                                    text: $filter('translate')("Close"), handler: function () {
                                        return false;
                                    }
                                }]
                        );
                    }
                }
            } catch (ex) {
                LoadingService.hide();
                console.log('===> EX ', ex)
            }

        };

        /*
        *  get Current position
        * */
        $scope.getCurrentPosition = function () {
            LoadingService.show('Retrieving your GPS coordinates');
            try {
                var optionsCurrentPosition = {enableHighAccuracy: true, maximumAge: 0, timeout: 5000};
                var onSuccess = function (position) {
                    //LoadingService.hide();
                    // prepareRequest(25.204542, 55.271738);
                    prepareRequest(position.coords.latitude, position.coords.longitude);
                };
                var onError = function (error) {
                    $scope.gpsFailureCount++;
                    LoadingService.hide();
                    $scope.getCurrentPosition();
                };

                if($scope.gpsFailureCount <= 2){
                    navigator.geolocation.getCurrentPosition(onSuccess, onError, optionsCurrentPosition);
                }else{
                    LoadingService.hide();
                    $notification.alert($filter('translate')("gps_signal_failure"));
                }
            } catch (ex) {
                LoadingService.hide();
                console.log('===> EX ', ex)
            }
        };

        $scope.nextPage = function (categoryId) {
            SharedService.setStateHistory('services', {
                payload: {category_id: categoryId},
                from: 'dashboard'
            });
            $state.go('services');
        }

        /**
         *  prepare Request From Upload Image
         * */
        function prepareRequest(lat, lng) {
            console.log(lat);
            console.log(lng);
            var location = {
                lat: lat,
                lng: lng
            };
            LoadingService.show('Getting Makani number from GPS coordinates');
            Makani.getNearestMakani(lat, lng).then(
                function (result) {
                    try {
                        result = JSON.parse(result);
                        if (result.hasOwnProperty('DATA')) {

                        } else {
                            var makaniInfo = result.makaniInfo[0];
                            var makani = makaniInfo.MAKANI;
                            $rootScope.makaniInfo = makaniInfo.MAKANI_INFO[0];

                        }

                            $scope.geocodeLatLng(lat, lng).then(function (result) {


                                var listServices = $scope.foundServices.responseJSON.services;
                                console.log('Create Request From upload PAYLOAD :: \n',
                                    'makani :: ', makani,
                                    'locationName :: ', result,
                                    'formatted_address :: ', result,
                                    'location :: ', location,
                                    'makaniInfo :: ', $rootScope.makaniInfo
                                );
                                LoadingService.hide();

                                var payload = {
                                    makani: makani,
                                    locationName: result,
                                    formattedAddress: result,
                                    location: location,
                                    makaniInfo: makaniInfo,
                                    listServices: listServices,
                                    requestText: $scope.additionalInfo,
                                }
                                SharedService.setStateHistory('smartRequest', {
                                    payload: payload,
                                    from:'dashboard'
                                });
                                $state.go('smartRequest',
                                    {
                                        payload: payload
                                    })



                            }, function (err) {
                                LoadingService.hide();
                                console.log(err);
                            })
                    }catch (e) {
                        console.log(e);
                        LoadingService.hide();
                    }

                }, function (err) {
                    LoadingService.hide();
                    console.log(err);
                });

        }

        $scope.searchByImage = function () {
            if ($auth.token || GuestUtils.getGuestPhoneNumber()) {
                if ($scope.sourceType == 'camera') {
                    $scope.checkGPS();
                } else {
                    if ($scope.metaData) {
                        console.log('Metadata From LocalStorage :: ', $scope.metaData);
                        var meta = JSON.parse($scope.metaData);
                        prepareRequestFromUploadImage(meta.lat, meta.lng);
                    } else {
                        $scope.checkGPS();
                    }
                }
            } else {
                $state.go('personalDetails_1');
            }
        };

        /**
         * Get address name by LatLng
         * @param latlng
         */
        $scope.geocodeLatLng = function (lat, lng) {
            var deferred = $q.defer();

            var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
            var geocoder = new google.maps.Geocoder()
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === 'OK') {
                    deferred.resolve(results[1].formatted_address);
                } else {
                    deferred.resolve('');
                }
            });
            return deferred.promise;
        };

        /*
        *  Convert FILE_URI to base64
        * */
        function toDataUrl(url, callback) {
            var xhr = new XMLHttpRequest();
            xhr.onload = function () {
                var reader = new FileReader();
                reader.onloadend = function () {
                    callback(reader.result);
                }
                reader.readAsDataURL(xhr.response);
            };
            xhr.open('GET', url);
            xhr.responseType = 'blob';
            xhr.send();
        }

        // record animation and events
        function pad(val) {
            var valString = val + "";
            if (valString.length < 2) {
                return "0" + valString;
            } else {
                return valString;
            }
        }

        var timer = null;

        function chrono() {
            var totalSeconds = 0;
            document.getElementById('heroInput').value = '    00:00';
            timer = setInterval(function () {
                ++totalSeconds;
                document.getElementById('heroInput').value = '    ' + pad(parseInt(totalSeconds / 60)) + ':' + pad(totalSeconds % 60);
            }, 1000);
        }

        var longPress = false;

        $scope.recorder = new Object;

        $scope.recorder.stop = function () {

            if (longPress) {
                LoadingService.show('Transforming your voice in text');
                AnalyticsService.trackRoute(Constants.HIT_TYPE.EVENT, '', {
                    eventCategory: 'Search_By_Voice',
                    eventAction: 'Search By voice in dashboard'

                });
                window.plugins.audioRecorderAPI.stop(function (msg) {
                    // success
                    console.log('ok: ' + msg);
                    $scope.recordedAudio = msg;
                    var xhr = new XMLHttpRequest();
                    xhr.open("GET", msg, true);
                    xhr.responseType = "blob";
                    xhr.onload = function (e) {
                        console.log(this.response);
                        var reader = new FileReader();
                        reader.onload = function (event) {
                            var res = event.target.result;
                            console.log('res:', res);
                            console.log('File extension:', msg.substr(msg.lastIndexOf('/')));
                            $scope.speechToText(res.split(",").pop(), 'speech');
                        };
                        var file = this.response;
                        reader.readAsDataURL(file);
                    };
                    xhr.send();
                    longPress = false;
                }, function (msg) {
                    LoadingService.hide();
                    // failed
                    console.log('ERROR from recorder.stop: ' + msg);
                });
            } else {
                console.log('Shwoing tooltip...');
                $('.recTooltip').css('visibility', 'visible');
                $('.recTooltip').css('opacity', 1);
                $('.recTooltiptext').css('visibility', 'visible');
                $('.recTooltiptext').css('opacity', 1);

                var hideTooltip = _.debounce(function (searchRequest, mediaType) {
                    $('.recTooltip').css('opacity', 0);
                    $('.recTooltiptext').css('opacity', 0);
                    $('.recTooltip').css('visibility', 'hidden');
                    $('.recTooltiptext').css('visibility', 'hidden');
                }, 3000);

                hideTooltip();

            }
        }

        $scope.recorder.record = function () {
            cordova.plugins.permissions.requestPermission("android.permission.RECORD_AUDIO",
                function(resPermission){
                    console.log(resPermission);
                    window.plugins.audioRecorderAPI.record(function (msg) {
                        // complete
                        console.log('ok: ' + msg);

                    }, function (msg) {
                        // failed
                        console.log('ERROR from recorder.record: ' + msg);
                        clearTimeout(timer);
                    }, 20); // record 30 seconds
                }, function(errorPermission){
                    console.log(errorPermission)
                });
        }

        $scope.recorder.playback = function () {
            window.plugins.audioRecorderAPI.playback(function (msg) {
                // complete
                console.log('ok: ' + msg);
            }, function (msg) {
                // failed
                console.log('ERROR from recorder.playback : ' + msg);
            });
        }

        $scope.startRecording = function () {

            if (!$scope.isScroling) {
                longPress = true;

                $('#rectButton').addClass("blueRec");

                $('.recTooltip .recTooltiptext').css({
                    'visibility': 'hidden',
                    'opacity': 0
                });

                chrono();
                $scope.recorder.record();
            }


            //callDb();
        }

        $scope.stopRecording = function () {
            $('#rectButton').removeClass("blueRec");
            document.getElementById('heroInput').value = '';
            clearTimeout(timer);
            $scope.recorder.stop();
            /*navigator.DBMeter.stop(function () {
                console.log("DBMeter well stopped");
            }, function (e) {
                console.log('code: ' + e.code + ', message: ' + e.message);
            });*/

        }


        function getWeather() {
            $scope.weather = '';
            $http.get('http://api.apixu.com/v1/current.json?key=8d7f612332a941a9bf9161410181709&q=dubai').then(function (response) {
                console.log(response);


                if (response.data) {
                    $scope.temp_c = response.data.current.temp_c;
                    $scope.temp_f = response.data.current.temp_f;
                    switch (response.data.current.condition.code) {
                        case 1000 : {
                            if (response.data.current.is_day == 1) {
                                $scope.weather = 'icons8Sun.svg';
                            } else {
                                $scope.weather = 'icons8BrightMoon.svg';
                            }
                            break;
                        }
                        case 1003 :
                        case 1006 :
                        case 1135 :
                        case 1180 :
                        case 1186 :
                        case 1192 :
                        case 1210 :
                        case 1216 :
                        case 1243 :
                        case 1240 :
                        case 1249 :
                        case 1252 :
                        case 1255 :
                        case 1258 :
                        case 1264 : {
                            if (response.data.current.is_day == 1) {
                                $scope.weather = 'icons8PartlyCloudyDay.svg';
                            } else {
                                $scope.weather = 'icons8PartlyCloudyNight.svg';
                            }
                            break;
                        }
                        case 1087:
                        case 1273:
                        case 1276:
                        case 1279:
                        case 1282: {
                            $scope.weather = "icons8Storm.svg";
                            break;
                        }
                        case 1009 :
                            $scope.weather = "icons8WindyWeather.svg";
                            break;
                        case 1195 :
                        case 1246 :
                            $scope.weather = 'icons8KeepDry.svg';
                            break;
                        case 1030 :
                        case 1063 :
                        case 1066 :
                        case 1069 :
                        case 1072 :
                        case 1114 :
                        case 1117 :
                        case 1147 :
                        case 1150 :
                        case 1153 :
                        case 1168 :
                        case 1171 :
                        case 1183 :
                        case 1189 :
                        case 1198 :
                        case 1201 :
                        case 1204 :
                        case 1207 :
                        case 1213 :
                        case 1219 :
                            $scope.weather = 'icons8Rain.svg';
                            break;
                    }

                    console.log($scope.weather);


                }


            })
        }

        getWeather();
    }
]);
