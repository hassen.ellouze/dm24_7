﻿iDubaiApp.directive('myHeader', ['$auth', '$rootScope', '$state', 'underscore',
        function ($auth, $rootScope, $state) {
            return {
                restrict: 'E',
                templateUrl: 'widgets/my-header.html',
                scope: {
                    title: "@",
                    showBack: "=",
                    hideNav: "@",
                    loginToken: "="
                },
                link: function (scope, element, attrs) {
                    scope.currentLang = localStorage.getItem('dm-lang');
                    scope.isLoggedIn = $auth.token ? true : false;
                    $rootScope.isLoggedIn = $auth.token ? true : false;
                    if ($auth.token)
                        $rootScope.userName = $auth.dubaiUserProfile.firstName;
                    $rootScope.hideBtn = $state.current.hideBack;

                    if ($state.current.hideBack) {
                        jQuery('#backBtn').css('display', 'none');
                    }

                    scope.back = function () {
                        try {
                            if (attrs.back === 'true') {
                                $rootScope.isBack = true;
                                if ($rootScope.history[$rootScope.history.length - 2] === undefined) {
                                    $state.go('dashboard');
                                } else {
                                    $state.go($rootScope.history[$rootScope.history.length - 2].name, {
                                        isBack: true,
                                        params: $rootScope.history[$rootScope.history.length - 2].params
                                    });
                                }
                            }
                            else {
                                $rootScope.step = "1";
                                $rootScope.back = true;
                                $rootScope.makani = '';
                                $rootScope.makaniInfo = '';
                                $rootScope.loadLocationMap();
                            }
                        } catch (e) {
                            console.log('===> ERROR ', e);
                        }
                    };

                    /**
                     * Toggle menu
                     */
                    scope.toggleMenu = function () {
                        toggleMenu();
                    };


                    scope.$watch(
                        // This function returns the value being watched. It is
                        // called for each turn of the $digest loop
                        function () {
                            return $auth.token ? true : false;
                        },
                        // This is the change listener, called when the value
                        // returned from the above function changes
                        function (newValue, oldValue) {
                            if (newValue !== oldValue) {
                                scope.isLoggedIn = newValue;
                                $rootScope.isLoggedIn = newValue;
                            }
                        }
                    );
                }
            };
        }
    ]
);