
/* JavaScript content from js/components/carouselslide.js in folder common */
﻿iDubaiApp.directive('carouselSlide', function () {
    return {
        restrict: 'E',
        templateUrl: 'widgets/carousel-slide.html',
        scope: {
            item: "=",
            hideText: "@", 
            //            title: "@",
            //            desc: "@",
            //            normalPic: "@",
            //            largePic: "@",
            //            date: "@",
            //            fullDesc: "@"
        },

		link: function (scope, element, attrs) {
			 
			angular.element(element).find("img").remove();
			var img = document.createElement("img");			
			if(navigator.onLine){
				img.src=scope.item.largePic;
			}else{
				img.src='assets/img/images/no-img.jpg';
			}
			img.setAttribute("style","width:100%; height:auto;");
			img.onload=function(){				 
				angular.element(element).find(".slide").prepend(img);
			};
			    
            scope.goToArticle = function () {
            	if(scope.item.fullDesc != null && scope.item.fullDesc.length > 0){
                    //localStorage.setItem("ARTICLE_ITEM", JSON.stringify({id: scope.item.id, storageKey: scope.item.storageKey, isAboutView : false})); 
            		localStorage.setItem("ARTICLE_ITEM", JSON.stringify({id: scope.item.id, collectionName: scope.item.collectionName, isAboutView : false})); 
            		location.hash = "#/article-master";
            	}
            };
        }
    };
}); 

iDubaiApp.directive( "csInit",["$logger", "$timeout",function($logger,$timeout) {
	
	// Return the directive configuration.
	return({
        link: link,
        restrict: "A"
    });
	 
    // I bind the UI events to the scope.
    function link( scope, element, attributes ) {
    	//$logger.log( "Linked:", attributes.bnLineItem );
    	if( !scope.$last ) {
  	        return false;
  	    }    	
    	
    	function initComponents(){
    		$timeout(function() {
    			scope.$apply();
    			initCycleCarousel(); 
    			jQuery('.cycle-gallery').scrollAbsoluteGallery({
    				mask: '.mask',
    				slider: '.slideset',
    				slides: '.slide',
    				btnPrev: 'a.btn-prev',
    				btnNext: 'a.btn-next',
    				pagerLinks: '.pagination li',
    				stretchSlideToMask: true,
    				pauseOnHover: true,
    				maskAutoSize: true,
    				autoRotation: true,
    				switchTime: 4000,
    				animSpeed: 500
    			});    	 
    			//$logger.warn("initComponents : ");
    			$timeout(function() { 				
    				jQuery('div.carousel .btn-next').click(); 
    				//$logger.warn("switch : ");
    			}, 100); 
    		},50);
    	}
    	
    	scope.$evalAsync(function(){
    		initComponents();
  	    });  
	 } 
}]);