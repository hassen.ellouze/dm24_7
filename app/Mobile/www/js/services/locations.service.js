
/* JavaScript content from js/services/locations.service.js in folder common */
iDubaiApp.factory("$myLocations", ['$q', 'utils', '$log', '$rootScope', '$filter',
    function ($q, utils, $log, $rootScope, $filter) {


        function getLocations() {
            var deferred = $q.defer();
            setAllToFalse().then(function (data) {
                deferred.resolve(data);
            });
            return deferred.promise;
        }

        function setLocations(locations) {
            var deferred = $q.defer();
            localStorage.setItem("locations", locations);
            deferred.resolve();
            return deferred.promise;
        }

        function setAllToFalse() {
            var deferred = $q.defer();
            var locations = localStorage.getItem("locations");
            if (locations != '' && locations != null && locations != undefined) {
                if (typeof(locations) == "string") {
                    locations = JSON.parse(locations);
                } else {
                    if (typeof(locations[0]) != 'undefined') {
                        locations = JSON.parse(locations[0].LOCATIONS);
                    }
                }
            }
            var updatedLocations = [];
            angular.forEach(locations, function (item) {
                item.current = false;
                updatedLocations.push(item);
            });
            updatedLocations = JSON.stringify(updatedLocations);
            setLocations(updatedLocations).then(function () {
                deferred.resolve(updatedLocations);
            });
            return deferred.promise;
        }

        function count() {
            var loactions = [];
            var data = localStorage.getItem("locations");
            if (data) {
                if (typeof(data) === "string") {
                    loactions = JSON.parse(data);
                } else {
                    if (typeof(data[0]) !== 'undefined') {
                        loactions = JSON.parse(data[0].LOCATIONS);
                    }
                }
            }
            return loactions.length;
        }

        return {
            getLocations: getLocations,
            setLocations: setLocations,
            count: count

        };
    }]);