
/* JavaScript content from js/services/error-handler.service.js in folder common */
angular.module('iDubaiApp').factory('errorHandler', ['$filter', '$logger' , '$http',
		                 function($filter , $logger , $http) {
		                 
     var lang = localStorage.getItem('dm-lang');
     
     var showMessage = function(msg , lang){
	     var txt = msg[lang?lang:"en"];
	     var ok = $filter("translate")("OK");
	     WL.SimpleDialog.show("DM 24/7 App", txt, [{text: ok}]);
     };
     
     var errorHandler = {
	     checkResponse : function(response){
    	 $logger.log("errorHandler.response : ",response);
		 if(typeof(response) == "undefined"){
			 return false;
		 }
	     var errorType = (response.errorCode != undefined)? WL.ErrorCode[response.errorCode] : undefined;
	     if(!navigator.onLine){
			errorType = "NO_CONNECTION";
		}
	     if(errorType != undefined){					 
		     $http.get('lookups/errors.json').then(function(res){
                   var msg = res.data[errorType];	
                   if(msg != undefined){
                	   showMessage(msg , lang);
                   }else{
                	   return false;
                   }
                   
               });	
		     	return true;
		     }else{
		    	 return false;
		     }				 
	     }
     }; 	
     
     return errorHandler;
 }]);