// var smsReceive = {};
//
// smsReceive.startWatch = function(successCallback, failureCallback) {
// 	cordova.exec( successCallback, failureCallback, 'SMSReceive', 'startWatch', [] );
// };
//
// smsReceive.stopWatch = function(successCallback, failureCallback) {
// 	cordova.exec( successCallback, failureCallback, 'SMSReceive', 'stopWatch', [] );
// };

// module.exports = smsExport;

// cordova.define("SMSReceive", function(require, exports, module) {
    function SMSReceive() {
    }

    SMSReceive.prototype.startWatch = function (success, error) {
        cordova.exec(
            success,
            error,
            'SMSReceive', 'startWatch', []
        );
    };

    SMSReceive.prototype.stopWatch = function (success, error) {
        cordova.exec(
            success,
            error,
            'SMSReceive', 'stopWatch', []
        );
    };


    SMSReceive.install = function () {
        if (!window.plugins) {
            window.plugins = {};
        }

        window.plugins.smsReceive = new SMSReceive();

        return window.plugins.smsReceive;
    };

    cordova.addConstructor(SMSReceive.install);
// });


