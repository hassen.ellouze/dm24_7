function getPopularServices() {

    var input = {
        method: 'GET',
        path: "smartcrmapi.dm.gov.ae/CRMTrendingNotificationAPI/api/TrendingNotification?NumofDays=15",
        returnedContentType: 'json',
        returnedContentEncoding: 'UTF-8',

    };

    try{



        WL.Logger.error("getPopularServices :: request  : " + JSON.stringify(input));
        var x = WL.Server.invokeHttp(input);
        WL.Logger.error("getPopularServices :: response is : " + JSON.stringify(x));
        return x ;


    } catch(e) {
        return {
            isSuccessful : false,
            reference : e.toString()
        };
    }
}