
/* JavaScript content from js/controllers/personalDetails/personalDetails_update.controller.js in folder common */
iDubaiApp.controller('personalDetailsUpdateController',
    ['$scope', 'PhoneUtils', '$filter', '$notification', '$timeout', 'GuestUtils', '$stateParams', '$state', '$auth', '$DMLinks', 'LoadingService', '$countries', '$rootScope', '$DbStorageService',
        function ($scope, PhoneUtils, $filter, $notification, $timeout, GuestUtils, $stateParams, $state, $auth, $DMLinks, LoadingService, $countries, $rootScope, $DbStorageService) {
    console.log('Personal Details update');
    $scope.currentLang = localStorage.getItem('dm-lang');
    $scope.listCountries = $countries;
    $scope.username='';
    $scope.emailAddress='';
    $scope.mobileNumber = '';
    $scope.areaCode = 'UAE +971';
    //$scope.country = ($scope.currentLang == 'en') ? 'United Arab Emirates' : 'الامارات العربية المتحدة';

    if ($auth.token) {
        $scope.mobileNumber = $auth.dubaiUserProfile.mobile;
        $scope.username = $auth.dubaiUserProfile.username;
        $scope.emailAddress = ($auth.dubaiUserProfile.email)? $auth.dubaiUserProfile.email : '';
    } else if(!GuestUtils.isEmptyGuest()) {
        var guestInfo = GuestUtils.getGuestInfo();
        if(guestInfo != '') {
            $scope.mobileNumber= (guestInfo.mobileNumber) ? guestInfo.mobileNumber : '';
            $scope.mobileNumber= ($scope.mobileNumber != '') ? guestInfo.mobileNumber.replace(guestInfo.mobileArea, '') : '';
            $scope.username= (guestInfo.name) ? guestInfo.name : '';
            $scope.emailAddress= (guestInfo.email) ? guestInfo.email : '';
            if(guestInfo.country) {
                $scope.country = guestInfo.country;
            }

        }
    }

    $scope.save= function() {
        console.log($scope.username);
        console.log($scope.emailAddress);
        console.log($scope.mobileNumber);
        console.log($scope.country);
         GuestUtils.updateGuestInfo('+971'+$scope.mobileNumber, $scope.username, $scope.emailAddress, $scope.country);
         $state.go('dashboard');
    }

            // Logout
            $rootScope.logout = function () {
                // if ($scope.isLoggedIn) {
                $auth.logout();
                $scope.isLoggedIn = $auth.token ? true : false;
                $rootScope.isLoggedIn = $auth.token ? true : false;
                localStorage.removeItem("iDubaiAuthToken");
                localStorage.removeItem("guestInfo");
                $DbStorageService.clear().then(function (data) {
                    console.log("Storage cleared successfully");
                });
                //}
                $state.go("dashboard", {}, {reload: true});
            };


        }]);
/*

iDubaiApp.controller('personalDetails_2Controller', ['$scope' , function ($scope) {
    console.log('Personal Details 2');
}]);*/
