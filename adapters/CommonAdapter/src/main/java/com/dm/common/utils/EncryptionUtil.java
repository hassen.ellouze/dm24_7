package com.dm.common.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Scanner;

import javax.crypto.Cipher;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dm.common.utils.Base64;

//import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

public class EncryptionUtil {

    public static Log logger = LogFactory.getLog(EncryptionUtil.class);


    private static String readFileContent(File f) throws FileNotFoundException {

        StringBuffer buffer;
        Scanner scanner = null ;
        try {
            scanner = new Scanner(f);
            buffer = new StringBuffer();
            while (scanner.hasNextLine()) {
                buffer.append(scanner.nextLine()) ;
            }

        } finally {
            if(scanner != null)		scanner.close();
        }
        return buffer.toString() ;
    }


    @SuppressWarnings("unused")
    private static String extractCertificate(File f) throws FileNotFoundException{
        String fileContent = readFileContent(f);
        String out = fileContent.replaceAll("-----BEGIN RSA PRIVATE KEY-----", "");
        out = out.replaceAll("-----END RSA PRIVATE KEY-----", "");
        out = out.replaceAll("-----BEGIN PUBLIC KEY-----", "");
        out = out.replaceAll("-----END PUBLIC KEY-----", "");
        out = out.replaceAll("\n", "");
        return out ;
    }


    private static String extractCertificate(String certificate) {
        String out = certificate.replaceAll("-----BEGIN RSA PRIVATE KEY-----", "");
        out = out.replaceAll("-----END RSA PRIVATE KEY-----", "");
        out = out.replaceAll("-----BEGIN PUBLIC KEY-----", "");
        out = out.replaceAll("-----END PUBLIC KEY-----", "");
        out = out.replaceAll("\n", "");
        return out ;
    }


    public static EncryptionKeyPair generateKeyPair() throws NoSuchAlgorithmException{
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(1024);
        KeyPair keyPair = kpg.genKeyPair();

        byte[] pk = keyPair.getPublic().getEncoded();
        String pubK = new String(Base64.encode(pk)) ;

        byte[] priv = keyPair.getPrivate().getEncoded();
        String privK = new String(Base64.encode(priv)) ;
        return new EncryptionKeyPair(privK, pubK);

    }

    public static String encrypt(String toEncrypt, String publicKeyString) throws GeneralSecurityException{
        String certString = extractCertificate(publicKeyString) ;
        byte[] publicKey = Base64.decode(certString);
        X509EncodedKeySpec spec = new X509EncodedKeySpec(publicKey);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey pubKey = keyFactory.generatePublic(spec);
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, pubKey);
        byte[] encrypted = cipher.doFinal(toEncrypt.getBytes());

        return new String(Base64.encode(encrypted));
    }


    public static String decrypt(String toDecrypt, String privateKeyString) throws  GeneralSecurityException, UnsupportedEncodingException{
        try {
            toDecrypt = toDecrypt.replaceAll("\n", "");
            String certString = extractCertificate(privateKeyString) ;
            byte[] privateKey = Base64.decode(certString) ;
            PKCS8EncodedKeySpec spec2 = new PKCS8EncodedKeySpec(privateKey);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PrivateKey privKey= keyFactory.generatePrivate(spec2);
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.DECRYPT_MODE, privKey);
            byte[] plain = cipher.doFinal(Base64.decode(toDecrypt));
            //decode b 64 twice
            //return new String(Base64.decode(new String(plain, "UTF-8")));
            return new String(plain, "UTF-8") ;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            return "false";
        }
    }


    public static class EncryptionKeyPair{
        private String base64PrivateKey;
        private String base64PublicKey;

        EncryptionKeyPair(String base64PrivateKey, String base64PublicKey){
            this.base64PrivateKey = base64PrivateKey ;
            this.base64PublicKey = base64PublicKey ;
        }

        public String getBase64PrivateKey() {
            return base64PrivateKey;
        }

        public void setBase64PrivateKey(String base64PrivateKey) {
            this.base64PrivateKey = base64PrivateKey;
        }

        public String getBase64PublicKey() {
            return base64PublicKey;
        }

        public void setBase64PublicKey(String base64PublicKey) {
            this.base64PublicKey = base64PublicKey;
        }

        public String toString(){
            return "Private Key : "+base64PrivateKey+"\n" + "Public Key : "+base64PublicKey ;
        }

    }
}
