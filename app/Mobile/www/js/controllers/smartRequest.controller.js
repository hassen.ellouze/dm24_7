iDubaiApp.controller('smartRequestController',
    ['$state', '$scope', '$stateParams', '$filter', '$notification', 'DMService', '$procedures', '$auth', '$logger',
        '$rootScope', 'LoadingService', 'Makani', '$myLocations', 'PhoneUtils', 'GuestUtils', 'underscore', '$dmArea', 'DMCategories', '$DMLinks', '$interval', 'AnalyticsService', 'Constants', 'SearchEngine', 'ngDialog', 'SharedService',
        function ($state, $scope, $stateParams, $filter, $notification, DMService, $procedures, $auth, $logger,
                  $rootScope, LoadingService, Makani, $myLocations, PhoneUtils, GuestUtils, underscore, $dmArea, DMCategories, $DMLinks, $interval, AnalyticsService, Constants, SearchEngine, ngDialog, SharedService) {
            $scope.currentLang = localStorage.getItem('dm-lang');
            $rootScope.currentLang = localStorage.getItem('dm-lang');
            $scope.fromSearchByImage = false;
            $scope.requireInsectType = false;
            $scope.requirePetType = false;
            $scope.requireAnonymous = false;
            $scope.serviceChecked = false;
            $scope.modal = {
                openNoise: false,
                showTimer: false
            };
            $scope.isMandatory = true;
            var PLATFORM = WL.Client.getEnvironment();
            var _ = underscore;
            $stateParams = SharedService.getStateHistory($state.current.name);

            function toDataUrl(url, callback) {
                var xhr = new XMLHttpRequest();
                xhr.onload = function () {
                    var reader = new FileReader();
                    reader.onloadend = function () {
                        callback(reader.result);
                    }
                    reader.readAsDataURL(xhr.response);
                };
                xhr.open('GET', url);
                xhr.responseType = 'blob';
                xhr.send();
            }


            if (localStorage.getItem('sourceType')) {
                $scope.fromSearchByImage = true;
            }
            if ($stateParams.result) {
                $scope.ExternalTranasactionId = $stateParams.result.ExternalTranasactionId;
                LoadingService.hide();
            }
            else {

                $scope.services = $stateParams.payload.listServices;
                $scope.makani = $stateParams.payload.makani;
                $scope.locationName = $stateParams.payload.locationName;
                $scope.formattedAddress = $stateParams.payload.formattedAddress;
                $scope.additionalInfo = ($stateParams.payload.requestText) ? $stateParams.payload.requestText : '';
                $scope.location = $stateParams.payload.location;
                $scope.locationDetail = $stateParams.payload.locationDetail;

                $scope.recordedAudio = localStorage.getItem("recordedAudio");
                if ($scope.recordedAudio) {
                    $scope.hasAudio = true;
                }
                $scope.isPlaying = false;

                if ($scope.services[$scope.services.length - 1].category_id != 1000) {
                    $scope.services.push({
                        "serial": 1000,
                        "category_id": 1000,
                        "category_CRM_id": 1000,
                        "pet_type": 0,
                        "insect_type": 0,
                        "is_prefered_time": 0,
                        "service_title_en": "Other",
                        "service_title_ar": "خدمة أخرى",
                        "sla": 99,
                        "isService": false,
                        "isPopular": false,
                        "isVisitor": false,
                        "isAnonymous": false,
                        "isHidden": false,
                        "serviceEN": "Mobile Unmapped Service",
                        "serviceAR": "Mobile Unmapped Service",
                        "organization_unit_en": "Mobile Unmapped Service",
                        "organization_unit_ar": "Mobile Unmapped Service",
                        "description_en": "Mobile Unmapped Service",
                        "description_ar": "Mobile Unmapped Service",
                        "service_request_id": 1000
                    });
                }

                /*$scope.services.map(function (item, index) {
                 if(item.service_title_en == "Report Construction Noise") {
                 $scope.service = item;
                 $scope.requirePetType = false;
                 $scope.requireInsectType = false;
                 $scope.requireAnonymous = false;
                 checkPetType(item);
                 checkInsectType(item);
                 checkAnonymous(item);

                 }
                 });*/

                $scope.isNotUpload = false;
                $scope.isAnonymous = false;
                $scope.modal = {
                    toggle: false,
                    toggleTerms: false,
                    //toggleDelete: false
                };
                $scope.forDeleting = '';
                $scope.mediaUpload = [];
                if (localStorage.getItem('uploadImage')) {

                    toDataUrl(localStorage.getItem('uploadImage'), function (myBase64) {

                        $scope.mediaUpload.push({
                            src: myBase64,
                            id: $scope.mediaUpload.length
                        });


                    });

                    /*toDataUrl(localStorage.getItem('uploadImage'), function (myBase64) {
                     console.log("Converted Base 64 :: ", myBase64);
                     $scope.mediaUpload.push({
                     //src: 'data:image/jpg;base64,' + localStorage.getItem('uploadImage'),
                     src: myBase64,
                     id: $scope.mediaUpload.length
                     });
                     });*/

                }
                /* Get All Pets*/
                DMCategories.getPets().then(function (result) {
                    $scope.pets = result;
                });
                /* Get All Insects*/
                DMCategories.getInsects().then(function (result) {

                    $scope.insects = result

                });


                $scope.onChangeService = function (element, service) {
                    $scope.serviceChecked = true;
                    //$scope.service = service;
                    $scope.isMandatory = (service.service_title_en == "Other") ? true : false;
                    $scope.isNoiseService = (service.service_title_en == "Report Construction Noise") ? true : false;


                    console.log(service);
                    $scope.service = service;
                    $scope.requirePetType = false;
                    $scope.requireInsectType = false;
                    $scope.requireAnonymous = false;
                    $scope.service = null;


                    $scope.service = service;
                    checkPetType(service);
                    checkInsectType(service);
                    checkAnonymous(service);
                };


                console.log($stateParams);


                $scope.imgSrc = encodeURI("https://maps.googleapis.com/maps/api/staticmap?" +
                    "center=" + $scope.location.lat + ',' + $scope.location.lng +
                    "&zoom=15" +
                    "&scale=false" +
                    "&size=600x300" +
                    "&maptype=satellite" +
                    "&key=AIzaSyDAyPe0GL66TrDBhkzQCF9-8pIWTLoxg7o" +
                    "&format=png" +
                    "&visual_refresh=true" +
                    "&markers=" + $scope.location.lat + ',' + $scope.location.lng);

                //https://maps.googleapis.com/maps/api/staticmap?center=25.11724358770323,55.210145758231874&zoom=15&scale=false&size=600x600&maptype=roadmap&key=AIzaSyDAyPe0GL66TrDBhkzQCF9-8pIWTLoxg7o&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C25.11724358770323,55.210145758231874


                $scope.returnToMap = function () {
                    localStorage.setItem('recordedAudio', $scope.recordedAudio);
                    console.log('RETURN FROM MAP :: serviceInfo == ', $stateParams.payload.serviceInfo);
                    console.log('RETURN FROM MAP :: makani == ', $stateParams.payload.makani);
                    console.log('RETURN FROM MAP :: locationName == ', $stateParams.payload.locationName);
                    console.log('RETURN FROM MAP :: formattedAddress == ', $stateParams.payload.formattedAddress);
                    console.log('RETURN FROM MAP :: location == ', $stateParams.payload.location);


                    var payload = {
                        service: {
                            "serial": 5,
                            "service_request_id": 99999,
                            "category_id": 2,
                            "category_CRM_id": 2,
                            "pet_type": 1,
                            "insect_type": 0,
                            "is_prefered_time": 1,
                            "service_title_en": "",
                            "service_title_ar": "",
                            "sla": 21,
                            "isService": true,
                            "isPopular": false,
                            "isVisitor": false,
                            "isAnonymous": false,
                            "isHidden": false,
                            "serviceEN": "",
                            "serviceAR": "",
                            "organization_unit_en": "",
                            "organization_unit_ar": "",
                            "description_en": "",
                            "description_ar": ""
                        },
                        category: {
                            title: '',
                            titleAR: '',
                            description: '',
                            descriptionAR: 'يتيح لك هذا القسم طلب خدمات مكافحة الآفات لمنزلك أو في مرافق تديرها بلدية دبي',
                            image: '',
                            gear: '',
                            backgroundColor: '',
                            category_id: 1
                        },
                        listServices: $scope.services,
                        requestText: $scope.additionalInfo,
                    }
                    SharedService.setStateHistory('map', {
                        payload: payload,
                        from: 'smartRequest'
                    });
                    $state.go('map', {
                        payload: payload,
                        from: 'smartRequest'
                    });
                    /* $state.go('map', {
                     service: {
                     "serial": 5,
                     "service_request_id": 99999,
                     "category_id": 2,
                     "category_CRM_id": 2,
                     "pet_type": 1,
                     "insect_type": 0,
                     "is_prefered_time": 1,
                     "service_title_en": "",
                     "service_title_ar": "",
                     "sla": 21,
                     "isService": true,
                     "isPopular": false,
                     "isVisitor": false,
                     "isAnonymous": false,
                     "isHidden": false,
                     "serviceEN": "",
                     "serviceAR": "",
                     "organization_unit_en": "",
                     "organization_unit_ar": "",
                     "description_en": "",
                     "description_ar": ""
                     },
                     category: {
                     title: '',
                     titleAR: '',
                     description: '',
                     descriptionAR: 'يتيح لك هذا القسم طلب خدمات مكافحة الآفات لمنزلك أو في مرافق تديرها بلدية دبي',
                     image: '',
                     gear: '',
                     backgroundColor: '',
                     category_id: 1
                     },
                     listServices: $scope.services,
                     requestText: $scope.additionalInfo,
                     from: 'smartRequest'
                     });*/
                };

                /*
                 *  Fill Pets
                 * */
                function checkPetType(service) {


                    if (service.serviceEN == "Stray Cats") {
                        // pet = cat
                        $scope.pets.map(function (item, index) {
                            if (item.pet_type_en.toUpperCase() == "CATS") {
                                $scope.pet_type_id = item.pet_type_id;
                            }
                        })
                    } else if (service.serviceEN == "Stray Dogs") {
                        // pet = dog
                        $scope.pets.map(function (item, index) {
                            if (item.pet_type_en.toUpperCase() == "CATS") {
                                $scope.pet_type_id = item.pet_type_id;
                            }
                        })
                    } else {
                        if (service.pet_type == '1') {
                            $scope.requirePetType = true;
                        }
                    }


                }

                /*
                 *  Fill Insects
                 * */
                function checkInsectType(service) {
                    if (service.insect_type == '1') {
                        $scope.requireInsectType = true;
                    }
                }

                /*
                 *  checkAnonymous
                 * */
                function checkAnonymous(service) {
                    if (service.isAnonymous) {
                        $scope.requireAnonymous = true;
                    }
                }

                if ($scope.location) {
                    Makani.getNearestMakani($scope.location.lat, $scope.location.lng).then(function (results) {
                        if (results.hasOwnProperty('DATA')) {
                            return;
                        } else {
                            var makaniInfo = JSON.parse(results).makaniInfo[0];
                            $scope.makani = makaniInfo.MAKANI;
                            $rootScope.makani = makaniInfo.MAKANI;
                            $rootScope.makaniInfo = makaniInfo.MAKANI_INFO[0];
                        }
                    }, function (err) {
                        console.log('Error makani : ', err);
                    })
                }

                /*DMCategories.getServices().then(function (result) {
                 if (result.isSuccessful) {
                 if ($scope.serviceInfo.service.incident_id !== undefined) {
                 $scope.serviceName = underscore.find(result.array, function (s) {
                 return s.incident_id == $scope.serviceInfo.service.incident_id;
                 })
                 } else {
                 $scope.serviceName = underscore.find(result.array, function (s) {
                 return s.service_request_id == $scope.serviceInfo.service.service_request_id;
                 })
                 }
                 }
                 })*/

            }

            if ($scope.location) {
                Makani.getNearestMakani($scope.location.lat, $scope.location.lng).then(function (result) {
                    try {
                        result = JSON.parse(result);
                        if (result.hasOwnProperty('DATA')) {
                            return;
                        } else {
                            var makaniInfo = result.makaniInfo[0];
                            $rootScope.makani = makaniInfo.MAKANI;
                            $rootScope.makaniInfo = makaniInfo.MAKANI_INFO[0];
                        }
                    }catch (e) {
                        console.log(e);
                    }
                }, function (err) {
                    console.log('Error makani : ', err);
                })
            }

            /**
             * Get picture depend of source
             * @param source
             */
            $scope.openCamera = function (source) {
                try {
                    if (navigator.camera) {
                        var cameraOptions = {
                            quality: 50,
                            targetWidth: 720,
                            targetHeight: 600,
                            encodingType: Camera.EncodingType.JPEG,
                            destinationType: Camera.DestinationType.DATA_URL, //Camera.DestinationType.DATA_URL
                            correctOrientation: true,
                            sourceType: (source === 'camera') ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY
                        };
                        navigator.camera.getPicture(function (imageData) {
                            setTimeout(function () {
                                if (WL.Client.getEnvironment() == WL.Environment.ANDROID) {
                                    imageData = JSON.parse(imageData);
                                    $scope.mediaUpload.push({
                                        src: 'data:image/jpg;base64,' + imageData.filename,
                                        id: $scope.mediaUpload.length
                                    });
                                } else {

                                    $scope.mediaUpload.push({
                                        src: 'data:image/jpg;base64,' + imageData,
                                        id: $scope.mediaUpload.length
                                    });
                                }

                                //$scope.mediaUpload.push({src: imageData, id: $scope.mediaUpload.length});
                                $scope.$apply();
                            });
                        }, function (message) {
                        }, cameraOptions);
                    }
                } catch (e) {
                    catchError(e);
                }

            };


            $scope.changeMe = function (id) {
                console.log('pressed');

                WL.SimpleDialog.show(
                    $filter('translate')('Confirm Delete Photo'),
                    $filter('translate')('Are you sure to delete this photo ?'), [{
                        text: $filter('translate')('Cancel'), handler: function () {
                            return;
                        }
                    },
                        {
                            text: $filter('translate')('Yes'), handler: function () {
                            $scope.deletePhoto(id)
                        }
                        }]);
                //$scope.modal.toggleDelete = !$scope.modal.toggleDelete;
                //$scope.forDeleting = id;
            }

            /*
             *  Delete photo
             * */
            $scope.deletePhoto = function (id) {
                //$scope.modal.toggleDelete = !$scope.modal.toggleDelete;
                //var index = $scope.mediaUpload.indexOf(id);
                $scope.mediaUpload.splice(id, 1);
                $scope.$apply();
                console.log("Id to be deleted : ", id);
            };

            /*
             *   Start Detecting Noise
             * */
            $scope.startDetectingNoise = function (isAnonymous, pet, insect, service) {

                try {
                    var format = 'hh:mm';

                    var actualDay = moment().format('dddd');
                    var actualTime = moment(moment(), format);

                    var time1 = moment('06:00', format);
                    var time2 = moment('20:00', format);

                    var time1Weekend = moment('07:00', format);

                    var notInDay = false;
                    switch (actualDay) {
                        case 'Sunday':
                        case 'Monday':
                        case 'Tuesday':
                        case 'Wednesday':
                        case 'Thursday': {
                            if (actualTime.isBetween(time1, time2)) {
                                notInDay = true;
                            }
                            break;
                        }

                        case 'Friday':
                        case 'Saturday': {
                            if (actualTime.isBetween(time1Weekend, time2)) {
                                notInDay = true;
                            }
                            break;
                        }

                    }

                    if (notInDay) {
                        WL.SimpleDialog.show(
                            $filter('translate')('Alert'), $filter('translate')("MessageNoiseDay"), [
                                {
                                    text: $filter('translate')("Close"), handler: function () {
                                    window.history.back();
                                }
                                }]
                        );
                    } else {

                        $scope.modal.openNoise = !$scope.modal.openNoise;
                        $scope.LoudNoise = false;
                        var buffer = [];
                        var bufferPick = [];
                        var i = 0;
                        var j = 0;
                        $scope.isStopped = false;
                        $scope.timerPromise = null;
                        document.getElementById('noise').innerText = 0;

                        var NOISE_THRESHOLD = 75;
                        var PICK_DURATION = 200;
                        var MAX_NOISE_DURATION = 5000;
                        var PICK_NUMBER = 3;
                        var startNoise = null;
                        var endNoise = null;
                        var count = 16;

                        var MIN_DB = 30;
                        var MAX_DB = NOISE_THRESHOLD + 20;
                        var LEVELS = 12;
                        var STEP = (MAX_DB - MIN_DB) / LEVELS;
                        var peak = (NOISE_THRESHOLD / STEP) - (MIN_DB / STEP);

                        $scope.timerPromise = $interval(function () {
                            count--;
                            navigator.DBMeter.isListening(function (isListening) {
                                if (!isListening && !$scope.isStopped) {
                                    navigator.DBMeter.start(function (dB) {
                                        dB = (PLATFORM == 'android') ? parseInt(dB) - 10 : parseInt(dB);
                                        var soundLevel = Math.round((Math.round(parseInt(dB)) / STEP) - (MIN_DB / STEP));

                                        for (var c = 1; c <= LEVELS; c++) {
                                            if (c < peak) {
                                                document.getElementById("u" + c).style.backgroundColor = "#003400";
                                                if (c <= soundLevel) {
                                                    document.getElementById("u" + c).style.backgroundColor = "#09d909";
                                                }
                                            }

                                            if (c >= peak) {
                                                document.getElementById("u" + c).style.backgroundColor = "#660000";
                                                if (c <= soundLevel) {
                                                    document.getElementById("u" + c).style.backgroundColor = "#e10e0e";
                                                }
                                            }
                                        }
                                        if (parseInt(dB) > NOISE_THRESHOLD) {
                                            if (!startNoise) {
                                                startNoise = new Date().getTime();
                                                buffer[i] = parseInt(dB);
                                                i++;
                                            }

                                        }

                                        if (parseInt(dB) < NOISE_THRESHOLD - 5 && startNoise) {

                                            endNoise = new Date().getTime();
                                            var diff = (endNoise - startNoise);

                                            if (diff >= PICK_DURATION && diff < MAX_NOISE_DURATION) {
                                                bufferPick[j] = _.reduce(buffer, function (memo, num) {
                                                        return memo + num;
                                                    }, 0) / (buffer.length === 0 ? 1 : buffer.length);

                                                console.log("PICK ==> ", bufferPick[j]);
                                                console.log("BUFFER PICK ==> ", bufferPick);

                                                j++;
                                            }

                                            startNoise = null;
                                            endNoise = null;
                                            buffer = [];
                                            i = 0;


                                        }

                                        if (startNoise && (new Date().getTime() - startNoise) >= MAX_NOISE_DURATION) {

                                            $scope.isStopped = true;
                                            cancelTimer();
                                            $scope.LoudNoise = true;
                                            document.getElementById('noise').innerText = '';
                                            $scope.DBMesure = dB;
                                            navigator.DBMeter.stop(function () {
                                                console.log("DBMeter well stopped");
                                            }, function (e) {
                                                console.log('code: ' + e.code + ', message: ' + e.message);
                                            });
                                            $scope.$apply();
                                            $scope.DBMesure = _.reduce(bufferPick, function (memo, num) {
                                                    return memo + num;
                                                }, 0) / (bufferPick.length === 0 ? 1 : bufferPick.length);

                                            showPopupAfterNoiseDetected(isAnonymous, pet, insect, service);
                                            return;

                                        }

                                        document.getElementById('noise').innerText = count;

                                        if (bufferPick.length >= PICK_NUMBER) {
                                            $scope.isStopped = true;
                                            cancelTimer();
                                            $scope.LoudNoise = true;
                                            document.getElementById('noise').innerText = '';
                                            $scope.DBMesure = dB;
                                            navigator.DBMeter.stop(function () {
                                                console.log("DBMeter well stopped");
                                            }, function (e) {
                                                console.log('code: ' + e.code + ', message: ' + e.message);
                                            });
                                            $scope.$apply();
                                            $scope.DBMesure = _.reduce(bufferPick, function (memo, num) {
                                                    return memo + num;
                                                }, 0) / (bufferPick.length === 0 ? 1 : bufferPick.length);
                                            showPopupAfterNoiseDetected(isAnonymous, pet, insect, service);


                                        }
                                    }, function (e) {
                                        console.log('code: ' + e.code + ', message: ' + e.message);
                                    });
                                }
                            });

                        }, 1000, 16).then(function () {
                            if (!$scope.isStopped) {
                                $notification.alert($filter('translate')("Noise level is not detected"));
                                cancelTimer();
                                console.log('post timer')
                            }


                        });

                    }

                } catch (e) {

                    console.log(e);
                    $notification.alert($filter('translate')("An error has occurred while detecting noise level"));


                }
            }

            function showPopupAfterNoiseDetected(isAnonymous, pet, insect, service) {
                WL.SimpleDialog.show(
                    $filter('translate')('Noise detected'),
                    $filter('translate')('Do you want to submit your request?'), [{
                        text: $filter('translate')('Yes'), handler: function () {
                            /*$scope.callNoiseRequest($scope.DBMesure, isAnonymous, pet, insect, service);
                             return;*/
                            WL.SimpleDialog.show(
                                $filter('translate')('Record_Noise'),
                                $filter('translate')('Record_Noise_Message'), [{
                                    text: $filter('translate')('Yes'), handler: function () {
                                        $scope.timer = 15;
                                        $scope.modal.showTimer = true;
                                        window.plugins.audioRecorderAPI.record(function (msg) {
                                            // complete
                                            console.log('ok: ' + msg);

                                        }, function (msg) {
                                            // failed
                                            console.log('ERROR from recorder.record: ' + msg);
                                        }, 20); // record 30 seconds
                                        $("#progressTimer").progressTimer({
                                            timeLimit: 15,
                                            warningThreshold: 5,
                                            baseStyle: 'progress-bar-warning-custom',
                                            warningStyle: 'progress-bar-danger-custom',
                                            completeStyle: 'progress-bar-info-custom',
                                            onFinish: function () {
                                                console.log("I'm done");
                                            }
                                        });
                                        $interval(function () {
                                            $scope.timer--;
                                        }, 1000, 15)
                                            .then(function () {
                                                window.plugins.audioRecorderAPI.stop(function (msg) {
                                                    // success
                                                    console.log('ok: ' + msg);
                                                    $scope.recordedNoiseAudio = msg;
                                                    $scope.modal.showTimer = false;
                                                    $scope.$apply();
                                                    $scope.callNoiseRequest($scope.DBMesure, isAnonymous, pet, insect, service);
                                                }, function (msg) {
                                                    // failed
                                                    console.log('ERROR from recorder.stop: ' + msg);
                                                });

                                            });

                                    }
                                },
                                    {
                                        text: $filter('translate')('Cancel'), handler: function () {
                                        return;
                                    }
                                    }]);
                        }
                    },
                        {
                            text: $filter('translate')('Cancel'), handler: function () {
                            return;
                        }
                        }]);
            }

            function cancelTimer() {
                $scope.isStopped = true;
                $scope.stopDetectingNoise();
                $scope.modal.openNoise = !$scope.modal.openNoise;
                $interval.cancel($scope.timerPromise);
            }

            /*
             *   Stop Detecting Noise
             * */
            $scope.stopDetectingNoise = function () {
                $interval.cancel($scope.timerPromise);
                $scope.isStopped = true;
                navigator.DBMeter.isListening(function (isListening) {
                    console.log(isListening);
                    navigator.DBMeter.stop(function () {
                        console.log("DBMeter well stopped");
                    }, function (e) {
                        console.log('code: ' + e.code + ', message: ' + e.message);
                    });
                });
            }

            /*
             *  Preapare Noise request
             * */
            $scope.callNoiseRequest = function (dBLevel, isAnonymous, pet, insect, service) {
                if ($auth.token || GuestUtils.getGuestPhoneNumber()) {
                    $scope.sendRequest(isAnonymous, pet, insect, service);
                } else {
                    WL.SimpleDialog.show(
                        $filter('translate')('Registration'),
                        $filter('translate')('Please register'), [{
                            text: $filter('translate')('I don\'t want to register'), handler: function () {
                                $scope.sendRequest(isAnonymous, pet, insect, service);
                                return;
                            }
                        },
                            {
                                text: $filter('translate')('Register now'), handler: function () {
                                localStorage.setItem('recordedAudio', $scope.recordedAudio);
                                var payload = {
                                    listServices: $scope.services,
                                    makani: $scope.makani,
                                    locationName: $scope.locationName,
                                    formattedAddress: $scope.formattedAddress,
                                    requestText: $scope.additionalInfo,
                                    location: $scope.location,
                                    locationDetail: $scope.locationDetail
                                };

                                SharedService.setStateHistory('personalDetails_1', {
                                    payload: payload,
                                    from: 'smartRequest'
                                });

                                $state.go('personalDetails_1', {
                                    from: 'smartRequest',
                                    payload: payload
                                });
                            }
                            },
                            {
                                text: $filter('translate')('Cancel'), handler: function () {
                                return;
                            }
                            }]);
                }
            };

            function isValidPlace() {
                return ($scope.locationName.toUpperCase().indexOf('DUBAI') >= 0 ||
                    $scope.locationName.indexOf('دبي') >= 0 ||
                    $scope.locationName.toUpperCase().indexOf('DUBAÏ') >= 0 ||
                    $scope.locationName.indexOf('دبى') >= 0) &&
                    ($scope.location.lat >= 24.792136 && $scope.location.lat <= 25.358561 &&
                    $scope.location.lng >= 54.890454 && $scope.location.lng <= 55.565039)
            }

            $scope.checkConnection = function (isAnonymous, pet, insect, service) {
//                if ( $scope.locationName.toUpperCase().indexOf('DUBAI') >= 0 ||
//                                            $scope.locationName.indexOf('دبي') >= 0 ||
//                                            $scope.locationName.toUpperCase().indexOf('DUBAÏ') >= 0 ||
//                                            $scope.locationName.indexOf('دبى') >= 0) {
                if (isValidPlace()) {
                    service = $scope.service;
                    if (service && service.service_title_en == 'Report Construction Noise') {
                        $scope.startDetectingNoise(isAnonymous, pet, insect, service);
                    } else {
                        if ($auth.token || GuestUtils.getGuestPhoneNumber()) {
                            $scope.sendRequest(isAnonymous, pet, insect, service);
                        } else {
                            WL.SimpleDialog.show(
                                $filter('translate')('Registration'),
                                $filter('translate')('Please register'), [{
                                    text: $filter('translate')('I don\'t want to register'), handler: function () {
                                        $scope.sendRequest(isAnonymous, pet, insect, service);
                                        return;
                                    }
                                },
                                    {
                                        text: $filter('translate')('Register now'), handler: function () {
                                        var payload = {
                                            listServices: $scope.services,
                                            makani: $scope.makani,
                                            locationName: $scope.locationName,
                                            formattedAddress: $scope.formattedAddress,
                                            requestText: $scope.additionalInfo,
                                            location: $scope.location,
                                            recordedAudio: $scope.recordedAudio,
                                            locationDetail: $scope.locationDetail
                                        }
                                        SharedService.setStateHistory('personalDetails_1', {
                                            payload: payload,
                                            from: 'smartRequest'
                                        });

                                        $state.go('personalDetails_1', {
                                            from: 'smartRequest',
                                            payload: payload
                                        });
                                    }
                                    },
                                    {
                                        text: $filter('translate')('Cancel'), handler: function () {
                                        return;
                                    }
                                    }]);
                        }
                    }

                }

                else {
                    LoadingService.hide();
                    $notification.alert($filter('translate')("This location is not covered by the service"));
                }


            }

            /*
             *  send Request
             * */

            $scope.sendRequest = function (isAnonymous, pet, insect, service) {


                try {

                    LoadingService.show();
                    //service = service.$modelValue;

                    if (!$scope.service) {
                        service = {
                            "serial": 1000,
                            "category_id": 1000,
                            "category_CRM_id": 1000,
                            "pet_type": 0,
                            "insect_type": 0,
                            "is_prefered_time": 0,
                            "service_title_en": "Mobile Unmapped Service",
                            "service_title_ar": "Mobile Unmapped Service",
                            "sla": 99,
                            "isService": false,
                            "isPopular": false,
                            "isVisitor": false,
                            "isAnonymous": false,
                            "isHidden": false,
                            "serviceEN": "Mobile Unmapped Service",
                            "serviceAR": "Mobile Unmapped Service",
                            "organization_unit_en": "Mobile Unmapped Service",
                            "organization_unit_ar": "Mobile Unmapped Service",
                            "description_en": "Mobile Unmapped Service",
                            "description_ar": "Mobile Unmapped Service",
                            "service_request_id": 1000
                        }
                    } else {
                        service = $scope.service;
                    }

                    $scope.serviceFinal = service;
                    moment.locale('en-gb'); // Local format (Grand Britain)

                    var mobileNumber = '';
                    var username = '';
                    var emailAddress = '';


                    var areaId = (!$rootScope.makaniInfo || !$rootScope.makaniInfo.COMMUNITY_NUM) ? '' : $rootScope.makaniInfo.COMMUNITY_NUM;

                    if ($auth.token) {
                        mobileNumber = $auth.dubaiUserProfile.mobile;
                        username = $auth.dubaiUserProfile.username;
                        emailAddress = ($auth.dubaiUserProfile.email) ? $auth.dubaiUserProfile.email : '';
                    } else if (!GuestUtils.isEmptyGuest()) {
                        var guestInfo = GuestUtils.getGuestInfo();
                        if (guestInfo != '') {
                            mobileNumber = (guestInfo.mobileNumber) ? guestInfo.mobileNumber : '';
                            username = (guestInfo.name) ? guestInfo.name : '';
                            emailAddress = (guestInfo.email) ? guestInfo.email : '';
                        }
                    } else {
                        mobileNumber = '+971000000000';
                        username = '';
                        emailAddress = '';
                    }

                    var showContactData = true;
                    if (service.isAnonymous == true) {

                        if (isAnonymous.$viewValue == true) {
                            showContactData = false;
                        }
                    }
                    // todo add preferred time

                    var PreferredDate = '';
                    var PreferredTime = '';
                    if (service.is_prefered_time && service.is_prefered_time == 1) {
                        //PreferredDate= moment().format('L');
                        PreferredDate = new Date();
                        PreferredTime = moment().format('HH:mm');
                    }

                    var pet_type_id = '';
                    if ($scope.pet_type_id) {
                        pet_type_id = $scope.pet_type_id;
                    } else {
                        if (pet) {
                            pet_type_id = pet.$modelValue;
                        }
                    }

                    var req = {
                        "ServiceTypeId": (service.incident_id !== undefined) ? 1 : 2,
                        "ServiceId": (service.incident_id !== undefined) ? service.incident_id : service.service_request_id,
                        "ServiceCategoryId": service.category_CRM_id,
                        "Street": $scope.locationName,
                        "Building": '',
                        "AreaId": areaId,
                        "Makani": $scope.makani,
                        "Floor": ($scope.floorNumber) ? $scope.floorNumber + '' : '',
                        "Flat": ($scope.flatNumber) ? $scope.flatNumber + '' : '',
                        "Location_X": $scope.location.lng + '',
                        "Location_Y": $scope.location.lat + '',
                        "AdditionalAddressDetails": '',
                        "SLA": service.sla,
                        "SourceOfCreation": 'Dubai 24/7',
                        "PetTypeId": pet_type_id,
                        "InsectTypeId": (!service.insect_type) ? '' : (service.insect_type == 1) ? insect.$modelValue : '',
                        "Description": ($scope.DBMesure) ? '[Noise Mesure Average : ' + $scope.DBMesure + '] ' + $scope.additionalInfo : $scope.additionalInfo,
                        "ContactFullName": username,
                        "ContactMobileNumber": mobileNumber,
                        "ContactEmail": emailAddress,
                        "ContactEmirateId": null,
                        "LicenseNo": '',
                        "TradeLicenseNameEN": '',
                        "TradeLicesnseNameAR": '',
                        "SmartCrmTransactionId": Date.now(),
                        "ShowContactData": showContactData,
                        "PreferredDate": PreferredDate,
                        "PreferredTime": PreferredTime,
                        "VeterniaryCardNumber": ''
                    };
                    console.log('==> REQUEST  ', req);

                    var params = [req.ServiceTypeId, req.ServiceId, req.ServiceCategoryId, req.Street,
                        req.Building, req.AreaId, req.Makani, req.Floor, req.Flat,
                        req.Location_X, req.Location_Y, req.AdditionalAddressDetails, req.SLA,
                        req.SourceOfCreation, req.PetTypeId, req.InsectTypeId, req.Description,
                        req.ContactFullName, req.ContactMobileNumber, req.ContactEmail, req.ContactEmirateId,
                        req.LicenseNo, req.TradeLicenseNameEN, req.TradeLicesnseNameAR, req.SmartCrmTransactionId,
                        req.ShowContactData, req.PreferredDate, req.PreferredTime, req.VeterniaryCardNumber
                    ];

                    if ($scope.mediaUpload.length == 0 && !$scope.recordedAudio && !$scope.recordedNoiseAudio) {
                        createNotification(params);
                    } else {
                        LoadingService.show('Uploading Media');
                        /*$scope.attachmentsToBeUploaded = [];*/
                        $scope.attachmentsToBeUploaded = {"imageList": [], "audioList": []};
                        $scope.mediaUpload.map(function (item, index) {

                            $scope.attachmentsToBeUploaded.imageList.push({
                                fileName: item.id + '',
                                fileContent: item.src.replace(/^data:image\/(png|jpg|jpeg);base64,/, "")
                            });
                        });

                        if ($scope.recordedAudio) {

                            /*var xhr = new XMLHttpRequest();
                             xhr.open("GET", $scope.recordedAudio, true);
                             xhr.responseType = "blob";
                             xhr.onload = function (e) {
                             console.log(this.response);
                             var reader = new FileReader();
                             reader.onload = function (event) {
                             var res = event.target.result;
                             console.log('res:', res);
                             console.log('File extension:', $scope.recordedAudio.substr($scope.recordedAudio.lastIndexOf('/')));
                             $scope.attachmentsToBeUploaded.audioList.push({
                             fileName:'additionalDetails',
                             fileContent: res.split(",").pop(),
                             fileExtension: $scope.recordedAudio.substr($scope.recordedAudio.lastIndexOf('.')+1)
                             })
                             addAttachments(params);
                             };
                             var file = this.response;
                             reader.readAsDataURL(file);
                             };
                             xhr.send();*/
                            convertAudioToBase64('GET', $scope.recordedAudio, function (err, data) {
                                if (err) {
                                    throw err;
                                }
                                console.log(data);
                                $scope.attachmentsToBeUploaded.audioList.push({
                                    fileName: 'additionalDetails',
                                    fileContent: data.res.split(",").pop(),
                                    fileExtension: $scope.recordedAudio.substr($scope.recordedAudio.lastIndexOf('.') + 1)
                                });

                            });
                        }

                        if ($scope.recordedNoiseAudio) {
                            convertAudioToBase64('GET', $scope.recordedNoiseAudio, function (err, data) {
                                if (err) {
                                    throw err;
                                }
                                console.log(data);
                                $scope.attachmentsToBeUploaded.audioList.push({
                                    fileName: 'noiseAudio',
                                    fileContent: data.res.split(",").pop(),
                                    fileExtension: $scope.recordedNoiseAudio.substr($scope.recordedNoiseAudio.lastIndexOf('.') + 1)
                                });

                            });
                        }

                        setTimeout(function () {
                            addAttachments(params);
                        }, 2000)

                    }


                } catch (e) {
                    catchError(e);
                    $notification.alert($filter('translate')("Can't create request. Please try again later"));
                }
            };


            function addAttachments(params) {
                DMService.addAttachment($scope.attachmentsToBeUploaded).then(function (result) {

                    if (result.isSuccessful) {
                        var attachment = [];
                        for (var i = 0; i < result.response.length; i++) {
                            if (result.response[i].Success == true) {
                                attachment.push({
                                    'FileName': result.response[i].OriginalFileName,
                                    'PhysicalFileName': result.response[i].PhysicalFileName
                                });
                            }
                        }


                        params.push(attachment);
                        createNotification(params);


                    } else {
                        LoadingService.hide();
                        $notification.alert($filter('translate')("Error while uploading photo, please try again later"));
                    }
                }, function (err) {
                    LoadingService.hide();
                    $notification.alert($filter('translate')("Error while uploading photo, please try again later"));
                    console.log(err);
                })
            }


            /*
             *  Create request
             * */
            function createNotification(params) {

                DMService.createNotification(params).then(function (result) {

                        $scope.serviceResponse = result;

                        if (result.isSuccessful === true && result.ExternalTranasactionId != -1) {
                            AnalyticsService.trackRoute(Constants.HIT_TYPE.EVENT, '', {
                                eventCategory: Constants.EVENTS.Services_Submitted,
                                eventAction: Constants.EVENTS.Services_Submitted,
                                eventLabel: $scope.serviceFinal.serviceEN
                            });

                            //save location in storage
                            $scope.service = $scope.serviceFinal;
                            //get lat
                            var lat = $scope.location.lat;
                            //get lng
                            var lng = $scope.location.lng;
                            //get service title
                            var serviceTitleAr = $scope.service.service_title_ar;
                            var serviceTitleEn = $scope.service.service_title_en;
                            //get makani number
                            var makaniNumber = $scope.makani;
                            //get location by google maps
                            var locationName = $scope.locationName;
                            //get location by user
                            var locationDetail = $scope.locationDetail;
                            //get request number
                            var requestNumber = result.ExternalTranasactionId;

                            var isNewLocation = true;
                            window.db.transaction(function (txDB) {

                                txDB.executeSql('CREATE TABLE IF NOT EXISTS locationList (id, list)', [], function (tx, res) {
                                    var query = "SELECT list FROM locationList";
                                    txDB.executeSql(query, [], function (tx, resultSet) {
                                        if (resultSet.rows.length != 0) {
                                            locationList = JSON.parse(resultSet.rows.item(0).list);
                                            angular.forEach(locationList, function (locationItem, locationkey) {
                                                if ('' + lat + '-' + lng == locationkey) {
                                                    //save the new request
                                                    isNewLocation = false;

                                                    locationItem.requestList.push({
                                                        "serviceTitleAr": serviceTitleAr,
                                                        "serviceTitleEn": serviceTitleEn,
                                                        "RequestNumber": requestNumber
                                                    });
                                                    txDB.executeSql('UPDATE locationList SET list = ?', [JSON.stringify(locationList)], function (tx, resultSet) {
                                                        //console.log("Update location list success:", JSON.stringify(tx), JSON.stringify(resultSet));
                                                        SharedService.setStateHistory('requestDone', {
                                                            result: result
                                                        });
                                                        $state.go('requestDone', {
                                                            result: result
                                                        });
                                                    }, function (tx, error) {
                                                        console.log('Update location list error: ' + error.message);
                                                    });
                                                }
                                            });
                                            if (isNewLocation) {
                                                //add a new location in list
                                                newKey = '' + lat + '-' + lng;
                                                locationList[newKey] = {
                                                    "lat": lat,
                                                    "lng": lng,
                                                    "makaniNumber": makaniNumber,
                                                    "location": locationName,
                                                    "locationDetail": locationDetail,
                                                    requestList: [{
                                                        "serviceTitleAr": serviceTitleAr,
                                                        "serviceTitleEn": serviceTitleEn,
                                                        "RequestNumber": requestNumber
                                                    }]
                                                };
                                                txDB.executeSql('UPDATE locationList SET list = ?', [JSON.stringify(locationList)], function (tx, resultSet) {
                                                    //console.log("Update location list success:", JSON.stringify(tx), JSON.stringify(resultSet));
                                                    SharedService.setStateHistory('requestDone', {
                                                        result: result
                                                    });
                                                    $state.go('requestDone', {
                                                        result: result
                                                    });
                                                }, function (tx, error) {
                                                    console.log('Update location list error: ' + error.message);
                                                });
                                            }
                                        }
                                        else {
                                            //cretae locationList and save the first data in storage
                                            keyLocation = '' + lat + '-' + lng;
                                            locationResult = {};
                                            locationResult[keyLocation] = {
                                                "lat": lat,
                                                "lng": lng,
                                                "makaniNumber": makaniNumber,
                                                "location": locationName,
                                                "locationDetail": locationDetail,
                                                requestList: [{
                                                    "serviceTitleAr": serviceTitleAr,
                                                    "serviceTitleEn": serviceTitleEn,
                                                    "RequestNumber": requestNumber
                                                }]
                                            };
                                            txDB.executeSql('CREATE TABLE IF NOT EXISTS locationList (id, list)', [], function (tx1, res1) {
                                                //console.log('CREATE TABLE IF NOT EXISTS locationList', JSON.stringify(tx1), JSON.stringify(res1));
                                                txDB.executeSql('INSERT INTO locationList VALUES (?,?)', ['1', JSON.stringify(locationResult)], function (tx11, res11) {
                                                    SharedService.setStateHistory('requestDone', {
                                                        result: result
                                                    });
                                                    $state.go('requestDone', {
                                                        result: result
                                                    });
                                                });
                                            });
                                        }
                                    }, function (tx, error) {
                                        console.log('SELECT location error: ' + error.message);
                                    });
                                }, function (tx, error) {
                                    console.log('failure to create table locationList: ' + error.message);
                                });
                            }, function () {
                                console.log('transaction ok');
                            });

                        } else {
                            LoadingService.hide();
                            $notification.alert($filter('translate')("Some error while creating the request"));
                        }
                    },
                    function (error) {
                        LoadingService.hide();
                        $notification.alert($filter('translate')("Can't create request. Please try again later"));
                    });
            }

            /*
             * Catch Error
             */

            function catchError(e) {
                console.log('===> ERROR ', e);
                LoadingService.hide();
            }


            /**
             * Open makani website
             */
            $scope.openWebsite = function () {
                window.open($DMLinks.makani, "_blank", 'location=yes');
            };
            setTimeout(function () {
                $('.search-Item.search-item-Radio.md-radio input').on('change', function () {
                    $('.search-Item.search-item-Radio.md-radio input').not(this).prop('checked', false);
                    $(this).prop('checked', true);
                });
            }, 2000);


            /**
             *  Gallery
             * */
            /*angular.element(document).ready(function () {
             setTimeout(function () {

             $("#mediaGallery").lightGallery({
             fullScreen: false
             });


             }, 2000);

             });*/


            $scope.clearText = function () {
                $scope.additionalInfo = '';
                setTimeout(function () {
                    $('#additionalInfo').click();
                }, 100);
            };

            /*
             *  Speech To Text
             * */
            $scope.speechToText = function (searchRequest, mediaType) {
                var requestFrom = 'api'
                var platform = WL.Client.getEnvironment();
                var lang = localStorage.getItem('dm-lang') == 'ar' ? 'ar-AE' : 'en-US';

                SearchEngine.search(searchRequest, mediaType, requestFrom, platform, lang).then(
                    function (result) {
                        console.log('Speech:', result);
                        LoadingService.hide();
                        if (result) {
                            $scope.additionalInfo = $scope.additionalInfo + ' ' + result.requestText;
                        } else {
                            $scope.recordedAudio = '';
                        }

                    },
                    function (error) {
                        $scope.recordedAudio = '';
                        console.log(error);
                        LoadingService.hide();
                    }
                );

            };

            // record animation and events
            function pad(val) {
                var valString = val + "";
                if (valString.length < 2) {
                    return "0" + valString;
                } else {
                    return valString;
                }
            }

            var timer = null;

            function chrono() {
                var totalSeconds = 0;
                document.getElementById('heroInput').value = '    00:00';
                timer = setInterval(function () {
                    ++totalSeconds;
                    document.getElementById('heroInput').value = '    ' + pad(parseInt(totalSeconds / 60)) + ':' + pad(totalSeconds % 60);
                }, 1000);
            }

            function convertAudioToBase64(method, url, done) {
                var xhr = new XMLHttpRequest();
                xhr.open(method, url, true);
                xhr.responseType = "blob";
                xhr.onload = function () {
                    var reader = new FileReader();
                    reader.onload = function (event) {
                        var res = event.target.result;
                        console.log('res:', res);
                        console.log('File extension:', url.substr(url.lastIndexOf('/')));
                        done(null, {res: res, url: url});
                    };
                    var file = this.response;
                    reader.readAsDataURL(file);

                };
                xhr.onerror = function () {
                    done(xhr.response);
                };
                xhr.send();
            }

            var longPress = false;

            $scope.recorder = new Object;

            $scope.recorder.stop = function () {
                if (longPress) {
                    LoadingService.show('Transforming your voice in text');
                    AnalyticsService.trackRoute(Constants.HIT_TYPE.EVENT, '', {
                        eventCategory: 'Search_By_Voice',
                        eventAction: 'Search By voice in create request'

                    });
                    window.plugins.audioRecorderAPI.stop(function (msg) {
                        // success
                        console.log('ok: ' + msg);
                        $scope.recordedAudio = msg;
                        /*var xhr = new XMLHttpRequest();
                         xhr.open("GET", msg, true);
                         xhr.responseType = "blob";
                         xhr.onload = function (e) {
                         console.log(this.response);
                         var reader = new FileReader();
                         reader.onload = function (event) {
                         var res = event.target.result;
                         console.log('res:', res);
                         console.log('File extension:', msg.substr(msg.lastIndexOf('/')));
                         $scope.speechToText(res.split(",").pop(), 'speech');
                         };
                         var file = this.response;
                         reader.readAsDataURL(file);
                         };
                         xhr.send();*/
                        convertAudioToBase64('GET', msg, function (err, data) {
                            if (err) {
                                throw err;
                            }
                            console.log(data);
                            $scope.speechToText(data.res.split(",").pop(), 'speech');
                        });
                        longPress = false;
                    }, function (msg) {
                        // failed
                        console.log('ERROR from recorder.stop: ' + msg);
                    });
                } else {
                    console.log('Shwoing tooltip...');
                    $('.recTooltip').css('visibility', 'visible');
                    $('.recTooltip').css('opacity', 1);
                    $('.recTooltiptext').css('visibility', 'visible');
                    $('.recTooltiptext').css('opacity', 1);

                    var hideTooltip = _.debounce(function (searchRequest, mediaType) {
                        $('.recTooltip').css('opacity', 0);
                        $('.recTooltiptext').css('opacity', 0);
                        $('.recTooltip').css('visibility', 'hidden');
                        $('.recTooltiptext').css('visibility', 'hidden');
                    }, 3000);

                    hideTooltip();

                }
            };
            /*
             *  open details service
             * */
            $scope.openDetailService = function (service) {
                ngDialog.open({
                    template: 'views/modal/detail-service-dialog.html',
                    className: 'ngdialog-theme-default popoup',
                    // hide the close button on modals
                    showClose: true,
                    closeByNavigation: true,
                    // closeByDocument : false,
                    controller: function ($scope) {
                        console.log("ngdialog ", $scope);
                        $scope.service = service;
                        $scope.currentLang = $scope.currentLang;
                    },
                    scope: $scope//$scope
                })
            };

            $scope.recorder.record = function () {
                window.plugins.audioRecorderAPI.record(function (msg) {
                    // complete
                    console.log('ok: ' + msg);

                }, function (msg) {
                    // failed
                    console.log('ERROR from recorder.record: ' + msg);
                }, 15); // record 15 seconds
            }

            $scope.recorder.playback = function () {
                $scope.isPlaying = true;
                window.plugins.audioRecorderAPI.playback(function (msg) {
                    // complete
                    $scope.isPlaying = false;
                    $scope.$apply();
                    console.log('ok: ' + msg);
                }, function (msg) {
                    // failed
                    console.log('ERROR from recorder.playback : ' + msg);
                });
            };

            $scope.recorder.pausePlayback = function () {
                window.plugins.audioRecorderAPI.pausePlayback(function (msg) {
                    // complete
                    console.log('ok: ' + msg);
                }, function (msg) {
                    // failed
                    console.log('ko: ' + msg);
                });
            }
            $scope.recorder.stopPlayback = function () {

                window.plugins.audioRecorderAPI.stopPlayback(function (msg) {
                    $scope.isPlaying = false;
                    $scope.$apply();
                    // complete
                    console.log('ok: ' + msg);
                }, function (msg) {
                    // failed
                    console.log('ko: ' + msg);
                });
            }

            $scope.startRecording = function () {
                longPress = true;

                $('#rectButton').addClass("blueRec");
                $('.recTooltip .recTooltiptext').css({
                    'visibility': 'hidden',
                    'opacity': 0
                });

                // chrono();
                $scope.recorder.record();

                //callDb();
            }

            $scope.stopRecording = function () {
                $('#rectButton').removeClass("blueRec");
                //document.getElementById('heroInput').value = '';
                clearTimeout(timer);
                $scope.recorder.stop();
                /*navigator.DBMeter.stop(function () {
                 console.log("DBMeter well stopped");
                 }, function (e) {
                 console.log('code: ' + e.code + ', message: ' + e.message);
                 });*/

            }

            $scope.displayServiceDetails = function (service) {
                $rootScope.serviceInfo = service;

                var SLA = ( (parseInt($rootScope.serviceInfo.sla) % 7) > 0) ? (parseInt($rootScope.serviceInfo.sla) / 7) + 1 : (parseInt($rootScope.serviceInfo.sla) / 7);
                $rootScope.serviceInfo.CalculatedSLA = Math.round(SLA);

                ngDialog.open({
                    template: 'service-details-dialog',
                    className: 'ngdialog-theme-default'
                });
            }


        }]
);