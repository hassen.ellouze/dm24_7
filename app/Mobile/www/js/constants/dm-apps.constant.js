
/* JavaScript content from js/constants/dm-apps.constant.js in folder common */
iDubaiApp.factory('$appList', [function () {
    /*
     * List of applications
     */
    var appList = {
		"android" : {
			"1" : {
				"name" : "Makani",
				"icon" : "images/makani.png",
				"ibiApp" : "makani",
				"link" : "com.dm.makani"
			},
			"2" : {
				"name" : "Dubai Parks & Beaches",
				"icon" : "images/parks-and-beaches.svg",
				"ibiApp" : "parksandbeaches",
				"link" : "com.dm.parksandbeaches"
			},
			"3" : {
				"name" : "Taheel",
				"icon" : "images/TAHEEL.svg",
				"ibiApp" : "taheel",
				"link" : "org.dm.taheel"
			},
			"4" : {
				"name" : "Najm Suhail",
				"icon" : "images/najm-sohail.svg",
				"ibiApp" : "najmsuhail",
				"link" : "com.microstepmis.android.ims.uae537LightVersion"
			},
			"5" : {
				"name" : "Green Buildings",
				"icon" : "images/green-buildings.svg",
				"ibiApp" : "greenbuildings",
				"link" : "com.dubaimunicipality.greenbuildings"
			}
		},
		"ios" : {
			"1" : {
				"name" : "Makani",
				"icon" : "images/makani.png",
				"ibiApp" : "makani",
				"link" : "731838002&&makani://"
			},
			"2" : {
				"name" : "Dubai Parks & Beaches",
				"icon" : "images/parks-and-beaches.svg",
				"ibiApp" : "parksandbeaches",
				"link" : "925324303&&dubaiparksbeaches://"
			},
			"3" : {
				"name" : "Taheel",
				"icon" : "images/TAHEEL.svg",
				"ibiApp" : "taheel",
				"link" : "962726743&&taheel://"
			},
			"4" : {
				"name" : "Najm Suhail",
				"icon" : "images/najm-sohail.svg",
				"ibiApp" : "najmsuhail",
				"link" : "898731793&&najmsuhail://"
			},
			"5" : {
				"name" : "Green Buildings",
				"icon" : "images/green-buildings.svg",
				"ibiApp" : "greenbuildings",
				"link" : "989622573&&greenbuildings://"
			}
		},
		"blackberry" : {
			
		},
		"windowsPhone" : {
			
		},
		"DESKTOP" : {
			"1" : {
				"name" : "Makani",
				"icon" : "images/makani.png",
				"ibiApp" : "makani",
				"link" : "com.dm.makani"
			},
			"2" : {
				"name" : "Dubai Parks & Beaches",
				"icon" : "images/parks-and-beaches.png",
				"ibiApp" : "parksandbeaches",
				"link" : "com.dm.parksandbeaches"
			},
			"3" : {
				"name" : "Taheel",
				"icon" : "images/TAHEEL.png",
				"ibiApp" : "taheel",
				"link" : "org.dm.taheel"
			},
			"4" : {
				"name" : "Najm Suhail",
				"icon" : "images/najm-sohail.png",
				"ibiApp" : "najmsuhail",
				"link" : "com.microstepmis.android.ims.uae537LightVersion"
			},
			"5" : {
				"name" : "Green Buildings",
				"icon" : "images/green-buildings.png",
				"ibiApp" : "greenbuildings",
				"link" : "com.dubaimunicipality.greenbuildings"
			}
		}
    };
    
    /*
     * Return size of an object
     */
    Object.size = function(obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };
    
    /*
     * Contain current Environment
     */
    
    //var sys = WL.Client.getEnvironment() == WL.Environment.IPHONE ? "ios" : (WL.Client.getEnvironment() == WL.Environment.ANDROID ? "android" : (WL.Client.getEnvironment() == WL.Environment.WINDOWS_PHONE ? "windowsPhone" : (WL.Client.getEnvironment() == WL.Environment.BLACKBERRY ? "blackberry" : "DESKTOP")));
    
    var sys = "DESKTOP";    
    if(WL.Client.getEnvironment() == WL.Environment.WINDOWS_PHONE || WL.Client.getEnvironment() == WL.Environment.WINDOWS_PHONE_8){
    	sys = "windowsPhone";
    }else if(WL.Client.getEnvironment() == WL.Environment.BLACKBERRY || WL.Client.getEnvironment() == WL.Environment.BLACKBERRY10){
    	sys = "blackberry";
    }else if(WL.Client.getEnvironment() == WL.Environment.IPHONE){
    	sys = "ios";
    }else if(WL.Client.getEnvironment() == WL.Environment.ANDROID){
    	sys = "android";
    } 
	
	
    /*
     * Return current Environment
     */
    
    appList.currentEnv = function(){
    	return sys;
    };
	
    /*
     * Return size of applications for current Environment
     */
    
    appList.listSize = function(){
    	return Object.size(appList[sys]);
    };
    
    /*
     * Return list of applications for current Environment
     */
    
    appList.getList = function(){
    	return typeof(appList[sys]) != 'undefined' ? appList[sys] : {};
    };

    return appList;
}]); 