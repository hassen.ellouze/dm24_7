iDubaiApp.directive('maps', ["$rootScope", "$myLocations", "$timeout", '$filter', '$notification',
    "$logger", "$q", 'LoadingService', '$DMLinks', 'Makani', 'underscore', '$stateParams', '$state', 'AnalyticsService', 'Constants',
    function ($rootScope, $myLocations, $timeout, $filter, $notification, $logger, $q, LoadingService,
              $DMLinks, Makani, underscore, $stateParams, $state, AnalyticsService, Constants) {
        return {
            restrict: 'E',
            replace: false,
            scope: {selectedPosition: '=', errorMsg: '@', isRequired: '='},
            templateUrl: "widgets/maps.html",
            link: function (scope, el, attrs) {
                scope.serviceInfo =$stateParams.payload;
                var _ = underscore;
                scope.searchRes = {
                    toggleShow: false
                };
                $rootScope.makani='';
                scope.isMakaniSearch = false;
                scope.currentLang = localStorage.getItem('dm-lang');
                scope.isMyLocation = (attrs.state === 'myLocations');
                scope.locationName = '';
                var container =document.getElementsByClassName('pac-container');
                if (container){
                    $(".pac-container").remove();
                }
                $('#inputMap').on('focus',function(){
                    $('#pac-input').focus();
                })

                var bounds = new google.maps.LatLngBounds();
                var boundsSet = false;
                var autocomplete = null;

                scope.sendToRequest= function(serviceInfo, makani, locationName, formattedAddress, location, makaniInfo, locationDetail){
                        var payload={};
                        var from='';

                        if ($stateParams.from == 'smartRequest') {
                            payload = {
                                serviceInfo: JSON.stringify(serviceInfo),
                                makani: makani,
                                locationName: locationName,
                                formattedAddress: formattedAddress,
                                location: location,
                                makaniInfo: makaniInfo,
                                locationDetail: locationDetail,
                                listServices: $stateParams.payload.listServices,
                                requestText: $stateParams.payload.requestText
                            }

                            from = 'smartRequest';

                        } else {
                            payload = {
                                serviceInfo: JSON.stringify(serviceInfo),
                                makani: makani,
                                locationName: locationName,
                                formattedAddress: formattedAddress,
                                location: location,
                                makaniInfo: makaniInfo,
                                locationDetail: locationDetail,
                                additionalInfo: $stateParams.payload.additionalInfo
                            }

                            from = 'createRequest';

                        }

                    $state.go(from,
                        {
                            payload: payload
                        })


                }

                scope.goBack = function () {
                    $state.go('dashboard');
                };

                // show detail location
                scope.sLocation = function () {
                    var bottom = $('.location-Service');
                    var sLocHeight = bottom.height();
                    var marker = $('.getPosition');
                    var circleMarker = $('.circleMarker');
                    var actualBottom = bottom.offset().top - bottom.outerHeight(true);

                    if (actualBottom > sLocHeight) {
                        if (circleMarker.hasClass('isCircleMarker')) {
                            bottom.css('bottom', -(actualBottom + 500));
                            marker.css('bottom', 70);
                            circleMarker.removeClass('isCircleMarker');
                        } else {
                            bottom.css('bottom', 0);
                            marker.css('bottom', sLocHeight + 30);
                            circleMarker.addClass('isCircleMarker');
                        }

                    } else {
                        bottom.css('bottom', -(actualBottom + 500));
                        marker.css('bottom', 70);
                        circleMarker.removeClass('isCircleMarker');
                    }

                }

                var height = $(window).height();
                $(".requests-details ").css("max-height", height / 3 * 2);

                // map height
                var heightMaps = ($(window).height()) - 72;
                if (WL.Client.getEnvironment() === WL.Environment.IPHONE) {
                    heightMaps = ($(window).height()) - 90;
                }

                // Hide back button
                jQuery('#createRequestMap').css({'height': heightMaps + "px"});

                /**
                 * Open makani website
                 */
                scope.openWebsite = function () {
                    window.open($DMLinks.makani, "_blank", 'location=yes');
                };


                /**
                 * Get location by makani number
                 * @param makaniNumber
                 */
                scope.makaniLookup = function (makaniNumber) {
                    try {
                        LoadingService.show();
                        var invocationData = {
                            adapter: "MakaniDM24Adapter",
                            procedure: "checkMakani",
                            parameters: [makaniNumber]
                        };
                        WL.Client.invokeProcedure(invocationData, {
                            onSuccess: function (data) {
                                console.log('==> checkMakani ', data);
                                LoadingService.hide();
                                if (data.invocationResult.result.x != null && data.invocationResult.result.x.length > 0
                                    && data.invocationResult.result.y != null && data.invocationResult.result.y.length > 0) {
                                    var location = {
                                        lat: data.invocationResult.result.x,
                                        lng: data.invocationResult.result.y
                                    };

                                    scope.searchRes.toggleShow = false;
                                    scope.search = $('#pac-input').val();

                                    scope.map.setCenter(new google.maps.LatLng(location.lat, location.lng));

//                                    // Reload Maps
//                                    try {
//                                        scope.locationName = '';
//
//                                        bounds = new google.maps.LatLngBounds();
//                                        boundsSet = false;
//                                        autocomplete = null;
//                                        $rootScope.loadLocationMap(location.lat, location.lng);
//                                        $rootScope.$apply();
//                                    } catch (ex) {
//                                        console.log('===> EX ', ex)
//                                    }
                                    //scope.map.setZoom(17);

                                } else {
                                    scope.my_location = null;
                                    $notification.alert($filter('translate')("Invalid MAKANI Number"));
                                }
                            },
                            onFailure: function (err) {
                                scope.my_location = null;
                                LoadingService.hide();
                                $logger.error("KO", err);
                                $notification.alert($filter('translate')("Invalid MAKANI Number"));
                            }
                        });
                    } catch (e) {
                        console.log('===> ERROR ', e);
                    }
                };

                // Show location map
                scope.showLocationPopup = function (location) {
                    setTimeout(function () {
                        scope.locationName = location.locationName;
                        scope.location = location;
                        $('#addLocationPopup').css('display', 'block');
                        scope.$apply();
                    });
                };

                function nearestMakani(lat, lng) {
                    try {
                        $rootScope.makaniInfo = '';
                        $rootScope.makani = '';
                        var deferred = $q.defer();
                        Makani.getNearestMakani(lat, lng).then(
                            function (data) {
                                console.log('===> getNearestMakani :: lat, lng ::  ', lat, lng);
                                console.log('===> getNearestMakani ::  ', data);
                                deferred.resolve(data);
                            }, function (error) {
                                $logger.log('ERROR GetNearestMakani : ', error)
                            }
                        );
                        return deferred.promise;
                    } catch (e) {
                        console.log('===> ERROR ', e);
                    }
                }

                function switchToStepTwo() {
                    LoadingService.hide();
                    $rootScope.step = '2';
                    $rootScope.back = false;
                    $rootScope.location = scope.location;
                }

                /**
                 * Create request step two
                 */
                scope.createRequestStepTwo = function () {
                    try {
                        $rootScope.makaniInfo = '';
                        $rootScope.makani = '';
                        if (attrs.state === 'createRequest') {
                            LoadingService.show();
                            scope.closePopup();
                            if (!scope.isMakaniSearch) {
                                $rootScope.makani = '';
                                Makani.getNearestMakani(scope.location.lat, scope.location.lng).then(
                                    function (result) {
                                        if (result.hasOwnProperty('DATA')) {
                                            switchToStepTwo();
                                            return;
                                        } else {
                                            var makaniInfo = result.makaniInfo[0];
                                            $rootScope.makani = makaniInfo.MAKANI;
                                            $rootScope.makaniInfo = makaniInfo.MAKANI_INFO[0];
                                            switchToStepTwo();
                                        }
                                    });
                            } else {
                                Makani.getMakaniDetails(scope.search).then(
                                    function (result) {
                                        if (result.hasOwnProperty('DATA')) {
                                            switchToStepTwo();
                                            return;
                                        } else {
                                            $rootScope.makani = result.MAKANI;
                                            $rootScope.makaniInfo = result.MAKANI_INFO[0];
                                            switchToStepTwo();
                                        }
                                    });
                            }
                        } else {
                            switchToStepTwo();
                            scope.closePopup();
                        }
                    } catch (e) {
                        switchToStepTwo();
                    }
                };



                /**
                 * Google maps search box
                 */
                var searchPositionHandler = _.throttle(function () {

                    // TODO hide keyboard
                    if (navigator.userAgent.match(/(iPad|iPhone|iPod)/g)) {
                        setTimeout(function () {
                            var container = document.getElementsByClassName('pac-container')[0];
                            console.log('==> container ', container);
                            container.addEventListener('touchend', function (e) {
                                e.stopImmediatePropagation();
                            });
                        }, 500);
                    }
                    var regexMakaniNumber = /\(?([0-9]{5})\)?([ .-]?)([0-9]{5})/;
                    if (regexMakaniNumber.test(scope.search)) {
                        // Check makani number is valid
                        scope.isMakaniSearch = true;
                        scope.makaniLookup(scope.search);

                    } else {
                        // Search from google map
                        scope.isMakaniSearch = false;
                        $rootScope.makani = '';
                        if (scope.alreadyFilled) {
                            document.getElementById('pac-input').focus();
                            $('#pac-input').val($('#pac-input').val() + ' ');
                        }
                        google.maps.event.addListener(autocomplete, 'place_changed', function () {


                            var place = autocomplete.getPlace();
                            if (!place.geometry) {
                                return;
                            }
                            var location = {
                                "lat": place.geometry.location.lat(),
                                "lng": place.geometry.location.lng()
                            };
                            if (scope.previousLat_ != location.lat && scope.previousLng_ != location.lng) {
                                scope.previousLat_ = location.lat;
                                scope.previousLng_ = location.lng;

                                AnalyticsService.trackRoute(Constants.HIT_TYPE.EVENT, '', {
                                    eventCategory: Constants.EVENTS.Map_Search,
                                    eventAction: Constants.EVENTS.Map_Search,
                                    eventLabel: place.formatted_address
                                });

                                scope.searchRes.toggleShow = false;
                                scope.search = $('#pac-input').val();
                                scope.formatted_address = place.formatted_address;
                                scope.makani = '';
                                Makani.getNearestMakani(location.lat, location.lng).then(
                                    function (result) {
                                        if (!result.hasOwnProperty('DATA')) {
                                            var makaniInfo = result.makaniInfo[0];
                                            scope.makani = makaniInfo.MAKANI;
                                            $rootScope.makaniInfo = makaniInfo.MAKANI_INFO[0];
                                            //switchToStepTwo();
                                        }
                                    });
                                var center = new google.maps.LatLng(location.lat, location.lng);
                                scope.map.setCenter({lat:location.lat, lng:location.lng});
                                //scope.map.setZoom(17);
                            }
                        });


                    }

                },3000);

                scope.searchPosition = _.debounce(function (alreadyFilled) {
                    scope.alreadyFilled = alreadyFilled;
                    searchPositionHandler();
                }, 3000)



                /**
                 * Get address name by LatLng
                 * @param latlng
                 */
                scope.geocodeLatLng = function (latlng) {
                    var deferred = $q.defer();
                    var latlngStr = latlng.split(',', 2);
                    var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
                    scope.geocoder.geocode({'location': latlng}, function (results, status) {
                        if (status === 'OK') {
                            deferred.resolve(results[1].formatted_address);
                        } else {
                            deferred.resolve('');
                        }
                    });
                    return deferred.promise;
                };

                $rootScope.loadLocationMap = function (lat, lng) {

                	var center = new google.maps.LatLng(lat, lng);
                    setTimeout(function () {
                        try {
                            if (typeof(google) !== "undefined") {
                                scope.geocoder = new google.maps.Geocoder;

                                scope.map = new google.maps.Map(document.getElementById('createRequestMap'), {
                                    center: center,
                                    zoom: 10,
                                    minZoom: 9,
                                    zoomControl: false,
                                    fullscreenControl: false,
                                    streetViewControl: false,
                                    mapTypeId: 'satellite',
                                    mapTypeControlOptions: {
                                        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR ,
                                        position: google.maps.ControlPosition.BOTTOM_CENTER
                                    }
                                });

                                //scope.addMarkersToMap();

                                google.maps.event.addListener(scope.map, 'bounds_changed', function () {
                                    if (!boundsSet) {
                                        bounds = scope.map.getBounds();
                                        boundsSet = true;
                                    }
                                });

                                scope.geocodeLatLng(lat+','+lng).then(function (newAddress) {
                                    scope.locationName = newAddress;
                                    scope.search = (newAddress) ? newAddress : scope.search;
                                    $('#pac-input').val(scope.search) ;
                                    scope.location = {
                                        lat: scope.map.getCenter().lat(),
                                        lng: scope.map.getCenter().lng(),
                                        address: newAddress,
                                        locationName: newAddress,
                                        location_id: lat+ "-" + lng
                                    };
                                });

                                // Add Center marker
                                google.maps.event.addListener(scope.map, 'center_changed', _.throttle(function () {
                                	if($(".circleMarker").hasClass('isCircleMarker')){
                                        $(".circleMarker").removeClass('isCircleMarker')
                                	}
                                    /*document.getElementById('default_latitude').value = map.getCenter().lat();
                                    document.getElementById('default_longitude').value = map.getCenter().lng();*/
                                    console.log('Current lat : ', scope.map.getCenter().lat());
                                    console.log('Current long : ', scope.map.getCenter().lng());
                                    var bottom = $('.location-Service');
                                    var sLocHeight = bottom.height();
                                    var marker = $('.getPosition')
                                    var actualBottom = bottom.offset().top - bottom.outerHeight(true);

                                    if (actualBottom < sLocHeight) {
                                        bottom.css('bottom', -(actualBottom + 500));
                                        marker.css('bottom', 70)
                                    }
                                    scope.geocodeLatLng(scope.map.getCenter().lat() + ',' + scope.map.getCenter().lng()).then(function (newAddress) {
                                        scope.locationName = newAddress;
                                        scope.search = (newAddress) ? newAddress : scope.search;
                                        $('#pac-input').val(scope.search);
                                        scope.location = {
                                            lat: scope.map.getCenter().lat(),
                                            lng: scope.map.getCenter().lng(),
                                            address: newAddress,
                                            locationName: newAddress,
                                            location_id: scope.map.getCenter().lat() + "-" + scope.map.getCenter().lng()
                                        };
                                    });


                                }, 1000));





                                // add marker to map
                                $('.marker').appendTo(scope.map.getDiv())
                                //do something onclick
                                    .click(function() {
                                        scope.makani ='';
                                        Makani.getNearestMakani(scope.map.getCenter().lat(), scope.map.getCenter().lng()).then(
                                            function (result) {
                                                if (!result.hasOwnProperty('DATA')) {
                                                    var makaniInfo = result.makaniInfo[0];
                                                    scope.makani = makaniInfo.MAKANI;
                                                    $rootScope.makaniInfo = makaniInfo.MAKANI_INFO[0];
                                                }
                                            });
                                    });


                                //addSearchBox();
                                var autoElm = document.getElementById('pac-input');
                                autocomplete = new google.maps.places.Autocomplete(autoElm);
                                google.maps.event.addListener(scope.map, 'click', function (event) {
                                    scope.search = '';
                                    $rootScope.makani = '';
                                    scope.isMakaniSearch = false;
                                    var position = event.latLng;
                                    if (bounds.contains(position)) {
                                        scope.toggle = 1;
                                        //scope.popupRequest();
                                        $('#pac-input').val('');
                                        scope.showBtn = '1';
                                        //scope.placeMarker(event.latLng.lat(), event.latLng.lng());
                                    } else {
                                        scope.map.panTo(center);
                                    }
                                });
                            }
                        } catch (ex) {
                            console.log('===> EX ', ex)
                        }
                    }, 1000);
                };

                // Init Maps
                try {
                    /*if(scope.serviceInfo && scope.serviceInfo.location)
                    {
                        $rootScope.loadLocationMap(scope.serviceInfo.location.lat, scope.serviceInfo.location.lng);
                    }else {
                        $rootScope.loadLocationMap(25.063890, 55.193373);

                    }*/
                    $rootScope.loadLocationMap(25.063890, 55.193373);
                } catch (ex) {
                    console.log('===> EX ', ex)
                }


                /**
                 * Get current position
                 */
                var getCurrentPosition = false;
                scope.getCurrentPosition = function () {
                    try {
                        var optionsCurrentPosition = {enableHighAccuracy: true, maximumAge: 0, timeout: 10000};
                        var onSuccess = function (position) {
                            getCurrentPosition = true;
                            var pos = new google.maps.LatLng(+position.coords.latitude, +position.coords.longitude);
                            if (bounds.contains(pos)) {
                                var location = {};
                                var lat = position.coords.latitude;
                                var lng = position.coords.longitude;
                                var isExist = false;
                                angular.forEach(scope.locations, function (item) {
                                    if (item.location_id === lat + "-" + lng) {
                                        item.current = true;
                                        isExist = true;
                                    } else {
                                        item.current = false;
                                    }
                                });
                                if (!isExist) {
                                    var position = new google.maps.LatLng(lat, lng);
                                    var image = './assets/img3.0/pin.png';
                                    marker = new google.maps.Marker({
                                        position: position,
                                        map: scope.map,
                                        icon: image
                                    });
                                    var latlng = lat + ',' + lng;
                                    scope.geocodeLatLng(latlng).then(
                                        function (res) {
                                            location = {
                                                "location_id": lat + "-" + lng,
                                                "lat": lat,
                                                "lng": lng,
                                                "address": (res) ? res : "",
                                                "locationName": res,
                                                "current": true
                                            };
                                            $filter('filter')(scope.locations, function (l) {
                                                l.current = false;
                                            });
                                            google.maps.event.addListener(marker, 'click', (function (marker, location) {
                                                return function () {
                                                    scope.isMakaniSearch = false;
                                                    scope.showBtn = '1';
                                                    scope.showLocationPopup(location);
                                                };
                                            })(marker, location));
                                            scope.map.setZoom(15);
                                            scope.map.panTo(position);
                                        });
                                } else {
                                    $rootScope.loadLocationMap();
                                }
                            }
                        };
                        var onError = function (error) {
                            scope.getCurrentPosition();
                        };
                        navigator.geolocation.getCurrentPosition(onSuccess, onError, optionsCurrentPosition);
                    } catch (ex) {
                        console.log('===> EX ', ex)
                    }
                };

                /**
                 * Check if GPS is enabled
                 */
                scope.checkGPS = function () {
                    try {
                        if (typeof(diagnostic) !== "undefined" && typeof(cordova) !== "undefined") {
                            if ($rootScope.geoLocation) {
                                // In case the GPS option is ENABLED in the application settings
                                diagnostic.isLocationEnabled(function (enabled) {
                                    if (!enabled) {
                                        $filter('filter')(scope.locations, function (l) {
                                            l.current = false;
                                        });
                                        WL.SimpleDialog.show(
                                            $filter('translate')('Alert'), $filter('translate')("Your GPS seems to be disabled, do you want to enable it ?"), [
                                                {
                                                    text: $filter('translate')("Settings"),
                                                    handler: function () {
                                                        if (WL.Client.getEnvironment() === WL.Environment.IPHONE) {
                                                            diagnostic.switchToSettings();
                                                        }
                                                        else {
                                                            diagnostic.switchToLocationSettings();
                                                        }
                                                    }
                                                },
                                                {
                                                    text: $filter('translate')("Close"), handler: function () {
                                                        return false;
                                                    }
                                                }]
                                        );
                                    } else {
                                        setTimeout(function () {
                                            scope.getCurrentPosition();
                                        }, 1500);
                                    }
                                }, function (error) {
                                    console.log("The following error occurred: " + error);
                                });
                            } else {
                                // In case the GPS option is DISABLED in the application settings
                                WL.SimpleDialog.show(
                                    $filter('translate')('Alert'), $filter('translate')("Please enable location service from application settings"), [
                                        {
                                            text: $filter('translate')("App Settings"),
                                            handler: function () {
                                                $state.go('settings');
                                            }
                                        },
                                        {
                                            text: $filter('translate')("Close"), handler: function () {
                                                return false;
                                            }
                                        }]
                                );
                            }
                        }
                    } catch (ex) {
                        console.log('===> EX ', ex)
                    }
                };
                //scope.checkGPS();

                /**
                 * Interval to test GPS status and get current position
                 * @type {number}
                 */
                var interval = setInterval(function () {
                    try {
                        if (typeof(diagnostic) !== "undefined" && typeof(cordova) !== "undefined") {
                            diagnostic.isLocationEnabled(function (enabled) {
                                if (enabled && !getCurrentPosition) {
                                    scope.getCurrentPosition();
                                } else if (enabled && getCurrentPosition) {
                                    clearInterval(interval);
                                }
                            }, function (error) {
                                console.log("The following error occurred: " + error);
                            });
                        }
                    } catch (ex) {
                        console.log('===> EX ', ex)
                    }
                }, 3000);


            }
        };
    }]);