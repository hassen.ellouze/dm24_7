package com.dm.common.attachment;

import it.sauronsoftware.jave.AudioAttributes;
import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.EncoderException;
import it.sauronsoftware.jave.EncodingAttributes;
import it.sauronsoftware.jave.InputFormatException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;


import org.apache.commons.codec.binary.Base64;
import com.dm.common.config.GlobalConfig;
import org.json.JSONArray;
import org.json.JSONObject;

public class UploadAttachments {
    private static Logger logger = Logger.getLogger(UploadAttachments.class
            .getName());


    public static String uploadAttachments(String attachmentsObjects) {

        System.out.println("JSON Object ==> imageList && audioList"+attachmentsObjects);


        JSONObject attachmentsObject = new JSONObject(attachmentsObjects);

        System.out.println("JSON Object ==> imageList && audioList" /*+attachmentsObject.getString("nom")*/);


        System.out.println("*********** uploadAttachments ******************");
        JSONArray imageList = (JSONArray) attachmentsObject.get("imageList");
        JSONArray audioList = (JSONArray) attachmentsObject.get("audioList");


        JSONArray attachmentsFilesPath = new JSONArray();
        JSONObject fileInfo;
        String responseString = "";
        try {
            //image handler
            for (int i = 0; i < imageList.length(); i++) {
                JSONObject attachment = (JSONObject) imageList.get(i);
                String fileContentBase64 = (String) attachment
                        .get("fileContent");
                Path tempFile = Files.createTempFile("dubai247", ".jpg");
                Files.write(tempFile, Base64.decodeBase64(fileContentBase64));
                logger.info("Temporary file " + i + " created successfully in path " + tempFile.toString() + "");
                fileInfo = new JSONObject();
                fileInfo.put("fileName", tempFile.getFileName().toString());
                fileInfo.put("filePath", tempFile.toString());
                logger.info("********fileInfo**********: " + fileInfo);
                attachmentsFilesPath.put(fileInfo);
            }

            //audio handler
            File temp = null;
            File tempMp3 = null;
            FileOutputStream os = null;
            for (int i = 0; i < audioList.length(); i++) {
                JSONObject attachment = (JSONObject) audioList.get(i);
                String fileContentBase64 = (String) attachment
                        .get("fileContent");

                byte[] decoded = Base64.decodeBase64(fileContentBase64);
                if (((String) attachment.get("fileExtension")).equals("amr")) {
                    temp = File.createTempFile("dubai247", ".amr");
                } else {
                    temp = File.createTempFile("dubai247", ".wav");
                }

                tempMp3 = File.createTempFile("dubai247", ".mp3");
                System.out.println("temp audio path: " + temp.getPath());
                os = new FileOutputStream(temp, true);
                os.write(decoded);
                os.close();

                //convert wav or amr to mp3
                AudioAttributes audio = new AudioAttributes();
                audio.setCodec("libmp3lame");
                audio.setBitRate(new Integer(128000));
                audio.setChannels(new Integer(2));
                audio.setSamplingRate(new Integer(44100));
                EncodingAttributes attrs = new EncodingAttributes();
                attrs.setFormat("mp3");
                attrs.setAudioAttributes(audio);
                Encoder encoder = new Encoder();

                try {
                    encoder.encode(temp, tempMp3, attrs);
                } catch (IllegalArgumentException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (InputFormatException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (EncoderException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } finally {
                    logger.info("Temporary file " + i + " created successfully in path " + tempMp3.getPath() + "");
                    fileInfo = new JSONObject();
                    fileInfo.put("fileName", tempMp3.getName().toString());
                    fileInfo.put("filePath", tempMp3.getPath());
                    logger.info("********fileInfo**********: " + fileInfo);
                    attachmentsFilesPath.put(fileInfo);
                    temp.delete();
                }


            }

            logger.info("++++++++++++++AttachmentsFilesPathObject++++++++++++++++:"
                    + attachmentsFilesPath);
            try {
                responseString = sendAttachments(attachmentsFilesPath);

            } catch (Exception e) {
                logger.info("Error when sending Attachments");
                e.printStackTrace();
            }
        } catch (IOException e) {
            logger.info("Error when save image in temporary dir");
            e.printStackTrace();
        }
        return responseString;
    }

    private static String sendAttachments(JSONArray AttachmentsObj)
            throws Exception {

        String responseString = "";


        // TODO: PROD [Upload attachement HOST]
        //String BASE_URL = "https://dubai247mobileapi.dm.gov.ae/DM360.dm.gov.ae/NotificationsSystem/api/Attachments/Add";

        //TODO: STAGING [Upload attachement HOST]
//        String BASE_URL = "https://dubai247mobileapi.dm.gov.ae/STGDM360.dm.gov.ae/NotificationsSystem/api/Attachments/Add";
        //STAGING
        String BASE_URL =  GlobalConfig.baseURLAttachment+ "STGDM360.dm.gov.ae/NotificationsSystem/api/Attachments/Add"; // "https://dubai247mobileapi.dm.gov.ae/STGDM360.dm.gov.ae/NotificationsSystem/api/Attachments/Add";

        CredentialsProvider provider = new BasicCredentialsProvider();
        // UsernamePasswordCredentials credentials = new UsernamePasswordCredentials("dm360_sys", "dUbaI_MunP@ss");
        UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(GlobalConfig.usernameAttachment, GlobalConfig.passwordAttachment);
        provider.setCredentials(AuthScope.ANY, credentials);
        HttpClient client = HttpClientBuilder.create()
                .setDefaultCredentialsProvider(provider).build();
        HttpPost postRequest = new HttpPost(BASE_URL);
        try {

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

            //adding an image part
            for (int i = 0; i < AttachmentsObj.length(); i++) {
                JSONObject attachment = (JSONObject) AttachmentsObj.get(i);
                FileBody fileBody = new FileBody(new File(
                        (String) attachment.get("filePath")));
                builder.addPart((String) attachment.get("fileName"), fileBody);
            }

            // Set to request body
            postRequest.setEntity(builder.build());

            // Send request
            HttpResponse response = client.execute(postRequest);

            // Verify response if any
            if (response != null) {
                logger.info("response status code of upload attachment Api: "
                        + response.getStatusLine().getStatusCode());
                responseString = EntityUtils.toString(response.getEntity(),
                        "UTF-8");
                logger.info("response of upload attachment Api: "
                        + responseString);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return responseString;
    }
}