
/* JavaScript content from js/components/navBar.js in folder common */
iDubaiApp.directive('navBar',['$state','$rootScope'],function($state, $rootScope){
    return {
        restrict: 'E',
        templateUrl: 'widgets/menu.html',
        scope: {
            title: "@",
            showBack: "=",
            hideNav: "@",
            loginToken: "="
        },
        link: function (scope, element, attrs) {
            scope.showMenu = function() {
                var body = angular.element(document.querySelector(".toShowMenu"));
                if (!body.hasClass('sidebar-open')) {
                    body.addClass('sidebar-open');

                } else {
                    body.removeClass('sidebar-open');

                }
            }
        }
    }
});