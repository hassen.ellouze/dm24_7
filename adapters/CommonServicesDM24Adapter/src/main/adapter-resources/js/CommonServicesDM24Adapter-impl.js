
function getCommonServices() {
    var commonServices = {
        'categNameEn' : 'Common services & Incidents',
        'categNameAr' : 'الخدمات و البلاغات الأكثر طلبا',
        'services' : [
            {
                'area': 'Public Health and Safety',
                'nameEn': 'Sprinkling Pests house Request',
                'nameAr': 'طلب رش الحشرات المنزلية'
            },
            {
                'area': 'Projects and Infrastructure',
                'nameEn': 'Take out sewage Request',
                'nameAr': 'سحب مياه الصرف الصحي'
            },
            {
                'area': 'Environment',
                'nameEn': 'Bulky Waste Removal-household',
                'nameAr': 'إزالة النفايات الحجمية/منازل'
            },
            {
                'area': 'Environment',
                'nameEn': 'Removal of general waste',
                'nameAr': 'طلب إزالة النفايات العامة'
            },
            {
                'area': 'Coasts and the Green Area',
                'nameEn': 'Control agricultural leison',
                'nameAr': 'طلب رش الآفات الزراعية'
            },
            {
                'area': 'Projects and Infrastructure',
                'nameEn': 'Obstruction of Sewage network',
                'nameAr': 'إنسداد شبكة الصرف الصحي'
            },
            {
                'area': 'Projects and Infrastructure',
                'nameEn': 'Water Leakage',
                'nameAr': 'سرب مياه'
            },
        ]
    };

    return commonServices;
}
