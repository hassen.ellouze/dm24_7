
/* JavaScript content from js/configs/translation.config.js in folder common */
iDubaiApp.config(['$translateProvider', '$sceDelegateProvider', '$compileProvider', '$logProvider',
        function ($translateProvider, $sceDelegateProvider, $compileProvider, $logProvider) {

            $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|geo|tel|local|ghttps?|ms-appx|x-wmapp0):/);
            if (WL.Client.getEnvironment() == WL.Environment.BLACKBERRY10) {
                $compileProvider.imgSrcSanitizationWhitelist(/^\s*((https?|ftp|file|blob|local):|data:image\/)/);
            } else {
                $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|file|ms-appx|x-wmapp0):|data:image\//);
            }

            $logProvider.debugEnabled(true);

            var userLang = localStorage.getItem('dm-lang');
            if (!userLang) {
                localStorage.setItem('dm-lang', '');
                userLang = '';
            }
            $translateProvider
                .useStaticFilesLoader({
                    prefix: 'i18n/',
                    suffix: '.json'
                })
                .registerAvailableLanguageKeys(['en', 'ar'], {
                    'en-*': 'en',
                    'ar-*': 'ar'
                });
            $translateProvider.use(userLang);

            $sceDelegateProvider.resourceUrlWhitelist([
                // Allow same origin resource loads.
                'self',
                // Allow loading from our assets domain. Notice the difference between *
                // and **.
                'http://*.dm.gov.ae/**'
            ]);
        }
    ]
);
