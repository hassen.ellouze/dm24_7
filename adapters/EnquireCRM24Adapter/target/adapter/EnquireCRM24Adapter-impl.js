var username = 'dm360_sys';
var password = 'dUbaI_MunP@ss';

//CRMQueryAPI
function enquireByServiceRequestNumber(srNumber){
	//STAGE
	// var url ="STGDM360.dm.gov.ae/NotificationsSystem/api/crmticket/GetStatus?RequestNumber=";
	//PROD
    var response = null;
    try {
        var url="DM360.dm.gov.ae/NotificationsSystem/api/crmticket/GetStatus?RequestNumber=";
        var encoded =  com.dm.common.utils.Base64.encodeString(username + ":" + password);
        var path = url+ srNumber;
        var input = {
            method : 'get',
            headers: {
                'Authorization': 'basic ' + encoded,
            },
            path : path
        };
        //MFP.Logger.error("enquireByServiceRequestNumber :: request : " + JSON.stringify(input));
        response = MFP.Server.invokeHttp(input);
        MfP.logger.log(JSON.stringify(response));
        //MFP.Logger.error("enquireByServiceRequestNumber :: request : " + JSON.stringify(response));
        return response;
    } catch(e) {
        return {
            isSuccessful: false,
            reference: e
        };
    }
}
