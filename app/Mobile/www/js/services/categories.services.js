
/* JavaScript content from js/services/categories.services.js in folder common */
iDubaiApp.factory('DMCategories',
    ['$q', '$procedures', '$timeout', '$logger', '$DbStorageService', '$config', 'LoadingService', 'CommonUtils', 'ConfigAdapters',
        function ($q, $procedures, $timeout, $logger, $DbStorageService, $config, LoadingService, CommonUtils, ConfigAdapters) {

            var getCategories = function (loader) {
                var defer = $q.defer();
                $DbStorageService.get('categories_json_store').then(function (responseData) {
                    //Data exist
                    var data = JSON.parse(responseData.value);
                    defer.resolve(data.categories);
                    var t = new Date().getTime();
                    var hourDiff = t - data.cacheTime;
                    var diffMins = (hourDiff / 60 / 1000);
                    // if (data.cacheTime === null || (diffMins > $config.cacheTimes.categoriesCacheTime)) {
                    var defer = $q.defer();
                    CommonUtils.invokeGetResources({
                        path: ConfigAdapters.ServicesDM24Adapter.getCategories,
                        params: [],
                        showLoading: true
                    }).then(function (result) {
                        console.log('result ', result);
                        LoadingService.hide();
                        if (result.invocationResult.hasOwnProperty("categories")) {
                            $DbStorageService.remove('categories_json_store').then(function (resp) {
                                var temp = {
                                    categories: result.invocationResult.array,
                                    cacheTime: t
                                };
                                $DbStorageService.add('categories_json_store', JSON.stringify(temp)).then(function (resp) {
                                    $logger.log("Categories data is saved");
                                }, function (err) {
                                    $logger.log("Failed to save categories data");
                                });
                            }, function (err) {
                                $logger.log("Failed to remove categories data");
                            });
                        }
                        defer.resolve([]);
                    }, function (error) {
                        console.log('error ', error);
                        LoadingService.hide();
                        defer.resolve(error);
                    });
/*                        $procedures.getCategories.send([], false).then(function (resposne) {
                            if (resposne.invocationResult.hasOwnProperty("categories")) {
                                $DbStorageService.remove('categories_json_store').then(function (resp) {
                                    var temp = {
                                        categories: resposne.invocationResult.array,
                                        cacheTime: t
                                    };
                                    $DbStorageService.add('categories_json_store', JSON.stringify(temp)).then(function (resp) {
                                        $logger.log("Categories data is saved");
                                    }, function (err) {
                                        $logger.log("Failed to save categories data");
                                    });
                                }, function (err) {
                                    $logger.log("Failed to remove categories data");
                                });
                            }
                        }, function (error) {
                            $logger.log("WS : Failed to get CATEGORIES data");
                        });*/
                    // }
                }, function (error) {
                    CommonUtils.invokeGetResources({
                        path: ConfigAdapters.ServicesDM24Adapter.getCategories,
                        params: [],
                        showLoading: true
                    }).then(function (result) {
                        console.log('result ', result);
                        LoadingService.hide();
                        if (result.invocationResult.hasOwnProperty("categories")) {
                            defer.resolve(result.invocationResult.array);
                            var t = new Date().getTime();
                            var temp = {
                                categories: result.invocationResult.array,
                                cacheTime: t
                            };
                            if (result.invocationResult.array) {
                                $DbStorageService.add('categories_json_store', JSON.stringify(temp)).then(function (resp) {
                                    $logger.log("Categories Data is saved");
                                }, function (err) {
                                    $logger.log("Failed to save categories data");
                                });
                            }
                        }
                        defer.resolve([]);
                    }, function (error) {
                        console.log('error ', error);
                        LoadingService.hide();
                        defer.resolve(error);
                    });

/*                    $procedures.getCategories.send([], loader).then(function (resposne) {
                        if (resposne.invocationResult.hasOwnProperty("categories")) {
                            defer.resolve(resposne.invocationResult.array);
                            var t = new Date().getTime();
                            var temp = {
                                categories: resposne.invocationResult.array,
                                cacheTime: t
                            };
                            if (resposne.invocationResult.array) {
                                $DbStorageService.add('categories_json_store', JSON.stringify(temp)).then(function (resp) {
                                    $logger.log("Categories Data is saved");
                                }, function (err) {
                                    $logger.log("Failed to save categories data");
                                });
                            }
                        }
                    }, function (error) {
                        $logger.log("WS : Failed to get CATEGORIES data");
                        defer.resolve([]);
                    });*/
                });
                return defer.promise;
            };


            var _getCommonServices = function () {
                var defer = $q.defer();
                $DbStorageService.get('common_services_json_store').then(function (responseData) {
                    //Data exist
                    var data = JSON.parse(responseData.value);
                    defer.resolve(data.commonServices);
                    var t = new Date().getTime();
                    var hourDiff = t - data.cacheTime;
                    var diffMins = (hourDiff / 60 / 1000);
                    console.log('==> cacheTime : ', data.cacheTime, ' categoriesCacheTime : ', $config.cacheTimes.categoriesCacheTime, ' hourDiff : ', hourDiff, ' diffMins : ', diffMins)
                    console.log('==> IFcacheTime : ', (data.cacheTime === null), ' IFdiffMins : ', (diffMins > $config.cacheTimes.categoriesCacheTime));
                    if (data.cacheTime === null || (diffMins > $config.cacheTimes.categoriesCacheTime)) {
                        $procedures.getCommonServices.send([], false).then(function (resposne) {
                            $DbStorageService.remove('common_services_json_store').then(function (resp) {
                                var temp = {
                                    commonServices: resposne.invocationResult,
                                    cacheTime: t
                                };
                                $DbStorageService.add('common_services_json_store', JSON.stringify(temp)).then(function (resp) {
                                    $logger.log("Categories data is saved");
                                }, function (err) {
                                    $logger.log("Failed to save categories data");
                                });
                            }, function (err) {
                                $logger.log("Failed to remove categories data");
                            });
                        }, function (error) {
                            $logger.log("WS : Failed to get CATEGORIES data");
                        });
                    }
                }, function (error) {
                    //Data does not exist
                    $procedures.getCommonServices.send([]).then(function (resposne) {
                        defer.resolve(resposne.invocationResult);
                        var t = new Date().getTime();
                        var temp = {
                            commonServices: resposne.invocationResult,
                            cacheTime: t
                        };
                        if (resposne.invocationResult) {
                            $DbStorageService.add('common_services_json_store', JSON.stringify(temp)).then(function (resp) {
                                $logger.log("Categories Data is saved");
                            }, function (err) {
                                $logger.log("Failed to save categories data");
                            });
                        }
                    }, function (error) {
                        $logger.log("WS : Failed to get CATEGORIES data");
                        defer.resolve([]);
                    });
                });
                return defer.promise;
            };

            
            var getCommonServices = function () {
                console.log('entering getCommonServices');
                var defer = $q.defer();
                CommonUtils.invokeGetResources({
                    path: ConfigAdapters.CommonServicesDM24Adapter.getCommonServices,
                    params: [],
                    showLoading: true
                }).then(function (result) {
                    console.log('result ', result);
                    LoadingService.hide();
                    if (result.responseJSON.hasOwnProperty("array")) {
                        defer.resolve(result.responseJSON.array);
                    }
                    defer.resolve([]);
                }, function (error) {
                    console.log('error ', error);
                    LoadingService.hide();
                    defer.resolve(error);
                });
              /*  var defer = $q.defer();
                $procedures.getCommonServices.send([], false).then(function (resposne) {
                    if (resposne.invocationResult.hasOwnProperty("array")) {
                        defer.resolve(resposne.invocationResult.array);
                    }
                    defer.resolve([]);
                }, function (error) {

                });*/
                return defer.promise;
            };

            var getRequests = function() {
                var defer = $q.defer();
                CommonUtils.invokeGetResources({
                    path: ConfigAdapters.ServicesDM24Adapter.getRequests,
                    params: [],
                    showLoading: true
                }).then(function (result) {
                    console.log('result ', result);
                    LoadingService.hide();
                    if (result.responseJSON.hasOwnProperty("array")) {
                        defer.resolve(result.responseJSON.array);
                    }
                    defer.resolve([]);
                }, function (error) {
                    console.log('error ', error);
                    LoadingService.hide();
                    defer.resolve(error);
                });

         /*       var defer = $q.defer();
                $procedures.getRequests.send([], false).then(function (resposne) {
                    if (resposne.invocationResult.hasOwnProperty("array")) {
                        defer.resolve(resposne.invocationResult.array);
                    }
                    defer.resolve([]);
                }, function (error) {

                });*/

                return defer.promise;

            }

            var getServices = function () {
                var defer = $q.defer();
                CommonUtils.invokeGetResources({
                    path: ConfigAdapters.ServicesDM24Adapter.getCategories,
                    params: [],
                    showLoading: true
                }).then(function (result) {
                    console.log('result ', result);
                    try {
                        LoadingService.hide();
                        if (result.responseJSON.hasOwnProperty("categories")) {
                            defer.resolve(result.responseJSON);
                        }
                        else{
                            defer.resolve([]);
                        }
                    }catch(e){
                        console.log(e);
                    }
                }, function (error) {
                    console.log('error ', error);
                    LoadingService.hide();
                    defer.resolve(error);
                });
                /*
                $procedures.getCategories.send([], false).then(function (resposne) {
                    if (resposne.invocationResult.hasOwnProperty("categories")) {
                        defer.resolve(resposne.invocationResult.categories);
                    }
                    defer.resolve([]);
                }, function (error) {

                });
                */
                return defer.promise;
            };

            var getIncidents = function () {
                var defer = $q.defer();
                CommonUtils.invokeGetResources({
                    path: ConfigAdapters.ServicesDM24Adapter.getIncidents,
                    params: [],
                    showLoading: true
                }).then(function (result) {
                    console.log('result ', result);
                    LoadingService.hide();
                    if (result.responseJSON.hasOwnProperty("array")) {
                        defer.resolve(result.responseJSON.array);
                    }
                    defer.resolve([]);
                }, function (error) {
                    console.log('error ', error);
                    LoadingService.hide();
                    defer.resolve(error);
                });

/*                var defer = $q.defer();
                $procedures.getIncidents.send([], false).then(function (resposne) {
                    if (resposne.invocationResult.hasOwnProperty("array")) {
                        defer.resolve(resposne.invocationResult.array);
                    }
                    defer.resolve([]);
                }, function (error) {

                });*/

                return defer.promise;
            };

            var getAreas = function () {
                var defer = $q.defer();
                CommonUtils.invokeGetResources({
                    path: ConfigAdapters.ServicesDM24Adapter.getAreas,
                    params: [],
                    showLoading: true
                }).then(function (result) {
                    console.log('result ', result);
                    LoadingService.hide();
                    if (result.responseJSON.hasOwnProperty("array")) {
                        defer.resolve(result.responseJSON.array);
                    }
                    defer.resolve([]);
                }, function (error) {
                    console.log('error ', error);
                    LoadingService.hide();
                    defer.resolve(error);
                });
/*                var defer = $q.defer();
                $procedures.getAreas.send([], false).then(function (resposne) {
                    if (resposne.invocationResult.hasOwnProperty("array")) {
                        defer.resolve(resposne.invocationResult.array);
                    }
                    defer.resolve([]);
                }, function (error) {

                });*/
                return defer.promise;
            };

            var getPets = function () {
                var defer = $q.defer();
                CommonUtils.invokeGetResources({
                    path: ConfigAdapters.ServicesDM24Adapter.petTypes,
                    params: [],
                    showLoading: true
                }).then(function (result) {
                    console.log('result ', result);
                    LoadingService.hide();
                    if (result.responseJSON.hasOwnProperty("array")) {
                        defer.resolve(result.responseJSON.array);
                    }
                    defer.resolve([]);
                }, function (error) {
                    console.log('error ', error);
                    LoadingService.hide();
                    defer.resolve(error);
                });
/*                var defer = $q.defer();
                $procedures.petTypes.send([], false).then(function (resposne) {
                    if (resposne.invocationResult.hasOwnProperty("array")) {
                        defer.resolve(resposne.invocationResult.array);
                    }
                    defer.resolve([]);
                }, function (error) {

                });*/
                return defer.promise;
            };


            var getInsects = function () {
                var defer = $q.defer();
                CommonUtils.invokeGetResources({
                    path: ConfigAdapters.ServicesDM24Adapter.insectTypes,
                    params: [],
                    showLoading: true
                }).then(function (result) {
                    console.log('result ', result);
                    LoadingService.hide();
                    if (result.responseJSON.hasOwnProperty("array")) {
                        defer.resolve(result.responseJSON.array);
                    }
                    defer.resolve([]);
                }, function (error) {
                    console.log('error ', error);
                    LoadingService.hide();
                    defer.resolve(error);
                });
/*                var defer = $q.defer();
                $procedures.insectTypes.send([], false).then(function (resposne) {
                    if (resposne.invocationResult.hasOwnProperty("array")) {
                        defer.resolve(resposne.invocationResult.array);
                    }
                    defer.resolve([]);
                }, function (error) {

                });*/
                return defer.promise;
            };

            /*var getAreas = function () {
                var deferred = $q.defer();
                $procedures.getAreas([], false).then(function (response) {
                    defer.resolve(response.invocationResult);
                }, function(error){
                    defer.reject(error);
                });
                    return defer.promise;
            };*/


            return {
                getCategories: getCategories,
                getCommonServices: getCommonServices,
                getServices: getServices,
                getIncidents: getIncidents,
                getAreas: getAreas,
                getRequests: getRequests,
                getInsects: getInsects,
                getPets: getPets
            };
        }
    ]
);
