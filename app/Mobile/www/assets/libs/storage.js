
/* JavaScript content from assets/libs/storage.js in folder common */

/* JavaScript content from js/libs/storage.js in folder common */
angular.module('ngStorage', [])
.factory('$DbStorageService',['$window', '$q', '$logger', '$config',
                              function($window, $q, $logger, $config){
		
		//indexedDB.deleteDatabase('iDubai_Corporates_Data');		
		var factory = {
			dbName     : $config.cache.dbName,
			dbDesc     : $config.cache.dbDesc,
			objectStore: $config.cache.objectStore,
			serviceID  : $config.cache.serviceID,
			version    : $config.cache.version,	
			bSize      : $config.cache.bSize, // big size
			mSize      : $config.cache.mSize, // medium size
			lSize      : $config.cache.lSize, // low size
			storageType : $config.cache.storageType,
			clearDbCashe  : $config.cache.clearDbCashe,
			expiredCasheDate: $config.cache.expiredCasheDate, // not implemented yet 
		}; 
		
		var dbI = null;
		var dbW = null;
		//var setUp = false;
		var openDatabase = window.openDatabase;
		var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
		
		var dbs = {
				INDEXED: 'INDEXED',
				WEBSQL: 'WEBSQL'
		}; 
		 		
		/*** IndexedDBStorage ***/
		function getIndexedDBStorage() { 
			return indexedDB != null ? dbs.INDEXED : null;
		};
		
		/*** WebSqlStorage ***/
		function getWebSqlStorage() { 
			return openDatabase != null ? dbs.WEBSQL : null;
		};
		
		/*** chek fo DB Type ***/
		function getDbStorageType(){  
			 return getWebSqlStorage() || getIndexedDBStorage() ;
		};		
		
		/*** init/open IndexedDB ***/
		function initIndexedDB() {
			
			var deferred = $q.defer();
			/*if(setUp) {
				deferred.resolve(true);
				return deferred.promise;
			}*/			
			var openRequest = indexedDB.open(factory.dbName,factory.version);
			openRequest.onerror = function(e) {
				//$logger.log("Error opening db");
				//$logger.dir(e);
				deferred.reject(e.toString());
			};
			openRequest.onupgradeneeded = function(e) {
				var thisDb = e.target.result;
				var objectStore;				
				//Create Note OS
				if(!thisDb.objectStoreNames.contains(factory.objectStore)) {
					objectStore = thisDb.createObjectStore(factory.objectStore, { keyPath: "key", unique: true });//autoIncrement:false
					objectStore.createIndex("value", "value", {keyPath: "value", unique: false,multiEntry:true });
					//objectStore.createIndex("tags","tags", {keyPath: "updated",unique:false,multiEntry:true});
				}
			};
			openRequest.onsuccess = function(e) {
				dbI = e.target.result;				
				dbI.onerror = function(event) {
					// Generic error handler for all errors targeted at this database's
					// requests!
					deferred.reject("Database error: " + event.target.errorCode);
				};
				//setUp=true;
				deferred.resolve(true);			
			};	
			return deferred.promise;
		} 
		
		/*** init/open WebSql ***/
		function initWebSqlDB(){
			var deferred = $q.defer();   
			
			if(dbW != null) {deferred.resolve();return deferred.promise;}
			try {
				dbW = openDatabase(factory.dbName, factory.version, factory.dbDesc, factory.bSize);
			} catch(e) { 
				try{
					dbW = openDatabase(factory.dbName, factory.version, factory.dbDesc, factory.mSize);
				}catch(e){ 
					dbW = openDatabase(factory.dbName, factory.version, factory.dbDesc, factory.lSize);
				}
			}
			dbW.transaction(function (tx) {
				tx.executeSql('CREATE TABLE IF NOT EXISTS '+factory.objectStore+' (key TEXT PRIMARY KEY, value TEXT)', [], function () {
					//emr.fire('storageLoaded', self);
				});
				deferred.resolve();
			});	
		
			return deferred.promise;
		};
		 
	/*** CRUD operations for IndexedDB ***/
	var IndexedDBfactory = { 
			
		   add: function(key, value) {
				
				var deferred = $q.defer(); 
				
				initIndexedDB().then(function() {	
					
					var transaction = dbI.transaction([factory.objectStore], "readwrite");  
					var objectStore = transaction.objectStore(factory.objectStore); 
					objectStore.put({key: (factory.serviceID + "_" + key), value: value});						
					transaction.oncomplete = function(event) {  deferred.resolve(true);  };						
					transaction.onerror = function(e){ deferred.reject(); };			
					
				});
				
				return deferred.promise; 
			},
			addAll: function(data) {
				 
				var length = data.length; 
				var deferred = $q.defer();	 
				 
				initIndexedDB().then(function() {	
					
					var transaction = dbI.transaction([factory.objectStore], 'readwrite'); 
					var objectStore = transaction.objectStore(factory.objectStore);
					for (var i = 0; i < length; i++) {
						objectStore.put({key: factory.serviceID + "_" +data[i].key, value: data[i].value}); 
					} 
					transaction.oncomplete = function(e){ deferred.resolve(true); };
					transaction.onerror = function(e){  deferred.reject(); };
				});
				
				return deferred.promise; 
			},
			remove: function(key) { 
				
				var deferred = $q.defer();	 
				
				initIndexedDB().then(function() {	
					
					var transaction = dbI.transaction([factory.objectStore], "readwrite");  
					var objectStore = transaction.objectStore(factory.objectStore);  
					var request = objectStore.delete((factory.serviceID + "_" + key)); 
					request.onsuccess = function(event) { deferred.resolve(true); }; 
					request.onerror = function(event) { deferred.reject()};  
					//transaction.oncomplete = function(event) {  deferred.resolve(true);  };						
					//transaction.onerror = function(event){ deferred.reject(); };			
					
				});
				
				return deferred.promise;  
			},
			clear: function() {
				  
				var deferred = $q.defer(); 
				indexedDB.deleteDatabase(factory.dbName);	
				deferred.resolve("true");
				return deferred.promise;
			 
				/*initIndexedDB().then(function() {	
					
					var transaction = dbI.transaction([factory.objectStore], "readwrite");  
					var objectStore = transaction.objectStore(factory.objectStore);  
					var keyRange = IDBKeyRange.lowerBound(0);
					var cursorRequest = objectStore.openCursor(keyRange);
					cursorRequest.onerror = function(evt){ deferred.reject(); };
					cursorRequest.onsuccess = function(evt) {   
						var cursor = evt.target.result;
						if (cursor) { 
							if(cursor.value.key.substr(0, factory.serviceID.length) == factory.serviceID){ 
								 var request = objectStore.delete(cursor.value.key); 
								  request.onsuccess = function(e) { deferred.resolve(true);  }; 
								  request.onerror = function(e) { deferred.reject(); };  
							}
							cursor.continue();
						} else {
							deferred.resolve(true);
						}
					}; 
					//transaction.oncomplete = function(event) {  deferred.resolve(true);  };						
					//transaction.onerror = function(event){ deferred.reject(); };			
					
				}); 
				
				return deferred.promise;*/
			},
			get: function(key) { 
				
				var deferred = $q.defer();  
				 
				initIndexedDB().then(function() {

					var result = undefined;

					var handleResult = function(event) {   
						if(typeof(event.target.result) != "undefined"){
							result = {key:event.target.result.key, value:event.target.result.value};
						} 
					}; 
					
					var transaction = dbI.transaction([factory.objectStore], "readonly");  
					var objectStore = transaction.objectStore(factory.objectStore);  
					var request = objectStore.get((factory.serviceID + "_" + key));
					request.onsuccess =  handleResult; 
					request.onerror = function(e) {deferred.reject();};   
		            
					transaction.oncomplete = function(event) { 
						if(typeof(result) != "undefined"){
							deferred.resolve(result);
						}else{
							deferred.reject();
						}  
					};
				
				}); 
				
				return deferred.promise; 
			},
			getAll: function() { 
				
				var deferred = $q.defer(); 
				
				initIndexedDB().then(function() { 
					
					var result = []; 
					var handleResult = function(event) {  
						var cursor = event.target.result;
						if (cursor) {
							result.push(cursor.value);
							cursor.continue();
						}
					};  
					
					var transaction = dbI.transaction([factory.objectStore], "readonly");  
					var objectStore = transaction.objectStore(factory.objectStore);
		            objectStore.openCursor().onsuccess = handleResult;

					transaction.oncomplete = function(event) {
						deferred.resolve(result);
					};
				
				});
				
				return deferred.promise; 
			} 
		};  
	
		/*** CRUD operations for WebSqlDB ***/
		var webSqlDbfactory = {  
				
			add: function(key, value) {	
				
				var deferred = $q.defer();  
				
				initWebSqlDB().then(function() { 
					 
					dbW.transaction(function(tx) {  
							tx.executeSql('INSERT OR REPLACE INTO '+factory.objectStore+' (key, value) VALUES (?, ?)', [(factory.serviceID + "_" + key), value], function(tx, results){
							deferred.resolve(true);
						}, function(tx, e){ 
							deferred.reject();
						}); 
					}); 
					  
				}); 
				 
				return deferred.promise;
			},
			addAll: function(data) {
				
				var deferred = $q.defer();   
				var length = data.length; 
				 
				initWebSqlDB().then(function() { 
					 
					dbW.transaction(function(tx) {
						for (var i = 0; i < length; i++) {							 
							tx.executeSql('INSERT OR REPLACE INTO '+factory.objectStore+' (key, value) VALUES (?, ?)', [factory.serviceID + "_" + data[i].key, data[i].value], function(tx, results){
								deferred.resolve(true); 
							}, function(tx, e){ 
								deferred.reject();
							});
						}
					});					  
				}); 
				
				return deferred.promise;
			},
			remove: function(key) {
				
				var deferred = $q.defer(); 
				
				initWebSqlDB().then(function() { 
					 
					dbW.transaction(function(tx) {
						tx.executeSql('DELETE FROM '+factory.objectStore+' WHERE key = ? ', [(factory.serviceID + "_" + key)], function(tx, results){ 
							 
							deferred.resolve(true);
						}, function(tx, e){
							$logger.log("There has been an error: " + e.message);
							deferred.reject(false);
						});
					}); 					  
				});  
				
				return deferred.promise;
			},
			clear: function() {
				 
				var deferred = $q.defer();
				
				initWebSqlDB().then(function() { 
					 
					dbW.transaction(function(tx) {
						tx.executeSql('DELETE FROM '+factory.objectStore+' WHERE key LIKE ? ', [factory.serviceID+"%"], function(tx, results){ 
							deferred.resolve(true);
						}, function(tx, e){ 
							deferred.reject();
						});
					}); 				  
				});  
				
				return deferred.promise;
			},
			get: function(key) { 
				
				var deferred = $q.defer(); 
				 
				initWebSqlDB().then(function() { 
					 
					dbW.transaction(function(tx) {
						tx.executeSql('SELECT * FROM '+factory.objectStore+' WHERE key = ?', [(factory.serviceID + "_" + key)], function(tx, results){  
							
							if(results.rows.length > 0){
								deferred.resolve(results.rows.item(0));
							}else{
								deferred.reject();
							}
							//deferred.resolve(results.rows.length > 0 ? results.rows.item(0) : undefined);
						}, function(tx, e){ 
							deferred.reject();
						});
					});	  
				});   	
					  
				return deferred.promise;
			},
			getAll: function() { 
				var deferred = $q.defer();  
				 
				initWebSqlDB().then(function() { 
					 
					dbW.transaction(function(tx) {
						tx.executeSql('SELECT * FROM '+factory.objectStore+' ', [], function(tx, results){ 
							
							var tempsList = [];
							for(var i = 0; i < results.rows.length; i++) {
								//$logger.log("result : ",results.rows.item(i));
								tempsList[i] = results.rows.item(i);
							}																								
							deferred.resolve(tempsList);
						}, function(tx, e){							 
							deferred.reject();
						});
					});
				});  
				
				return deferred.promise;
			}   
		}; 
 
		if(factory.storageType === dbs.INDEXED){
			//$logger.log("factory INDEXED : ",dbs.INDEXED);
			return  IndexedDBfactory; 
		}else if(factory.storageType === dbs.WEBSQL){
			//$logger.log("factory WEBSQL : ",dbs.WEBSQL);
			return webSqlDbfactory; 
		}else{ 

			//$logger.log("factory AUTO : ",getDbStorageType());
			return getDbStorageType() === dbs.INDEXED ? IndexedDBfactory : webSqlDbfactory;
		}	 
}]);

