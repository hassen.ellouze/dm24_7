/*
 *  Licensed Materials - Property of IBM
 *  5725-I43 (C) Copyright IBM Corp. 2011, 2016. All Rights Reserved.
 *  US Government Users Restricted Rights - Use, duplication or
 *  disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */

function requestOTP(phoneNumber, transactionID, message) {

    var response = null;

    try {
        //Prepare the SOAP Request
        var soapReq = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://www.dm.gov.ae/dmsmg/DmSmsBrokerService/v1" xmlns:v11="http://www.dm.gov.ae/dmsmg/DmSmsBroker/v1">'+
            '<soapenv:Header/>'+
            '<soapenv:Body>' +
            '      <v1:DmSmsBrokerRequest>' +
            '         <!--Optional:-->' +
            '         <v11:TransactionID>'+transactionID+'</v11:TransactionID>' +
            '         <v11:Message>'+message+'</v11:Message>' +
            '         <v11:Recipients>' +
            '            <!--Zero or more repetitions:-->' +
            '            <item>'+phoneNumber+'</item>' +
            '        </v11:Recipients>' +
            '      </v1:DmSmsBrokerRequest>' +
            '   </soapenv:Body>' +
            '</soapenv:Envelope>';
        var input = {
            method : 'POST',
            returnedContentType : 'xml',
            path : 'bexml10.dm.gov.ae/DMSmsGatewayBroker/services/DmSmsBrokerServiceP',
            headers : {'soapAction' : "http://www.dm.gov.ae/dmsmg/DmSmsBrokerService/v1/DmSmsBrokerRequest", Host:'bexml10.dm.gov.ae'},
            body : {
                content : soapReq.toString(),
                contentType : 'text/xml; charset=utf-8'
            }
        };
        // Invoke the SOAP Request
        response = MFP.Server.invokeHttp(input);
        WL.Logger.warn(JSON.stringify(response));
    } catch (e) {
        return {
            isSuccessful : false,
            reference : e
        };
    }
    return response;
}