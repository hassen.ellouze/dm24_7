
/* JavaScript content from js/services/OneTimePassword.service.js in folder common */
iDubaiApp.factory('OTP',
    ['$q', '$procedures', 'CommonUtils', 'ConfigAdapters',
        function ($q, $procedures, CommonUtils, ConfigAdapters) {
            return{
                sendCode : function (mobileNumber, transactionID, message) {
                    var defer = $q.defer();
                    CommonUtils.invokeGetResources({
                        path: ConfigAdapters.OneTimePasswordAdapter.requestOTP,
                        params: [mobileNumber, transactionID, message],
                        showLoading: true
                    }).then(function (response) {
                        console.log('result ', response);
                        try {
                            if( response && response.responseJSON && response.responseJSON.isSuccessful ){
                                var result = response.responseJSON.Envelope.Body.DmSmsBrokerPesponse;
                                defer.resolve(result);
                            }
                            else{
                                defer.reject({});
                            }
                        }catch (e) {
                            console.log(e);
                            defer.reject(e);
                        }
                    }, function (error) {
                        console.log('error ', error);
                        defer.reject(error);
                    });
                    /*
                    $procedures.requestOTP.send([mobileNumber, transactionID, message], true).then(
                        function (response) {
                            try {
                                if( response && response.responseJSON && response.responseJSON.isSuccessful ){
                                    var result = response.responseJSON.Envelope.Body.DmSmsBrokerPesponse;
                                    defer.resolve(result);
                                }
                                else{
                                    defer.reject({});
                                }
                            }catch (e) {
                                console.log(e);
                                defer.reject({})
                            }
                        }, function (error) {
                            defer.reject({})
                        });
                    */
                    return defer.promise;
                }
            }
        }
    ]
);

