
/* JavaScript content from js/services/makani.service.js in folder common */
iDubaiApp.factory('Makani',
    ['$q', '$procedures', 'CommonUtils', 'ConfigAdapters',
        function ($q, $procedures, CommonUtils, ConfigAdapters) {

            var getNearestMakani = function (lat, lng) {
                var defer = $q.defer();
                CommonUtils.invokeGetResources({
                    path: ConfigAdapters.MakaniDM24Adapter.getNearestMakani,
                    params: [lat, lng],
                    showLoading: true
                }).then(function (response) {
                    console.log('result ', response);
                    try {
                        var result = response.responseJSON.details;
                        defer.resolve(result);
                    }catch (e) {
                        console.log(e);
                        defer.reject(e);
                    }
                }, function (error) {
                    console.log('error ', error);
                    defer.reject(error);
                });
                /*
                $procedures.getNearestMakani.send([lat, lng], false).then(
                    function (response) {
                        try {
                            var result = response.responseJSON.details;
                            defer.resolve(JSON.parse(result));
                        }catch (e) {
                            console.log(e);
                            defer.reject({})
                        }
                    }, function (error) {
                        defer.reject(e);
                    });
                */
                return defer.promise;
            };

            var getMakaniDetails = function (makaniNumber) {
                var defer = $q.defer();
                $procedures.getMakaniNumberDetails.send([makaniNumber], false).then(
                    function (response) {
                        try {
                            var result = response.responseJSON.details;
                            defer.resolve(JSON.parse(result));
                        }catch (e) {
                            defer.reject(e);
                        }
                    }, function (error) {
                        defer.reject({})
                    });
                return defer.promise;
            };

            return {
                getMakaniDetails: getMakaniDetails,
                getNearestMakani: getNearestMakani
            };
        }
    ]
);

