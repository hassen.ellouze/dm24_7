
/* JavaScript content from js/services/favorites.service.js in folder common */
angular.module('iDubaiApp').factory("$favorites", ['$q', 'utils', '$log', '$rootScope',
    function ($q, utils, $log, $rootScope) {

        function getFavorites() {
            var currentLang = localStorage.getItem('dm-lang');
            var deferred = $q.defer();

            if (localStorage.getItem("favorites")) {
                deferred.resolve(localStorage.getItem("favorites"));
            } else {
                localStorage.setItem("favorites", "[]");
                deferred.resolve(localStorage.getItem("favorites"));
            }
            return deferred.promise;
        }

        function setFavorites(favorites) {
            var currentLang = localStorage.getItem('dm-lang');
            var deferred = $q.defer();
            localStorage.setItem("favorites", favorites);
            deferred.resolve();
            return deferred.promise;
        }

        function count() {
            var favs = [];
            var data = localStorage.getItem("favorites");
            if (data) {
                if (typeof(data) === "string") {
                    favs = JSON.parse(data);
                } else {
                    if (typeof(data[0]) !== 'undefined') {
                        favs = JSON.parse(data[0].FAVORITES);
                    }
                }
            }
            return favs.length;
        }

        return {
            getFavorites: getFavorites,
            setFavorites: setFavorites,
            count: count
        };
    }]);