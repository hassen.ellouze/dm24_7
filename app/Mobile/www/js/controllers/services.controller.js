iDubaiApp.controller('servicesController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'DMCategories', 'underscore', 'cat', '$dmServices', 'GuestUtils', '$auth', 'Makani', '$q', 'LoadingService', '$notification', '$filter', '$interval', 'AnalyticsService', 'Constants', 'SharedService', 'underscore', '$cordovaGeolocation',
        function ($scope, $rootScope, $stateParams, $state, DMCategories, underscore, cat, $dmServices, GuestUtils, $auth, Makani, $q, LoadingService, $notification, $filter, $interval, AnalyticsService, Constants, SharedService, underscore, $cordovaGeolocation) {
            var _ = underscore;
            $scope.goToSubmittingReq = false;
            $scope.nbrPage = 0;

            var sourceType = localStorage.getItem('sourceType');
            var favourites = localStorage.getItem('Favourites');

            if (sourceType) {
                $scope.sourceType = sourceType;
                $scope.metaData = localStorage.getItem('metaData');
            }
            var _ = underscore;
            var PLATFORM = WL.Client.getEnvironment();
            var MSGID = WL.Client.getMessageID();
            $scope.nbrPage++;
            console.log($scope.nbrPage)
            $scope.modal = {
                openNoise: false
            };
            $scope.currentLang = localStorage.getItem('dm-lang');
            $scope.isNoiseService = false;
            $scope.serachByImage = false;
            $scope.active = false;
            if (localStorage.getItem('uploadImage')) {
                $scope.serachByImage = true;
            }
            var myparams = SharedService.getStateHistory($state.current.name);
            $stateParams =_.clone(myparams);

            if ($state.current.name == 'serviceDetails') {
                if ($stateParams.from == 'services') {
                    AnalyticsService.trackRoute(Constants.HIT_TYPE.EVENT, '', {
                        eventCategory: Constants.EVENTS.Dashboard_Services,
                        eventAction: Constants.EVENTS.Dashboard_Services,
                        eventLabel: $stateParams.service.service_title_en
                    });
                }

                if ($stateParams.category) {
                    $scope.category = $stateParams.category;
                } else {
                    /*$scope.category = _.find(cat, function(item, index){
                     return item.category_id == $stateParams.category;
                     });*/
                    $scope.category = _.find(cat, function (item, index) {
                        return item.category_id == 99;
                    });
                }
                $scope.service = $stateParams.service;


                var SLA = ( (parseInt($scope.service.sla) % 7) > 0) ? (parseInt($scope.service.sla) / 7) + 1 : (parseInt($scope.service.sla) / 7);
                $scope.service.CalculatedSLA = Math.round(SLA);
                ///////////////
                if ($stateParams.service.incident_id == 87) {
                    $scope.isNoiseService = true;
                }
                ///////////////
                if (favourites) {
                    favourites = JSON.parse(favourites);
                    var type = ($scope.service.service_request_id) ? 'service' : 'incident';
                    var id = ($scope.service.service_request_id) ? $scope.service.service_request_id : $scope.service.incident_id;
                    favourites.map(function (item, key) {
                        if (item.id == id && item.type == type) {
                            $scope.active = true;
                            $('.click').addClass('active')
                            $('.click').addClass('active-2')
                            setTimeout(function () {
                                $('#span-star').addClass('fa-star')
                                $('#span-star').removeClass('fa-star-o')
                            }, 150)
                            setTimeout(function () {
                                $('.click').addClass('active-3')
                            }, 150)
                            $('.info').addClass('info-tog')
                            setTimeout(function () {
                                $('.info').removeClass('info-tog')
                            }, 1000)
                        }
                    })
                }
            } else {

                var categoryId = $stateParams.payload.category_id;


                /*if ( GuestUtils.getGuestPhoneNumber()) {
                 var guestInfo = GuestUtils.getGuestInfo();
                 if (guestInfo && guestInfo.option3 == true) {
                 $scope.listService = _.filter($dmServices, function(item, index) {
                 return item.category_id == $stateParams.category_id &&
                 item.isVisitor == true;
                 });
                 }else {
                 $scope.listService = _.filter($dmServices, function(item, index) {
                 return item.category_id == $stateParams.category_id;
                 });
                 }

                 } else {
                 $scope.listService = _.filter($dmServices, function(item, index) {
                 return item.category_id == $stateParams.category_id;
                 });
                 }*/

                $scope.listService = _.filter($dmServices, function (item, index) {
                    return item.category_id == $stateParams.payload.category_id;
                });


                $scope.cat = _.find(cat, function (item, index) {
                    return item.category_id == $stateParams.payload.category_id;
                });
            }

            $scope.goBack = function () {
                /*     if ($stateParams.from == 'services') {
                 var prev=SharedService.getStateHistory($state.current.name).from;
                 $state.go('services', {payload: {category_id: $scope.category.category_id}, from: 'serviceDetails'})
                 } else if ($stateParams.from) {
                 $state.go($stateParams.from);
                 } else {
                 $state.go('dashboard');
                 }*/
                var prev = SharedService.getStateHistory($state.current.name).from;
                $state.go(prev);
            };

            $scope.goTo = function (service, cat) {
                //go to send request
                $scope.goToSubmittingReq = true;
                if ($stateParams.payload) {
                    LoadingService.show();
                    prepareRequest($stateParams.payload.location.lat, $stateParams.payload.location.lng);
                } else {
                    $scope.checkGPS();
                }
            };

            $scope.nextPage = function (service, cat) {
                SharedService.setStateHistory('serviceDetails', {
                    service: service,
                    category: cat,
                    from: 'services'
                });

                $state.go('serviceDetails');
            }
            // Load categories
            function loadCategories() {
                DMCategories.getCategories(true).then(function (data) {
                    $scope.categories = data;
                    $scope.categories.splice(0, 1);
                    $scope.viewLoaded = true;
                });
            }

            //loadCategories();

            /**
             * Name of corresponding language
             * @param category
             * @returns {name}
             */
            /*$scope.getName = function (category) {
             return (this.currentLang === 'ar') ? category.service_title_ar : category.service_title_en;
             };*/


            /**
             * Redirect to service details
             * @param service
             */
            $scope.goToServiceDetails = function (service, category) {
                var category = {
                    categoryId: category.categoryId,
                    nameAr: category.service_title_ar,
                    nameEn: category.service_title_en,
                    nameFr: category.nameFr,
                    cssClass: category.cssClass
                };
                SharedService.setStateHistory('map', {service: service, category: category});
                $state.go('map', {service: service, category: category});
            };

            /*
             *   Start Detecting Noise
             * */
            $scope.startDetectingNoise = function () {
                /*if (typeof(diagnostic) !== "undefined" && typeof(cordova) !== "undefined") {
                 diagnostic.isMicrophoneAuthorized( function(authorized){
                 console.log("App is " + (authorized ? "authorized" : "denied") + " access to the microphone");
                 if (!authorized) {

                 diagnostic.requestMicrophoneAuthorization({
                 successCallback: function(status){
                 if(status === cordova.plugins.diagnostic.permissionStatus.GRANTED){
                 console.log("Microphone use is authorized");
                 }
                 },
                 errorCallback: function(error){
                 console.error(error);
                 }})

                 } else {
                 $scope.LoudNoise = false;

                 document.getElementById('noise').innerText =  0;
                 navigator.DBMeter.start(function(dB){
                 console.log(dB);

                 document.getElementById('noise').innerText =  dB;
                 if(dB >= 86){
                 $scope.LoudNoise = true;

                 navigator.DBMeter.stop(function(){
                 console.log("DBMeter well stopped");
                 }, function(e){
                 console.log('code: ' + e.code + ', message: ' + e.message);
                 });
                 $scope.$apply();
                 }
                 }, function(e){
                 console.log('code: ' + e.code + ', message: ' + e.message);
                 });
                 }
                 },function(error){
                 console.error("The following error occurred: "+error);
                 }
                 );
                 }*/
                try {
                    var format = 'hh:mm';

                    var actualDay = moment().format('dddd');
                    var actualTime = moment(moment(), format);

                    var time1 = moment('06:00', format);
                    var time2 = moment('20:00', format);

                    var time1Weekend = moment('07:00', format);

                    var notInDay = false;
                    switch (actualDay) {
                        case 'Sunday':
                        case 'Monday':
                        case 'Tuesday':
                        case 'Wednesday':
                        case 'Thursday': {
                            if (actualTime.isBetween(time1, time2)) {
                                notInDay = true;
                            }
                            break;
                        }

                        case 'Friday':
                        case 'Saturday': {
                            if (actualTime.isBetween(time1Weekend, time2)) {
                                notInDay = true;
                            }
                            break;
                        }

                    }

                    if (notInDay) {
                        WL.SimpleDialog.show(
                            $filter('translate')('Alert'), $filter('translate')("MessageNoiseDay"), [
                                {
                                    text: $filter('translate')("Close"), handler: function () {
                                    window.history.back();
                                }
                                }]
                        );
                        $scope.nbrPage++;
                        console.log($scope.nbrPage);
                    } else {

                        $scope.modal.openNoise = !$scope.modal.openNoise;
                        $scope.LoudNoise = false;
                        var buffer = [];
                        var bufferPick = [];
                        var i = 0;
                        var j = 0;
                        $scope.isStopped = false;
                        $scope.timerPromise = null;
                        document.getElementById('noise').innerText = 0;

                        var NOISE_THRESHOLD = 75;
                        var PICK_DURATION = 200;
                        var MAX_NOISE_DURATION = 5000;
                        var PICK_NUMBER = 3;
                        var startNoise = null;
                        var endNoise = null;
                        var count = 16;

                        var MIN_DB = 30;
                        var MAX_DB = NOISE_THRESHOLD + 20;
                        var LEVELS = 12;
                        var STEP = (MAX_DB - MIN_DB) / LEVELS;
                        var peak = (NOISE_THRESHOLD / STEP) - (MIN_DB / STEP);

                        $scope.timerPromise = $interval(function () {
                            count--;
                            navigator.DBMeter.isListening(function (isListening) {
                                if (!isListening && !$scope.isStopped) {
                                    navigator.DBMeter.start(function (dB) {
                                        dB = (PLATFORM == 'android') ? parseInt(dB) - 10 : parseInt(dB);
                                        var soundLevel = Math.round((Math.round(parseInt(dB)) / STEP) - (MIN_DB / STEP));

                                        for (var c = 1; c <= LEVELS; c++) {
                                            if (c < peak) {
                                                document.getElementById("u" + c).style.backgroundColor = "#003400";
                                                if (c <= soundLevel) {
                                                    document.getElementById("u" + c).style.backgroundColor = "#09d909";
                                                }
                                            }

                                            if (c >= peak) {
                                                document.getElementById("u" + c).style.backgroundColor = "#660000";
                                                if (c <= soundLevel) {
                                                    document.getElementById("u" + c).style.backgroundColor = "#e10e0e";
                                                }
                                            }
                                        }
                                        if (parseInt(dB) > NOISE_THRESHOLD) {
                                            if (!startNoise) {
                                                startNoise = new Date().getTime();
                                                buffer[i] = parseInt(dB);
                                                i++;
                                            }

                                        }

                                        if (parseInt(dB) < NOISE_THRESHOLD - 5 && startNoise) {

                                            endNoise = new Date().getTime();
                                            var diff = (endNoise - startNoise);

                                            if (diff >= PICK_DURATION && diff < MAX_NOISE_DURATION) {
                                                bufferPick[j] = _.reduce(buffer, function (memo, num) {
                                                        return memo + num;
                                                    }, 0) / (buffer.length === 0 ? 1 : buffer.length);

                                                console.log("PICK ==> ", bufferPick[j]);
                                                console.log("BUFFER PICK ==> ", bufferPick);

                                                j++;
                                            }

                                            startNoise = null;
                                            endNoise = null;
                                            buffer = [];
                                            i = 0;


                                        }

                                        if (startNoise && (new Date().getTime() - startNoise) >= MAX_NOISE_DURATION) {

                                            $scope.isStopped = true;
                                            cancelTimer();
                                            $scope.LoudNoise = true;
                                            document.getElementById('noise').innerText = '';
                                            $scope.DBMesure = dB;
                                            navigator.DBMeter.stop(function () {
                                                console.log("DBMeter well stopped");
                                            }, function (e) {
                                                console.log('code: ' + e.code + ', message: ' + e.message);
                                            });
                                            $scope.$apply();
                                            $scope.DBMesure = _.reduce(bufferPick, function (memo, num) {
                                                    return memo + num;
                                                }, 0) / (bufferPick.length === 0 ? 1 : bufferPick.length);
                                            showPopupAfterNoiseDetected();
                                            return;

                                        }

                                        document.getElementById('noise').innerText = count;

                                        if (bufferPick.length >= PICK_NUMBER) {
                                            $scope.isStopped = true;
                                            cancelTimer();
                                            $scope.LoudNoise = true;
                                            document.getElementById('noise').innerText = '';
                                            $scope.DBMesure = dB;
                                            navigator.DBMeter.stop(function () {
                                                console.log("DBMeter well stopped");
                                            }, function (e) {
                                                console.log('code: ' + e.code + ', message: ' + e.message);
                                            });
                                            $scope.$apply();
                                            $scope.DBMesure = _.reduce(bufferPick, function (memo, num) {
                                                    return memo + num;
                                                }, 0) / (bufferPick.length === 0 ? 1 : bufferPick.length);
                                            showPopupAfterNoiseDetected();


                                        }
                                    }, function (e) {
                                        console.log('code: ' + e.code + ', message: ' + e.message);
                                    });
                                }
                            });

                        }, 1000, 16).then(function () {
                            if (!$scope.isStopped) {
                                $notification.alert($filter('translate')("Noise level is not detected"));
                                cancelTimer();
                                console.log('post timer')
                            }


                        });
                    }

                } catch (e) {

                    console.log(e);
                    $notification.alert($filter('translate')("An error has occurred while detecting noise level"));


                }
            }

            function showPopupAfterNoiseDetected() {
                WL.SimpleDialog.show(
                    $filter('translate')('Noise detected'),
                    $filter('translate')('Do you want to continue?'), [{
                        text: $filter('translate')('Yes'), handler: function () {
                            $scope.timer = 15;
                            $scope.modal.showTimer = true;
                            window.plugins.audioRecorderAPI.record(function (msg) {
                                // complete
                                console.log('ok: ' + msg);

                            }, function (msg) {
                                // failed
                                console.log('ERROR from recorder.record: ' + msg);
                            }, 20); // record 30 seconds
                            $("#progressTimer").progressTimer({
                                timeLimit: 15,
                                warningThreshold: 5,
                                baseStyle: 'progress-bar-warning-custom',
                                warningStyle: 'progress-bar-danger-custom',
                                completeStyle: 'progress-bar-info-custom',
                                onFinish: function () {
                                    console.log("I'm done");
                                }
                            });
                            $interval(function () {
                                $scope.timer--;
                            }, 1000, 15)
                                .then(function () {
                                    window.plugins.audioRecorderAPI.stop(function (msg) {
                                        // success
                                        console.log('ok: ' + msg);
                                        $scope.recordedNoiseAudio = msg;
                                        localStorage.setItem('recordedNoiseAudio', msg);
                                        $scope.modal.showTimer = false;
                                        $scope.$apply();
                                        if ($stateParams.payload) {
                                            LoadingService.show();
                                            $scope.prepareNoiseRequest($stateParams.payload.location.lat, $stateParams.payload.location.lng);
                                        } else {
                                            $scope.checkGPS();
                                        }
                                    }, function (msg) {
                                        // failed
                                        console.log('ERROR from recorder.stop: ' + msg);
                                    });

                                });

                            return;
                        }
                    },
                        {
                            text: $filter('translate')('Cancel'), handler: function () {
                            return;
                        }
                        }]);

                $scope.nbrPage++;
                console.log($scope.nbrPage);
            }

            function cancelTimer() {
                $scope.isStopped = true;
                $scope.stopDetectingNoise();
                $scope.modal.openNoise = !$scope.modal.openNoise;
                $interval.cancel($scope.timerPromise);
            }

            /*
             *   Stop Detecting Noise
             * */
            $scope.stopDetectingNoise = function () {
                $interval.cancel($scope.timerPromise);
                $scope.isStopped = true;
                navigator.DBMeter.isListening(function (isListening) {
                    console.log(isListening);
                    navigator.DBMeter.stop(function () {
                        console.log("DBMeter well stopped");
                    }, function (e) {
                        console.log('code: ' + e.code + ', message: ' + e.message);
                    });
                });
            }

            /**
             * Get address name by LatLng
             * @param latlng
             */
            $scope.geocodeLatLng = function (lat, lng) {
                var deferred = $q.defer();

                var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
                var geocoder = new google.maps.Geocoder()
                geocoder.geocode({'location': latlng}, function (results, status) {
                    if (status === 'OK') {
                        deferred.resolve(results[1].formatted_address);
                    } else {
                        deferred.resolve('');
                    }
                });
                return deferred.promise;
            };

            $scope.prepareNoiseRequest = function (lat, lng) {
                // serviceInfo, makani, locationName, formatted_address,location, makaniInfo

                console.log(lat);
                console.log(lng);

                var location = {
                    lat: lat,
                    lng: lng
                };

                var serviceInfo = {
                    'service': $scope.service,
                    'category': $scope.category,
                };


                LoadingService.show('Getting Makani number from GPS coordinates');
                Makani.getNearestMakani(lat, lng).then(
                    function (result) {
                        if (result.hasOwnProperty('DATA')) {

                        } else {
                            var makaniInfo = result.makaniInfo[0];
                            var makani = makaniInfo.MAKANI;
                            makaniInfo = makaniInfo.MAKANI_INFO[0];

                        }

                        $scope.geocodeLatLng(lat, lng).then(function (result) {

                            LoadingService.hide();

                            console.log('NOISE CONSTRUCTION PAYLOAD :: \n',
                                'serviceInfo :: ', serviceInfo,
                                'makani :: ', makani,
                                'locationName :: ', result,
                                'formatted_address :: ', result,
                                'location :: ', location,
                                'makaniInfo :: ', makaniInfo,
                                'DBMesure :: ', $scope.DBMesure
                            );
                            var payload = {
                                serviceInfo: serviceInfo,
                                makani: makani,
                                locationName: result,
                                formattedAddress: result,
                                location: location,
                                makaniInfo: makaniInfo,
                                DBMesure: $scope.DBMesure
                            }
                            SharedService.setStateHistory('createRequest', {
                                payload: payload,
                                from: 'serviceDetails'
                            });
                            $state.go('createRequest')

                        })

                    });


            };

            $scope.searchByImage = function () {
                if ($scope.sourceType == 'camera') {
                    $scope.checkGPS();
                } else {
                    if ($scope.metaData) {
                        console.log('Metadata From LocalStorage :: ', $scope.metaData);
                        var meta = JSON.parse($scope.metaData);
                        prepareRequestFromUploadImage(meta.lat, meta.lng);
                    } else {
                        $scope.checkGPS();
                    }

                }

            };

            /**
             *  prepare Request From Upload Image
             * */
            /*function prepareRequestFromUploadImage(lat, lng) {
             console.log(lat);
             console.log(lng);

             var location = {
             lat: lat,
             lng: lng
             };

             var serviceInfo = {
             'service': $scope.service,
             'category': $scope.category,
             };

             Makani.getNearestMakani(lat, lng).then(
             function (result) {
             if (result.hasOwnProperty('DATA')) {

             } else {
             var makaniInfo = result.makaniInfo[0];
             var makani = makaniInfo.MAKANI;
             makaniInfo = makaniInfo.MAKANI_INFO[0];

             }

             $scope.geocodeLatLng(lat, lng).then(function (result) {

             LoadingService.hide();

             console.log('Create Request From upload PAYLOAD :: \n',
             'serviceInfo :: ', serviceInfo,
             'makani :: ', makani,
             'locationName :: ', result,
             'formatted_address :: ', result,
             'location :: ', location,
             'makaniInfo :: ', makaniInfo,
             'fromImageUpload :: imageUpload'
             );
             SharedService.setStateHistory('createRequest', {
             payload:payload
             });
             $state.go('createRequest',
             {
             serviceInfo: serviceInfo,
             makani: makani,
             locationName: result,
             formattedAddress: result,
             location: location,
             makaniInfo: makaniInfo,
             fromImageUpload: 'imageUpload'
             })



             }, function (err) {
             LoadingService.hide();
             console.log(err);
             })

             });

             }*/

            function prepareRequest(lat, lng) {
                console.log(lat);
                console.log(lng);

                var location = {
                    lat: lat,
                    lng: lng
                };

                var serviceInfo = {
                    'service': $scope.service,
                    'category': $scope.category,
                };

                Makani.getNearestMakani(lat, lng).then(
                    function (result) {
                        if (result.hasOwnProperty('DATA')) {

                        } else if(result.makaniInfo){
                            var makaniInfo = result.makaniInfo[0];
                            var makani = makaniInfo.MAKANI;
                            makaniInfo = makaniInfo.MAKANI_INFO[0];
                        }

                        $scope.geocodeLatLng(lat, lng).then(function (result) {

                            LoadingService.hide();

                            console.log('Request PAYLOAD :: \n',
                                'serviceInfo :: ', serviceInfo,
                                'makani :: ', makani,
                                'locationName :: ', result,
                                'formattedAddress :: ', result,
                                'location :: ', location,
                                'makaniInfo :: ', makaniInfo
                            );

                            LoadingService.hide();
                            var payload = {
                                serviceInfo: serviceInfo,
                                makani: makani,
                                locationName: result,
                                formattedAddress: result,
                                location: location,
                                makaniInfo: makaniInfo
                            }
                            SharedService.setStateHistory('createRequest', {
                                from: 'serviceDetails',
                                payload: payload
                            });
                            $state.go('createRequest',
                                {
                                    payload: payload
                                })

                        }, function (err) {
                            LoadingService.hide();
                            console.log(err);
                        })

                    }, function (err) {
                        LoadingService.hide();
                        console.log(err);
                    });

            }

            /*
             *  check GPS
             * */
            $scope.checkGPS = function () {
                LoadingService.show('Checking your GPS connection');
                try {
                    if (typeof(diagnostic) !== "undefined" && typeof(cordova) !== "undefined") {
                        if ($rootScope.geoLocation) {
                            // In case the GPS option is ENABLED in the application settings
                            cordova.plugins.diagnostic.isLocationEnabled(function(enabled){
                                console.log("Successfully switched to Settings app ", enabled);
                                if (!enabled) {
                                    LoadingService.hide();
                                    WL.SimpleDialog.show(
                                        $filter('translate')('Alert'), $filter('translate')("Your GPS seems to be disabled, do you want to enable it ?"), [
                                            {
                                                text: $filter('translate')("Settings"),
                                                handler: function () {
                                                    if (WL.Client.getEnvironment() === WL.Environment.IPHONE) {
                                                        diagnostic.switchToSettings();
                                                    }
                                                    else {
                                                        diagnostic.switchToLocationSettings();
                                                    }
                                                }
                                            },
                                            {
                                                text: $filter('translate')("Close"), handler: function () {
                                                    if ($scope.goToSubmittingReq) {
                                                        LoadingService.show();
                                                        $scope.goToSubmittingReq = false;
                                                        prepareRequest(25.063890, 55.193373);
                                                    }
                                                    else
                                                        return false;
                                                }
                                            }]
                                    );
                                    $scope.nbrPage++;
                                    console.log($scope.nbrPage);
                                } else {
                                    setTimeout(function () {
                                        $scope.gpsFailureCount = 0;
                                        $scope.getCurrentPosition();
                                    }, 1500);
                                }
                            }, function(error){
                                LoadingService.hide();
                                console.log("The following error occurred: " + error);
                            });
                            /*
                            diagnostic.isLocationEnabled(function (enabled) {
                                if (!enabled) {
                                    LoadingService.hide();
                                    WL.SimpleDialog.show(
                                        $filter('translate')('Alert'), $filter('translate')("Your GPS seems to be disabled, do you want to enable it ?"), [
                                            {
                                                text: $filter('translate')("Settings"),
                                                handler: function () {
                                                    if (WL.Client.getEnvironment() === WL.Environment.IPHONE) {
                                                        diagnostic.switchToSettings();
                                                    }
                                                    else {
                                                        diagnostic.switchToLocationSettings();
                                                    }
                                                }
                                            },
                                            {
                                                text: $filter('translate')("Close"), handler: function () {
                                                if ($scope.goToSubmittingReq) {
                                                    LoadingService.show();
                                                    $scope.goToSubmittingReq = false;
                                                    prepareRequest(25.063890, 55.193373);
                                                }
                                                else
                                                    return false;
                                            }
                                            }]
                                    );
                                    $scope.nbrPage++;
                                    console.log($scope.nbrPage);
                                } else {
                                    setTimeout(function () {
                                        $scope.gpsFailureCount = 0;
                                        $scope.getCurrentPosition();
                                    }, 1500);
                                }
                            }, function (error) {
                                LoadingService.hide();
                                console.log("The following error occurred: " + error);
                            });
                            */
                        } else {
                            LoadingService.hide();
                            // In case the GPS option is DISABLED in the application settings
                            WL.SimpleDialog.show(
                                $filter('translate')('Alert'), $filter('translate')("Please enable location service from application settings"), [
                                    {
                                        text: $filter('translate')("App Settings"),
                                        handler: function () {
                                            $state.go('settings');
                                        }
                                    },
                                    {
                                        text: $filter('translate')("Close"), handler: function () {
                                        if ($scope.goToSubmittingReq) {
                                            $scope.goToSubmittingReq = false;
                                            prepareRequest(25.063890, 55.193373);
                                        }
                                        else
                                            return false;
                                    }
                                    }]
                            );
                            $scope.nbrPage++;
                            console.log($scope.nbrPage);
                        }
                    }
                } catch (ex) {
                    LoadingService.hide();
                    console.log('===> EX ', ex)
                }

            };

            /*
             *  get Current position
             * */
            $scope.getCurrentPosition = function () {
                LoadingService.show('Retrieving your GPS coordinates');
                try {
                    /*
                    var posOptions = {timeout: 10000, enableHighAccuracy: false};
                    $cordovaGeolocation
                        .getCurrentPosition(posOptions)
                        .then(function (position) {
                            console.log("getCurrentPosition ", position);
                            LoadingService.hide();
                            if ($scope.sourceType) {
                                prepareRequestFromUploadImage(position.coords.latitude, position.coords.longitude);
                            }
                            else if ($scope.goToSubmittingReq) {
                                $scope.goToSubmittingReq = false;
                                prepareRequest(position.coords.latitude, position.coords.longitude);
                            }
                            else {
                                $scope.prepareNoiseRequest(position.coords.latitude, position.coords.longitude);
                            }
                        }, function(error) {
                            // error
                            console.log("error ", error);
                            $scope.gpsFailureCount++;
                            LoadingService.hide();
                            $scope.getCurrentPosition();
                        });
                    */

                    var optionsCurrentPosition = {enableHighAccuracy: true, maximumAge: 3000, timeout: 10000};
                    var onSuccess = function (position) {
                        if ($scope.sourceType) {
                            prepareRequestFromUploadImage(position.coords.latitude, position.coords.longitude);
                        }
                        else if ($scope.goToSubmittingReq) {
                            $scope.goToSubmittingReq = false;
                            prepareRequest(position.coords.latitude, position.coords.longitude);
                        }
                        else {
                            $scope.prepareNoiseRequest(position.coords.latitude, position.coords.longitude);
                        }
                    };
                    var onError = function (error) {
                        $scope.gpsFailureCount++;
                        LoadingService.hide();
                        $scope.getCurrentPosition();
                    };
                    if ($scope.gpsFailureCount <= 2) {
                        navigator.geolocation.getCurrentPosition(onSuccess, onError, optionsCurrentPosition);
                    } else {
                        LoadingService.hide();
                        $notification.alert($filter('translate')("gps_signal_failure"));
                    }

                } catch (ex) {
                    LoadingService.hide();
                    console.log('===> EX ', ex)
                }
            };

            $scope.addToFavourite = function () {
                if ($('#span-star').hasClass("fa-star")) {
                    removeService();
                    $('.click').removeClass('active')
                    setTimeout(function () {
                        $('.click').removeClass('active-2')
                    }, 30)
                    $('.click').removeClass('active-3')
                    setTimeout(function () {
                        $('#span-star').removeClass('fa-star')
                        $('#span-star').addClass('fa-star-o')
                    }, 15)
                } else {
                    addService();
                    $('.click').addClass('active')
                    $('.click').addClass('active-2')
                    setTimeout(function () {
                        $('#span-star').addClass('fa-star')
                        $('#span-star').removeClass('fa-star-o')
                    }, 150)
                    setTimeout(function () {
                        $('.click').addClass('active-3')
                    }, 150)
                    $('.info').addClass('info-tog')
                    setTimeout(function () {
                        $('.info').removeClass('info-tog')
                    }, 1000)
                }
            }

            function addService() {
                var favourites = localStorage.getItem('Favourites');
                if (!favourites) {
                    localStorage.setItem('Favourites', '[]');
                    favourites = localStorage.getItem('Favourites');
                }

                favourites = JSON.parse(favourites);
                favourites.push({
                    id: ($scope.service.service_request_id) ? $scope.service.service_request_id : $scope.service.incident_id,
                    type: ($scope.service.service_request_id) ? 'service' : 'incident',
                    service: {
                        service_title_en: $scope.service.service_title_en,
                        service_title_ar: $scope.service.service_title_ar,
                        category_id: $scope.service.category_id
                    }
                });

                localStorage.setItem('Favourites', JSON.stringify(favourites));


            }

            function removeService() {
                var favourites = JSON.parse(localStorage.getItem('Favourites'));
                var type = ($scope.service.service_request_id) ? 'service' : 'incident';
                var id = ($scope.service.service_request_id) ? $scope.service.service_request_id : $scope.service.incident_id;
                var position = [];
                favourites.map(function (item, key) {
                    if (item.id == id && item.type == type) {
                        position.push(key);
                    }
                })

                position.slice(0).reverse().map(function (item, key) {
                    favourites.splice(item, 1);
                });

                localStorage.setItem('Favourites', JSON.stringify(favourites));

            }

            angular.element(document).ready(function () {
                $('#categoryServices').scroll(function () {
                    var scrollTop = $('#categoryServices').scrollTop();
                    console.log('scrollTop:', scrollTop);

                    if (scrollTop > 35) {
                        $('#servicesTopBar').addClass("stickyTopBar");
                        $('.services-topbar').addClass("serviceIsscrolled");
                        $('#topBarInfo').addClass("topbar-info-fixed");
                        $('#topBarWrapper').addClass('topBarWrapperscrolled');
                        $('.header-icon.fixedArrow').addClass('fixedArrowIsscrolled');
                    } else {
                        $('#servicesTopBar').removeClass("stickyTopBar");
                        $('.services-topbar').removeClass("serviceIsscrolled");
                        $('#topBarInfo').removeClass("topbar-info-fixed");
                        $('#topBarWrapper').removeClass('topBarWrapperscrolled');
                        $('.header-icon.fixedArrow').removeClass('fixedArrowIsscrolled');
                    }


                    // if (scrollTop >= 110) {

                    // } else {

                    // }
                });
                $('li.waste-item a.list-link').click(function () {
                    $('.serviceContent').addClass('isServiceContent');
                    $('.content--margin').css('display', 'none');
                    $('#serviceDetails').addClass('topbarFixedScrolled')

                });
            });

        }
    ]
);