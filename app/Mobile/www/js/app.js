/**
 * Copyright 2016 IBM Corp.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var Messages = {
    // Add here your messages for the default language.
    // Generate a similar file with a language suffix containing the translated messages.
    // key1 : message1,
};

var wlInitOptions = {
    // Options to initialize with the WL.Client object.
    // For initialization options please refer to IBM MobileFirst Platform Foundation Knowledge Center.
};

// Called automatically after MFP framework initialization by WL.Client.init(wlInitOptions).
function wlCommonInit() {
	// angular.bootstrap(document, ['iDubaiApp']);
    var response = typeof (error) != 'undefined' ? error : null;
    angular.element(document).ready(function () {
        angular.bootstrap(document, ['iDubaiApp']);
        if (response != null) {
            angular.element('*[ng-controller]').injector().get("errorHandler").checkResponse(response);
        }
    });

}

var iDubaiApp = angular.module('iDubaiApp', [
    'pascalprecht.translate',
    'cgBusy',
    'ui.router',
    'ui.load',
    'ngMap',
    'ngSanitize',
    'ngTouch',
    'ngStorage',
    'ngDebug',
    'ngUnderscore',
    'ngDebug',
    'ngCordova',
    'oauth.providers',
    'once',
    'AnalyticsService',
    'angular-preload-image',
    'angularRipple',
    'slickCarousel',
    'ui.bootstrap',
    'rzModule',
    'ngDialog'
]);

iDubaiApp.service("$history", function ($state, $rootScope, $window) {

        var history = [];

        angular.extend(this, {
            push: function (state, params) {
                history.push({state: state, params: params});
            },
            all: function () {
                return history;
            },
            go: function (step) {
                // TODO:
                // (1) Determine # of states in stack with URLs, attempt to
                //    shell out to $window.history when possible
                // (2) Attempt to figure out some algorthim for reversing that,
                //     so you can also go forward

                var prev = this.previous(step || -1);
                return $state.go(prev.state, prev.params);
            },
            previous: function (step) {
                return history[history.length - Math.abs(step || 1)];
            },
            back: function () {
                return this.go(-1);
            }
        });

    })
    .directive('whenScrolled', function () {
        return function (scope, elm, attr) {
            var raw = elm[0];

            elm.bind('scroll', function () {
                if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
                    scope.$apply(attr.whenScrolled);
                }
            });
        };
    })
    .directive('scrollOnClick', function () {
        return {
            restrict: 'A',
            link: function (scope, $elm, attrs) {
                var idToScroll = attrs.href;
                $elm.on('click', function () {
                    var $target;
                    if (idToScroll) {
                        $target = $(idToScroll);
                    } else {
                        $target = $elm;
                    }
                    $("body").animate({scrollTop: $target.offset().top}, "slow");
                });
            }
        };
    });

iDubaiApp.config(['$controllerProvider',
    function ($controllerProvider) {
        $controllerProvider.allowGlobals();
    }
]);

iDubaiApp.config(['ngDialogProvider', function (ngDialogProvider) {
    ngDialogProvider.setDefaults({
        className: 'ngdialog-theme-default',
        plain: false,
        showClose: true,
        closeByDocument: true,
        closeByEscape: true,
        appendTo: false,
        preCloseCallback: function () {
            console.log('default pre-close callback');
        }
    });
}]);

iDubaiApp.constant("appConfig", {
    "CLICK": "click"
});
//'http://dubai.proxym-it.net/search'; //Prod
// 'http://dubai247.proxym-it.net/search'; //Staging
// 'http://10.20.0.12:1234/search';
// 'http://ahmed.servehttp.com:3030/search';
iDubaiApp.constant("Constants", {
    "HIT_TYPE": {
        "SCREEN_VIEW": 'screenview',
        "EVENT": 'event',
    },
    "EVENTS": {
        "Search_By_Voice": "Search_By_Voice",
        "Search_By_Image": "Search_By_Image",
        "Search_By_Text": "Search_By_Text",
        "Trending_Services": "Trending_Services",
        "Dashboard_Services": "Dashboard_Services",
        "Services_Submitted": "Services_Submitted",
        "Map_Search": "Map_Search"
    },
    "SEARCH_ENGINE_URL": "http://dubai.proxym-it.net/search",
    "IS_STAGING": null // WL.Client.getAppProperty(WL.AppProp.WORKLIGHT_ROOT_URL).indexOf("mfpprod.dm.gov.ae") > 0 ? true : false
});

iDubaiApp.value("cat", [
    {
        title: 'Pest Control',
        titleAR: 'مكافحة الحشرات',
        description: '',
        descriptionAR: '',
        image: 'c-img01.png',
        gear: 'PestControl/pest.png',
        backgroundColor: 'pestControl',
        category_id: 1
    },
    {
        title: 'Pets & Animals',
        titleAR: 'الحيوانات الأليفة',
        description: '',
        descriptionAR: '',
        image: 'c-img02.png',
        gear: 'PetsAnimals/pets.png',
        backgroundColor: 'petsAnimals',
        category_id: 2
    },
    {
        title: 'Drainage & Irrigation',
        titleAR: 'الصرف الصحي',
        description: '',
        descriptionAR: '',
        image: 'c-img03.png',
        gear: 'Drainage/drainage.png',
        backgroundColor: 'drainageSewage',
        category_id: 3
    },
    {
        title: 'Waste Management',
        titleAR: 'إدارة النفايات',
        description: '',
        descriptionAR: '',
        image: 'c-img04.png',
        gear: 'WasteManagement/waste.png',
        backgroundColor: 'wasteManagement',
        category_id: 4
    },
    {
        title: 'Health & Safety',
        titleAR: 'الصحة والأمان',
        description: '',
        descriptionAR: '',
        image: 'c-img05.png',
        gear: 'HealthSafety/health.png',
        backgroundColor: 'healthSafety',
        category_id: 5
    },
    {
        title: 'Environment',
        titleAR: 'البيئة',
        description: '',
        descriptionAR: '',
        image: 'c-img06.png',
        gear: 'Environement/environement.png',
        backgroundColor: 'environment',
        category_id: 6
    },
    {
        title: 'Building & Construction',
        titleAR: 'اعمال بناء',
        description: '',
        descriptionAR: '',
        image: 'c-img07.png',
        gear: 'Construction/construction.png',
        backgroundColor: 'construction',
        category_id: 7
    },
    {
        title: 'Food Safety',
        titleAR: 'سلامة الأغذية',
        description: '',
        descriptionAR: '',
        image: 'c-img08.png',
        gear: 'FoodSafety/food.png',
        backgroundColor: 'foodSafety',
        category_id: 8
    },
    {
        title: 'General Services',
        titleAR: 'خدمات عامة',
        description: '',
        descriptionAR: '',
        image: 'generalCategory.png',
        gear: 'other-gear.png',
        backgroundColor: 'generalCategory',
        category_id: 99
    }
]);

iDubaiApp.run(['execProcedure', '$rootScope', '$timeout', '$logger', '$translate', '$auth', '$state', '$stateParams',
    '$DbStorageService', '$config', '$interval', 'AnalyticsService', '$dmServices', '$stopWords', '$arabicStopWords', 'GuestUtils', '$cordovaNetwork', 'Constants', '$filter', '$history', 'underscore', 'SharedService',
    function (execProcedure, $rootScope, $timeout, $logger, $translate, $auth, $state, $stateParams,
              $DbStorageService, $config, $interval, AnalyticsService, $dmServices, $stopWords, $arabicStopWords, GuestUtils, $cordovaNetwork, Constants, $filter, $history, underscore, SharedService) {

        var _ = underscore;


        console.log('===> APP START <===');


        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $rootScope.emailPattern = "[A-Z-a-z-0-9---.-_,á,à,é,í,ó,ú,â,ê,ô,ã,õ,ç,Á,É,Í,Ó,Ú,Â,Ê,Ô,Ã,Õ,Ç,ü,ñ,Ü,Ñ]+@+[a-z]+.+[a-z]";//"[a-z0-9._-]+@+[a-z0-9.-]([.][a-z]){1,3}$";//"/^[\\w\\-\\+]+(\\.[\\w\\-]+)*@[\\w\\-]+(\\.[\\w\\-]+)*\\.[\\w\\-]{2,4}$/";
        $rootScope.emirateMobilePattern = "([0-9][0-9]{3}[A-Z]{1}[0-9]{3})";


        document.addEventListener(WL.Events.WORKLIGHT_IS_CONNECTED, function () {
            console.log('Worklight CONNECTED');
        }, false);
        document.addEventListener(WL.Events.WORKLIGHT_IS_DISCONNECTED, function () {
            console.log('Worklight DISCONNECTED');
        }, false);

        function onBackKeyDown(e) {
            var states = [
                'requestDone',
                'chooseLanguage',
                'onBoarding'
            ];


            console.log('Device Back Button FROM : ' + $rootScope.previousState);
            console.log('Device Back Button TO   : ' + $rootScope.currentState);
            console.log('Device Back Button StateParams   : ' + JSON.stringify($stateParams));
            /*  if (states.indexOf($rootScope.previousState) > -1 || (statesRequest.indexOf($rootScope.previousState) > -1 && $rootScope.currentState == 'requestDone')) {
             e.preventDefault();
             } else {
             if (statesRequest.indexOf($rootScope.previousState) > -1 && $rootScope.currentState == 'map') {
             var payload = $rootScope.previousParams.payload;
             $state.go($rootScope.previousState, {
             payload: payload,

             });
             } else if (statesRequest.indexOf($rootScope.currentState) > -1 && $rootScope.previousState == 'map') {
             var payload = $rootScope.currentParams.payload;
             $state.go($rootScope.previousState, {
             payload: payload,

             });
             } else {
             $state.go($rootScope.previousState, $rootScope.previousParams)
             //window.history.go(-1);
             }

             }
             e.preventDefault();*/
            var backButtonIsActive = SharedService.getStateHistory("backButton").isActive;

            if($state.current.name=="personalDetails_1"){
                if (WL.Client.getEnvironment() === WL.Environment.ANDROID) {
                    window.plugins.smsReceive.stopWatch(function () {
                        console.log('smsreceive: watching stopped');
                    }, function () {
                        console.warn('smsreceive: failed to stop watching');
                    });
                }
            }

            if (!backButtonIsActive) {
                e.preventDefault();
            } else if (states.indexOf($rootScope.currentState) > -1) {
                e.preventDefault();
            } else if ($state.current.name == 'map') {
                $state.go($rootScope.previousState);
            } else if ($state.current.name != 'dashboard') {
                var prev = SharedService.getStateHistory($state.current.name).from;
                $state.go(prev)
            } else {
                WL.SimpleDialog.show(
                    $filter('translate')('Confirm_Close_App'),
                    $filter('translate')('Message_Exit_App'), [{
                        text: $filter('translate')('Cancel'), handler: function () {
                            return;
                        }
                    },
                        {
                            text: $filter('translate')('Yes'), handler: function () {
                                navigator.app.exitApp();
                            }
                        }]);

            }


        }

        // App rating
        document.addEventListener("deviceready", function () {


            document.addEventListener("backbutton", onBackKeyDown, true);
            $rootScope.$on('$cordovaNetwork:online', function (event, networkState) {
                var onlineState = networkState;
                console.log(onlineState);
            })
            document.addEventListener("offline", function () {
                console.log('Offline');
            }, false);

            if ($cordovaNetwork.isOffline()) {
                WL.SimpleDialog.show('Connection is down', ("Please check your internet connection"), [{
                    text: ('Retry'), handler: function () {
                        WL.Client.reloadApp();
                    }
                }]);
            }

            document.addEventListener("online", function () {
                console.log('online')
            }, false);

            if (!window.db) {
                window.db = null;
                console.log('init DB');
            }

            console.log("WL.Client.getEnvironment() ", WL.Client.getEnvironment());
            if (WL.Client.getEnvironment() == WL.Environment.IPHONE) {
                navigator.DBMeter = DBMeter;
                window.db = window.sqlitePlugin.openDatabase({
                    name: 'DM247.db',
                    location: 'default',
                    androidDatabaseImplementation: 2
                });
            } else {
                navigator.DBMeter = window.plugins.dbmeter;
                window.db = window.openDatabase('DM247', '1.0', 'todo list example db', 2 * 1024 * 1024)
            }


        }, false);

        $rootScope.back = function () {
            window.history.go(-1)
        }

        // Clear storage
        if ($config.cache.clearDbCashe) {
            $DbStorageService.clear().then(function (data) {
                console.log("Storage cleared successfully");
            });
        }

        execProcedure.init();
        $rootScope.$serviceHasBack = true;
        $rootScope.$currentLanguage = localStorage.getItem('dm-lang');
        if (localStorage.getItem('dm-lang') == 'en') {
            $('html').css('direction', 'ltr');
        }
        if (localStorage.getItem('dm-lang') == 'ar') {
            $('html').css('direction', 'rtl')
        }

        /**
         * Change Language
         */
        $rootScope.changeLanguage = function (val, fromMenu, fromOnboard) {
            console.log("changeLanguage ", val);
            var currentLang = localStorage.getItem('dm-lang');

            window.db.transaction(function (tx) {

                tx.executeSql('UPDATE language SET currentLang = ?', [val], function (tx, res) {
                    //console.log('UPDATE language SET currentLang', JSON.stringify(tx), JSON.stringify(res));
                    currentLang = val;
                    $("html").attr("lang", currentLang);
                    localStorage.setItem('dm-lang', currentLang);
                    $translate.use(currentLang);
                    $rootScope.$currentLanguage = currentLang;
                    if (currentLang == 'en') {
                        $('html').css('direction', 'ltr')
                    } else {
                        $('html').css('direction', 'rtl')
                    }
                    if (fromMenu) {
                        $state.reload();
                    }

                    /*if (fromOnboard) {
                     $state.go('dashboard');
                     }*/
                });
            }, function (error) {
                console.log('Transaction ERROR: ' + error.message);
            }, function () {
                console.log('Populated database OK');
            });

        }

        // List of original fonts
        if (localStorage.getItem('originalFonts')) {
            console.log('Original fonts:', localStorage.getItem('originalFonts'));
        } else {
            localStorage.setItem('originalFonts', JSON.stringify({
                page: 15,
                fz25R: 25,
                fz25B: 25,
                fz24B: 24,
                fz24R: 24,
                fz23R: 23,
                fz23B: 23,
                fz22R: 22,
                fz22B: 22,
                fz21R: 21,
                fz19R: 19,
                fz19B: 19,
                fz15R: 15,
                fz15B: 15,
                fz13R: 13,
                fz13B: 13,
                fz21B: 21,
                fz20R: 20,
                fz20B: 20,
                fz18B: 18,
                fz18R: 18,
                fz17R: 17,
                fz17B: 17,
                fz17600: 17,
                fz16B: 16,
                fz16R: 16,
                fz14B: 14,
                fz14R: 14,
                fz12B: 12,
                fz12R: 12
            }));
        }

        // Check application foreground / Background
        $rootScope.isBackground = false;
        document.addEventListener("pause", onPause, false);

        function onPause() {
            $rootScope.isBackground = true;
        }

        document.addEventListener("resume", onResume, false);

        function onResume() {
            $rootScope.isBackground = false;
        }

        //Where to hide menu
        const hidingStates = [
            'voiceRecord',
            'requestDone',
            'Request_A_Service',
            'chooseLanguage',
            'tour',
            'onBoarding',
            'personalDetails_1',
            'map',
            'createRequest',
            'comingSoon',
            'settings',
            'storage',
            'tellFriend',
            'contactUs',
            'chat',
            'takeTour',
            'smartRequest',
            'requestSummary'
        ];

        /**
         * Google analytics tracking routes
         */
        $rootScope.$on('$stateChangeSuccess',
            function (event, toState, toParams, fromState, fromParams) {
                SharedService.setStateHistory("backButton", {isActive: true});
                // $rootScope.applyFontSizes();
                /*if (!localStorage.getItem('dm-lang')) {
                 $state.go('onBoarding');
                 }*/

                // if (!fromState.abstract) {
                //     $history.push(fromState, fromParams);
                // }
                //
                //
                // $history.push($state.current, $state.params);


                $rootScope.previousState = fromState.name;
                $rootScope.previousParams = fromParams;

                $rootScope.currentState = toState.name;
                $rootScope.currentParams = toParams;
                var screenName = '';

                if($rootScope.previousState=="personalDetails_1"){
                    if (WL.Client.getEnvironment() === WL.Environment.ANDROID) {
                        window.plugins.smsReceive.stopWatch(function () {
                            console.log('smsreceive: watching stopped');
                        }, function () {
                            console.warn('smsreceive: failed to stop watching');
                        });
                    }
                }
                switch ($state.current.name) {
                    case 'personalDetails_1'     :
                        screenName = 'Registration';
                        break;
                    case 'personalDetailsUpdate' :
                        screenName = 'Update profile';
                        break;
                    case 'services'              :
                        screenName = 'Services list';
                        break;
                    case 'requestSummary'        :
                        screenName = 'Request Summary';
                        break;
                    case 'createRequest'        :
                        screenName = 'Request service';
                        break;
                    case 'contactus'             :
                        screenName = 'Contact us';
                        break;
                    case 'myRequests'            :
                        screenName = 'My requests';
                        break;
                    case 'chat'                  :
                        screenName = 'chat';
                        break;
                    case 'servicesAZ'            :
                        screenName = 'All services';
                        break;
                    case 'serviceDetails'        :
                        screenName = 'Service Details';
                        break;
                    case 'settings'              :
                        screenName = 'Settings';
                        break;
                    case 'feedback'              :
                        screenName = 'Feedback';
                        break;
                    case 'faq'                   :
                        screenName = 'FAQ';
                        break;
                    case 'smartRequest' :
                        screenName = 'Request Service from search engine';
                        break;
                    case 'takeTour'              :
                        screenName = 'App tips';
                        break;
                }
                if (screenName != '') {
                    AnalyticsService.trackRoute(Constants.HIT_TYPE.SCREEN_VIEW, screenName);
                }


                if ($state.current.name === 'search') {
                    if (document.getElementById("Search")) {
                        document.getElementById("Search").focus();
                    }
                }

                if (document.getElementById('nav-bar') != null) {
                    //console.log('FROM STATE :: ',JSON.stringify(fromState));
                    //console.log('TO STATE :: ',JSON.stringify(toState));
                    if (hidingStates.indexOf(toState.name) >= 0) {
                        document.getElementById('nav-bar').style.visibility = 'hidden';
                    } else {
                        document.getElementById('nav-bar').style.visibility = 'visible';
                        document.getElementById('nav-bar').classList.remove('hideNav');
                    }
                }
            });
        $history.push($state.current, $state.params);

    }]);

/*
iDubaiApp.config(function ($routeProvider) {
	$routeProvider.when('/login', {
		templateUrl: 'app/pages/login.html'
	}).when('/home', {
		templateUrl: 'app/pages/dashboard.html'
	}).when('/accounts', {
		templateUrl: 'app/pages/accounts.html'
	}).when('/appointments', {
		templateUrl: 'app/pages/appointments.html'
	}).when('/check-deposit', {
		templateUrl: 'app/pages/check-deposit.html'
	}).when('/find-atm', {
		templateUrl: 'app/pages/find-atm.html'
	}).when('/transactions/:account?', {
		templateUrl: 'app/pages/transactions.html'
	}).when('/transfer/:account?', {
		templateUrl: 'app/pages/transfer.html'
	}).when('/view-appointment', {
		templateUrl: 'app/pages/view-appointment.html'
	}).when('/logout', {
		template: '',
		controller: 'LogoutCtrl'
	}).otherwise({redirectTo: '/login'});
});
*/

/*
iDubaiApp.run(function ($rootScope, $location, Analytics) {

	$rootScope.$on('login-challenge', function (e) {
		$location.path('/login');
		$rootScope.$apply();
	});

	$rootScope.$on("$locationChangeSuccess", function (e, newUrl, oldUrl) {

		var path = function (path) {
			var parts = path.split('#');

			var uri = parts.length > 1 ? parts[1] : path;

			var parser = document.createElement('a');
			parser.href = uri;

			return parser.pathname;
		};


		var next = path(newUrl);
		var prev = path(oldUrl);

		Analytics.pageVisit(next);
		Analytics.pageNavigation(prev, next);

		$rootScope.toggleMenu(true);

		if ($rootScope.isHome($location.path())) {
			$rootScope.hideNavigation();
		} else if ($location.path() === '/logout') {
			alert('logout');
			Analytics.logout(prev);
		} else {
			$rootScope.showNavigation();
		}
	});

	$rootScope.navigation = false;
	$rootScope.activeElement = null;

	$rootScope.isHome = function (uri) {
		return uri === '/home';
	};

	$rootScope.back = function () {
		history.back();
	};


	$rootScope.showNavigation = function () {
		$rootScope.navigation = true;
	};

	$rootScope.hideNavigation = function () {
		$rootScope.navigation = false;
	};

	$rootScope.toggleMenu = function (hide) {
		var drawer = angular.element('#drawer');

		if (drawer.hasClass('open') || hide) {
			drawer.removeClass('open');
		} else {
			drawer.addClass('open');
		}
	};

	$rootScope.setTitle = function (title) {
		$rootScope.title = title;
	};

	$rootScope.toggle = function (id) {
		if ($rootScope.activeElement === id) {
			$rootScope.activeElement = null;
		} else {
			$rootScope.activeElement = id;
		}
	};
});
*/