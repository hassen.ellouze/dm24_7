var categories = {
	"categories": [
		{
			"categoryId": "1",
			"nameEn": "Most popular services & notifications",
			"nameFr": "Les services les plus populaires",
			"nameAr": "الخدمات والبلاغات الأكثر طلبا",
			"cssClass": "commonServices",
			"services": [
				{
					"code": "1",
					"service_request_id": 12,
					"category_id": 2,
					"pet_type": 0,
					"insect_type": 1,
					"ServiceTypeId":"2",
					"nameEn": "Sprinkling Pests house Request",
					"nameFr": "Sprinkling Pests house Request",
					"nameAr": "طلب رش الحشرات المنزلية",
					"sla": 28,
					"area": {
						"legacyArea": "Animals & Pest Control",
						"ou": "Pest Control Section",
						"dept": "public health services dept."
					},
					"tagsAr": ["حشرات منزليه", "حشرات", "افات", "افات صحة", "مكافحة افات", "رش حشرات", "حشرات عامة", "صراصير",
						"صرصور", "ثعبان", "نحل", "سماسيم", "سمسوم", "نمل", "عنكبوت", "عنكبوت أحمر", "عناكب", "دبابير",
						"دبور"],
					"tagsEn": ["pest", "rats", "mouses", "ants", "bugs", "bed bugs", "spider", "beehive", "snake", "cockroach"],
					"tagsFr": ["animaux", "rat", "souri", "mouche", "abeille", "serpent", "araigné", "punaise"]
				},
				{
					"code": "2",
					"service_request_id": 13,
					"category_id": 2,
					"pet_type": 1,
					"insect_type": 0,
					"ServiceTypeId":"2",
					"nameEn": "Stray Cats",
					"nameFr": "Stray Cats",
					"nameAr": "قطط سائبة",
					"sla": 28,
					"area": {
						"legacyArea": "Animals & Pest Control",
						"ou": "Veterinary Services Section",
						"dept": "public health services dept."
					},
					"tagsAr": ["قطط", "قطط سائبة", "قطط وايد", "ققط بكثرة", "اقفاص للقطط"],
					"tagsEn": ["stray cat", "street cat", "sick cat"],
					"tagsFr": ["chat errant", "chat de rue", "chat malade"]
				},
				{
					"code": "3",
					"nameEn": "control agricultural leison",
					"nameFr": "control agricultural leison",
					"nameAr": "طلب رش الآفات الزراعية",
					"sla": 49,
					"service_request_id": 17,
					"category_id": 4,
					"pet_type": 0,
					"insect_type": 0,
					"ServiceTypeId":"2",
					"area": {
						"legacyArea": "Environment & Agriculture",
						"ou": "Horticulture Services Section",
						"dept": "Parks & Horticulture Dept."
					},
					"tagsAr": ["رش زراعي", "سوسة النخل", "رش النخل", "النخل", "نخل", "زراعة", "رش زراعي", "مبيد زراعي",
						"سم للزراعة", "زراعة", "شجر", "اشجار", "دود"],
					"tagsEn": ["agri spary", "agriculture spray", "worms", "agriculture pest control"],
					"tagsFr": ["spray", "agriculture", "verre", "control"]
				},
				{
					"code": "4",
					"nameEn": "Bulky Waste Removal-household",
					"nameFr": "Bulky Waste Removal-household",
					"nameAr": "خدمة ازالة المخلفات الحجمية",
					"sla": 35,
					"category_id": 6,
					"pet_type": 0,
					"insect_type": 0,
					"ServiceTypeId":"2",
					"area": {
						"legacyArea": "Waste & Cleanness",
						"ou": "Specialized Cleaning Section",
						"dept": "Waste Dept."
					},
					"tagsAr": ["اثاث", "اثاث حجمي", "كنب", "قنفة", "اثاث", "كراسي", "طاولات",
						"كبتات", "ديكور", "ديكورات", "اثاث بيت", "اثاث منزلي", "تخلص من الاثاث", "أجهزة"],
					"tagsEn": ["old house hold", "furniture", "table", "cabinet", "sofa", "bed", "washing machine", "cook"],
					"tagsFr": ["vielle maison", "vieux", "lit", "sofa", "machine à laver", "cuisine"]
				},
				{
					"code": "5",
					"nameEn": "Tree pruning for household",
					"nameFr": "Tree pruning for household",
					"nameAr": "تقليم الأشجار لمنازل الأفراد",
					"sla": 35,
					"service_request_id": 65,
					"category_id": 4,
					"pet_type": 0,
					"insect_type": 0,
					"ServiceTypeId":"2",
					"area": {
						"legacyArea": "Environment & Agriculture",
						"ou": "Horticulture Services Section",
						"dept": "Parks & Horticulture Dept."
					},
					"tagsAr": ["تقليم شجر", "شجر", "زراعة", "زرع", "تقصون الزرع"],
					"tagsEn": ["tree trimming", "tree", "tree pruning", "tree cutting"],
					"tagsFr": ["taille", "arbre", "plante"]
				},
				{
					"code": "6",
					"nameEn": "general waste removel",
					"nameFr": "general waste removel",
					"nameAr": "مخلفات النظافة العامة",
					"sla": 28,
					"incident_id": 81,
					"category_id": 6,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Waste & Cleanness",
						"ou": "Operations & cleaning services",
						"dept": "Waste Dept."
					},
					"tagsAr": ["مخلفات", "نظافة", "مخلفات عامة", "نظافة عامة", "اوساخ",
						"وسخ", "نظافة", "غير نظيف", "نفايات", "نظافة", "بيئة", "نفايات عامة"],
					"tagsEn": ["general waste", "cleaning", "cleanness", "enviroment", "dirty"],
					"tagsFr": ["déchets", "poubelles", "sales"]
				},
				{
					"code": "7",
					"nameEn": "Obstruction of Sewage network",
					"nameFr": "Obstruction of Sewage network",
					"nameAr": "إنسداد شبكة الصرف الصحي",
					"sla": 35,
					"incident_id": 69,
					"category_id": 5,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Infrastructure & Irrigation",
						"ou": "Drainage Sys. Maintainance",
						"dept": "Sewerage & Irrigation Network"
					},
					"tagsAr": [
						"انسداد",
						"شبكة الصرف",
						"انسداد صرف",
						"انسداد مياه مجاري",
						"طفح مجاري",
						"مياه مجاري",
						"مجاري",
						"ماي مجاري "
					],
					"tagsEn": [
						"blockage",
						"sewerage",
						"overflow"
					],
					"tagsFr": [
						"obstruction", "egout", "debordement"
					]
				},
				{
					"code": "8",
					"nameEn": "water Leakage",
					"nameFr": "water Leakage",
					"nameAr": "تسرب مياه الري",
					"sla": 14,
					"incident_id": 75,
					"category_id": 5,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Infrastructure & Irrigation",
						"ou": "Irrigation Operation/Maintain",
						"dept": "Sewerage & Irrigation Network"
					},
					"tagsAr": [
						"تسرب مياه",
						"ماي يطشر",
						"ماي يطير",
						"مياه",
						"ري",
						"مياه ري",
						"ماي ري",
						"ماي زراعة",
						"زراعة مياه",
						"تسرب",
						"تسريب مياه "
					],
					"tagsEn": [
						"water leakage",
						"irregation water",
						"leak"
					],
					"tagsFr": [
						"eau", "fuite", "irrigation"
					]
				}
			]
		},
		{
			"categoryId": "2",
			"nameEn": "Municipality facilities",
			"nameFr": "Installations municipales",
			"nameAr": "مرافق البلدية",
			"cssClass": "municipalityFacility",
			"services": [
				{
					"code": "19",
					"nameEn": "AC defect",
					"nameFr": "AC defect",
					"nameAr": "عطل التكييف",
					"sla": 14,
					"incident_id": 1,
					"category_id": 1,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Municipality Facilities",
						"ou": "Bldg. & Facilities Maintenance",
						"dept": "General maintainance Dept."
					},
					"tagsAr": ["عطل التكييف", "تكييف الهواء", "صيانة"],
					"tagsEn": ["AC default", "AC", "air conditioning", "maintenance"],
					"tagsFr": ["climatiseur", "maintenance"]
				},
				{
					"code": "20",
					"nameEn": "DM car drivers attitude",
					"nameFr": "DM car drivers attitude",
					"nameAr": "أسلوب قيادة المركبات",
					"sla": 21,
					"incident_id": 98,
					"category_id": 1,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Municipality Facilities",
						"ou": "Administration support section",
						"dept": "Parks & Horticulture Dept."
					},
					"tagsAr": ["مرافق", "البلدية"],
					"tagsEn": ["Municipality", "Facilities"],
					"tagsFr": []
				},
				{
					"code": "21",
					"nameEn": "Doors and windows defect",
					"nameFr": "Doors and windows defect",
					"nameAr": "عطل الأبواب والشبابيك",
					"sla": 21,
					"incident_id": 2,
					"category_id": 1,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Municipality Facilities",
						"ou": "Bldg. & Facilities Maintenance",
						"dept": "General maintainance Dept."
					},
					"tagsAr": ["ابواب", "شبابيك البلدية", "عطل باب"],
					"tagsEn": ["door", "window", "lock"],
					"tagsFr": ["porte", "fenetre", "clé", "fermé"]
				},
				{
					"code": "22",
					"nameEn": "Electricity def. DM bldg.",
					"nameFr": "Electricity def. DM bldg.",
					"nameAr": "عطل كهربائي أفرع البلدية",
					"sla": 14,
					"incident_id": 3,
					"category_id": 1,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Municipality Facilities",
						"ou": "Bldg. & Facilities Maintenance",
						"dept": "General maintainance Dept."
					},
					"tagsAr": ["عطل كهربائي", "مرافق بلدية", "بلدية"],
					"tagsEn": ["electric"],
					"tagsFr": ["electrique"]
				},
				{
					"code": "23",
					"nameEn": "Electricity def. staff res.",
					"nameFr": "Electricity def. staff res.",
					"nameAr": "عطل كهربائي سكن موظفين",
					"sla": 14,
					"incident_id": 4,
					"category_id": 1,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Municipality Facilities",
						"ou": "Bldg. & Facilities Maintenance",
						"dept": "General maintainance Dept."
					},
					"tagsAr": ["عطل كهربائي", "مرافق بلدية", "بلدية"],
					"tagsEn": ["electric"],
					"tagsFr": ["electrique"]
				},
				{
					"code": "24",
					"nameEn": "Electronic Faults",
					"nameFr": "Electronic faults",
					"nameAr": "الأعطال الألكترونية",
					"sla": 21,
					"incident_id": 5,
					"category_id": 1,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Municipality Facilities",
						"ou": "Bldg. & Facilities Maintenance",
						"dept": "General maintainance Dept."
					},
					"tagsAr": [],
					"tagsEn": [],
					"tagsFr": []
				},
				{
					"code": "25",
					"nameEn": "Fire equipments defect",
					"nameFr": "Fire equipments defect",
					"nameAr": "عطل أجهزة الحريق",
					"sla": 7,
					"incident_id": 6,
					"category_id": 1,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Municipality Facilities",
						"ou": "Bldg. & Facilities Maintenance",
						"dept": "General maintainance Dept."
					},
					"tagsAr": ["عطل أجهزة الحريق"],
					"tagsEn": ["fire equipments", "fire alarm"],
					"tagsFr": ["equipement", "feu", "alarme"]
				},
				{
					"code": "26",
					"nameEn": "General notes about souqs",
					"nameFr": "General notes about souqs",
					"nameAr": "ملاحظات على الأسواق العامة",
					"sla": 21,
					"incident_id": 11,
					"category_id": 1,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Municipality Facilities",
						"ou": "Markets Section",
						"dept": "Assests Dept."
					},
					"tagsAr": [],
					"tagsEn": ["general notes about souq", "note"],
					"tagsFr": ["souq"]
				},
				{
					"code": "27",
					"nameEn": "Health works defect",
					"nameFr": "Health works defect",
					"nameAr": "عطل أعمال صحية",
					"sla": 14,
					"incident_id": 7,
					"category_id": 1,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Municipality Facilities",
						"ou": "Bldg. & Facilities Maintenance",
						"dept": "General maintainance Dept."
					},
					"tagsAr": ["مباني بلدية", "مرفق بلدية", "مرافق بلدية", "صيانة بلدية", "الصيانة"],
					"tagsEn": [],
					"tagsFr": []
				},
				{
					"code": "28",
					"nameEn": "Lefts defect",
					"nameFr": "Lefts defect",
					"nameAr": "عطل المصاعد",
					"sla": 7,
					"incident_id": 8,
					"category_id": 1,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Municipality Facilities",
						"ou": "Bldg. & Facilities Maintenance",
						"dept": "General maintainance Dept."
					},
					"tagsAr": [],
					"tagsEn": ["lefts", "defe"],
					"tagsFr": ["abondonné"]
				},
				{
					"code": "29",
					"nameEn": "Maintainance of parks utensils",
					"nameFr": "Maintainance of parks utensils",
					"nameAr": "صيانة مرافق الحدائق العامة",
					"sla": 21,
					"incident_id": 12,
					"category_id": 1,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Municipality Facilities",
						"ou": "Bldg. & Facilities Maintenance",
						"dept": "General maintainance Dept."
					},
					"tagsAr": ["صيانة", "الصيانة", "مرافق بلدية", "حدائق", "مبنى بلدية", "البلدية"],
					"tagsEn": ["maintainance of parks", "parks"],
					"tagsFr": ["maintenance", "park"]
				},
				{
					"code": "30",
					"nameEn": "Security of DM buildings",
					"nameFr": "Security of DM buildings",
					"nameAr": "أمن مباني بلدية دبي",
					"sla": 21,
					"incident_id": 9,
					"category_id": 1,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Municipality Facilities",
						"ou": "Security & Admin Services",
						"dept": "Assests Dept."
					},
					"tagsAr": [],
					"tagsEn": [],
					"tagsFr": []
				},
				{
					"code": "31",
					"nameEn": "Under const. park",
					"nameFr": "Under const. park",
					"nameAr": "الحدائق تحت الإنشاء",
					"sla": 21,
					"incident_id": 10,
					"category_id": 1,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Municipality Facilities",
						"ou": "General proj",
						"dept": "General proj"
					},
					"tagsAr": [],
					"tagsEn": ["under construction", "park"],
					"tagsFr": ["construction", "travaux"]
				}
			]
		},
		{
			"categoryId": "3",
			"nameEn": "Animals & pest control",
			"nameFr": "Animaux et contrôles anti-parasitaire",
			"nameAr": "حيوانات ومكافحة الحشرات",
			"cssClass": "pestBirdsPets",
			"services": [
				{
					"code": "32",
					"nameEn": "Animals In Residential Areas",
					"nameFr": "Animals In Residential Areas",
					"nameAr": "تربية حيوانات في المناطق السكنية",
					"sla": 21,
					"incident_id": 13,
					"category_id": 2,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Animals & Pest Control",
						"ou": "Veterinary Services Section",
						"dept": "public health services dept."
					},
					"tagsAr": ["حيوانات", "تربية حيوانات", "يربون حيوانات", "حيوانات في البيت", "بيت يربي حيوانات"],
					"tagsEn": ["pets", "breeding", "animals"],
					"tagsFr": ["animaux", "reproduction"]
				},
				{
					"code": "33",
					"nameEn": "Bird Flu",
					"nameFr": "Bird Flu",
					"nameAr": "إنفلونزا الطيور",
					"sla": 7,
					"incident_id": 14,
					"category_id": 2,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Animals & Pest Control",
						"ou": "Veterinary Services Section",
						"dept": "public health services dept."
					},
					"tagsAr": ["دواجن مجمدةـتخلص من جوادن", "تخلص من حيوانات", "تخلص دواجن", "حيوانات تخلص", "طيور", "طير مصاب"],
					"tagsEn": ["frozen", "disposal"],
					"tagsFr": ["gelé", "dépotoir", "dépôt"]
				},
				{
					"code": "34",
					"nameEn": "Left animals",
					"nameFr": "Left animals",
					"nameAr": "حيوانات سائبة",
					"sla": 28,
					"incident_id": 18,
					"category_id": 2,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Animals & Pest Control",
						"ou": "Veterinary Services Section",
						"dept": "public health services dept."
					},
					"tagsAr": ["حيوان في الشارع", "حيوانات", "حيوانات في الشارع", "جمال في الشارع"],
					"tagsEn": ["left animal"],
					"tagsFr": ["animal", "abondonné"]
				},
				{
					"code": "35",
					"nameEn": "Lost Pets",
					"nameFr": "Lost Pets",
					"nameAr": "حيوانات أليفة مفقودة",
					"sla": 21,
					"incident_id": 16,
					"category_id": 2,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Animals & Pest Control",
						"ou": "Veterinary Services Section",
						"dept": "public health services dept."
					},
					"tagsAr": ["حيوان مفقود", "حيوان ضايع", "حيوان ضائع", "قط مفقود", "قط ضائع", "كلب ضائع", "كلاب", "حيوانات", "قطط"],
					"tagsEn": ["lost pet"],
					"tagsFr": ["perdu"]
				},
				{
					"code": "36",
					"nameEn": "Pests Proliferate/ pub. Place",
					"nameFr": "Pests Proliferate/ pub. Place",
					"nameAr": "تكاثرالحشرات في المواقع العامة",
					"sla": 28,
					"incident_id": 17,
					"category_id": 2,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Animals & Pest Control",
						"ou": "Pest Control Section",
						"dept": "public health services dept."
					},
					"tagsAr": ["حشرات", "حشرات في الشارع", "صراصير", "فيران", "فئران", "حشرات صراصير", "صرصور", "نمل", "سماسيم", "سمسوم"],
					"tagsEn": ["pest", "public places", "rats", "mouses", "ants"],
					"tagsFr": ["animaux", "place publique", "souri", "fourmi", "rat", "cafard"]
				},
				{
					"code": "37",
					"nameEn": "Stray Animals Announcement",
					"nameFr": "Stray Animals Announcement",
					"nameAr": "حتا - حيوانات سائبة",
					"sla": 14,
					"incident_id": 18,
					"category_id": 2,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Animals & Pest Control",
						"ou": "Hatta Center",
						"dept": "Hatta Center"
					},
					"tagsAr": ["حتا", "بلاغات حتا", "بلاغات حتا حيوانات", "حيوانات سائبة", "بلاغات حيوانات حتا"],
					"tagsEn": ["hatta", "left animals"],
					"tagsFr": ["animal égaré", "abondonné"]
				},
				{
					"code": "38",
					"nameEn": "Stray Dogs",
					"nameFr": "Stray Dogs",
					"nameAr": "كلاب سائبة",
					"sla": 28,
					"incident_id": 19,
					"category_id": 2,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Animals & Pest Control",
						"ou": "Veterinary Services Section",
						"dept": "public health services dept."
					},
					"tagsAr": ["كلاب سائبة", "كلب في الشارع", "كلاب", "كلب", "كلب يهاجم", "كلاب تائهه", "كلب موجود"],
					"tagsEn": ["stray dog"],
					"tagsFr": ["chien errant"]
				},
				{
					"code": "39",
					"nameEn": "Stray Large Animals",
					"nameFr": "Stray Large Animals",
					"nameAr": "حيوانات كبيرة سائبة",
					"sla": 28,
					"incident_id": 20,
					"category_id": 2,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Animals & Pest Control",
						"ou": "Veterinary Services Section",
						"dept": "public health services dept."
					},
					"tagsAr": ["حيوانات", "حيوانات كبيرة", "جمال",
						"حيوان سائبة", "حيوانات سائبة", "جمال برع", "جمال في الشارع", "جمال سائبة", "حيوانات"],
					"tagsEn": ["stray cow", "stray horse", "stray"],
					"tagsFr": ["vache errant", "cheval errant", "errant"]
				},
				{
					"code": "40",
					"nameEn": "Vet. Establishments violation",
					"nameFr": "Vet. Establishments violation",
					"nameAr": "شكاوي علي المؤسسات البيطرية",
					"sla": 21,
					"incident_id": 21,
					"category_id": 2,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Animals & Pest Control",
						"ou": "Veterinary Services Section",
						"dept": "public health services dept."
					},
					"tagsAr": ["بطيرة", "حيوانات", "صالون بيطري", "ايواء حيوانات", "حيوانات بيطرية", "دكتور", "عيادة"],
					"tagsEn": ["Veterinary", "veterinary clinic", "violation"],
					"tagsFr": ["vétérinaire", "clinique", "violation"]
				},
				{
					"code": "41",
					"nameEn": "Violation of animal welfare",
					"nameFr": "Violation of animal welfare",
					"nameAr": "مخالفة قوانين الرفق بالحيوان",
					"sla": 21,
					"incident_id": 23,
					"category_id": 2,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Public Health and Safety",
						"ou": "Veterinary Services Section",
						"dept": "public health services dept."
					},
					"tagsAr": ["الرفق بالحيوان", "مافي رفع", "يعذبون الحيوان", "حيوانات", "قطط", "كلاب", "قطاو", "كلاب",
						"تعذيب", "رفق", "جائع", "ضرب", "عنف", "عطف", "عناية", "تهجم", "إهمال"],
					"tagsEn": ["Torture of animals", "violence", "attacking", "assault", "care", "starving"],
					"tagsFr": ["torture", "violence", "attaque"]
				},
				{
					"code": "42",
					"nameEn": "Violation Related To Pet Owner",
					"nameFr": "Violation Related To Pet Owner",
					"nameAr": "مخالفات مربي حيوانات",
					"sla": 21,
					"incident_id": 22,
					"category_id": 2,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Animals & Pest Control",
						"ou": "Veterinary Services Section",
						"dept": "public health services dept."
					},
					"tagsAr": [],
					"tagsEn": ["violation related to pet owner", "pets owner"],
					"tagsFr": ["violation", "propriétaire", "animaux"]
				}
			]
		},
		{
			"categoryId": "4",
			"nameEn": "Waste & Cleanness",
			"nameFr": "Waste & Cleanness",
			"nameAr": "النظافة والنفايات",
			"cssClass": "cleanlinessWaste",
			"services": [
				{
					"code": "43",
					"nameEn": "Accidents Of Main Streets",
					"nameFr": "Accidents Of Main Streets",
					"nameAr": "حوادث الطرق السريعة",
					"sla": 7,
					"incident_id": 76,
					"category_id": 6,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Waste & Cleanness",
						"ou": "Specialized Cleaning Section",
						"dept": "Waste Dept."
					},
					"tagsAr": ["حوادث طرق", "حوادث", "مخلفات حادث", "حواث طرق سريعة", "حادث"],
					"tagsEn": ["accident", "road", "waste", "high way", "main roads"],
					"tagsFr": ["Accident", "route", "autoroute", "déchet", "poubelle"]
				},
				{
					"code": "44",
					"nameEn": "Cleanliness of main streets",
					"nameFr": "Cleanliness of main streets",
					"nameAr": "مخلفات الطرق السريعة",
					"sla": 7,
					"incident_id": 77,
					"category_id": 6,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Waste & Cleanness",
						"ou": "Specialized Cleaning Section",
						"dept": "Waste Dept."
					},
					"tagsAr": ["مخلفات", "أوساخ على الطريق السريع", "مخلفات نظافة", "الشارع مب نظيف", "بقايا", "قاذورات"],
					"tagsEn": ["waste", "garbage", "highway", "main street", "main road"],
					"tagsFr": ["poubelle", "déchets", "route", "rue principale"]
				},
				{
					"code": "45",
					"nameEn": "Cleanliness/health facilities",
					"nameFr": "Cleanliness/health facilities",
					"nameAr": "نظافة المرافق الصحية",
					"sla": 28,
					"incident_id": 78,
					"category_id": 6,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Waste & Cleanness",
						"ou": "Specialized Cleaning Section",
						"dept": "Waste Dept."
					},
					"tagsAr": ["حمام حمومي نظافة", "نظافة حمام عمومي", "حمام عام"],
					"tagsEn": ["public toilet", "public washroom"],
					"tagsFr": []
				},
				{
					"code": "46",
					"nameEn": "general waste removel",
					"nameFr": "general waste removel",
					"nameAr": "مخلفات النظافة العامة",
					"sla": 28,
					"incident_id": 81,
					"category_id": 6,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Waste & Cleanness",
						"ou": "Operations & cleaning services",
						"dept": "Waste Dept."
					},
					"tagsAr": ["مخلفات", "نظافة", "مخلفات عامة", "نظافة عامة", "اوساخ",
						"وسخ", "نظافة", "غير نظيف", "نفايات", "نظافة", "بيئة", "نفايات عامة"],
					"tagsEn": ["general waste", "cleaning", "cleanness", "enviroment", "dirty"],
					"tagsFr": ["déchets", "poubelles", "sales"]
				},
				{
					"code": "47",
					"nameEn": "Internal Accidents waste",
					"nameFr": "Internal Accidents waste",
					"nameAr": "حوادث الطرق الداخلية",
					"sla": 7,
					"incident_id": 79,
					"category_id": 6,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Waste & Cleanness",
						"ou": "Operations & cleaning services",
						"dept": "Waste Dept."
					},
					"tagsAr": ["حوادث", "حوادث طرق سريع", "حادث", "حادث في الشارع", "حادث في المنطقة", "حوادث سيارات", "حادث سيارة"],
					"tagsEn": ["accident", "neighbourhood"],
					"tagsFr": ["accident", "voisin", "voisinage"]
				},
				{
					"code": "48",
					"nameEn": "pile waste in containers",
					"nameFr": "pile waste in containers",
					"nameAr": "تكدس النفايات العامة بالحاويات",
					"sla": 14,
					"incident_id": 80,
					"category_id": 6,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Waste & Cleanness",
						"ou": "Operations & cleaning services",
						"dept": "Waste Dept."
					},
					"tagsAr": ["نفايات", "نفايات في الحاوية", "نفايات في الزبالة", "وايد اوساخ في الحاوية",
						"تكدس", "تكدس مخلفات", "مخلفات", "نظافة", "مافي نظافة", "مخلفات عامة"],
					"tagsEn": ["pile waste", "container full", "general waste", "house waste"],
					"tagsFr": ["dechets", "poubelle"]
				},
				{
					"code": "49",
					"nameEn": "Removal old cars Request",
					"nameFr": "Removal old cars Request",
					"nameAr": "طلب إزالة المركبات المهملة",
					"sla": 28,
					"incident_id": 99,
					"category_id": 6,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Waste & Cleanness",
						"ou": "Specialized Cleaning Section",
						"dept": "Waste Dept."
					},
					"tagsAr": ["مركبة مهملة", "مركبة", "سيارة مهملة", "سيارة",
						"سيارات مهمله", "مهملة", "مهملة", "سيارة وسخة", "سيارة مب نظيفة", "سيارات غير نظيفة"],
					"tagsEn": ["abandoned car", "left car", "old car", "scrap car", "dirty car", "unclean car"],
					"tagsFr": ["déchet", "poubelle", "agriculture"]
				}
			]
		},
		{
			"categoryId": "5",
			"nameEn": "Health & safety",
			"nameFr": "Santé et sécurité",
			"nameAr": "الصحة والسلامة ",
			"cssClass": "healthSafety",
			"services": [
				{
					"code": "50",
					"nameEn": "Beggars & Roaming buyers",
					"nameFr": "Beggars & Roaming buyers",
					"nameAr": "متسولين وباعة ومغسلي السيارات",
					"sla": 14,
					"incident_id": 24,
					"category_id": 3,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Health & Safety",
						"ou": "Markets Section",
						"dept": "Assests Dept."
					},
					"tagsAr": ["متسول", "بائع", "شحات", "متسول يبيع", "متسولين", "يغسل سيارة", "متسولين في الشارع"],
					"tagsEn": ["beggar", "roaming", "selling"],
					"tagsFr": ["mendant", "vagabond", "sdf", "vendeur", "ambulant"]
				},
				{
					"code": "51",
					"nameEn": "Cheat on Food Discriptions",
					"nameFr": "Cheat on Food Discriptions",
					"nameAr": "غش في المواصفات الغذائية",
					"sla": 21,
					"incident_id": 25,
					"category_id": 3,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Health & Safety",
						"ou": "Food Control Section",
						"dept": "Food Control Dept."
					},
					"tagsAr": ["غش في المواصفات الغذائية", "أكل", "مأكولات أغذية", "طعام", "مطعم", "مطاعم", "كافتيريا", "أكل في المطعم", "تغيير", "تعديل", "خداع",
						"مسح", "طمس", "محو", "البطاقة الغذائية", "محتويات المنتج", "مكونات المنتج", "حلال", "نباتي", "جلوتين"],
					"tagsEn": ["cheating", "food", "restaurent", "erasing", "halal", "gluten free"],
					"tagsFr": ["tricher", "nourriture", "restaurant", "halal", "gluten", "effacement"]
				},
				{
					"code": "52",
					"nameEn": "Children games",
					"nameFr": "Children games",
					"nameAr": "ألعاب الأطفال",
					"sla": 21,
					"incident_id": 26,
					"category_id": 3,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Health & Safety",
						"ou": "Consumer Products Safety",
						"dept": "Health & Saftey Dept."
					},
					"tagsAr": ["خطأ في اللعبة", "لعبة خربانة", "لعبة", "العاب", "أطفال", "دون", "سن", "غير مناسبة", "غير لائقة"],
					"tagsEn": ["toy", "toys", "inappropriate", "under age", "kids", "babies", "baby"],
					"tagsFr": ["jouets", "enfant", "bébé", "age", "inopportun", "inapproprié"]
				},
				{
					"code": "53",
					"nameEn": "Cleanliness of food company",
					"nameFr": "Cleanliness of food company",
					"nameAr": "نظافة المؤسسات الغذائية",
					"sla": 21,
					"incident_id": 27,
					"category_id": 3,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Health & Safety",
						"ou": "Food Control Section",
						"dept": "Food Control Dept."
					},
					"tagsAr": ["مطعم مب نظيف", "مطعم غير نظيف", "مطعم", "مطاعم", "أكل", "مأكولات"],
					"tagsEn": ["cleanness", "hygiene", "HACCP", "food", "cuisine"],
					"tagsFr": ["hygiene", "nourriture", "cuisine", "propreté"]
				},
				{
					"code": "54",
					"nameEn": "Compliance of labores camps",
					"nameFr": "Compliance of labores camps",
					"nameAr": "اشتراطات الصحية/ مساكن العمال",
					"sla": 21,
					"incident_id": 28,
					"category_id": 3,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Health & Safety",
						"ou": "Health Control Section",
						"dept": "Health & Saftey Dept."
					},
					"tagsAr": ["سكن العمال", "السكن غير نظيف", "اشتراطات سكن العمل", "لايوجد صحة سكن عمال"],
					"tagsEn": ["labor", "accomodation", "workers", "housing"],
					"tagsFr": ["travail", "hébergement", "travailleur", "logement"]
				},
				{
					"code": "55",
					"nameEn": "Decayed Food",
					"nameFr": "Decayed Food",
					"nameAr": "أغذية فاسدة",
					"sla": 7,
					"incident_id": 29,
					"category_id": 3,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Public Health and Safety",
						"ou": "Food Control Section",
						"dept": "Food Control Dept."
					},
					"tagsAr": ["فاسد", "كريهة", "رائحة", "غش في المواصفات الغذائية", "أكل", "مأكولات أغذية",
						"طعام", "مطعم", "مطاعم", "كافتيريا", "أكل في المطعم", "أغذية فاسدة", "أكل فاسد"],
					"tagsEn": ["decayed", "rotten", "spoiled", "HACCP", "hygiene"],
					"tagsFr": ["pourri", "HACCP", "hygiene"]
				},
				{
					"code": "56",
					"nameEn": "Env. Emergency/ Food",
					"nameFr": "Env. Emergency/ Food",
					"nameAr": "طوارئ بيئية/ أغذية",
					"sla": 14,
					"incident_id": 30,
					"category_id": 3,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Health & Safety",
						"ou": "Environmental Emergency Office",
						"dept": "Health & Saftey Dept."
					},
					"tagsAr": ["طوارئ بيئية/صرف صحي وأمطار"],
					"tagsEn": [],
					"tagsFr": []
				},
				{
					"code": "57",
					"nameEn": "Env. Emergency/Env.&Pub.Health",
					"nameFr": "Env. Emergency/Env.&Pub.Health",
					"nameAr": "الطوارئ البيئي/مباني وتخطيط",
					"sla": 14,
					"incident_id": 31,
					"category_id": 3,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Health & Safety",
						"ou": "Environmental Emergency Office",
						"dept": "Health & Saftey Dept."
					},
					"tagsAr": [],
					"tagsEn": [],
					"tagsFr": []
				},
				{
					"code": "58",
					"nameEn": "Env.Emergency/Bldg.&planning",
					"nameFr": "Env.Emergency/Bldg.&planning",
					"nameAr": "الطوارئ البيئي/مباني وتخطيط",
					"sla": 14,
					"incident_id": 32,
					"category_id": 3,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Health & Safety",
						"ou": "Environmental Emergency Office",
						"dept": "Health & Saftey Dept."
					},
					"tagsAr": [],
					"tagsEn": [],
					"tagsFr": []
				},
				{
					"code": "59",
					"nameEn": "Expired food",
					"nameFr": "Expired food",
					"nameAr": "أغذية منتهية الصلاحية",
					"sla": 21,
					"incident_id": 33,
					"category_id": 3,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Health & Safety",
						"ou": "Food Control Section",
						"dept": "Food Control Dept."
					},
					"tagsAr": ["غش في المواصفات الغذائية", "أكل", "مأكولات أغذية", "طعام", "مطعم", "مطاعم",
						"كافتيريا", "أكل في المطعم", "أغذية فاسدة", "أكل فاسد", "منتهي"],
					"tagsEn": ["expired", "date", "canned food", "supermarket", "old", "restaurent", "cafeteria"],
					"tagsFr": ["expiré", "date", "conserve", "supermarché", "vieux", "café", "cafétéria", "restaurant"]
				},
				{
					"code": "60",
					"nameEn": "Health premises issues",
					"nameFr": "Health premises issues",
					"nameAr": "مخالفات المؤسسات الصحية",
					"sla": 21,
					"incident_id": 34,
					"category_id": 3,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Health & Safety",
						"ou": "Health Control Section",
						"dept": "Health & Saftey Dept."
					},
					"tagsAr": ["صالون", "نظافة صالون", "حلاق", "حلاق مب نظيف", "يحلق", "حلاق غير نظيف", "صالون نسائي", "صالون رجالي", "حلاق بدون قفاز",
						"قص", "صبغة", "أظافر", "حرق", "شعر", "اكستنشن", "كيراتين", "فرد", "تنعيم", "التهاب"],
					"tagsEn": ["saloon", "spa", "barber", "hygiene", "hair cut", "hair color",
						"nails", "burn", "straightening", "Keratin", "extension", "manicure", "padicure"],
					"tagsFr": ["salon", "spa", "coiffeur", "cheuveux", "barbe", "keratin", "extension", "manicure", "pedicure"]
				},
				{
					"code": "61",
					"nameEn": "Health requirement of building",
					"nameFr": "Health requirement of building",
					"nameAr": "الاشتراطات الصحية للمباني",
					"sla": 21,
					"incident_id": 35,
					"category_id": 3,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Health & Safety",
						"ou": "Health Control Section",
						"dept": "Health & Saftey Dept."
					},
					"tagsAr": ["اشتراطات المباني", "نظافة المبنى", "مبنى غير صحي",
						"مبنى غير نظيف", "ممرات", "غرفة النفايات", "مكاتب", "تهوية"],
					"tagsEn": ["building cleanness", "building garbage room", "Corridors", "ventilation"],
					"tagsFr": ["nettoyage batiments", "ventillation", "corridor", "chambre", "garage", "debaras"]
				},
				{
					"code": "62",
					"nameEn": "Indoor Air Quality",
					"nameFr": "Indoor Air Quality",
					"nameAr": "نوعية الهواء الداخلي",
					"sla": 21,
					"incident_id": 36,
					"category_id": 3,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Health & Safety",
						"ou": "Public Safety Section",
						"dept": "Health & Saftey Dept."
					},
					"tagsAr": [
						"هواء",
						"لا يوجد هواء نوع الهواء",
						"تدخين",
						"روائح"
					],
					"tagsEn": [
						"smoking",
						"smell",
						"no air",
						"fresh air"
					],
					"tagsFr": [
						"fumé", "odeur", "air frais"
					]
				},
				{
					"code": "63",
					"nameEn": "Labor Accommodate in premises",
					"nameFr": "Labor Accommodate in premises",
					"nameAr": "مساكن عمال داخل منشآت صناعية",
					"sla": 21,
					"incident_id": 37,
					"category_id": 3,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Health & Safety",
						"ou": "Occupational Safety & Health",
						"dept": "Health & Saftey Dept."
					},
					"tagsAr": [
						"مساكن عمال داخل منشآت صناعية",
						"سكن عمال",
						"مصنع",
						"ورشة",
						"كراج"
					],
					"tagsEn": [
						"labor accommodate illegal",
						"sleeping in workshop",
						"sleeping in warehouse",
						"sleeping in factory"
					],
					"tagsFr": [
						"travail", "lois", "dormir", "usine", "dépôt"
					]
				},
				{
					"code": "64",
					"nameEn": "Poisoning Food",
					"nameFr": "Poisoning Food",
					"nameAr": "التسمم الغذائي",
					"sla": 7,
					"incident_id": 38,
					"category_id": 3,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Health & Safety",
						"ou": "Food Control Section",
						"dept": "Food Control Dept."
					},
					"tagsAr": [
						"تسمم غذائي",
						"مطعم تسمم",
						"مطاعم",
						"كافتيريا",
						"مستشفى"
					],
					"tagsEn": [
						"poisoning",
						"hospital",
						"report"
					],
					"tagsFr": [
						"poison", "hopital"
					]
				},
				{
					"code": "65",
					"nameEn": "Public&Occup. Safety Accidents",
					"nameFr": "Public&Occup. Safety Accidents",
					"nameAr": "حوادث السلامة المهنية والعامة",
					"sla": 21,
					"incident_id": 39,
					"category_id": 3,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Health & Safety",
						"ou": "Occupational Safety & Health",
						"dept": "Health & Saftey Dept."
					},
					"tagsAr": [],
					"tagsEn": [],
					"tagsFr": []
				},
				{
					"code": "66",
					"nameEn": "Safety of Deterg./Disinfecting",
					"nameFr": "Safety of Deterg./Disinfecting",
					"nameAr": "سلامة مواد التطهير والتعقيم",
					"sla": 21,
					"incident_id": 40,
					"category_id": 3,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Health & Safety",
						"ou": "Consumer Products Safety",
						"dept": "Health & Saftey Dept."
					},
					"tagsAr": [
						"مواد تطهير",
						"معقم",
						"معقمات",
						"مواد استهلاكية",
						"مواد معقمة",
						"صابون",
						"منظف"
					],
					"tagsEn": [
						"antiseptics",
						"disinfectant",
						"detergent",
						"sterilizer",
						"cleaner"
					],
					"tagsFr": [
						"antiseptique", "stérile", "désinfectant"
					]
				},
				{
					"code": "67",
					"nameEn": "Safety of health supplements",
					"nameFr": "Safety of health supplements",
					"nameAr": "سلامة المكملات الصحية",
					"sla": 21,
					"incident_id": 41,
					"category_id": 3,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Health & Safety",
						"ou": "Consumer Products Safety",
						"dept": "Health & Saftey Dept."
					},
					"tagsAr": [
						"فيتامينات",
						"مكملات صحية",
						"بروتين"
					],
					"tagsEn": [
						"vitamins",
						"supplements",
						"protein",
						"protin",
						"protien"
					],
					"tagsFr": [
						"vitamine", "supplément", "protein"
					]
				},
				{
					"code": "68",
					"nameEn": "Safety requirements of pools",
					"nameFr": "Safety requirements of pools",
					"nameAr": "إشتراطات سلامة الأحواض",
					"sla": 21,
					"incident_id": 42,
					"category_id": 3,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Health & Safety",
						"ou": "Public Safety Section",
						"dept": "Health & Saftey Dept."
					},
					"tagsAr": [
						"حوض",
						"سلامة الحوض",
						"مافي سلامة في الحوض",
						"مسبح"
					],
					"tagsEn": [
						"swimming pool",
						"safety",
						"drowning",
						"pool"
					],
					"tagsFr": [
						"piscine", "natation", "nager"
					]
				},
				{
					"code": "69",
					"nameEn": "Smoking in cafes",
					"nameFr": "Smoking in cafes",
					"nameAr": "التدخين في المقاهي",
					"sla": 21,
					"incident_id": 43,
					"category_id": 3,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Health & Safety",
						"ou": "Public Safety Section",
						"dept": "Health & Saftey Dept."
					},
					"tagsAr": [
						"تدخين",
						"مقاهي",
						"صحة مقاهي",
						"التدخين",
						"دخان مقاهي",
						"مقهى غيرنظيف",
						"شيشة",
						"تهوية"
					],
					"tagsEn": [
						"shisha",
						"smoking",
						"fresh air",
						"café",
						"health",
						"vaiolation"
					],
					"tagsFr": [
						"shisha", "narguile", "fumée", "violation", "santé"
					]
				},
				{
					"code": "70",
					"nameEn": "Smoking violations",
					"nameFr": "Smoking violations",
					"nameAr": "مخالفات التدخين",
					"sla": 21,
					"incident_id": 44,
					"category_id": 3,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Health & Safety",
						"ou": "Health Control Section",
						"dept": "Health & Saftey Dept."
					},
					"tagsAr": [
						"التدخين",
						"دخان",
						"يدخنون"
					],
					"tagsEn": [
						"smoking",
						"violations"
					],
					"tagsFr": [
						"fumé", "violation"
					]
				},
				{
					"code": "71",
					"nameEn": "Storing Food on the pavement",
					"nameFr": "Storing Food on the pavement",
					"nameAr": "تخزين المواد الغذائية/الرصيف",
					"sla": 21,
					"incident_id": 45,
					"category_id": 3,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Public Health and Safety",
						"ou": "Food Control Section",
						"dept": "Food Control Dept."
					},
					"tagsAr": [
						"تخزين على الرصيف",
						"مطاعم",
						"سوبرماركت",
						"بقالة",
						"ثلاجة"
					],
					"tagsEn": [
						"food storage",
						"inapproperiate food storage",
						"food storing"
					],
					"tagsFr": [
						"nourriture", "stock", "inapproprié"
					]
				},
				{
					"code": "72",
					"nameEn": "The safety of cosmetics",
					"nameFr": "The safety of cosmetics",
					"nameAr": "سلامة مستحضرات التجميل",
					"sla": 28,
					"incident_id": 46,
					"category_id": 3,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Health & Safety",
						"ou": "Consumer Products Safety",
						"dept": "Health & Saftey Dept."
					},
					"tagsAr": [
						"تجميل",
						"عطور",
						"كريمات",
						"سلامة كريم",
						"كريم منتهي",
						"كريم خربان",
						"تجميل",
						"عطور",
						"صبغة",
						"حنة",
						"مرطب",
						"لوشن"
					],
					"tagsEn": [
						"cosmetics",
						"perfume",
						"cream",
						"expired cream",
						"beauty",
						"hair color",
						"henna",
						"lotion"
					],
					"tagsFr": [
						"cosmétique", "parfum", "beauté", "cheveux, henna", "lotion", "couleur", "créme", "expiré"
					]
				}
			]
		},
		{
			"categoryId": "6",
			"nameEn": "Infrastructure & irrigation",
			"nameFr": "Infrastructure et irrigation",
			"nameAr": "الصرف الصحي والري",
			"cssClass": "Infrastructure",
			"services": [
				{
					"code": "73",
					"nameEn": "Bad Smell",
					"nameFr": "Bad Smell",
					"nameAr": "انبعاث روائح كريهة",
					"sla": 35,
					"incident_id": 66,
					"category_id": 5,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Infrastructure & Irrigation",
						"ou": "Drainage Sys. Maintainance",
						"dept": "Sewerage & Irrigation Network"
					},
					"tagsAr": [
						"روائح كريهه",
						"ريحة",
						"روائح",
						"روائح محطة المعالجة",
						"المحطة",
						"ريحة بالوعة",
						"روائح نفايات"
					],
					"tagsEn": [
						"small",
						"sewerage"
					],
					"tagsFr": [
						"petit", "égouts"
					]
				},
				{
					"code": "74",
					"nameEn": "block Entrance/Sewage projects",
					"nameFr": "block Entrance/Sewage projects",
					"nameAr": "إغلاق مدخل جراء مشاريع صرف صحي",
					"sla": 21,
					"incident_id": 73,
					"category_id": 5,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Projects and Infrastructure",
						"ou": "Drainage Projects Executive",
						"dept": "Drainage & Irrigation Dept."
					},
					"tagsAr": [],
					"tagsEn": [
						"block entrance/sewage",
						"Entrance"
					],
					"tagsFr": [
						"block", "égout", "obstruction", "entrée"
					]
				},
				{
					"code": "75",
					"nameEn": "Comments abt Sewage Treatment",
					"nameFr": "Comments abt Sewage Treatment",
					"nameAr": "ملاحظة على محطات المعالجة",
					"sla": 14,
					"incident_id": 96,
					"category_id": 5,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Infrastructure & Irrigation",
						"ou": "Sewage Treatment Plant Dept",
						"dept": "Sewerage & Irrigation Network"
					},
					"tagsAr": [],
					"tagsEn": [],
					"tagsFr": []
				},
				{
					"code": "76",
					"nameEn": "Low pressure",
					"nameFr": "Low pressure",
					"nameAr": "الضغط ضعيف",
					"sla": 14,
					"incident_id": 67,
					"category_id": 5,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Infrastructure & Irrigation",
						"ou": "Irrigation Operation/Maintain",
						"dept": "Sewerage & Irrigation Network"
					},
					"tagsAr": [
						"ضغط المياه ضعيف",
						"مياه ري",
						"ماء ري ماي ري",
						"تسرب مياه الري",
						"لا يوجد مياه ري",
						"مافي ماي ري",
						"الري",
						"ماي الري",
						"ماي الاشجار",
						"مياه ري"
					],
					"tagsEn": [
						"low pressure",
						"irrigation water",
						"plants water",
						"grass water"
					],
					"tagsFr": [
						"pression", "basse", "irrigation", "pelouse", "gazon", "plante"
					]
				},
				{
					"code": "77",
					"nameEn": "No water",
					"nameFr": "No water",
					"nameAr": "لا توجد مياه ري",
					"sla": 14,
					"incident_id": 68,
					"category_id": 5,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Infrastructure & Irrigation",
						"ou": "Irrigation Operation/Maintain",
						"dept": "Sewerage & Irrigation Network"
					},
					"tagsAr": [],
					"tagsEn": [
						"no",
						"Wate"
					],
					"tagsFr": [
						"eau", "irrigation"
					]
				},
				{
					"code": "78",
					"nameEn": "Obstruction of Sewage network",
					"nameFr": "Obstruction of Sewage network",
					"nameAr": "إنسداد شبكة الصرف الصحي",
					"sla": 35,
					"incident_id": 69,
					"category_id": 5,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Infrastructure & Irrigation",
						"ou": "Drainage Sys. Maintainance",
						"dept": "Sewerage & Irrigation Network"
					},
					"tagsAr": [
						"انسداد",
						"شبكة الصرف",
						"انسداد صرف",
						"انسداد مياه مجاري",
						"طفح مجاري",
						"مياه مجاري",
						"مجاري",
						"ماي مجاري "
					],
					"tagsEn": [
						"blockage",
						"sewerage",
						"overflow"
					],
					"tagsFr": [
						"obstruction", "egout", "debordement"
					]
				},
				{
					"code": "79",
					"nameEn": "Sand gathering/sewage Project",
					"nameFr": "Sand gathering/sewage Project",
					"nameAr": "تجمع رمال بسبب مشاريع صرف صحي",
					"sla": 14,
					"incident_id": 70,
					"category_id": 5,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Infrastructure & Irrigation",
						"ou": "Drainage Projects Executive",
						"dept": "Drainage & Irrigation Dept."
					},
					"tagsAr": [],
					"tagsEn": [
						"sand gathering",
						"sewerage project"
					],
					"tagsFr": [
						"sable"
					]
				},
				{
					"code": "80",
					"nameEn": "Sewer Blockages Announcement",
					"nameFr": "Sewer Blockages Announcement",
					"nameAr": "حتا-انسدادات المجاري",
					"sla": 14,
					"incident_id": 71,
					"category_id": 5,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Infrastructure & Irrigation",
						"ou": "Hatta Center",
						"dept": "Hatta Center"
					},
					"tagsAr": [],
					"tagsEn": [],
					"tagsFr": []
				},
				{
					"code": "81",
					"nameEn": "The covers of sewage Exits",
					"nameFr": "The covers of sewage Exits",
					"nameAr": "أغطية مخارج الصرف الصحي",
					"sla": 28,
					"incident_id": 72,
					"category_id": 5,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Infrastructure & Irrigation",
						"ou": "Drainage Sys. Maintainance",
						"dept": "Sewerage & Irrigation Network"
					},
					"tagsAr": [
						"غطاء المنهول",
						"منهول",
						"مناهيل",
						"منهولات",
						"صرف صحي منهول",
						"منهول",
						"غطاءصرف صحي أغطية صرف"
					],
					"tagsEn": [
						"covers",
						"manhole",
						"sewerage"
					],
					"tagsFr": [
						"regard", "bouche", "égout", "couvercle"
					]
				},
				{
					"code": "82",
					"nameEn": "unfix Interlock/sewage Project",
					"nameFr": "unfix Interlock/sewage Project",
					"nameAr": "عدم إعادة الإنترلوك/مشاريع صرف",
					"sla": 21,
					"incident_id": 74,
					"category_id": 5,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Infrastructure & Irrigation",
						"ou": "Drainage Projects Executive",
						"dept": "Drainage & Irrigation Dept."
					},
					"tagsAr": [],
					"tagsEn": [
						"unfix interlock/sewage",
						"sewerage work"
					],
					"tagsFr": [
						"égouts"
					]
				},
				{
					"code": "83",
					"nameEn": "water Leakage",
					"nameFr": "water Leakage",
					"nameAr": "تسرب مياه الري",
					"sla": 14,
					"incident_id": 75,
					"category_id": 5,
					"ServiceTypeId":"1",
					"area": {
						"legacyArea": "Infrastructure & Irrigation",
						"ou": "Irrigation Operation/Maintain",
						"dept": "Sewerage & Irrigation Network"
					},
					"tagsAr": [
						"تسرب مياه",
						"ماي يطشر",
						"ماي يطير",
						"مياه",
						"ري",
						"مياه ري",
						"ماي ري",
						"ماي زراعة",
						"زراعة مياه",
						"تسرب",
						"تسريب مياه "
					],
					"tagsEn": [
						"water leakage",
						"irregation water",
						"leak"
					],
					"tagsFr": [
						"eau", "fuite", "irrigation"
					]
				}
			]
		},
		{
			"categoryId": "7",
			"nameEn": "Planning & buildings",
			"nameFr": "Planification et bâtiments",
			"nameAr": "التخطيط والبناء",
			"cssClass": "planningBuilding",
			"services": [
				{
					"ServiceTypeId":"1",
					"incident_id": 82,
					"category_id": 7,
					"code": "84",
					"nameEn": "Bldg. Permit Sign unavailable",
					"nameFr": "Bldg. Permit Sign unavailable",
					"nameAr": "عدم وجود لوحة ترخيص البناء",
					"sla": 21,
					"area": {
						"legacyArea": "Planning & Buildings",
						"ou": "Building Supervision Section",
						"dept": "Building Dept."
					},
					"tagsAr": [
						"لوحة مباني , لوحة ترخيص , لا يوجد لوحة",
						"تفاصيل المشروع",
						"بيانات المشروع",
						"اسم المشروع "
					],
					"tagsEn": [
						"board",
						"project number",
						"project details",
						"permit"
					],
					"tagsFr": [
						"détail", "projet", "nombre", "permis"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 83,
					"category_id": 7,
					"code": "85",
					"nameEn": "Building collapse",
					"nameFr": "Building collapse",
					"nameAr": "إنهيار المباني المنجزة",
					"sla": 7,
					"area": {
						"legacyArea": "Planning & Buildings",
						"ou": "Building Supervision Section",
						"dept": "Building Dept."
					},
					"tagsAr": [
						"مبنى انهار ,  , تساقط من المبنى",
						"انهيار"
					],
					"tagsEn": [
						"collaps",
						"falling"
					],
					"tagsFr": [
						"effondrer","tomber"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 84,
					"category_id": 7,
					"code": "86",
					"nameEn": "Building effect on the sight",
					"nameFr": "Building effect on the sight",
					"nameAr": "تأثير مبنى على المظهر العام",
					"sla": 21,
					"area": {
						"legacyArea": "Planning & Buildings",
						"ou": "Building Inspection Section",
						"dept": "Building Dept."
					},
					"tagsAr": [
						"مبنى غير لائق",
						"نظافة مبنى",
						"المبنى يشوه المظهر",
						"مظهر المبنى",
						"منظر العام",
						"تشويه",
						"متصدع",
						"قديم",
						"متهالك",
						"شقوق",
						"متشقق",
						"مكسر"
					],
					"tagsEn": [
						"building exterior",
						"Cracked old",
						"crannied"
					],
					"tagsFr": [
						"batiment", "extérieur", "fissuré", "vieux", "fendu"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 85,
					"category_id": 7,
					"code": "87",
					"nameEn": "Building without permission",
					"nameFr": "Building without permission",
					"nameAr": "البناء بدون ترخيص",
					"sla": 21,
					"area": {
						"legacyArea": "Planning & Buildings",
						"ou": "Building Inspection Section",
						"dept": "Building Dept."
					},
					"tagsAr": [
						"بدون ترخيص",
						"يبني بدون ترخيص",
						"بناء",
						"بناء بدون ترخيص",
						"إضافات بدون تصريح",
						"تعديلات",
						"ملحق",
						"مظلة",
						"باب خلفي",
						"خيمة",
						"كرفان"
					],
					"tagsEn": [
						"license",
						"illegal",
						"permission",
						"additions",
						"tent",
						"caravan"
					],
					"tagsFr": [
						"licence", "illegale", "permission", "caravane", "tente"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 86,
					"category_id": 7,
					"code": "88",
					"nameEn": "Change the use of the building",
					"nameFr": "Change the use of the building",
					"nameAr": "تغير استخدام المبنى",
					"sla": 21,
					"area": {
						"legacyArea": "Planning & Buildings",
						"ou": "Building Inspection Section",
						"dept": "Building Dept."
					},
					"tagsAr": [
						"تغير مبنى",
						"تغير في المبنى",
						"تغير استخدام المبنى",
						"تحويل"
					],
					"tagsEn": [
						"change",
						"different use",
						"change of use",
						"illegal"
					],
					"tagsFr": [
						"change", "illegal", "utilisation"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 87,
					"category_id": 7,
					"code": "89",
					"nameEn": "Construction Noise",
					"nameFr": "Construction Noise",
					"nameAr": "إزعاج المقاولين",
					"sla": 21,
					"area": {
						"legacyArea": "Planning & Buildings",
						"ou": "Building Supervision Section",
						"dept": "Building Dept."
					},
					"tagsAr": [
						"ازعاج مقاولين",
						"ازعاج بناء",
						"ازعاج",
						"بناء",
						"عمال ازعاج بناء",
						"يزعجون البناء",
						"ضجيج بناء",
						"ضوضاء"
					],
					"tagsEn": [
						"noise",
						"construction",
						"building",
						"late work"
					],
					"tagsFr": [
						"bruit", "construction", "travail tradif", "building"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 95,
					"category_id": 7,
					"code": "90",
					"nameEn": "contruction Bldg waste",
					"nameFr": "contruction Bldg waste",
					"nameAr": "مخلفات بناء تحت الإنشاء",
					"sla": 21,
					"area": {
						"legacyArea": "Planning & Buildings",
						"ou": "Building Supervision Section",
						"dept": "Building Dept."
					},
					"tagsAr": [
					],
					"tagsEn": [
					],
					"tagsFr": [
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 88,
					"category_id": 7,
					"code": "91",
					"nameEn": "Damaged advertisement boards",
					"nameFr": "Damaged advertisement boards",
					"nameAr": "تلف لوحة إعلانية",
					"sla": 35,
					"area": {
						"legacyArea": "Planning & Buildings",
						"ou": "Advertisement Control Section",
						"dept": "Planning Dept."
					},
					"tagsAr": [
						"لوحة",
						"تلف",
						"لوحة اعلانية تالفة",
						"لوحة خربانة"
					],
					"tagsEn": [
						"advertisment",
						"ads",
						"board"
					],
					"tagsFr": [
						"publicité", "panneau"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 89,
					"category_id": 7,
					"code": "92",
					"nameEn": "Falling Construction materials",
					"nameFr": "Falling Construction materials",
					"nameAr": "تساقط مواد في مبني تحت الإنشاء",
					"sla": 21,
					"area": {
						"legacyArea": "Planning & Buildings",
						"ou": "Building Supervision Section",
						"dept": "Building Dept."
					},
					"tagsAr": [
						"تساقط مواد بناء",
						"ابنية",
						"مخلفات بناء",
						"عمال بناء",
						"مبنى قيد الانشاء",
						"مبنى مخلفات"
					],
					"tagsEn": [
						"falling construction",
						"Construction"
					],
					"tagsFr": [
						"construction", "ruine", "chute"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 90,
					"category_id": 7,
					"code": "93",
					"nameEn": "Fencing not set properly",
					"nameFr": "Fencing not set properly",
					"nameAr": "وضعية الحاجزالخارجي غيرمناسبة",
					"sla": 21,
					"area": {
						"legacyArea": "Planning & Buildings",
						"ou": "Building Supervision Section",
						"dept": "Building Dept."
					},
					"tagsAr": [
						"وضعية الحاجزالخارجي غيرمناسبة",
						"سور",
						"حاجز",
						"حواجز",
						"موقع"
					],
					"tagsEn": [
						"fencing",
						"project fencing",
						"project entance",
						"project secured"
					],
					"tagsFr": [
						"cloture", "projet", "entrée", "securisé"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 91,
					"category_id": 7,
					"code": "94",
					"nameEn": "Misplacing construction tools",
					"nameFr": "Misplacing construction tools",
					"nameAr": "سوء وضع مواد ومعدات البناء",
					"sla": 21,
					"area": {
						"legacyArea": "Planning & Buildings",
						"ou": "Building Supervision Section",
						"dept": "Building Dept."
					},
					"tagsAr": [
						"سوء وضع مواد ومعدات البناء"
					],
					"tagsEn": [
						"misplacing construction",
						"construction"
					],
					"tagsFr": [
						"construction", "égarer"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 92,
					"category_id": 7,
					"code": "95",
					"nameEn": "Several families in one house",
					"nameFr": "Several families in one house",
					"nameAr": "عدة عائلات في منزل واحد",
					"sla": 21,
					"area": {
						"legacyArea": "Planning & Buildings",
						"ou": "Building Inspection Section",
						"dept": "Building Dept."
					},
					"tagsAr": [
						"سكن عزاب",
						"عزاب"
					],
					"tagsEn": [
						"singles"
					],
					"tagsFr": [
						"célibataire", "jeunes"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 93,
					"category_id": 7,
					"code": "96",
					"nameEn": "Singles residing/livings areas",
					"nameFr": "Singles residing/livings areas",
					"nameAr": "سكن العزاب في المناطق السكنية",
					"sla": 21,
					"area": {
						"legacyArea": "Planning & Buildings",
						"ou": "Building Inspection Section",
						"dept": "Building Dept."
					},
					"tagsAr": [
						"سكن عزاب",
						"عزاب",
						"عمال في بيت",
						"عمال في فيلا",
						"سكن عزاب في فيلا"
					],
					"tagsEn": [
						"signles",
						"a lot of families",
						"workers"
					],
					"tagsFr": [
						"célibataires", "travailleurs", "familles"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 94,
					"category_id": 7,
					"code": "97",
					"nameEn": "Spelling Error/advertisement",
					"nameFr": "Spelling Error/advertisement",
					"nameAr": "خطأ إملائي بلوحة إعلانية",
					"sla": 35,
					"area": {
						"legacyArea": "Planning & Buildings",
						"ou": "Advertisement Control Section",
						"dept": "Planning Dept."
					},
					"tagsAr": [],
					"tagsEn": [
						"spelling",
						"Error",
						"mistake"
					],
					"tagsFr": [
						"erreur", "orthographe"
					]
				}
			]
		},
		{
			"categoryId": "8",
			"nameEn": "Environment & Agriculture",
			"nameFr": "Environment & Agriculture",
			"nameAr": "البيئة والزراعة",
			"cssClass": "environmentAgriculture",
			"services": [
				{
					"ServiceTypeId":"1",
					"incident_id": 47,
					"category_id": 4,
					"code": "100",
					"nameEn": "Air Pollution",
					"nameFr": "Air Pollution",
					"nameAr": "تلوث جوي",
					"sla": 28,
					"area": {
						"legacyArea": "Environment & Agriculture",
						"ou": "Environmental control Sec",
						"dept": "Environment Dept."
					},
					"tagsAr": [
						"انبعاث روائح كريهة,كريه,سيء,رائحة"
					],
					"tagsEn": [
						"smell",
						"pollution",
						"smoke"
					],
					"tagsFr": [
						"pollution", "odeur", "fumée"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 48,
					"category_id": 4,
					"code": "101",
					"nameEn": "Bad usage of beaches",
					"nameFr": "Bad usage of beaches",
					"nameAr": "سوء استخدام الشاطئ من الزوار",
					"sla": 21,
					"area": {
						"legacyArea": "Environment & Agriculture",
						"ou": "Coastal Zone & Water Mngmnt",
						"dept": "Environment Dept."
					},
					"tagsAr": [
						"شواطئ",
						"نفايات في الشاطى",
						"سوء شواطئ",
						"شاطئ",
						"ساحل"
					],
					"tagsEn": [
						"beach",
						"coast",
						"waste"
					],
					"tagsFr": [
						"plage", "côte", "déchet", "poubelle"
					]
				},
				{
					"ServiceTypeId":"1",
					"code": "102",
					"nameEn": "Cleanness of Park Facilities",
					"nameFr": "Cleanness of Park Facilities",
					"nameAr": "نظافة مرافق الحدائق",
					"sla": 21,
					"area": {
						"legacyArea": "Environment & Agriculture",
						"ou": "Parks and Recreation Section",
						"dept": "Parks & Horticulture Dept."
					},
					"tagsAr": [
						"الحديقة غير نظيفة",
						"أوساخ في الحديقة",
						"نظافة الحديقة",
						"حديقة",
						"حديقة غير نظيفة"
					],
					"tagsEn": [
						"park cleanness",
						"park",
						"waste",
						"park facilities",
						"park toilet"
					],
					"tagsFr": [
						"park", "déchets", "toilette", "propreté"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 50,
					"category_id": 4,
					"code": "103",
					"nameEn": "Collision ships/coastal side",
					"nameFr": "Collision ships/coastal side",
					"nameAr": "إصطدام السفن بالشاطئ/ الحائط",
					"sla": 21,
					"area": {
						"legacyArea": "Environment & Agriculture",
						"ou": "Coastal Zone & Water Mngmnt",
						"dept": "Environment Dept."
					},
					"tagsAr": [],
					"tagsEn": [],
					"tagsFr": [
						"toilette", "publique", "douche",  "lavabos"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 51,
					"category_id": 4,
					"code": "104",
					"nameEn": "Concealing Trees/main roads",
					"nameFr": "Concealing Trees/main roads",
					"nameAr": "أشجار حاجبة للرؤية/ طرق رئيسية",
					"sla": 21,
					"area": {
						"legacyArea": "Environment & Agriculture",
						"ou": "Horticulture Section",
						"dept": "Parks & Horticulture Dept."
					},
					"tagsAr": [
						"أشجار تحجب الرؤية",
						"شجر",
						"شجر يغطي"
					],
					"tagsEn": [
						"trees",
						"visibility",
						"trimming"
					],
					"tagsFr": [
						"arbres", "visibilité", "taille", "réduction"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 52,
					"category_id": 4,
					"code": "105",
					"nameEn": "Damage on the Marine estab.",
					"nameFr": "Damage on the Marine estab.",
					"nameAr": "أضرار بالمرافق/المنشآت البحرية",
					"sla": 21,
					"area": {
						"legacyArea": "Environment & Agriculture",
						"ou": "Coastal Zone & Water Mngmnt",
						"dept": "Environment Dept."
					},
					"tagsAr": [],
					"tagsEn": [],
					"tagsFr": [

					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 53,
					"category_id": 4,
					"code": "106",
					"nameEn": "Digging Wells with no NOC",
					"nameFr": "Digging Wells with no NOC",
					"nameAr": "حفر آبار إرتوازية بدون ترخيص",
					"sla": 21,
					"area": {
						"legacyArea": "Environment & Agriculture",
						"ou": "Resources Protection Sec.",
						"dept": "Environment Dept."
					},
					"tagsAr": [],
					"tagsEn": [
						"digging",
						"well"
					],
					"tagsFr": [
						"puits", "creusement"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 55,
					"category_id": 4,
					"code": "107",
					"nameEn": "Fallen trees on main roads",
					"nameFr": "Fallen trees on main roads",
					"nameAr": "سقوط الأشجار/الطرق الرئيسية",
					"sla": 21,
					"area": {
						"legacyArea": "Environment & Agriculture",
						"ou": "Horticulture Section",
						"dept": "Parks & Horticulture Dept."
					},
					"tagsAr": [
						"اشجار",
						"شجر",
						"سقوط شجر",
						"شجر تالف",
						"شجر خربان",
						"اشجار",
						"نخيل",
						"زراعة"
					],
					"tagsEn": [
						"fallen trees on main",
						"tree in main street",
						"main street landscape"
					],
					"tagsFr": [
						"zone verte", "fleurs", "agriculture", "arbre", "pelouse", "rose"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 56,
					"category_id": 4,
					"code": "108",
					"nameEn": "Fallen trees/internal roads",
					"nameFr": "Fallen trees/internal roads",
					"nameAr": "سقوط الأشجارعلى الطرق الداخلية",
					"sla": 21,
					"area": {
						"legacyArea": "Environment & Agriculture",
						"ou": "Horticulture Services Section",
						"dept": "Parks & Horticulture Dept."
					},
					"tagsAr": [
						"اشجار",
						"شجر",
						"سقوط شجر",
						"شجر تالف",
						"شجر خربان",
						"اشجار",
						"نخيل",
						"زراعة",
						"شجر على الشارع",
						"شجر طريق سريع"
					],
					"tagsEn": [
						"tree",
						"internal road",
						"neighbourhood tree",
						"tree inforont of house"
					],
					"tagsFr": [
						"arbre", "route", "voisinage", "voisin"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 57,
					"category_id": 4,
					"code": "109",
					"nameEn": "Fishing",
					"nameFr": "Fishing",
					"nameAr": "الصيد البحري",
					"sla": 21,
					"area": {
						"legacyArea": "Environment & Agriculture",
						"ou": "Resources Protection Sec.",
						"dept": "Environment Dept."
					},
					"tagsAr": [
						"صيد",
						"صيد بحري",
						"يصيدون",
						"صيد بدون رخصة",
						"رخصة صيد"
					],
					"tagsEn": [
						"fishing",
						"illegal fishing"
					],
					"tagsFr": [
						"peche", "illegal"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 58,
					"category_id": 4,
					"code": "110",
					"nameEn": "Land Pollution",
					"nameFr": "Land Pollution",
					"nameAr": "تلوث بري",
					"sla": 28,
					"area": {
						"legacyArea": "Environment & Agriculture",
						"ou": "Environmental control Sec",
						"dept": "Environment Dept."
					},
					"tagsAr": [
						"تلوث بري",
						"تسرب",
						"مواد خطرة",
						"أصباغ",
						"ألوان"
					],
					"tagsEn": [
						"pollution",
						"paint",
						"leakage",
						"seepage"
					],
					"tagsFr": [
						"pollution", "peinture", "fuite", "ecoulement"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 59,
					"category_id": 4,
					"code": "111",
					"nameEn": "Maint. For Park Sports Area",
					"nameFr": "Maint. For Park Sports Area",
					"nameAr": "صيانة الملاعب الرياضية",
					"sla": 21,
					"area": {
						"legacyArea": "Environment & Agriculture",
						"ou": "Parks and Recreation Section",
						"dept": "Parks & Horticulture Dept."
					},
					"tagsAr": [
						"صيانة الملاعب",
						"ملعب خربان",
						"العشب خربان",
						"صيانة ملعب"
					],
					"tagsEn": [
						"playground",
						"play field"
					],
					"tagsFr": [
						"cour", "recreation"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 60,
					"category_id": 4,
					"code": "112",
					"nameEn": "Maint. Park Play Area",
					"nameFr": "Maint. Park Play Area",
					"nameAr": "صيانة ألعاب الحدائق",
					"sla": 21,
					"area": {
						"legacyArea": "Environment & Agriculture",
						"ou": "Parks and Recreation Section",
						"dept": "Parks & Horticulture Dept."
					},
					"tagsAr": [
						"صيانة",
						"صيانة العاب",
						"لعبة خربانه",
						"ترفيه",
						"ترفيهي",
						"منطقة ألعاب",
						"أطفال",
						"حديقة"
					],
					"tagsEn": [
						"games",
						"rides",
						"play area",
						"maintenance",
						"entertainment"
					],
					"tagsFr": [
						"jeux", "aire de jeu", "maintenance", "manèges"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 61,
					"category_id": 4,
					"code": "113",
					"nameEn": "Maintenance greenery/main road",
					"nameFr": "Maintenance greenery/main road",
					"nameAr": "صيانة مسطحات خضراء/ طريق عام",
					"sla": 21,
					"area": {
						"legacyArea": "Environment & Agriculture",
						"ou": "Horticulture Section",
						"dept": "Parks & Horticulture Dept."
					},
					"tagsAr": [
						"صيانة مسطحات خضراء",
						"طريق عام"
					],
					"tagsEn": [
						"maintenance greenery/main",
						"green"
					],
					"tagsFr": [
						"maintenance", "vert"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 62,
					"category_id": 4,
					"code": "114",
					"nameEn": "Marine pollution",
					"nameFr": "Marine pollution",
					"nameAr": "تلوث بحري",
					"sla": 28,
					"area": {
						"legacyArea": "Environment & Agriculture",
						"ou": "Coastal Zone & Water Mngmnt",
						"dept": "Environment Dept."
					},
					"tagsAr": [
						"تلوث بحري",
						"بقعة زيت",
						"مد أحمر",
						"سمك",
						"نافق",
						"نفوق أسماك",
						"كيميائي"
					],
					"tagsEn": [
						"pollution",
						"red tide",
						"oil spill",
						"dead fish",
						"chemicals"
					],
					"tagsFr": [
						"pollution", "marrée rouge", "poisson mort", "marée noire", "chimique"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 63,
					"category_id": 4,
					"code": "115",
					"nameEn": "Natural reservatories",
					"nameFr": "Natural reservatories",
					"nameAr": "المحميات الطبيعية",
					"sla": 21,
					"area": {
						"legacyArea": "Environment & Agriculture",
						"ou": "Resources Protection Sec.",
						"dept": "Environment Dept."
					},
					"tagsAr": [
						"المحميات الطبيعية",
						"محمية"
					],
					"tagsEn": [
						"natural",
						"Nature reserve"
					],
					"tagsFr": [
						"naturel", "nature", "réserve"
					]
				},
				{
					"ServiceTypeId":"1",
					"incident_id": 64,
					"category_id": 4,
					"code": "116",
					"nameEn": "Noises",
					"nameFr": "Noises",
					"nameAr": "ضجيج",
					"sla": 28,
					"area": {
						"legacyArea": "Environment & Agriculture",
						"ou": "Environmental control Sec",
						"dept": "Environment Dept."
					},
					"tagsAr": [
						"إزعاج",
						"ضوضاء",
						"مولد",
						"ورشة"
					],
					"tagsEn": [
						"noise",
						"generator",
						"workshop noise"
					],
					"tagsFr": [
						"bruit", "générer", "atelier"
					]
				}
			]
		}
	]
};

//MobileMasterService

function getCommonServices() {
	return categories.categories[0];
}

function getCategories() {
	return categories;
}

function getServicesById(id) {
	for (var i=0; i < categories.services.length; i++) {
		if (categories.services[i].categoryId === id) {
			return categories.services[i];
		}
	}
}

//todo get Areas

function _getCategories()
{
	var input = {
		method: 'GET',
		returnedContentType: 'json',
		path: 'smartcrmapi.dm.gov.ae/MobileMasterService/api/Service_Requests',
	}

	try {
		var response = MFP.Server.invokeHttp(input);
		MFP.Logger.info(JSON.stringify(response));
		return response;
	} catch (e) {
		return {
			isSuccessful: false,
			error: e.toString()
		};
	}
}

function getRequests()
{
	var input = {
		method: 'GET',
		returnedContentType: 'json',
		path: 'smartcrmapi.dm.gov.ae/MobileMasterService/api/Service_Requests',
	}

	try {
		var response = MFP.Server.invokeHttp(input);
		MFP.Logger.error('####### Get Service Requests #######');
		MFP.Logger.error(JSON.stringify(response));
		MFP.Logger.error('####### Get Service Requests #######');
		return response;
	} catch (e) {
		return {
			isSuccessful: false,
			error: e.toString()
		};
	}
}


function getIncidents()
{
	var input = {
		method: 'GET',
		returnedContentType: 'json',
		path: 'smartcrmapi.dm.gov.ae/MobileMasterService/api/incidents',
	}

	try {
		var response = MFP.Server.invokeHttp(input);
		MFP.Logger.error('####### Get Incidents #######');
		MFP.Logger.error(JSON.stringify(response));
		MFP.Logger.error('####### Get Incidents #######');
		return response;
	} catch (e) {
		return {
			isSuccessful: false,
			error: e.toString()
		};
	}
}

function getAreas()
{
	var input = {
		method: 'GET',
		returnedContentType: 'json',
		path: 'smartcrmapi.dm.gov.ae/MobileMasterService/api/Areas',
	}

	try {
		var response = MFP.Server.invokeHttp(input);
		MFP.Logger.error('####### Get Areas #######');
		MFP.Logger.info(JSON.stringify(response));
		MFP.Logger.error('####### Get Areas #######');
		return response;
	} catch (e) {
		return {
			isSuccessful: false,
			error: e.toString()
		};
	}
}

function petTypes()
{
	var input = {
		method: 'GET',
		returnedContentType: 'json',
		path: 'smartcrmapi.dm.gov.ae/MobileMasterService/api/Pet_Types',
	}

	try {
		var response = MFP.Server.invokeHttp(input);
		WMFPL.Logger.info(JSON.stringify(response));
		return response;
	} catch (e) {
		return {
			isSuccessful: false,
			error: e.toString()
		};
	}
}

function insectTypes()
{
	var input = {
		method: 'GET',
		returnedContentType: 'json',
		path: 'smartcrmapi.dm.gov.ae/MobileMasterService/api/Insect_Types',
	}

	try {
		var response = MFP.Server.invokeHttp(input);
		MFP.Logger.info(JSON.stringify(response));
		return response;
	} catch (e) {
		return {
			isSuccessful: false,
			error: e.toString()
		};
	}
}


function getAllServices() {
	var inputInsect = {
		method: 'GET',
		returnedContentType: 'json',
		path: 'smartcrmapi.dm.gov.ae/MobileMasterService/api/incidents',
	};

	var inputServices = {
		method: 'GET',
		returnedContentType: 'json',
		path: 'smartcrmapi.dm.gov.ae/MobileMasterService/api/Service_Requests',
	};

	try {
		var responseInsect = MFP.Server.invokeHttp(inputInsect);
		var responseService = MFP.Server.invokeHttp(inputServices);

		MFP.Logger.error('####### Get Incidents #######');
		MFP.Logger.error(JSON.stringify(responseInsect));
		MFP.Logger.error('####### Get Incidents #######');

		MFP.Logger.error('####### Get Services #######');
		MFP.Logger.error(JSON.stringify(responseService));
		MFP.Logger.error('####### Get Services #######');

		//var s= responseInsect.concat(responseService);
		return Object.assign(responseInsect, responseService);;

	} catch (e) {
		return {
			isSuccessful: false,
			error: e.toString()
		};
	}



}

