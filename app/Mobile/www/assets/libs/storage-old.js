
/* JavaScript content from assets/libs/storage-old.js in folder common */

/* JavaScript content from js/libs/storage-old.js in folder common */
angular.module('ngStorage', [])
.factory('$DbStorageService',['$window', '$q', '$logger', '$config',
                              function($window, $q, $logger, $config){
		
		//indexedDB.deleteDatabase('iDubai_Corporates_Data');		
		var factory = {
			dbName     : $config.cache.dbName,
			dbDesc     : $config.cache.dbDesc,
			objectStore: $config.cache.objectStore,
			serviceID  : $config.cache.serviceID,
			version    : $config.cache.version,	
			bSize      : $config.cache.bSize, // big size
			mSize      : $config.cache.mSize, // medium size
			lSize      : $config.cache.lSize, // low size
			storageType : $config.cache.storageType,
			clearDbCashe  : $config.cache.clearDbCashe,
			expiredCasheDate: $config.cache.expiredCasheDate, // not implemented yet,
			db : null
		}; 
		
		var openDatabase = $window.openDatabase;
		var indexedDB = $window.indexedDB || $window.mozIndexedDB || $window.webkitIndexedDB || $window.msIndexedDB;
		//var db = null; 
		var dbs = {
			INEDEXED: "INDEXED",
			WEBSQL: "WEBSQL"
		}; 
		
		/*var isModernIdb = function() {
		   var dbName = '__checkModernIdb';
		   var result = (indexedDB.open(dbName, 1).onupgradeneeded === null); // else undefined
		   return result;
		};*/
	 
		
		/*** IndexedDBStorage ***/
		 var getIndexedDBStorage = function () {			 
			return indexedDB != null ? dbs.INDEXED : null;
		};
		
		/*** WebSqlStorage ***/
		var getWebSqlStorage = function () {
			return openDatabase != null ? dbs.WEBSQL : null;
		};
		
		/*** chek fo DB Type ***/
		var getDbStorageType = function(){ 
			 return  getIndexedDBStorage()||getWebSqlStorage();  
		};		
		
		/*** init/open IndexedDB ***/
		var initIndexedDB = function(){ 
			var deferred = $q.defer();  
			var request = indexedDB.open(factory.dbName);	 
  
			request.onupgradeneeded = function(e) {
				factory.db = e.target.result;	  
				e.target.transaction.onerror = indexedDB.onerror;		
				var store = factory.db.createObjectStore(factory.objectStore, { keyPath: 'key'});			
				if(!factory.db.objectStoreNames.contains("key")) {
					factory.db.createObjectStore("key", {keyPath:"key"}, { unique: true });
				}
				if(!factory.db.objectStoreNames.contains("value")) {
					factory.db.createObjectStore("value", {keyPath:"value"});
				} 
			};	  
			request.onsuccess = function(e) {
				 
				 factory.db = e.target.result;
				 deferred.resolve(factory.db);
			};	  
			request.onerror = function(){
			  deferred.reject();
			};	
			return deferred.promise;
		};
		
		/*** init/open WebSql ***/
		var initWebSqlDB = function(){
			var deferred = $q.defer();   
			
			if(factory.db != null) {deferred.resolve();return deferred.promise;}
			try {
				factory.db = openDatabase(factory.dbName, factory.version, factory.dbDesc, factory.bSize);
			} catch(e) { 
				try{
					factory.db = openDatabase(factory.dbName, factory.version, factory.dbDesc, factory.mSize);
				}catch(e){ 
					factory.db = openDatabase(factory.dbName, factory.version, factory.dbDesc, factory.lSize);
				}
			}
			factory.db.transaction(function (tx) {
				tx.executeSql('CREATE TABLE IF NOT EXISTS '+factory.objectStore+' (key TEXT PRIMARY KEY, value TEXT)', [], function () {
					//emr.fire('storageLoaded', self);
				});
				deferred.resolve();
			});	
		
			return deferred.promise;
		};
		 
	/*** CRUD operations for IndexedDB ***/
	IndexedDBfactory = {
			init: function() {			  
				try {				
					initIndexedDB().then(function(data){
						$logger.log("Indexed initialized successufully");  
					});	 

					if (typeof(indexedDB) == "undefined"){
							throw "Browser does not support indexedDB";
					}					
					return {
						add: function(key, value) {
							
							var deferred = $q.defer();   
							var transaction = factory.db.transaction([factory.objectStore], 'readwrite');
							key = factory.serviceID + "_" + key;
							
							if(factory.db === null){
							  deferred.reject("IndexDB is not opened yet!");
							}else{								
								var transaction = factory.db.transaction([factory.objectStore], 'readwrite');
								transaction.oncomplete = function(e){
									deferred.resolve(true);
								};
								transaction.onerror = function(e){
									//$logger.log("something went wrong: "+ e.target.errorCode);
									deferred.reject(false);
								};
								var objectStore = transaction.objectStore(factory.objectStore);
								objectStore.put({key: key, value: value});
							}
							return deferred.promise;
						},
						addAll: function(data) {
							
							var deferred = $q.defer();  
							var length = data.length;
							
							if(factory.db === null){
							  deferred.reject("IndexDB is not opened yet!");
							}else{							
								var transaction = factory.db.transaction([factory.objectStore], 'readwrite');
								transaction.oncomplete = function(e){
									deferred.resolve(true);
								};
								transaction.onerror = function(e){ 
									deferred.reject(false);
								};
								var objectStore = transaction.objectStore(factory.objectStore);
								for (var i = 0; i < length; i++) {
									objectStore.put({key: data[i].key, value: data[i].value});
								}
							}							 
							return deferred.promise;
						},
						remove: function(key) {
						
							var deferred = $q.defer(); 

							if(factory.db === null){
							  deferred.reject("IndexDB is not opened yet!");
							}else{
							
							 var transaction = factory.db.transaction([factory.objectStore], 'readwrite');
							 var objectStore = transaction.objectStore(factory.objectStore);							
							 key = factory.serviceID + "_" + key;
							 var request = objectStore.delete(key);							
							  request.onsuccess = function(e) {
								deferred.resolve(true);
							  };							
							  request.onerror = function(e) { 
								deferred.reject(false);
							  };
							} 
							return deferred.promise;
						},
						clear: function() {
							  
							var deferred = $q.defer(); 
							//indexedDB.deleteDatabase(factory.dbName);	
							//deferred.resolve("true");
							//return deferred.promise;
							 
							if(factory.db === null){
							  deferred.resolve("DB is clear!");
							}else{
							  try{
									var trans = factory.db.transaction(factory.objectStore, 'readwrite');
									var store = trans.objectStore(factory.objectStore);   
									
									var keyRange = IDBKeyRange.lowerBound(0);
									var cursorRequest = store.openCursor(keyRange);
									cursorRequest.onerror = function(e){ 
										deferred.reject("Something went wrong!!!");
									  };
									cursorRequest.onsuccess = function(evt) {   
										var cursor = evt.target.result;
										if (cursor) { 
											if(cursor.value.key.substr(0, factory.serviceID.length) == factory.serviceID){
												//remove(cursor.value.key.replace(factory.serviceID + "_", "")); 
												 var request = store.delete(cursor.value.key); 
												  request.onsuccess = function(e) {
													deferred.resolve(e);
												  }; 
												  request.onerror = function(e) { 
													deferred.reject("Item couldn't be deleted");
												  };  
											}
											cursor.continue();
										} else {
											deferred.resolve();
										}
									};
								}catch (e) {
									try{
									   if (factory.db != null) {
										 var store = factory.db.transaction(factory.objectStore, "readwrite").objectStore(factory.objectStore); 
										 store.clear().onsuccess = function(event) { 
												 deferred.resolve();
										  };
									   }
									}catch(e){
										$logger.log(e)
									}
									deferred.resolve();
									$logger.log(e);
								}
							}
							return deferred.promise;
						},
						get: function(key) { 
							var deferred = $q.defer();  
							try{ 
								key = factory.serviceID + "_" + key;   
								var request = factory.db.transaction(factory.objectStore).objectStore(factory.objectStore).get(key);
								request.onsuccess = function(e) { 
									deferred.resolve(e.result);
							    };								
								request.onerror = function(e) { 
									deferred.reject("Item couldn't be found");
								};   
								 
								 
							}catch(e){
								$logger.log(e);
							}				
							return deferred.promise; 
						},
						getAll: function() { 
							var deferred = $q.defer(); 

							if(factory.db === null){
							  deferred.reject("IndexDB is not opened yet!");
							} else{
							  var trans = factory.db.transaction([factory.objectStore], "readwrite");
							  var store = trans.objectStore(factory.objectStore);
							  var images = [];
							
							  // Get everything in the store;
							  var keyRange = IDBKeyRange.lowerBound(0);
							  var cursorRequest = store.openCursor(keyRange);
							
							  cursorRequest.onsuccess = function(e) {
								var result = e.target.result;
								if(result === null || result === undefined){
								  deferred.resolve(images);
								}else{
									console.log("result",result);
								  images.push(result.value); 
								  result.continue();
								}
							  };								
							  cursorRequest.onerror = function(e){ 
								deferred.reject("Something went wrong!!!");
							  };
							} 
							return deferred.promise;
						},
						findAllByKeyType: function(keyType) { 
							var deferred = $q.defer(); 

							if(factory.db === null){
							  deferred.reject("IndexDB is not opened yet!");
							} else{
							  var trans = factory.db.transaction([factory.objectStore], "readwrite");
							  var store = trans.objectStore(factory.objectStore);
							  var images = [];
							
							  // Get everything in the store;
							  var keyRange = IDBKeyRange.lowerBound(0);
							  var cursorRequest = store.openCursor(keyRange);
							
							  cursorRequest.onsuccess = function(e) {
								var result = e.target.result;
								if(result === null || result === undefined){
								  deferred.resolve(images);
								}else{ 
									if(result.value.key.indexOf(keyType) != -1){
										images.push(result.value); 
									} 
								  result.continue();
								}
							  };								
							  cursorRequest.onerror = function(e){ 
								deferred.reject("Something went wrong!!!");
							  };
							} 
							return deferred.promise;
						}
					} 
				} catch (err) {
					$logger.error(err);
				}
			}
			
		};  
	
		/*** CRUD operations for WebSqlDB ***/
		webSqlDbfactory = {
			init: function() { 
				try {				
					initWebSqlDB().then(function(data){
						//$logger.log("WebSqlDb initialized successufully");
					});	

					if (typeof(openDatabase) == "undefined"){
							throw "Browser does not support openDatabase";
					}
					
						return {
							add: function(key, value) {								
								var deferred = $q.defer();  
								var key = factory.serviceID + "_" + key; 
								if(factory.db === null){
								  deferred.reject("WebSqlDB is not opened yet!");
								}else{
								   try{
									factory.db.transaction(function(tx) {  
										tx.executeSql('INSERT OR REPLACE INTO '+factory.objectStore+' (key, value) VALUES (?, ?)', [key, value], function(tx, results){
										deferred.resolve(true);
									}, function(tx, e){ 
										deferred.reject(false);
										}); 
									}); 
								  }catch (ee) {
										$logger.log(ee);
								  }   
								}
								return deferred.promise;
							},
							addAll: function(data) {
								
								var deferred = $q.defer();  
								if(factory.db === null){
								  deferred.reject("IndexDB is not opened yet!");
								}else{  
								 var length = data.length; 
								 try{
									factory.db.transaction(function(tx) {
										for (var i = 0; i < length; i++) {
											key = factory.serviceID + "_" + data[i].key;
											tx.executeSql('INSERT OR REPLACE INTO '+factory.objectStore+' (key, value) VALUES (?, ?)', [key, data[i].value], function(tx, results){
											//tx.executeSql(query, values, function(tx, results) {
												 
													deferred.resolve(true);
												 
												
											}, function(tx, e){
												$logger.log("There has been an error: " + e.message);
												deferred.reject(false);
											});
										}
									}); 
								  }catch (ee) {
										$logger.log(ee);
								  }  
								}
								return deferred.promise;
							},
							remove: function(key) {
								var deferred = $q.defer();
								if(factory.db === null){
								  deferred.reject("DB is clear!");
								}else{		
									 key = factory.serviceID + "_" + key;
									try{
										factory.db.transaction(function(tx) {
											tx.executeSql('DELETE FROM '+factory.objectStore+' WHERE key = ? ', [key], function(tx, results){ 
												 
												deferred.resolve(true);
											}, function(tx, e){
												$logger.log("There has been an error: " + e.message);
												deferred.reject(false);
											});
										}); 
									  }catch (ee) {
											$logger.log(ee);
									  } 								  
								}  
								return deferred.promise;
							},
							clear: function() {
								 
								var deferred = $q.defer();
								if(factory.db === null){
								  deferred.reject("DB is clear!");
								}else{								  
								  try{
									factory.db.transaction(function(tx) {
										tx.executeSql('DELETE FROM '+factory.objectStore+' WHERE key LIKE ? ', [factory.serviceID+"%"], function(tx, results){ 
											deferred.resolve(results);
										}, function(tx, e){
											$logger.log("There has been an error: " + e.message);
											deferred.reject();
										});
									}); 
								  }catch (ee) {
										$logger.log(ee);
								  } 
								} 
								return deferred.promise;
							},
							get: function(key) { 
								var deferred = $q.defer(); 
								if(factory.db === null){
								  deferred.reject("IndexDB is not opened yet!");
								} else{	 
									try{
										key = factory.serviceID + "_" + key;
										factory.db.transaction(function(tx) {
											tx.executeSql('SELECT * FROM '+factory.objectStore+' WHERE key = ?', [key], function(tx, results){  
												deferred.resolve(results.rows.length > 0 ? results.rows.item(0) : undefined);
											}, function(tx, e){
												$logger.log("There has been an error: " + e.message);
												deferred.reject();
											});
										});
									  }catch (ee) {
											$logger.log(ee);
									  } 
								}	 
								return deferred.promise;
							},
							getAll: function() { 
								var deferred = $q.defer();  
								if(factory.db === null){
								  deferred.reject("WebSqlDB is not opened yet!");
								} else{	 
									try{
										factory.db.transaction(function(tx) {
											tx.executeSql('SELECT * FROM '+factory.objectStore+' ', [], function(tx, results){ 
												
												var tempsList = [];
												for(var i = 0; i < results.rows.length; i++) {
													console.log("result : ",results.rows.item(i));
													tempsList[i] = results.rows.item(i);
												}																								
												deferred.resolve(tempsList);
											}, function(tx, e){
												$logger.log("There has been an error: " + e.message);
												deferred.reject();
											});
										});
									  }catch (ee) {
											$logger.log(ee);
									  } 
								}	 
								return deferred.promise;
							},
							findAllByKeyType: function(keyType) { 
								var deferred = $q.defer();  
								if(factory.db === null){
								  deferred.reject("WebSqlDB is not opened yet!");
								} else{	 
									try{
										factory.db.transaction(function(tx) {
											tx.executeSql('SELECT * FROM '+factory.objectStore+' ', [], function(tx, results){ 
												
												var tempsList = [];
												for(var i = 0; i < results.rows.length; i++) { 
													if(results.rows.item(i).key.indexOf(keyType) != -1){
														tempsList[i] = results.rows.item(i);
													}													
												}																								
												deferred.resolve(tempsList);
											}, function(tx, e){
												$logger.log("There has been an error: " + e.message);
												deferred.reject();
											});
										});
									  }catch (ee) {
											$logger.log(ee);
									  } 
								}	 
								return deferred.promise;
							}
						} 
				} catch (err) {
					$logger.error(err);
				}
			} 
		}  
		
		//return getDbStorageType() == dbs.INEDEXED ? IndexedDBfactory : webSqlDbfactory; 
		
		if(factory.storageType == dbs.INEDEXED){
			return IndexedDBfactory;
			
		}else if(factory.storageType == dbs.WEBSQL){
			return webSqlDbfactory;
			
		}else{
			return getDbStorageType() == dbs.INEDEXED ? IndexedDBfactory : webSqlDbfactory;
		}		 
}]);

