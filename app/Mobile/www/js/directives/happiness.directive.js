iDubaiApp.directive('happiness', ['$auth', 'happinessService', '$q', '$rootScope', '$config', 'LoadingService',
    function ($auth, happinessService, $q, $rootScope, $config, LoadingService) {
        return {
            templateUrl: "widgets/happiness.html",
            restrict: 'E',
            replace: true,
            scope: true,
            link: function (scope, element, attrs) {
                scope.currentLang = localStorage.getItem('dm-lang');

                // User data
                var user = {
                    source: ($auth.token) ? "" : "ANONYMOUS",
                    username: ($auth.token) ? $auth.dubaiUserProfile.firstName + "  " + $auth.dubaiUserProfile.lastname : "",
                    email: ($auth.token) ? $auth.dubaiUserProfile.email : "",
                    mobile: ($auth.token) ? $auth.dubaiUserProfile.mobile : ""
                };

                // Application data
                var application = {
                    applicationID: $config.happiness.applicationID,
                    type: $config.happiness.type,
                    platform: "" + WL.Client.getEnvironment(),
                    url: $config.happiness.url,
                    notes: $config.happiness.notes
                };

                // Header data
                var header = {
                    themeColor: $config.happiness.themeColor,
                    lang: scope.currentLang
                };

                var params = {user: user, application: application, header: header};

                function getHappinessUrl() {
                    var defer = $q.defer();
                    happinessService.getHappinessParameters(params).then(
                        function (data) {
                            if (!(data.invocationResult && data.invocationResult.isSuccessful)) {
                                defer.resolve(false);
                            }
                            var happinessMeterURL = happinessService.getHappinessMeterURL(data.invocationResult);
                            $('#hapinessMeteriframe').attr('src', happinessMeterURL);
                            defer.resolve(true);
                        }, function (error) {
                            defer.resolve(false);
                        }
                    );
                    return defer.promise;
                }
                scope.hideHappiness=function (){
                    scope.modal.happiness = !scope.modal.happiness
                }
                // Show happiness popup
                $rootScope.showHappiness = function () {
                    LoadingService.show();

                    //$('body').addClass('modal-open');
                    getHappinessUrl().then(
                        function (res) {
                            LoadingService.hide();
                            scope.modal.happiness = !scope.modal.happiness;
                            /*if (res) {
                                $("#happinessPopup").css('display', 'block');
                                $(".happinessPopup").css('top', '30%');
                            }*/
                        });
                    /*var display = $('#happinessPopup').css('display');
                    if (display === "none") {
                        getHappinessUrl().then(
                            function (res) {
                                if (res) {
                                    $("#happinessPopup").css('display', 'block');
                                    $(".happinessPopup").css('top', '30%');
                                }
                            });
                    } else {
                        $("#happinessPopup").css('display', 'none');
                    }*/
                };

            }
        };
    }]);