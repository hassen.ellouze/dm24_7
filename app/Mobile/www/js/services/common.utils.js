iDubaiApp.factory("CommonUtils", function($q, LoadingService) {
        return {
            invokeGetResources : function(data){
                var dfd = $q.defer();
                var requestTimeout = 30 * 1000; //  30 seconds

                if( data && data.showLoading ){
                    LoadingService.show();
                }
                const resourceRequest = new WLResourceRequest(
                    data.path,
                    WLResourceRequest.GET,
                    requestTimeout);

                // Array of params : ['params0', 'params1', ...]
                if( data && data.params && data.params.length>0 ) {
                    resourceRequest.setQueryParameter('params', data.params);
                }

                resourceRequest.send().then(
                    function(onSuccess){
                        console.log("onSuccess ", onSuccess);
                        LoadingService.hide();
                        dfd.resolve(onSuccess);
                    },
                    function(onFailure){
                        console.log("onFailure ", onFailure);
                        LoadingService.hide();
                        dfd.reject(onFailure);
                    }
                )
                return dfd.promise;
            },
            invokePostResources : function(data){
                var dfd = $q.defer();
                var requestTimeout = 30 * 1000; //  30 seconds

                if( data && data.showLoading ){
                    LoadingService.show();
                }

                const resourceRequest = new WLResourceRequest(
                    data.path,
                    WLResourceRequest.POST,
                    requestTimeout);

                var formParams = null;
                if( data && data.params ){
                    formParams = data.params;
                }
                // var formParams = {"username": "value1", "password": "value2"};
                resourceRequest.sendFormParameters(formParams).then(
                    function(onSuccess){
                        console.log("onSuccess ", onSuccess);
                        LoadingService.hide();
                        dfd.resolve(onSuccess);
                    },
                    function(onFailure){
                        console.log("onFailure ", onFailure);
                        LoadingService.hide();
                        dfd.reject(onFailure);
                    }
                )
                return dfd.promise;
            },
        }
    });