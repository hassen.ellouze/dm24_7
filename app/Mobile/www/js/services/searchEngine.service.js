iDubaiApp.factory("SearchEngine", ['$q', '$procedures', '$logger', 'CommonUtils', 'ConfigAdapters',
        function ($q, $procedures, $logger, CommonUtils, ConfigAdapters) {

            var search = function (searchRequest, mediaType, requestFrom, platform, lang) {
                var defer = $q.defer();
                CommonUtils.invokeGetResources({
                    path: ConfigAdapters.SearchEngineAdapter.searchRequest,
                    params: [searchRequest, mediaType, requestFrom, platform, lang]
                }).then(function (result) {
                    console.log('result ', result);
                    defer.resolve(result);
                }, function (error) {
                    console.log('error ', error);
                    defer.resolve(error);
                });
                /*
                $procedures.searchRequest.send([searchRequest, mediaType, requestFrom, platform, lang], false).then(
                    function (response) {
                        if (response.invocationResult.hasOwnProperty('errorMessage') || response.invocationResult.statusCode !== 200) {
                            defer.reject({});
                        } else if (response.invocationResult) {
                            defer.resolve(response.invocationResult);
                        }
                    }, function (error) {
                        defer.reject({});
                    });
                */
                return defer.promise;
            };

            return {
                search: search
            };
        }
    ]
);
