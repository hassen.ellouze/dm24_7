/*
 *    Licensed Materials - Property of IBM
 *    5725-I43 (C) Copyright IBM Corp. 2015. All Rights Reserved.
 *    US Government Users Restricted Rights - Use, duplication or
 *    disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
*/

package com.dm.common.notification;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.ibm.json.java.JSONObject;
//import com.worklight.adapters.rest.api.WLServerAPI;
//import com.worklight.adapters.rest.api.WLServerAPIProvider;
//import com.worklight.core.auth.OAuthSecurity;

////
import java.util.ArrayList;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("/")
public class NotificationsAdapterResource {
	/*
	 * For more info on JAX-RS see https://jsr311.java.net/nonav/releases/1.1/index.html
	 */
	//Instead of this static field, it could be a users DAO that works with Database or cloud storage	
	static ArrayList<String[]> users = new ArrayList<String[]>();
	
	//Define logger (Standard java.util.Logger)
	static Logger logger = Logger.getLogger(NotificationsAdapterResource.class.getName());
	@GET
	@Path("/users")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public JSONObject getusers() {
		String adapterName = "UsingJavaInAdapter";
		String procedureName = "ServicesDM24";
		String parameterArray;
		
		try {
			JSONObject response = new JSONObject();
			JSONObject adapterResponse = null;// invokeProcedure(adapterName, procedureName, "");
			
			return JSONObject.parse("{\"Hiiiiiiiiiiii\":\"Yooooooooooo\"}");
			
		} 
		catch(Exception e){
			e.printStackTrace();
			try {
				return JSONObject.parse("{\"isSuccessful\": false, \"statusCode\": \"002\", \"statusMessage\":\"An error has occurred : "+e.getMessage()+" \" }");
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				try {
					return JSONObject.parse("{\"isSuccessful\": false, \"statusCode\": \"001\", \"statusMessage\":\"An error has occurred : "+e1.getMessage()+" \" }");
				} catch (IOException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
			}
		}
		return null;
	}

	/*
	public JSONObject invokeProcedure(String adapterName, String procedureName, String parameterArray){
	    //parameterArray should be in format "['param1','param2','param3']
		logger.warning(adapterName);
		logger.warning(procedureName);
		logger.warning(parameterArray);
	
		try{
			DataAccessService service = WorklightBundles.getInstance().getDataAccessService(); 
		    ProcedureQName procedureQName = new ProcedureQName(adapterName, procedureName);
		    InvocationResult result = service.invokeProcedure(procedureQName, parameterArray);
		    JSONObject jsonObject = result.toJSON();
		    return jsonObject;
		}catch(Exception e){
			JSONObject response = new JSONObject();
			e.printStackTrace();
			response.put("ERROR", e.getMessage());
			response.put("code", "804");
			return response;
		}
	    
	}
	*/
		
}
