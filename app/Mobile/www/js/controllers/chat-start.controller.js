iDubaiApp.controller('chatStartController', ['$scope', '$rootScope', '$procedures', '$auth', 'GuestUtils', '$logger',
    'LoadingService', '$timeout', '$filter', '$stateParams', 'chatService', '$interval', '$http', '$notification', 'underscore',
    function ($scope, $rootScope, $procedures, $auth, GuestUtils, $logger,
              LoadingService, $timeout, $filter, $stateParams, chatService, $interval, $http, $notification, underscore) {
	    var _ = underscore;
	    
	    $scope.mobileNumber ='';
	 
	    if ($auth.token) {
	    	$scope.mobileNumberParam = $auth.dubaiUserProfile.mobile;
	    } else if(!GuestUtils.isEmptyGuest()) {
	        var guestInfo = GuestUtils.getGuestInfo();
	        if(guestInfo != '') {
	            $scope.mobileNumberParam = (guestInfo.mobileNumber) ? guestInfo.mobileNumber : '';
	        }
	    }
        
        $scope.agentName = $filter('translate')("Waiting for agent");

        $scope.chatTranslate = null;

        $scope.currentLang = localStorage.getItem('dm-lang');

        $scope.participantID = null;

        $scope.date = null;

        $scope.anchor = 0;

        $scope.isTyping = false;

        $scope.exiting = false;

        angular.element(".scroll-down").hide();

        $scope.process = function (res) {
            var data = (res.data.chat != undefined) ? ((res.data.chat.events) ? (res.data.chat.events) : {}) : {};
            $.each(data, function () {
                console.log("Content Type :: " + this.contentType);
                if (this.displayName != "Dubai Municipality" && !isNaN(this.displayName)) {
                    if (this.contentType == "text\/plain") {
                        $scope.addMessage(this.value, "client");
                    }
                } else {
                    if (this.contentType == "text\/plain") {
                        $scope.isTyping = false;
                        $scope.addMessage(this.value, "agent");
                    }
                }
                if (this.type == "typingIndicator") {
                    $scope.isTyping = true;
                    console.log("Typing");
                } else {
                    console.log("Typing End");
                }
                if (this.state == "active" && $scope.date == null) {
                    console.log("Start Conv");
                    $scope.conversationStarted = true;
                    var date = new Date();
                    //$scope.date = formatDate(date);
                    $scope.date = "date";
                }
                if (this.displayName != "Dubai Municipality" && isNaN(this.displayName) && this.participantName != null) {
                    $scope.agentName = $filter('translate')("Chatting with") + " " + this.participantName;
                }
            });
        };

        $scope.startChat = function (username) {
            if (typeof(username) != 'undefined' && username != null && username != "connected") {
                if (username != "") {
                    $rootScope.serverId = 1;
                    chatService.startChat(username).then(function (res) {
                        console.log("res", res.data.chat);
                        var chatData = res.data.chat;
                        $scope.participantID = chatData.participantID;
                        $scope.interval = $interval(function () {
                            if (!$scope.exiting) {
                                chatService.poll(chatData.participantID).then(function (res) {
                                    $scope.process(res);
                                });
                            }
                        }, chatData.pollWaitSuggestion);
                    });
                } else {
                    //$notification.alert($filter('translate')("You dont have a phone number associated to your account, please update your profile"));
                    $scope.exit();
                    location.hash = "#/chatPage";
                }
            } else {
                //$notification.alert($filter('translate')("You dont have a phone number associated to your account, please update your profile"));
                $scope.exit();
                location.hash = "#/chatPage";
            }
        };


        $scope.typing = false;
        $scope.conversationStarted = false;
        $scope.conversationDate = null;

        if ($rootScope.isLoggedIn !== undefined && $scope.mobileNumberParam === "connected") {
            if ($rootScope.isLoggedIn) {
                //$scope.busy.show();
                var data = WL.Client.getUserInfo("iDubaiRealm", "attributes");
                if (data.userInfo) {
                    $scope.mobileNumber = data.userInfo.mobileNo;
                    $http.get('./i18n/chat.json').success(function (response) {
                        $scope.chatTranslate = response;
                        $scope.startChat($scope.mobileNumber);
                    });
                }
            } else {
                $http.get('./i18n/chat.json').success(function (response) {
                    $logger.log("Not Logged In :: Require Mobile Number");
                    $scope.chatTranslate = response;
                    $scope.mobileNumber = $scope.mobileNumberParam;
                    $scope.startChat($scope.mobileNumber);
                });
            }
        } else {
            $http.get('./i18n/chat.json').success(function (response) {
                $scope.chatTranslate = response;
                $scope.mobileNumber = $scope.mobileNumberParam;
                $scope.startChat($scope.mobileNumber);
            });
        }

        $scope.busy = LoadingService;

//	$scope.$on('$locationChangeStart', function( event ) {
//		$scope.exit();
//	});

        $scope.exit = function () {
            $scope.exiting = true;
            $interval.cancel($scope.interval);
            if ($scope.participantID != null) {
                chatService.exit($scope.participantID).then(function (res) {
                    console.log("EXIT RES");
                    console.log(res);
                });
            }
        };

        $scope.writing = function () {
            if ($scope.chatMessage.length >= 631) {
                $scope.chatMessage = $scope.chatMessage.substring(0, 631);
            }
            if ($scope.chatMessage.trim() != "") {
                $scope.typing = true;
            } else {
                $scope.typing = false;
            }
        };

        $scope.addMessage = function (message, source) {
            var msg = '<li class="tiny-message">' +
            '<span class="message-data-time ' + source + '-message">10:24</span>'+
                '<div class="' + source + '-message message" id="' + $scope.anchor + '">' + (typeof $scope.chatTranslate[message] == "undefined" || $scope.currentLang == "ar" ? message : $scope.chatTranslate[message]) + '</div>' +
                '</li>';
            angular.element("#messages").append(msg);
            $scope.anchor++;
            $scope.gotoBottom();
        };

        $scope.sendMessage = function () {
            $("[name=chat-message]").focus();
            chatService.sendMessage($scope.participantID, $scope.chatMessage).then(function (res) {
                console.log("SENDING MESSAGE :: ");
                console.log(res);
                $scope.chatMessage = "";
                $scope.typing = false;
            });
        };

        $scope.$on('$stateChangeStart',
            function (event) {
                console.log("Exit chat :: /exit service");
                $scope.exit();
            });
        if (WL.Client.getEnvironment() == WL.Environment.IPHONE) {
            angular.element(window).bind('scroll',
                function (event) {
                    var elem = document.getElementById($scope.anchor) != undefined ? document.getElementById($scope.anchor) : document.getElementById($scope.anchor - 1);
                    if ($scope.isScrolledIntoView(elem) == false) {
                        angular.element(".scroll-down").show();
                    } else {
                        angular.element(".scroll-down").hide();
                    }
                });
        } else {
            angular.element(".ng-scope").bind('scroll',
                function (event) {
                    var elem = document.getElementById($scope.anchor) != undefined ? document.getElementById($scope.anchor) : document.getElementById($scope.anchor - 1);
                    if ($scope.isScrolledIntoView(elem) == false) {
                        angular.element(".scroll-down").show();
                    } else {
                        angular.element(".scroll-down").hide();
                    }
                });
        }


        $scope.gotoBottom = function () {
            if (WL.Client.getEnvironment() != WL.Environment.IPHONE) {
                $(".ng-scope").animate({
                    scrollTop: $("body").height() + 500
                }, 500);
            } else {
                $("body").animate({
                    scrollTop: $("body").height() + 500
                }, 500);
            }
        };

        angular.element("textarea").on("focus",_.throttle(function () {
	        $scope.gotoBottom();
        }, 100));

        $scope.isScrolledIntoView = function (elem) {
            if (typeof($(elem).offset()) != 'undefined') {
                var $elem = $(elem);
                var $window = $(window);

                var docViewTop = $window.scrollTop();
                var docViewBottom = docViewTop + $window.height() - 40;

                var elemTop = $elem.offset().top;
                var elemBottom = elemTop + $elem.height();

                return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
            } else {
                return false;
            }
        };


    }]);