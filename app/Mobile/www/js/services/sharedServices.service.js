iDubaiApp.factory('SharedService',
    ['$q', function ($q) {

        var states = {
            'backButton':{
                isActive:true
            },
            'services': {
                payload: null,
                from: null
            },
            'faq': {
                payload: null,
                from: null
            },
            'feedback': {
                payload: null,
                from: null
            },
            'chat': {
                payload: null,
                from: null
            },
            'settings': {
                payload: null,
                from: null
            },
            contactUs: {
                payload: null,
                from: null
            },
            'favourites': {
                payload: null,
                from: null
            },
            'myRequests': {
                requestDetails:null,
                payload: null,
                from: null,
                isBack: null
            },
            'myLocations': {
                payload: null,
                from: null,
                isBack: null
            },
            'requestSummary': {
                requestDetails: null,
                SRNumber: null,
                SLA: null,
                lat: null,
                lng: null,
                isBack: false,
                from: null
            },
            'map': {
                from: null,
                payload: null,
            },
            'createRequest': {
                from: null,
                payload: null
            },
            'servicesAZ': {
                from: null,
                payload: null
            },
            'serviceDetails': {
                category: null,
                service: null,
                from: null,
                payload: null
            },
            'faq-questions': {
                questions: null,
                from: null
            },
            'faq-question-answer': {
                question: null,
                forBack: null,
                from: null
            },
            'smartRequest': {
                serviceInfo: null,
                makani: null,
                locationName: null,
                formattedAddress: null,
                location: null,
                makaniInfo: null,
                locationDetail: null,
                DBMesure: null,
                listServices: null,
                requestText: null,
                payload: null,
                from: null
            },
            'personalDetailsUpdate': {
                from: null,
            }
        };

        function searchState(states, state) {
            for (var key in states) {
                var value = states[key];

                if (value === state) {
                    console.log('property=' + key + ' value=' + value);
                    return key;
                }

            }
        }

        /*shared.states.set = function (state, params) {
         shared.states.push({
         state: state,
         params: params
         })
         };

         shared.states.get = function (state) {
         return shared.states[shared.states.indexOf(state)];
         };*/


        return {
            setStateHistory: function (state, params) {
                //var st = searchState(states, state);
                states[state] = params;
            },
            getStateHistory: function (state) {
                //var st = searchState(states, state);
                return states[state];
            }

        }

    }
    ]
);

