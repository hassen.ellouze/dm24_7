iDubaiApp.constant('ConfigAdapters', {
    Authentication24Adapter: {
        authenticate: '/adapters/Authentication24Adapter/authenticate'
    },
    FaqAdapter:{
        getFAQ: '/adapters/FaqAdapter/getFAQ'
    },
    OneTimePasswordAdapter:{
        requestOTP: '/adapters/OneTimePasswordAdapter/requestOTP'
    },
    MakaniDM24Adapter:{
        getNearestMakani: '/adapters/MakaniDM24Adapter/getNearestMakani'
    },
    DMCRM24Adapter:{
        getMyLatestNotifications: '/adapters/DMCRM24Adapter/getMyLatestNotifications'
    },
    SearchEngineAdapter: {
        searchRequest : '/adapters/SearchEngineAdapter/searchRequest'
    },
    EmailAdapter:{
        sendEmail: '/adapters/EmailAdapter/sendEmail',
        senRateMail: '/adapters/EmailAdapter/senRateMail'
    },
    ServicesDM24Adapter:{
        getCategories : '/adapters/ServicesDM24Adapter/getCategories',
        getRequests : '/adapters/ServicesDM24Adapter/getRequests',
        getIncidents: '/adapters/ServicesDM24Adapter/getIncidents',
        getAreas: '/adapters/ServicesDM24Adapter/getAreas',
        insectTypes: '/adapters/ServicesDM24Adapter/insectTypes',
        petTypes: '/adapters/ServicesDM24Adapter/petTypes',
    },
    CommonServicesDM24Adapter:{
        getCommonServices: '/adapters/CommonServicesDM24Adapter/getCommonServices'
    },
    NotificationsDM24Adapter:{
        createNotification: '/adapters/NotificationsDM24Adapter/createNotification'
    },
    EnquireCRM24Adapter:{
        enquireByServiceRequestNumber: '/adapters/EnquireCRM24Adapter/enquireByServiceRequestNumber'
    },
    AttachmentAdapter: {
        addAttachment: '/adapters/AttachmentAdapter/addAttachment'
    }
});
