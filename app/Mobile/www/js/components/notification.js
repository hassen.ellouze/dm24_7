
/* JavaScript content from js/components/notification.js in folder common */
iDubaiApp.factory('$notification', ['$filter', function ($filter) {

    var service = {
        alert: function (message, title, buttonText) {
            try {
                WL.SimpleDialog.show($filter('translate')(title ? title : 'Alert'), message, [{
                    text: $filter('translate')(buttonText ? buttonText : 'OK'), handler: function () {
                    }
                }]);
            } catch (e) {
                alert(message);
            }
        }
    };
    return service;
}]);
