iDubaiApp.controller('favouritesController',
    ['$scope', '$state', '$rootScope', 'underscore', 'cat', '$dmServices', 'SharedService',
        function ($scope, $state, $rootScope, underscore, cat, $dmServices, SharedService) {

            $scope.currentLang = localStorage.getItem('dm-lang');
            var _ = underscore;
            var favourites = localStorage.getItem('Favourites');
            if(favourites){
                $scope.favourites = JSON.parse(favourites);
                if($scope.favourites.length > 0){
                    $scope.favourites.map(function (item, key) {
                        var category = _.find(cat, function(itemCat, indexCat){
                            return itemCat.category_id == item.service.category_id;
                        });
                        $scope.favourites[key].category = category;

                        $dmServices.map(function (itemServ, keyServ) {
                            if (item.type == 'incident'){
                                if(itemServ.incident_id == item.id){
                                    $scope.favourites[key].service = itemServ;
                                }
                            }else {
                                if(itemServ.service_request_id == item.id){
                                    $scope.favourites[key].service = itemServ;
                                }
                            }
                        })
                    })


                }
            }

        $scope.goTo = function (service, category) {
            SharedService.setStateHistory('serviceDetails', {
                service: service,
                category: category,
                from: 'favourites'
            });
            $state.go('serviceDetails', {
                service: service,
                category: category,
                from: 'favourites'
            });
        }


        }]);
