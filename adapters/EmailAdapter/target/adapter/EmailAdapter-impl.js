/*
 *  Licensed Materials - Property of IBM
 *  5725-I43 (C) Copyright IBM Corp. 2011, 2016. All Rights Reserved.
 *  US Government Users Restricted Rights - Use, duplication or
 *  disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */

/**
 * @param tag: a topic such as MobileFirst_Platform, Bluemix, Cordova.
 * @returns json list of items.
 */

function getFeed(tag) {
	var input = {
	    method : 'get',
	    returnedContentType : 'xml',
	    path : getPath(tag)
	};

	return MFP.Server.invokeHttp(input);
}

/**
 * Helper function to build the URL path.
 */
function getPath(tag){
    if(tag === undefined || tag === ''){
        return 'feed.xml';
    } else {
        return 'blog/atom/' + tag + '.xml';
    }
}

/**
 * @returns ok
 */
function unprotected(param) {
	return {result : "Hello from unprotected resource"};
}


var SEC_HASH = "/hf2G@%23$@gsdofugh23%25@%25)TFD";

function sendEmail(type, options) {
    var response = null;

    try {
        var msg = type == "Feedback" ? "Dear iDubai team, <br\>A new feedback has been submitted for iDubai App. Details below:" : "Dear IT support team, <br\> A new technical issue has been raised on iDubai. Details below:";
        var body = _getEmailMessage(options , msg);
        var subject = type == "Feedback" ? "iDubai - New feedback request" : "iDubai - Technical issue";


        var input = {
            method : 'post',
            path : "preprod.dm.gov.ae/ServiceEmailer/feedbackMailer?hash="+SEC_HASH,
            parameters: {
                subject: subject,
                type : type,
                body : body
            }
        };
        response = MFP.Server.invokeHttp(input);
    }
    catch (e) {
        return{
            isSuccessful: false,
            reference : e
        }
    }
    return response;

}

function _getEmailMessage(options , msg){

    messageBody = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">\
				<html>\
				<head>\
				<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">\
				</head>\
				<body bgcolor="#ffffff" text="#000000">\
				        '+msg+',<br>\
				<div class="moz-signature"><i><br>\
						<table border="1" cellpadding="0" cellspacing="0" width="100%">\
						  <tr>\
						   <td>\
						    Date\
						   </td>\
						    <td>\
						    '+ new Date() +'\
						   </td>\
						  </tr>\
						  <tr>\
						   <td>\
						    OS Informations\
						   </td>\
						    <td>\
						    '+ options.osInfo +'\
						   </td>\
						  </tr>\
						  <tr>\
						   <td>\
						    Name\
						   </td>\
						    <td>\
						    '+ options.name +'\
						   </td>\
						  </tr>\
						  <tr>\
						   <td>\
						   Phone Number\
						   </td>\
						    <td>\
						    '+ options.phone +'\
						   </td>\
						  </tr>\
						  <tr>\
						   <td>\
						   Email\
						   </td>\
						    <td>\
						    '+ options.email +'\
						   </td>\
						  </tr>\
						  <tr>\
						   <td>\
						   Description\
						   </td>\
						    <td>\
						    '+ options.description +'\
						   </td>\
						  </tr>\
						  <tr>\
						   <td>\
						   Subject\
						   </td>\
						    <td>\
						    '+ options.subject +'\
						   </td>\
						  </tr>\
						 </table>\
				<br>\
				</i></div>\
				</body>\
				</html>';

    return messageBody;
}

function _getRatingEmailMessage(options , msg){
    messageBody = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">\
				<html>\
				<head>\
				<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">\
				</head>\
				<body bgcolor="#ffffff" text="#000000">\
				        '+msg+',<br>\
				<div class="moz-signature"><i><br>\
						<table border="1" cellpadding="0" cellspacing="0" width="100%">\
						  <tr>\
						   <td>\
						    Date\
						   </td>\
						    <td>\
						    '+ new Date() +'\
						   </td>\
						  </tr>\
						  <tr>\
						   <td>\
						    OS Informations\
						   </td>\
						    <td>\
						    '+ options.osInfo +'\
						   </td>\
						  </tr>\
						  <tr>\
						   <td>\
						    Name\
						   </td>\
						    <td>\
						    '+ options.username +'\
						   </td>\
						  </tr>\
						  <tr>\
						   <td>\
						   Phone Number\
						   </td>\
						    <td>\
						    '+ options.mobileNumber +'\
						   </td>\
						  </tr>\
						  <tr>\
						   <td>\
						   Email\
						   </td>\
						    <td>\
						    '+ options.emailAddress +'\
						   </td>\
						  </tr>\
						  <tr>\
						   <td>\
						   Description\
						   </td>\
						    <td>\
						    '+ options.description +'\
						   </td>\
						  </tr>\
						  <tr>\
						   <td>\
						   Subject\
						   </td>\
						    <td>\
						    '+ options.subject +'\
						   </td>\
						  </tr>\
						  <tr>\
							<td>\
							  Rate\
							  </td>\
							  <td>\
							   '+ options.rate+' /5' +'\
							 </td>\
							</tr>\
						 </table>\
				<br>\
				</i></div>\
				</body>\
				</html>';

    return messageBody;
}

function senRateMail(type,options) {
    try {
        var msg = "Dear iDubai team, <br\>A new rating service has been submitted for iDubai App. Details below:";
        var body = _getRatingEmailMessage(options , msg);
        var subject = "Rating service";

        var input = {
            method : 'post',
            path : "preprod.dm.gov.ae/ServiceEmailer/feedbackMailer?hash="+SEC_HASH,
            parameters: {
                subject: subject,
                type : type,
                body : body
            }
        };

        return MFP.Server.invokeHttp(input);
    } catch(e) {
        return {
            isSuccessful: false,
            reference: e
        };
    }
}