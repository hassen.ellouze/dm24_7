iDubaiApp.controller('myRequestsController',
    ['$state', '$scope', '$translate', '$filter', 'underscore',
        'DMService', '$procedures', '$auth', '$logger', '$rootScope', 'DMCategories', '$DbStorageService', 'PhoneUtils', 'GuestUtils', '$dmServices', 'LoadingService', 'SharedService',
        function ($state, $scope, $translate, $filter, underscore,
                  DMService, $procedures, $auth, $logger, $rootScope, DMCategories, $DbStorageService, PhoneUtils, GuestUtils, $dmServices, LoadingService, SharedService) {


            var _ = underscore;
            $scope.currentLang = localStorage.getItem('dm-lang');
            $scope.phone = '';
            $scope.phoneNumberRequired = false;
            $scope.errorMessage = '';
            $scope.isActive = 'active';
            var latestNotificationsList = [];
            $scope.latestNotificationsList = [];
            $scope.ready = false;

            moment.locale('en-gb'); // Local format (Grand Britain)
            if ($scope.currentLang === "ar") {
                moment.locale('ar-sa');
            }

            var SLA = '';


            /*

             var ScheduledDate = moment(trackingDetails.ScheduledDate, 'DD/MM/YYYY HH:mm');
             var StatusDate = moment(trackingDetails.RequestStatusDate, 'DD/MM/YYYY HH:mm');
             var today = moment(moment().format('DD/MM/YYYY HH:mm'),'DD/MM/YYYY HH:mm');
             var duration = moment.duration(today.diff(StatusDate)).asHours();
             duration = Math.abs(duration);
             $scope.durationToDestination = ( isNaN(duration)) ? "--" : (SLA != '')? parseInt(SLA)-parseInt(duration): duration  ;
             $scope.durationToDestination = ( !isNaN($scope.durationToDestination) && $scope.durationToDestination <=0 )? 1 : Math.round($scope.durationToDestination);

             */


            /**
             * Load latest requests
             * @param phone
             */
            $scope.loadLatestNotifications = function (phone) {
                LoadingService.show();
                try {
                    phone = PhoneUtils.formatPhoneNumber(phone + "");
                    DMService.getMyLatestNotifications(phone, true).then(
                        function (result) {
                            console.log('==> results ', result);
                            $scope.latestNotificationsList = result;
                            latestNotificationsList = result;
                            if (!$scope.latestNotificationsList) {
                                WL.SimpleDialog.show($filter('translate')('Alert'), ($filter('translate')("No result found")), [{
                                    text: $filter('translate')('OK'), handler: function () {
                                        if (GuestUtils.isGuest())
                                            $scope.openPopup();
                                    }
                                }]);
                                LoadingService.hide();
                            } else {
                                if ($scope.latestNotificationsList.length == 0) {

                                    $('.myrequest-filter').addClass('hideMe');
                                    $('.myrequestTabs').addClass('hideMe');


                                } else {
                                    $('.myrequest-filter').removeClass('hideMe');
                                    $('.myrequestTabs').removeClass('hideMe');
                                }
                                for (var i = 0; i < $scope.latestNotificationsList.length; i++) {
                                    //$rootScope.allServices
                                    var allServices = $dmServices;


                                    var srv = _.find(allServices, function (item, index) {
                                        return item.serviceEN == $scope.latestNotificationsList[i].ServiceEN;
                                    });

                                    if (srv) {
                                        SLA = ( (parseInt(srv.sla) % 7) > 0) ? (parseInt(srv.sla) / 7) + 1 : (parseInt(srv.sla) / 7);
                                        $scope.latestNotificationsList[i].SLA = Math.round(SLA);
                                        $scope.latestNotificationsList[i].SLA = Math.round(SLA);
                                    } else {
                                        $scope.latestNotificationsList[i].SLA = '';
                                    }

                                    $scope.latestNotificationsList[i].RequestCreatedDate = moment($scope.latestNotificationsList[i].RequestCreatedDate, ['DD/MM/YYYY HH:mm', moment.ISO_8601]).format('D MMM');
                                    var StatusDate = moment($scope.latestNotificationsList[i].RequestStatusDate, ['DD/MM/YYYY HH:mm', moment.ISO_8601]);
                                    var today = moment(moment().format('DD/MM/YYYY HH:mm'), ['DD/MM/YYYY HH:mm', moment.ISO_8601]);
                                    var duration = moment.duration(StatusDate.diff(today)).asHours();
                                    //duration = Math.abs(duration);

                                    allServices.some(function (item, index) {
                                        if (item.serviceEN == $scope.latestNotificationsList[i].ServiceEN) {
                                            $scope.latestNotificationsList[i].service_title_en = item.service_title_en;
                                            $scope.latestNotificationsList[i].service_title_ar = item.service_title_ar;
                                            $scope.latestNotificationsList[i].description_ar = item.description_ar;
                                            $scope.latestNotificationsList[i].description_en = item.description_en;
                                            return;
                                        }
                                    });

                                    switch ($scope.latestNotificationsList[i].RequestStatus) {
                                        case 'Initiation' : {
                                            $scope.latestNotificationsList[i].ServiceENHighlighted = 'Received';
                                            $scope.latestNotificationsList[i].ServiceARHighlighted = 'تم الاستلام';

                                            $scope.latestNotificationsList[i].ServiceENDetailed = 'Initiation';
                                            $scope.latestNotificationsList[i].ServiceARDetailed = 'تم الاستلام';
                                            break;
                                        }
                                        case 'Under Processing' : {
                                            if ($scope.latestNotificationsList[i].RequestUserFullName == '' || ($scope.latestNotificationsList[i].RequestUserFullName && $scope.latestNotificationsList[i].RequestUserFullName.indexOf('Group:') >= 0)) {
                                                $scope.latestNotificationsList[i].ServiceENHighlighted = 'Received';
                                                $scope.latestNotificationsList[i].ServiceARHighlighted = 'تم الاستلام';

                                                $scope.latestNotificationsList[i].ServiceENDetailed = 'Initiation';
                                                $scope.latestNotificationsList[i].ServiceARDetailed = 'تم الاستلام';
                                            } else {
                                                $scope.latestNotificationsList[i].ServiceENHighlighted = 'In Progress';
                                                $scope.latestNotificationsList[i].ServiceARHighlighted = 'في تَقَدم';

                                                $scope.latestNotificationsList[i].ServiceENDetailed = 'Under Processing';
                                                $scope.latestNotificationsList[i].ServiceARDetailed = 'قيد التنفيذ';
                                            }

                                            break;
                                        }
                                        case 'Canceled' : {
                                            $scope.latestNotificationsList[i].ServiceENHighlighted = 'Completed';
                                            $scope.latestNotificationsList[i].ServiceARHighlighted = 'مكتملة';

                                            $scope.latestNotificationsList[i].ServiceENDetailed = 'Cancelled';
                                            $scope.latestNotificationsList[i].ServiceARDetailed = 'ألغيت';
                                            break;
                                        }
                                        case 'Rejected' : {
                                            $scope.latestNotificationsList[i].ServiceENHighlighted = 'Completed';
                                            $scope.latestNotificationsList[i].ServiceARHighlighted = 'مكتملة';

                                            $scope.latestNotificationsList[i].ServiceENDetailed = 'Rejected';
                                            $scope.latestNotificationsList[i].ServiceARDetailed = 'رفض';
                                            break;
                                        }
                                        case 'Approved' : {
                                            $scope.latestNotificationsList[i].ServiceENHighlighted = 'Completed';
                                            $scope.latestNotificationsList[i].ServiceARHighlighted = 'مكتملة';

                                            $scope.latestNotificationsList[i].ServiceENHighlightedView = 'Accomplished';
                                            $scope.latestNotificationsList[i].ServiceARHighlightedView = 'مكتملة';

                                            $scope.latestNotificationsList[i].ServiceENDetailed = 'Accomplished'; // or Unaccomplished
                                            $scope.latestNotificationsList[i].ServiceARDetailed = 'مكتملة';
                                            break;
                                        }
                                        case 'Closed' : {
                                            $scope.latestNotificationsList[i].ServiceENHighlighted = 'Completed';
                                            $scope.latestNotificationsList[i].ServiceARHighlighted = 'مكتملة';

                                            $scope.latestNotificationsList[i].ServiceENHighlightedView = 'Unaccomplished';
                                            $scope.latestNotificationsList[i].ServiceARHighlightedView = 'غير مكتملة';

                                            $scope.latestNotificationsList[i].ServiceENDetailed = 'Unaccomplished';
                                            $scope.latestNotificationsList[i].ServiceARDetailed = 'غير مكتملة';
                                            break;
                                        }
                                    }


                                }
                                $scope.ready = true;

                                $scope.filterRequestByStatus($scope.isActive);
                                LoadingService.hide();
                            }
                        },
                        function (error) {
                            WL.SimpleDialog.show($filter('translate')('Alert'), ($filter('translate')("Can't fetch data. Please try again later")), [{
                                text: $filter('translate')('OK'), handler: function () {
                                    if (GuestUtils.isGuest())
                                        $scope.openPopup();
                                }
                            }]);
                            LoadingService.hide();
                        });
                } catch (e) {
                    console.log('===> ERROR ', e);
                    LoadingService.hide();
                }
            };


            /**
             * Get categories to translate category name
             */

            $scope.filterRequestByStatus = function (val) {
                $scope.isActive = val;
                LoadingService.show();


                if (val === 'resolved') {
                    $scope.latestNotificationsListFiltered = _.filter(latestNotificationsList, function (item) {
                        return item.RequestStatus.toUpperCase() === 'REJECTED'
                            || item.RequestStatus.toUpperCase() === 'CLOSED'
                            || item.RequestStatus.toUpperCase() === 'APPROVED'
                            || item.RequestStatus.toUpperCase() === 'CANCELED';
                    });

                } else {
                    $scope.latestNotificationsListFiltered = _.filter(latestNotificationsList, function (item) {
                        return item.RequestStatus.toUpperCase() !== 'REJECTED'
                            && item.RequestStatus.toUpperCase() !== 'CLOSED'
                            && item.RequestStatus.toUpperCase() !== 'APPROVED'
                            && item.RequestStatus.toUpperCase() !== 'CANCELED';
                    });

                }
                LoadingService.hide();

            };

            $scope.sendRequest = function (request) {
                SharedService.setStateHistory('requestSummary', {
                    requestDetails: request,
                    from: "myRequests",
                })
                $state.go('requestSummary', {})
            }

            /**
             * Check user authenticated or guest
             */
            $scope.token = $auth.token;
            if ($rootScope.isLoggedIn || $auth.token) {
                $scope.loadLatestNotifications(PhoneUtils.formatPhoneNumber($auth.dubaiUserProfile.mobile + ""));
                //$scope.getLatestNotifications("+971506140463");
            } else {
                if (GuestUtils.isGuest()) {
                    if (!GuestUtils.isEmptyGuest()) {
                        var guestMobileNumber = GuestUtils.getGuestPhoneNumber();
                        setTimeout(function () {
                            $scope.loadLatestNotifications(guestMobileNumber);
                            $scope.$apply();
                        });
                    }
                }
                //$scope.openPopup();
            }

        }
    ]
).filter('searchByTransactionNumber', function () {
    return function (currentList, searchText) {

        if (searchText) {
            var filtered = [];
            angular.forEach(currentList, function (item) {

                if (item.RequestNumber.toUpperCase().indexOf(searchText.toUpperCase()) !== -1)
                    filtered.push(item);
            });

            return filtered;
        }
        else {
            return currentList;
        }

    };
});