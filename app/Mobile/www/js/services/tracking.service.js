
/* JavaScript content from js/services/tracking.service.js in folder common */
iDubaiApp.factory("TrackingService", ['$q', '$DbStorageService', '$procedures', '$config', '$logger','CommonUtils', 'ConfigAdapters',
        function ($q, $DbStorageService, $procedures, $config, $logger,CommonUtils, ConfigAdapters) {

            var enquireByServiceRequestNumber = function (sr) {
                //var defer = $q.defer();
                var deferred = $q.defer();
                /*                    var invocationData = {
                                        adapter: 'NotificationsDM24Adapter',
                                        procedure: 'createNotification',
                                        parameters: params
                                    };*/
                CommonUtils.invokeGetResources({
                    path: ConfigAdapters.EnquireCRM24Adapter.enquireByServiceRequestNumber,
                    params: [sr],
                    showLoading: true
                }).then(function (response) {
                    console.log('result ', response);
                    if( response && response.responseJSON && response.responseJSON.isSuccessful ){
                        deferred.resolve(response.responseJSON);
                    }
                }, function (error) {
                    console.log('error ', error);
                    deferred.reject(error);
                });
               /* $procedures.enquireByServiceRequestNumber.send([sr]).then(
                    function (resposne) {
                        if (resposne.invocationResult.hasOwnProperty('errorMessage') || resposne.invocationResult.statusCode !== 200) {
                            defer.reject({});
                        } else if (resposne.invocationResult) {
                            /!*if (resposne.invocationResult.hasOwnProperty('querySROutput')) {
                                if (resposne.invocationResult.querySROutput.hasOwnProperty('ListOfDmmobilequerysr')) {
                                    var trackingDetails = resposne.invocationResult.querySROutput.ListOfDmmobilequerysr.DmNotificationRequest;
                                    defer.resolve(trackingDetails);
                                }
                            }*!/
                            defer.resolve(resposne.invocationResult);
                        }
                    }, function (error) {
                        defer.reject({});
                    });*/
                return deferred.promise;
            };

            var vehicleEnquireService = function (vehicleID) {
                var defer = $q.defer();
                $procedures.vehicleEnquireService.send([vehicleID]).then(
                    function (resposne) {
                        defer.resolve(resposne);
                    }, function (error) {
                        defer.reject({});
                    });
                return defer.promise;
            };

            return {
                enquireByServiceRequestNumber: enquireByServiceRequestNumber,
                vehicleEnquireService: vehicleEnquireService
            };
        }
    ]
);
