iDubaiApp.controller('settingController', ['$scope', '$rootScope', 'underscore', '$filter', function ($scope, $rootScope, underscore, $filter) {
    var _ = underscore;

    $scope.isInvert = {
        toggleShow: localStorage.getItem('invertColors') == 'inverted' ? true : false
    };

    $scope.isLocation = {
        toggleShow: localStorage.getItem('geoLocation') ? JSON.parse(localStorage.getItem('geoLocation')) : true
    };

    $scope.isNotif = {
        toggleShow: localStorage.getItem('isNotif') ? JSON.parse(localStorage.getItem('isNotif')) : true
    };

    $scope.isWifi = {
        toggleShow: localStorage.getItem('isWifi') ? JSON.parse(localStorage.getItem('isWifi')) : true
    };


    if (localStorage.getItem('fontsFactor')) {
        $scope.slider = JSON.parse(localStorage.getItem('fontsFactor'));
    } else {
        $scope.slider = {
            minValue: 3,
            value: 5,
            options: {
                floor: 1,
                ceil: 5,
                step: 1,
                showTicks: true,
                draggableRange: true,
                showTicksValues: false,
                showSelectionBar: true,
                getSelectionBarColor: function (value) {
                    if (value <= 1)
                        return 'rgb(76, 217, 100)';
                    if (value <= 2)
                        return 'rgb(76, 217, 100)';
                    if (value <= 3)
                        return 'rgb(76, 217, 100)';
                    if (value <= 4)
                        return 'rgb(76, 217, 100)';
                    if (value <= 5)
                        return 'rgb(76, 217, 100)';

                    return 'rgb(76, 217, 100)';
                }
            }
        };

        localStorage.setItem('fontsFactor', JSON.stringify($scope.slider));
    }


    $scope.$watch('slider', function () {
        $rootScope.fontsFactor = $scope.slider.minValue;
        localStorage.setItem('fontsFactor', JSON.stringify($scope.slider));
        $rootScope.applyFontSizes();
    }, true);

    $scope.$watch('isLocation', function () {
        console.log('LOCATION: ', $scope.isLocation.toggleShow);
        localStorage.setItem('geoLocation', $scope.isLocation.toggleShow);
        $rootScope.geoLocation = $scope.isLocation.toggleShow;
    }, true);

    $scope.$watch('isNotif', function () {
        console.log('Notification: ', $scope.isNotif.toggleShow);
        localStorage.setItem('isNotif', $scope.isNotif.toggleShow);
        $rootScope.isNotif = $scope.isNotif.toggleShow;
    }, true);

    $scope.$watch('isWifi', function () {
        console.log('Enable Uploads on Wifi: ', $scope.isWifi.toggleShow);
        localStorage.setItem('isWifi', $scope.isWifi.toggleShow);
        $rootScope.isWifi = $scope.isWifi.toggleShow;
    }, true);

    $scope.invertColor = function () {
        if ($scope.isInvert.toggleShow) {
            localStorage.setItem('invertColors', 'inverted');
            $('html').addClass('inverted');
        } else {
            localStorage.setItem('invertColors', 'normal');
            $('html').removeClass('inverted');
        }
    };

    /*
     * Open app accessibility
     */
    $scope.onOpenAccessibility = function () {
        window.NativeSettings.open('accessibility');
    };
}]);
