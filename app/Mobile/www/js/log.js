
/* JavaScript content from js/log.js in folder common */
angular.module('iDubaiApp').factory('$logger', ['$config', '$debugService' ,function ($config,$debugService) {
	var enableLogger = $config.enableLogger;
    //var logLevel  = $config.logLevel;
    var LevelType ={
			LOG:0, INFO:1, DEBUG:2, WARN:3, ERROR:4
	};
     
    /*
    var writeLog = function(console,arguments,type){
        if(type >= $config.logLevel){
            switch (type){
                case LevelType.LOG:
                    //console.log(msg);
                	console.log.apply(console,arguments); 
                    break;
                case LevelType.INFO:
                    //console.info(msg);
                	console.info.apply(console,arguments); 
                    break;
                case LevelType.DEBUG:
                    //console.debug(msg);
                	console.debug.apply(console,arguments); 
                    break;
                case LevelType.WARN:
                    //console.warn(msg);
                	console.warn.apply(console,arguments); 
                    break;
                case LevelType.ERROR:
                    //console.error(msg);
                    console.error.apply(console,arguments); 
                    break;
            }
        }
    };*/
    
    var service =  {
    	enableLogger : enableLogger,
        info:function(){
        	if(service.enableLogger){ 
//        		if(console){ 			
//					writeLog(console,arguments,LevelType.INFO);									
//				} 
        			$debugService.Log().write(Array.prototype.slice.call(arguments, 0),LevelType.INFO);
        	}
        },
        debug:function(){
        	if(service.enableLogger){ 
//        		if(console){ 			
//					writeLog(console,arguments,LevelType.DEBUG);									
//				} 	
        			$debugService.Log().write(Array.prototype.slice.call(arguments, 0),LevelType.DEBUG);
        	}
        },
        warn:function(){
        	if(service.enableLogger){ 
//        		if(console){ 			
//					writeLog(console,arguments,LevelType.WARN);									
//				} 	
        			$debugService.Log().write(Array.prototype.slice.call(arguments, 0),LevelType.WARN);
        	}
        },
        error:function(){
        	if(service.enableLogger){ 
//        		if(console){ 			
//					writeLog(console,arguments,LevelType.ERROR);									
//				} 	
        			$debugService.Log().write(Array.prototype.slice.call(arguments, 0),LevelType.ERROR);
        	}
        },
        log : function(){
        	if(service.enableLogger){
//        		if(console){ 			
//					writeLog(console,arguments,LevelType.LOG);									
//				}         		
        			$debugService.Log().write(Array.prototype.slice.call(arguments, 0),LevelType.LOG);
        	}
        } 
    };
    
    return service;

}]);
