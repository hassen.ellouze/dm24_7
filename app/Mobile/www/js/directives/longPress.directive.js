
/* JavaScript content from js/directives/longPress.directive.js in folder common */
// Add this directive where you keep your directives

iDubaiApp.directive('onLongPress', function($timeout) {

        return {

            restrict: 'A',

            link: function($scope, $elm, $attrs) {

                $elm.bind('touchstart', function(evt) {

                    // Locally scoped variable that will keep track of the long press

                    $scope.longPress = true;



                    // We'll set a timeout for 600 ms for a long press

                    $timeout(function() {

                        if ($scope.longPress) {

                            // If the touchend event hasn't fired,

                            // apply the function given in on the element's on-long-press attribute

                            $scope.$apply(function() {

                                $scope.$eval($attrs.onLongPress)

                            });

                        }

                    }, 300);

                });



                $elm.bind('touchend', function(evt) {

                    // Prevent the onLongPress event from firing

                    $scope.longPress = false;

                    // If there is an on-touch-end function attached to this element, apply it

                    if ($attrs.onTouchEnd) {

                        $scope.$apply(function() {

                            $scope.$eval($attrs.onTouchEnd)

                        });

                    }

                });

            }

        };

    })
iDubaiApp.directive('pressableElement', function ($timeout) {
    return {
        restrict: 'C', // only matches class name
        link: function ($scope, $elm, $attrs) {
            $elm.bind('mousedown', function (evt) {
                $scope.longPress = true;
                $scope.click = true;
                $scope._pressed = null;

                // onLongPress: on-long-press
                $scope._pressed = $timeout(function () {
                    $scope.click = false;
                    if ($scope.longPress && $attrs.onLongPress) {
                        $scope.$apply(function () {
                            $scope.$eval($attrs.onLongPress, { $event: evt });
                        });
                    }
                }, $attrs.timeOut || 100); // timeOut: time-out

                // onTouch: on-touch
                if ($attrs.onTouch) {
                    $scope.$apply(function () {
                        $scope.$eval($attrs.onTouch, { $event: evt });
                    });
                }
            });

            $elm.bind('mouseup', function (evt) {
                $scope.longPress = false;
                $timeout.cancel($scope._pressed);

                // onTouchEnd: on-touch-end
                if ($attrs.onTouchEnd) {
                    $scope.$apply(function () {
                        $scope.$eval($attrs.onTouchEnd, { $event: evt });
                    });
                }

                // onClick: on-click
                if ($scope.click && $attrs.onClick) {
                    $scope.$apply(function () {
                        $scope.$eval($attrs.onClick, { $event: evt });
                    });
                }
            });
        }
    };
})