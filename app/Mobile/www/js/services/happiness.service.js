
/* JavaScript content from js/services/happiness.service.js in folder common */
iDubaiApp.factory('happinessService', ['$q', '$procedures', function ($q, $procedures) {

        /**
         * Get happiness parameters
         * @adapter HappinessDM24Adapter
         * @param params
         */
        function getHappinessParameters(params) {
            var defer = $q.defer();
            $procedures.getHappinessMeterParams.send([params], false).then(function (resposne) {
                if (resposne.invocationResult.isSuccessful) {
                    defer.resolve(resposne);
                } else {
                    defer.reject({});
                }
            }, function (error) {
                defer.reject({});
            });
            return defer.promise;
        }

        /**
         * Generate happiness meter URL
         * @param data
         * @returns {string}
         */
        function getHappinessMeterURL(data) {
            var happinessURL = '';
            var json_payload = data.json_payload;
            var client_id = data.client_id;
            var signature = data.signature;
            var lang = data.lang;
            var random = data.random;
            var timestamp = data.timestamp;
            var nonce = data.nonce;

            happinessURL = './widgets/happiness-page.html' + '?'
                + encodeURI('json_payload=' + json_payload + '&client_id=' + client_id + '&signature=' + signature
                    + '&lang=' + lang + '&random=' + random + '&timestamp=' + timestamp + '&nonce=' + nonce);

            console.log('===> happinessURL ', happinessURL);
            return happinessURL;
        }


        return {
            getHappinessParameters: getHappinessParameters,
            getHappinessMeterURL: getHappinessMeterURL
        };
    }
    ]
);