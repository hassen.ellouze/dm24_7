var rand = function() {
	return Math.random().toString(36).substr(2); // remove `0.`
};

var token = function() {
	return rand() + rand(); // to make it longer
};

function authenticate(username, email, mobile) {

	var userInfo = {
		"mobile": mobile,
		"username": username,
		"email": username
		/*"country": country,
        "isPetOwner": isPetOwner,
        "isFarmOwner": isFarmOwner,
        "isFarmAnimals": isFarmAnimals,
        "isEmployee": isEmployee,
        "mobileArea": mobileArea*/
	};

	var response = {
		'token': null,
		'userInfo': null,
	};

	var tokenResponse = token();
	MFP.Logger.error("===> TOKEN RESPONE  <===");
	MFP.Logger.error(JSON.stringify(tokenResponse));

	response.token = tokenResponse;
	response.userInfo = userInfo;
	response.isSuccessful = true;

	return response;

}
