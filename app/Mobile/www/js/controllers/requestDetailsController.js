iDubaiApp.controller('requestDetailsController',
    ['$myLocations', '$scope', '$stateParams', '$translate', '$filter', '$notification', 'DMService',
        '$procedures', '$auth', '$logger', '$rootScope', '$q', 'LoadingService', '$state',
        function ($myLocations, $scope, $stateParams, $translate, $filter, $notification, DMService,
                  $procedures, $auth, $logger, $rootScope, $q, LoadingService, $state) {

            if (!$stateParams.service && !$stateParams.isBack) {
                $state.go('dashboard');
            }
            if ($stateParams.isBack) {
                $stateParams = $state.current.params;
            }


            // Popup Request
            $rootScope.toggle = 0;
            var height = $(window).height();
            $scope.maxHeight = (height / 3 * 2) - 40;
            $scope.showPopupRequest = function (valid) {
                $scope.requestDetails = valid;
                if ($rootScope.toggle === 0) {
                    $(".requests-details").addClass("topSlide");
                    $rootScope.toggle = 1;
                } else {
                    $(".requests-details").removeClass("topSlide");
                    $rootScope.toggle = 0;
                }
            };

            $scope.requestDetails = true;

            $scope.showLocations = function () {
                if ($scope.locations.length > 0) {
                    $scope.showPopupRequest(false);
                }
            };

            $scope.currentLang = localStorage.getItem('dm-lang');
            var category = $stateParams.category;
            var service = $stateParams.service;
            var area = $stateParams.area;
            var serviceCode = $stateParams.service.code;
            var index = $stateParams.id;
            $scope.isCommonService = $stateParams.isCommonService;
            // Get service Name
            $scope.serviceName = ($scope.currentLang === 'en') ? service.service_title_en : service.service_title_ar;
            // Details
            var enDetails = "The service allows the applicant to obtain the <span class='detailsServiceName'>" + service.service_title_en + "</span> . This application should be submitted prior to implementation of the service. The service is provided by Dubai Municipality.";
            var arDetails = 'تسمح الخدمة لمقدم الطلب بالحصول على  <span class="detailsServiceName">' + service.service_title_ar + ' </span><span>  وينبغي تقديم هذا الطلب قبل تنفيذ الخدمة. يتم تقديم الخدمة من قبل بلدية دبي </span>';
            $scope.details = ($scope.currentLang === 'en') ? enDetails : arDetails;

            $scope.notes = ($scope.currentLang === 'en')
                ? 'You will be provided with the result of your application.'
                : 'سيتم تزويدك بنتيجة طلبك';

            // Go to create request page
            $scope.createRequest = function () {
                $state.go('map', {id: index, service: service, category: category, area: area});
            };

            // Add/Remove favorites
            $scope.favs = [];
            $scope.isEnabled = false;

            $favorites.getFavorites().then(function (data) {
                if (typeof(data) === "string") {
                    data = JSON.parse(data);
                } else {
                    if (typeof(data[0]) !== 'undefined') {
                        data = JSON.parse(data[0].FAVORITES);
                    }
                }

                if (data.length) {
                    $scope.favs = data;
                    if ($scope.isFavPage) {
                        $scope.services = data;
                        $timeout(function () {
                            $scope.$apply();
                        });
                    }
                } else {
                    $scope.favs = [];
                }
            });

            $scope.isFavorite = function () {
                var theObj = $filter('filter')($scope.favs, function (d) {
                    return d.service.code === serviceCode;
                })[0];
                return (typeof(theObj) !== 'undefined') ? true : false;
            };

            $scope.addRemoveFavorite = function () {
                var item = service;
                var categ = category;
                var obj = {
                    "code": categ.categoryId,
                    "category": {
                        "categoryId": category.categoryId,
                        "nameEn": categ.service_title_en,
                        "nameAr": categ.service_title_ar,
                        "nameFr": categ.nameFr,
                        "cssClass": categ.cssClass
                    },
                    "service": item,
                    "fav-id": item.code + "-" + categ.categoryId
                };
                var theObj = $filter('filter')($scope.favs, function (d) {
                    var rs = d['fav-id'] === obj.service.code + "-" + obj.code;
                    return rs;
                })[0];

                if (typeof(theObj) === 'undefined') {
                    $scope.favs.push(obj);
                    $favorites.setFavorites(JSON.stringify($scope.favs));
                } else {
                    for (var i = $scope.favs.length - 1; i >= 0; i--) {
                        if ($scope.favs[i].service.code === item.code && $scope.favs[i].code === categ.categoryId) {
                            $scope.favs.splice(i, 1);
                        }
                    }
                    $favorites.setFavorites(JSON.stringify($scope.favs));
                }
                $scope.isFavorite(item.code);
            };
        }
    ]
);