
/* JavaScript content from js/services/analytics.service.js in folder common */
var _gaq = _gaq || [];

angular.module('AnalyticsService', []).run(['$http', function($http) {
}]).factory('AnalyticsService', ['$rootScope', '$window', '$location', '$logger', '$state', '$auth', function ($rootScope, $window, $location, $logger, $state, $auth) {

    var trackId = "UA-104624421-3"; // DEV Account
	
	try{
		var currentServerUrl = WL.Client.getAppProperty(WL.AppProp.WORKLIGHT_ROOT_URL);
	    if(currentServerUrl.indexOf("mfpprod.dm.gov.ae") > -1){
	    	//@TODO : PROD Track ID
	    	trackId = "UA-104624421-1";
	    }else if(currentServerUrl.indexOf("mfpstag.dm.gov.ae") > -1){
	    	// STAG Track ID
            trackId = "UA-104624421-3";
	    }else{
	    	// DEV Track ID
            trackId = "UA-104624421-3";
	    }
	}catch(e){
		
	}


    var trackRoute = function (hitType, screenName, eventPayload) {
		

		ga('create', trackId, {
			'storage' : 'none',
			'clientId' : null //WL.Client.getUserInfo("wl_anonymousUserRealm", "userId")
		});
        ga('set', 'checkProtocolTask', null);
        ga('set', 'appName', 'Dubai 24/7');

		try{
            //var hash = $location.path();
            var hash = $state.current.url;
            var name = $state.current.name;
			var username = "username";// WL.Client.getUserName('iDubaiRealm' ) == null ? 'anonymous' : WL.Client.getUserName('iDubaiRealm');
            if (hitType == 'screenview') {
                ga('send', 'screenview', {
                    screenName: screenName,
                    appVersion: WL.Client.getAppProperty("APP_VERSION")
                });
            }

            if (hitType == 'event') {
                ga('send', 'event', eventPayload);
            }



			$logger.log("Google Analytics : Route details pushed to " + trackId);
		}catch(e){
			$logger.log(e);
		}
	};
	return {trackRoute : trackRoute};
}]);
