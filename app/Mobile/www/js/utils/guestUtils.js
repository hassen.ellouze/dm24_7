
/* JavaScript content from js/utils/guestUtils.js in folder common */
iDubaiApp.factory('GuestUtils', ['$rootScope', 'PhoneUtils', 'underscore','$DbStorageService',
        function ($rootScope, PhoneUtils, underscore,$DbStorageService) {

            var _ = underscore;

            /**
             * Save name
             * @param name
             * @param email
             * @param country
             * @param isPetOwner
             * @param isFarmOwner
             * @param isFarmAnimals
             * @param isEmployee
             * @param mobileArea
             */
            function saveGuestInfo(mobileNumber, name, email, country, option1, option2, option3, option4, mobileArea) {
                var guestInfo = {
                    mobileNumber: mobileNumber,
                    name: name,
                    email: email,
                    country: country,
                    option1: option1,
                    option2: option2,
                    option3: option3,
                    option4: option4,
                    mobileArea: mobileArea,
                };
                localStorage.setItem('guestInfo', JSON.stringify(guestInfo));
                window.db.transaction(function(tx) {
                    // mobile, name, email, country, option1, option2, option3, option4, mobileArea   
                                  
                        tx.executeSql('INSERT INTO personalDetails VALUES (? , ? , ? , ? , ? , ?  , ? , ? , ?)  ', [mobileNumber, name, email , country, option1, option2, option3, option4, mobileArea],function(tx, res){
                            console.log('INSERT INTO personalDetails',JSON.stringify(tx), JSON.stringify(res));
                            
                        });
                      }, function(error) {
                        console.log('Transaction ERROR: ' + error.message);
                      }, function() {
                        console.log('Populated database OK');
                      });
            }
            /**
             *  update Guest info
             *
             * */
            function updateGuestInfo(mobileNumber, name, email, country) {

                var guestObj = JSON.parse(localStorage.getItem('guestInfo'));
                guestObj.mobileNumber = mobileNumber;
                guestObj.name = name;
                guestObj.email = email;
                guestObj.country = country;
                localStorage.setItem('guestInfo', JSON.stringify(guestObj));
                window.db.transaction(function(tx) {
                      
                                  
                        tx.executeSql('UPDATE personalDetails SET mobileNumber = ? , name = ? , email = ? , country = ? ', [mobileNumber, name, email , country],function(tx, res){
                            console.log('UPDATE personalDetails',JSON.stringify(tx), JSON.stringify(res));
                            
                        });
                      }, function(error) {
                        console.log('Transaction ERROR: ' + error.message);
                      }, function() {
                        console.log('Populated database OK');
                      });
            }


            /**
             * Save name
             * @param name
             */
            function saveGuestName(name) {
                var guestInfo = {
                    mobileNumber: getGuestPhoneNumber(),
                    name: name,
                };
                localStorage.setItem('guestInfo', JSON.stringify(guestInfo));
            }

            /**
             * Return guest name
             */
            function getGuestName() {
                var guestObj = JSON.parse(localStorage.getItem('guestInfo'));
                if (!_.isEmpty(guestObj)) {
                    return (guestObj.name) ? guestObj.name : "";
                }
                return "";
            }

            /**
             * Return guest email
             */
            function getGuestEmail() {
                var guestObj = JSON.parse(localStorage.getItem('guestInfo'));
                if (!_.isEmpty(guestObj)) {
                    return (guestObj.email) ? guestObj.email : "";
                }
                return "";
            }

            /**
             * Return guest Info
             */
            function getGuestInfo() {
                var guestObj = JSON.parse(localStorage.getItem('guestInfo'));
                if (!_.isEmpty(guestObj)) {
                    return guestObj ;
                }
                return "";
            }


            /**
             * Save quest phone number
             * @param mobileNumber
             */
            function saveGuestPhoneNumber(mobileNumber) {
                if (PhoneUtils.formatPhoneNumber(mobileNumber + "") !== getGuestPhoneNumber()) {
                    $DbStorageService.remove('requests_json_store');
                }
                var guestInfo = {
                    mobileNumber: PhoneUtils.formatPhoneNumber(mobileNumber + ""),
                    name : getGuestName()
                };
                localStorage.setItem('guestInfo', JSON.stringify(guestInfo));
            }

            /**
             * Return guest phone number
             */
            function getGuestPhoneNumber() {
                var guestObj = JSON.parse(localStorage.getItem('guestInfo'));
                if (!_.isEmpty(guestObj)) {
                    return (guestObj.mobileNumber) ? guestObj.mobileNumber : "";
                }
                return "";
            }

            /**
             * Check if guest has data
             */
            function isEmptyGuest() {
                var guestObj = JSON.parse(localStorage.getItem('guestInfo'));
                return _.isEmpty(guestObj);
            }

            /**
             * Check if is guest
             */
            function isGuest() {
                return !$rootScope.isLoggedIn;
            }


            /**
             * Delete guest info
             */
            function deleteGuest() {
                localStorage.removeItem("guestInfo");
            }

            return {
                saveGuestName: saveGuestName,
                getGuestName: getGuestName,
                getGuestEmail: getGuestEmail,
                saveGuestPhoneNumber: saveGuestPhoneNumber,
                getGuestPhoneNumber: getGuestPhoneNumber,
                isEmptyGuest: isEmptyGuest,
                deleteGuest: deleteGuest,
                isGuest: isGuest,
                saveGuestInfo: saveGuestInfo,
                getGuestInfo: getGuestInfo,
                updateGuestInfo: updateGuestInfo
            };

        }
    ]
);
