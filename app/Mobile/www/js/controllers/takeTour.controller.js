iDubaiApp.controller('takeTourController', ['$scope', '$state', function ($scope, $state) {

    //$scope.currentLang= $('html').attr('lang')
    $scope.currentLang = localStorage.getItem('dm-lang');
    console.log($scope.currentLang);


    $scope.spotWith = document.querySelectorAll(".welcome-slider")[0].clientWidth;
    $scope.snapPos = [0, -($scope.spotWith), -($scope.spotWith * 2), -($scope.spotWith * 3), -($scope.spotWith * 4)];
    $scope.snapPos1 = [0, ($scope.spotWith), ($scope.spotWith * 2), ($scope.spotWith * 3), ($scope.spotWith * 4)];

    $scope.snaps = $scope.currentLang == 'ar' ? $scope.snapPos1 : $scope.snapPos;
    console.log($scope.snaps);

    $('.welcome-skips').click(function () {
        var onboardBG1 = document.getElementById("onboardBG1");
        var onboardBG2 = document.getElementById("onboardBG2");
        var onboardBG3 = document.getElementById("onboardBG3");
        var onboardBG4 = document.getElementById("onboardBG4");
        var spotHoler = document.getElementById("spot-holder");
        if ($scope.currentLang === 'ar') {
            console.log('its arab');
            onboardBG1.classList.add("animate2");
            onboardBG2.classList.add("animate2");
            onboardBG3.classList.add("animate2");
            onboardBG4.classList.add("animate2");
            spotHoler.classList.add("animate2");
            onboardBG1.style.webkitTransform = 'translate3d(' + (($scope.spotWith * 3.19)) + 'px, 0px, 0px)';
            onboardBG2.style.webkitTransform = 'translate3d(' + (($scope.spotWith * 3.25)) + 'px, 0px, 0px)';
            // clouds2.style.webkitTransform = 'translate3d(' + (restPosition *0.4) + 'px, 0px, 0px)';
            onboardBG3.style.webkitTransform = 'translate3d(' + (($scope.spotWith * 2.4)) + 'px, 0px, 0px)';
            onboardBG4.style.webkitTransform = 'translate3d(' + (($scope.spotWith * 2.1)) + 'px, 0px, 0px)';
            // clouds2.style.transform = 'translate3d(' + (restPosition *0.4) + 'px, 0px, 0px)';
            onboardBG1.style.transform = 'translate3d(' + (($scope.spotWith * 3.19)) + 'px, 0px, 0px)';
            onboardBG2.style.transform = 'translate3d(' + (($scope.spotWith * 3.25)) + 'px, 0px, 0px)';
            onboardBG3.style.transform = 'translate3d(' + (($scope.spotWith * 2.4)) + 'px, 0px, 0px)';
            onboardBG4.style.transform = 'translate3d(' + (($scope.spotWith * 2.1)) + 'px, 0px, 0px)';
            spotHoler.style.transform = 'translate3d(' + ($scope.spotWith * 4) + 'px, 0px, 0px)';
            // $timeout(function(){
            //     $location.path("/dashboard");
            //     $('')
            //
            // },1000);
        } else {
            onboardBG1.classList.add("animate2");
            onboardBG2.classList.add("animate2");
            onboardBG3.classList.add("animate2");
            onboardBG4.classList.add("animate2");
            spotHoler.classList.add("animate2");
            onboardBG1.style.webkitTransform = 'translate3d(' + ((-$scope.spotWith * 3.19)) + 'px, 0px, 0px)';
            onboardBG2.style.webkitTransform = 'translate3d(' + ((-$scope.spotWith * 3.25)) + 'px, 0px, 0px)';
            // clouds2.style.webkitTransform = 'translate3d(' + (restPosition *0.4) + 'px, 0px, 0px)';
            onboardBG3.style.webkitTransform = 'translate3d(' + ((-$scope.spotWith * 2.4)) + 'px, 0px, 0px)';
            onboardBG4.style.webkitTransform = 'translate3d(' + ((-$scope.spotWith * 2.1)) + 'px, 0px, 0px)';
            // clouds2.style.transform = 'translate3d(' + (restPosition *0.4) + 'px, 0px, 0px)';
            onboardBG1.style.transform = 'translate3d(' + ((-$scope.spotWith * 3.19)) + 'px, 0px, 0px)';
            onboardBG2.style.transform = 'translate3d(' + ((-$scope.spotWith * 3.25)) + 'px, 0px, 0px)';
            onboardBG3.style.transform = 'translate3d(' + ((-$scope.spotWith * 2.4)) + 'px, 0px, 0px)';
            onboardBG4.style.transform = 'translate3d(' + ((-$scope.spotWith * 2.1)) + 'px, 0px, 0px)';
            spotHoler.style.transform = 'translate3d(' + (-$scope.spotWith * 4) + 'px, 0px, 0px)';
        }

    });
}]);

angular.module("iDubaiApp").directive('swipeAndSnap', function ($location, $timeout) {
    return {

        link: function (scope, element, attr) {
            // var clouds2= document.getElementById("clouds2");
            var onboardBG1 = document.getElementById("onboardBG1");
            var onboardBG2 = document.getElementById("onboardBG2");
            var onboardBG3 = document.getElementById("onboardBG3");
            var onboardBG4 = document.getElementById("onboardBG4");
            scope.currentLang = localStorage.getItem('dm-lang');
            scope.spotWith = document.querySelectorAll(".welcome-slider")[0].clientWidth;
            var snapLocations = JSON.parse(attr.snapLocations),
                restPosition = 0, // Define the location to end.
                positionX = 0; // The current position.
            scope.bestSnap = 0;
            if (scope.currentLang === 'en') {
                console.log($('html').attr('lang'));
                var calculate_snap_location = function (position) {

                    // Used to store each difference between current position and each snap point.
                    var currentDiff;

                    // Used to store the current best difference.
                    var minimumDiff;

                    // User to store the best snap position.
                    var bestSnap;

                    // We're going to cycle through each snap location
                    // and work out which is closest to the current position.
                    for (var i = 0; i < snapLocations.length; i++) {

                        // Calculate the difference.
                        currentDiff = Math.abs(positionX - snapLocations[i]);

                        // Works out if this difference is the closest yet.
                        if (minimumDiff === undefined || currentDiff < minimumDiff) {
                            minimumDiff = currentDiff;
                            bestSnap = snapLocations[i];
                        }
                    }

                    scope.bestSnap = bestSnap;
                    return bestSnap;

                };

                /**
                 * Perform any setup for the drag actions.
                 */
                Hammer(element[0]).on("dragstart", function (ev) {

                    // We dont want an animation delay when dragging.
                    element.removeClass('animate');
                    // clouds2.classList.remove("animate");
                    onboardBG1.classList.remove("animate");
                    onboardBG2.classList.remove("animate");
                    onboardBG4.classList.remove("animate");
                    onboardBG3.classList.remove("animate");
                });

                /**
                 * Follow the drag position when the user is interacting.
                 */
                Hammer(element[0]).on("drag", function (ev) {
                    // Set the current position.
                    positionX = restPosition + parseInt(ev.gesture.deltaX);
                    onboardBG1.style.webkitTransform = 'translate3d(' + (positionX * 0.8) + 'px, 0px, 0px)';
                    onboardBG2.style.webkitTransform = 'translate3d(' + (positionX * 1.2) + 'px, 0px, 0px)';
                    // clouds2.style.webkitTransform = 'translate3d(' + (positionX*0.4) + 'px, 0px, 0px)';
                    onboardBG3.style.webkitTransform = 'translate3d(' + (positionX * 0.4) + 'px, 0px, 0px)';
                    onboardBG4.style.webkitTransform = 'translate3d(' + (positionX * 0.1) + 'px, 0px, 0px)';
                    // clouds2.style.transform = 'translate3d(' + (positionX*0.4) + 'px, 0px, 0px)';
                    onboardBG1.style.transform = 'translate3d(' + (positionX * 0.8) + 'px, 0px, 0px)';
                    onboardBG2.style.transform = 'translate3d(' + (positionX * 1.2) + 'px, 0px, 0px)';
                    onboardBG3.style.transform = 'translate3d(' + (positionX * 0.4) + 'px, 0px, 0px)';
                    onboardBG4.style.transform = 'translate3d(' + (positionX * 0.1) + 'px, 0px, 0px)';
                    element.css('-webkit-transform', 'translate3d(' + positionX + 'px,0px,0px)');
                    element.css('transform', 'translate3d(' + positionX + 'px,0px,0px)');
                    // if ($('.welcome-dots .dot-item.lastDot').hasClass('active')){
                    //     $('.welcome-dots .dot-item.lastDot').toggleClass('active');
                    // }
                    // if (!$('.welcome-dots .dot-item.firstDot').hasClass('active')){
                    //     $('.welcome-dots .dot-item.firstDot').toggleClass('active');
                    // }

                });

                /**
                 * The drag is finishing so we'll animate to a snap point.
                 */
                Hammer(element[0]).on("dragend", function (ev) {
                    element.addClass('animate');
                    // clouds2.classList.add("animate");
                    onboardBG1.classList.add("animate");
                    onboardBG2.classList.add("animate");
                    onboardBG3.classList.add("animate");
                    onboardBG4.classList.add("animate");
                    // Work out where we should "snap" to.
                    restPosition = calculate_snap_location(positionX);
                    scope.$apply(function () {
                        scope.bestSnap = restPosition;
                    });

                    onboardBG1.style.webkitTransform = 'translate3d(' + (restPosition * 0.8) + 'px, 0px, 0px)';
                    onboardBG2.style.webkitTransform = 'translate3d(' + (restPosition * 1.2) + 'px, 0px, 0px)';
                    // clouds2.style.webkitTransform = 'translate3d(' + (restPosition *0.4) + 'px, 0px, 0px)';
                    onboardBG3.style.webkitTransform = 'translate3d(' + (restPosition * 0.4) + 'px, 0px, 0px)';
                    onboardBG4.style.webkitTransform = 'translate3d(' + (restPosition * 0.1) + 'px, 0px, 0px)';
                    // clouds2.style.transform = 'translate3d(' + (restPosition *0.4) + 'px, 0px, 0px)';
                    onboardBG1.style.transform = 'translate3d(' + (restPosition * 0.8) + 'px, 0px, 0px)';
                    onboardBG2.style.transform = 'translate3d(' + (restPosition * 1.2) + 'px, 0px, 0px)';
                    onboardBG3.style.transform = 'translate3d(' + (restPosition * 0.4) + 'px, 0px, 0px)';
                    onboardBG4.style.transform = 'translate3d(' + (restPosition * 0.1) + 'px, 0px, 0px)';
                    element.css('-webkit-transform', 'translate3d(' + restPosition + 'px, 0px, 0px)');
                    element.css('transform', 'translate3d(' + restPosition + 'px, 0px, 0px)');
                });
            } else {
                console.log($('html').attr('lang'));
                var arspotWitdh = document.querySelectorAll(".welcome-slider")[0].clientWidth;
                var calculate_snap_location = function (position) {

                    // Used to store each difference between current position and each snap point.
                    var currentDiff;

                    // Used to store the current best difference.
                    var minimumDiff;

                    // User to store the best snap position.
                    var bestSnap;

                    // We're going to cycle through each snap location
                    // and work out which is closest to the current position.
                    for (var i = 0; i < snapLocations.length; i++) {

                        // Calculate the difference.
                        currentDiff = Math.abs(positionX - snapLocations[i]);

                        // Works out if this difference is the closest yet.
                        if (minimumDiff === undefined || currentDiff < minimumDiff) {
                            minimumDiff = currentDiff;
                            bestSnap = snapLocations[i];
                        }
                    }

                    scope.bestSnap = bestSnap;
                    return bestSnap;

                };

                /**
                 * Perform any setup for the drag actions.
                 */
                Hammer(element[0]).on("dragstart", function (ev) {

                    // We dont want an animation delay when dragging.
                    element.removeClass('animate');
                    // clouds2.classList.remove("animate");
                    onboardBG1.classList.remove("animate");
                    onboardBG2.classList.remove("animate");
                    onboardBG4.classList.remove("animate");
                    onboardBG3.classList.remove("animate");
                });

                /**
                 * Follow the drag position when the user is interacting.
                 */
                Hammer(element[0]).on("drag", function (ev) {
                    // Set the current position.
                    positionX = -(restPosition + parseInt(ev.gesture.deltaX));
                    onboardBG1.style.webkitTransform = 'translate3d(' + (-positionX * 0.8) + 'px, 0px, 0px)';
                    onboardBG2.style.webkitTransform = 'translate3d(' + (-positionX * 1.2) + 'px, 0px, 0px)';
                    // clouds2.style.webkitTransform = 'translate3d(' + (positionX*0.4) + 'px, 0px, 0px)';
                    onboardBG3.style.webkitTransform = 'translate3d(' + (-positionX * 0.4) + 'px, 0px, 0px)';
                    onboardBG4.style.webkitTransform = 'translate3d(' + (-positionX * 0.1) + 'px, 0px, 0px)';
                    // clouds2.style.transform = 'translate3d(' + (positionX*0.4) + 'px, 0px, 0px)';
                    onboardBG1.style.transform = 'translate3d(' + (-positionX * 0.8) + 'px, 0px, 0px)';
                    onboardBG2.style.transform = 'translate3d(' + (-positionX * 1.2) + 'px, 0px, 0px)';
                    onboardBG3.style.transform = 'translate3d(' + (-positionX * 0.4) + 'px, 0px, 0px)';
                    onboardBG4.style.transform = 'translate3d(' + (-positionX * 0.1) + 'px, 0px, 0px)';
                    element.css('-webkit-transform', 'translate3d(' + (-positionX) + 'px,0px,0px)');
                    element.css('transform', 'translate3d(' + (-positionX) + 'px,0px,0px)');
                });

                /**
                 * The drag is finishing so we'll animate to a snap point.
                 */
                Hammer(element[0]).on("dragend", function (ev) {
                    element.addClass('animate');
                    // clouds2.classList.add("animate");
                    onboardBG1.classList.add("animate");
                    onboardBG2.classList.add("animate");
                    onboardBG3.classList.add("animate");
                    onboardBG4.classList.add("animate");
                    // Work out where we should "snap" to.
                    restPosition = -(calculate_snap_location(positionX));
                    console.log(restPosition);
                    scope.$apply(function () {
                        scope.bestSnap = restPosition;
                    });

                    onboardBG1.style.webkitTransform = 'translate3d(' + (restPosition * 0.8) + 'px, 0px, 0px)';
                    onboardBG2.style.webkitTransform = 'translate3d(' + (restPosition * 1.2) + 'px, 0px, 0px)';
                    // clouds2.style.webkitTransform = 'translate3d(' + (restPosition *0.4) + 'px, 0px, 0px)';
                    onboardBG3.style.webkitTransform = 'translate3d(' + (restPosition * 0.4) + 'px, 0px, 0px)';
                    onboardBG4.style.webkitTransform = 'translate3d(' + (restPosition * 0.1) + 'px, 0px, 0px)';
                    // clouds2.style.transform = 'translate3d(' + (restPosition *0.4) + 'px, 0px, 0px)';
                    onboardBG1.style.transform = 'translate3d(' + (restPosition * 0.8) + 'px, 0px, 0px)';
                    onboardBG2.style.transform = 'translate3d(' + (restPosition * 1.2) + 'px, 0px, 0px)';
                    onboardBG3.style.transform = 'translate3d(' + (restPosition * 0.4) + 'px, 0px, 0px)';
                    onboardBG4.style.transform = 'translate3d(' + (restPosition * 0.1) + 'px, 0px, 0px)';
                    element.css('-webkit-transform', 'translate3d(' + (restPosition) + 'px, 0px, 0px)');
                    element.css('transform', 'translate3d(' + (restPosition) + 'px, 0px, 0px)');
                });
            }
        }
    };

});
