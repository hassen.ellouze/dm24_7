var username = 'dm360_sys';
var password = 'dUbaI_MunP@ss';
function createNotification(ServiceTypeId, ServiceId, ServiceCategoryId, Street, Building, AreaId, Makani, Floor, Flat, Location_X, Location_Y, AdditionalAddressDetails,
                            SLA, SourceOfCreation, PetTypeId, InsectTypeId, Description, ContactFullName, ContactMobileNumber, ContactEmail, ContactEmirateId,
                            LicenseNo, TradeLicenseNameEN, TradeLicesnseNameAR, SmartCrmTransactionId, ShowContactData, PreferredDate, PreferredTime, VeterniaryCardNumber, Attachment) {

    if (Attachment !== undefined) {
        var req = {
            "ServiceTypeId": ServiceTypeId,
            "ServiceId": ServiceId,
            "ServiceCategoryId": ServiceCategoryId,
            "Street": Street,
            "Building": Building,
            "AreaId": AreaId,
            "Makani": Makani,
            "Floor": Floor ? Floor : "",
            "Flat": Flat ? Flat : "",
            "Location_X": Location_X,
            "Location_Y": Location_Y,
            "AdditionalAddressDetails": AdditionalAddressDetails,
            "SLA": SLA,
            "SourceOfCreation": SourceOfCreation,
            "PetTypeId": PetTypeId,
            "InsectTypeId": InsectTypeId,
            "Description": Description,
            "ContactFullName": ContactFullName,
            "ContactMobileNumber": ContactMobileNumber,
            "ContactEmail": ContactEmail,
            "ContactEmirateId": ContactEmirateId,
            "LicenseNo": LicenseNo,
            "TradeLicenseNameEN": TradeLicenseNameEN,
            "TradeLicesnseNameAR": TradeLicesnseNameAR,
            "SmartCrmTransactionId": SmartCrmTransactionId,
            "ShowContactData": ShowContactData,
            "PreferredDate": PreferredDate,
            "PreferredTime": PreferredTime,
            "VeterniaryCardNumber": VeterniaryCardNumber,
            "Attachments": Attachment
        };
    }else {
        var req = {
            "ServiceTypeId": ServiceTypeId,
            "ServiceId": ServiceId,
            "ServiceCategoryId": ServiceCategoryId,
            "Street": Street,
            "Building": Building,
            "AreaId": AreaId,
            "Makani": Makani,
            "Floor": Floor ? Floor : "",
            "Flat": Flat ? Flat : "",
            "Location_X": Location_X,
            "Location_Y": Location_Y,
            "AdditionalAddressDetails": AdditionalAddressDetails,
            "SLA": SLA,
            "SourceOfCreation": SourceOfCreation,
            "PetTypeId": PetTypeId,
            "InsectTypeId": InsectTypeId,
            "Description": Description,
            "ContactFullName": ContactFullName,
            "ContactMobileNumber": ContactMobileNumber,
            "ContactEmail": ContactEmail,
            "ContactEmirateId": ContactEmirateId,
            "LicenseNo": LicenseNo,
            "TradeLicenseNameEN": TradeLicenseNameEN,
            "TradeLicesnseNameAR": TradeLicesnseNameAR,
            "SmartCrmTransactionId": SmartCrmTransactionId,
            "ShowContactData": ShowContactData,
            "PreferredDate": PreferredDate,
            "PreferredTime": PreferredTime,
            "VeterniaryCardNumber": VeterniaryCardNumber
        };
    }

    var encoded = com.dm.common.Base64.encodeString(username + ":" + password);

    var input = {
        method: 'post',
        path: "NotificationsSystem/api/crmticket/Add",
        headers: {
            'Authorization': 'basic ' + encoded,
        },
        body: {
            contentType: 'application/json',
            content: JSON.stringify(req)
        }
    };
    WL.Logger.error("createNotification :: request  : " + JSON.stringify(input));
    var x = WL.Server.invokeHttp(input);
    WL.Logger.error("createNotification :: response : " + JSON.stringify(x));
    return x;
}
