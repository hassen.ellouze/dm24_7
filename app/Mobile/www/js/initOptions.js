
var wlInitOptions = {

    connectOnStartup: true,
    // # To disable automatic hiding of the splash screen uncomment this property and use WL.App.hideSplashScreen() API
    autoHideSplash: false,

    // # The callback function to invoke in case application fails to connect to MobileFirst Server
    onConnectionFailure: function (response) {
        console.log("Connection to MobileFirst dev server failed.");
        wlCommonInit(response);
    },

    // # MobileFirst Server connection timeout
    timeout: 10000,

    // # How often heartbeat request will be sent to MobileFirst Server
    heartBeatIntervalInSecs: 5 * 60,

    // # Enable FIPS 140-2 for data-in-motion (network) and data-at-rest (JSONStore) on iOS or Android.
    //   Requires the FIPS 140-2 optional feature to be enabled also.
    //enableFIPS : false,

    // # The options of busy indicator used during application start up
    /*busyOptions: {
        text: "Loading..."
    }*/
};

if (window.addEventListener) {
    window.addEventListener('load', function () {
        WL.Client.init(wlInitOptions);
        //WL.Client.init({});
    }, false);
} else if (window.attachEvent) {
    window.attachEvent('onload', function () {
        WL.Client.init(wlInitOptions);
    });
}


$('.inputMobile').on('focus', function () {

    $(".inputMobile").addClass("otp");

});

$('.inputMobile').on('blur', function () {
    $(".inputMobile").removeClass("otp");
});


/*
$(function () {
    FastClick.attach(document.body);
});*/