iDubaiApp.controller('servicesAZController', ['$scope', '$state', '$dmServices', '$stopWords', '$location', 'LoadingService', 'cat', 'underscore', '$listEnglishAZ', '$listArabicAZ', '$stateParams', 'SharedService',
    function ($scope, $state, $dmServices, $stopWords, $location, LoadingService, cat, underscore, $listEnglishAZ, $listArabicAZ, $stateParams, SharedService) {

        var _ = underscore;
        var currentLang = localStorage.getItem('dm-lang');

        var englishAlphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
        var arabicAlphabet = ['أ', 'ب', 'ت', 'ث', 'ج', 'ح', 'خ', 'د', 'ذ', 'ر', 'ز', 'س', 'ش', 'ص', 'ض', 'ط', 'ظ', 'ع', 'غ', 'ف', 'ق', 'ك', 'ل', 'م', 'ن', 'هـ', 'و', 'ي'];

        $scope.azFilter = currentLang === 'ar' ? arabicAlphabet : englishAlphabet;
        $stateParams = SharedService.getStateHistory($state.current.name);

        function cleanList(services) {
            var tmp = [];
            var previousService = {
                Letter: ''
            };

            var remainingLetters = [];

            services.map(function (service) {
                if (previousService.Letter && !service.Letter) {
                    tmp.push({
                        Letter: previousService.Letter
                    });

                    remainingLetters.push(previousService.Letter);

                    tmp.push({
                        originalTitle: service.originalTitle,
                        title: service.title,
                        category_id: service.category_id,
                        unit: service.unit
                    });
                } else if (!previousService.Letter && !service.Letter) {
                    tmp.push({
                        originalTitle: service.originalTitle,
                        title: service.title,
                        category_id: service.category_id,
                        unit: service.unit
                    });
                }
                previousService = service;
            });

            $scope.azFilter = remainingLetters;
            return tmp;
        }

        $scope.goBack = function () {
            var prev=SharedService.getStateHistory($state.current.name).from;
            $state.go(prev);
        }

        //Services list coming from app.js
        //var originalAZ = currentLang == 'ar' ? cleanList(JSON.parse(localStorage.getItem('listArabicAZ'))) : cleanList(JSON.parse(localStorage.getItem('listEnglishAZ')));
        var originalAZ = currentLang == 'ar' ? cleanList($listArabicAZ) : cleanList($listEnglishAZ);

        // =================================================================================================================================================================


        $scope.servicesList = originalAZ;

        if (currentLang == 'ar') {
            console.log(localStorage.getItem('listArabicAZ'));
        } else {
            console.log(localStorage.getItem('listEnglishAZ'));
        }

        // var localServiceTitles = $serviceTitles;
        // var localDMServices = $dmServices;
        // var organizationByService = [];
        $scope.selectedFilters = [];
        $scope.selectedFilterNames = [];
        $scope.selectedFullFilters = [];

        var tempOrgnz = [];

        $dmServices.some(function (service) {
            if (currentLang == 'ar') {
                if (tempOrgnz.indexOf(service.organization_unit_ar) === -1) {
                    tempOrgnz.push(service.organization_unit_ar);
                }
            } else {
                if (tempOrgnz.indexOf(service.organization_unit_en) === -1) {
                    tempOrgnz.push(service.organization_unit_en);
                }
            }
        });

        // function getCategoryName(category_id) {
        //     switch (ategory_id) {
        //         case 1:
        //             return 'Pest Control';
        //         case 2:
        //             return 'Pets & Animals';
        //         case 3:
        //             return 'Drainage & Irrigation';
        //         case 4:
        //             return 'Waste Management';
        //         case 5:
        //             console.log("Cherries are $3.00 a pound.");
        //             break;
        //         case 6:
        //             console.log("Cherries are $3.00 a pound.");
        //             break;
        //         case 7:
        //             console.log("Cherries are $3.00 a pound.");
        //             break;
        //         case 8:
        //             console.log("Cherries are $3.00 a pound.");
        //             break;
        //         default:
        //             console.log("Sorry, we are out of " + expr + ".");
        //     }
        // }


        $scope.organizations = [];

        tempOrgnz.some(function (unit) {
            $scope.organizations.push({
                filter: unit,
                class: unit.toLowerCase().replace(/ /g, '-').replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '_'),
                type: 'unit'
            });
        });

        $scope.hideElements = false;

        $scope.filterItems = [
            {
                name: 'Building & Construction',
                class: 'construction',
                category_id: 7
            },
            {
                name: 'Drainage & Irrigation',
                class: 'drainageSewage',
                category_id: 3
            },
            {
                name: 'Environment',
                class: 'environment',
                category_id: 6
            },
            {
                name: 'Food Safety',
                class: 'foodSafety',
                category_id: 8
            },
            {
                name: 'Health & Safety',
                class: 'healthSafety',
                category_id: 5
            },
            {
                name: 'Pets & Animals',
                class: 'petsAnimals',
                category_id: 2
            },
            {
                name: 'Pest Control',
                class: 'pestControl',
                category_id: 1
            },
            {
                name: 'Waste Management',
                class: 'wasteManagement',
                category_id: 4
            }
        ];

        function categoryExist(categoriesList, category) {
            var EXIST = -1;
            if (categoriesList) {
                for (var i = 0; i < categoriesList.length; i++) {
                    if (categoriesList[i].filter == category) {
                        EXIST = i;
                        break;
                    }
                }

                return EXIST;
            }
        }

        function unitExist(unitsList, unit) {
            var EXIST = -1;
            if (unitsList) {
                for (var i = 0; i < unitsList.length; i++) {
                    if (unitsList[i].filter == unit) {
                        EXIST = i;
                        break;
                    }
                }

                return EXIST;
            }
        }

        function findCategory(category_id) {
            var foundCategory = null;
            cat.some(function (category) {
                if (category.category_id === category_id) {
                    foundCategory = category;
                }
            });

            return foundCategory;
        }

        function findService(title) {
            var foundService = null;
            var serviceTitle = null;
            $dmServices.some(function (service) {
                //serviceTitle = currentLang == 'ar' ? service.service_title_ar : service.service_title_en;
                serviceTitle = currentLang == 'ar' ? service.serviceAR : service.service_title_en;
                if (serviceTitle === title) {
                    foundService = service;
                }
            });

            return foundService;
        }

        // $scope.toggleShow = false;
        $scope.goTo = function (elementId) {
            $location.hash(elementId + '-anchor');
        };

        $scope.cancelFilters = function () {
            // $scope.selectedFilters = [];
            // $scope.selectedFilterNames = [];
            // $scope.selectedFullFilters = [];
        };

        // Heading to service details
        $scope.toServiceDetails = function (category_id, serviceTitle) {
            console.log('serviceTitle :: ', serviceTitle);
            var payload;
            if ($stateParams.payload) {
                payload = $stateParams.payload;
            }

            SharedService.setStateHistory('serviceDetails', {
                category: findCategory(category_id),
                service: findService(serviceTitle),
                payload: payload,
                from: $state.current.name,
            })

            $state.go('serviceDetails', {
                category: findCategory(category_id),
                service: findService(serviceTitle),
                payload: payload

            });

        }

        // Applying category filters
        $scope.selectedCategoryFilters = [];
        $scope.toggleCategoryFilter = function (filterName, category, filterClass, toggle) {
            if (toggle) {
                if (categoryExist($scope.selectedCategoryFilters, category) === -1) {
                    $scope.selectedCategoryFilters.push({
                        name: filterName,
                        filter: category,
                        class: filterClass,
                        type: 'category'
                    });
                }
            } else {
                if (categoryExist($scope.selectedCategoryFilters, category) !== -1) {
                    $scope.selectedCategoryFilters.splice(categoryExist($scope.selectedCategoryFilters, category), 1);
                }
            }
        };

        // Applying organization filters
        $scope.selectedUnitFilters = [];
        $scope.toggleUnitFilter = function (unit, filterClass, toggle) {
            if (toggle) {
                if (unitExist($scope.selectedUnitFilters, unit) === -1) {
                    $scope.selectedUnitFilters.push({
                        filter: unit,
                        class: filterClass,
                        type: 'unit'
                    });
                }
            } else {
                if (unitExist($scope.selectedUnitFilters, unit) !== -1) {
                    $scope.selectedUnitFilters.splice(unitExist($scope.selectedUnitFilters, unit), 1);
                }
            }
        };

        // Removing filters
        $scope.removeFilter = function (filter) {
            $scope.searchText = '';
            if (filter.type === 'category') {
                if (categoryExist($scope.selectedCategoryFilters, filter.filter) !== -1) {
                    $scope.selectedCategoryFilters.splice(categoryExist($scope.selectedCategoryFilters, filter.filter), 1);

                }
            } else if (filter.type === 'unit') {
                if (unitExist($scope.selectedUnitFilters, filter.filter) !== -1) {
                    $scope.selectedUnitFilters.splice(unitExist($scope.selectedUnitFilters, filter.filter), 1);
                }
            }
            $('.' + filter.class).addClass('filterWhite').removeClass(filter.class);
            // $scope.$apply();
            $scope.applyFilters();
        };

        // Search by filters
        $scope.applyFilters = function () {
            $scope.searchText = '';
            if ($scope.selectedCategoryFilters || $scope.selectedUnitFilters) {
                $scope.servicesList = originalAZ;
                var searchServices = [];

                if ($scope.selectedCategoryFilters.length > 0) {
                    $scope.servicesList.map(function (service) {
                        if (service.Letter) {
                            searchServices.push({
                                Letter: service.Letter
                            });
                        } else {
                            if (categoryExist($scope.selectedCategoryFilters, service.category_id) !== -1) {
                                searchServices.push({
                                    originalTitle: service.originalTitle,
                                    title: service.title,
                                    category_id: service.category_id,
                                    unit: service.unit
                                });
                            }
                        }
                    });
                } else {
                    searchServices = $scope.servicesList;
                }

                $scope.servicesList = searchServices;
                if ($scope.selectedUnitFilters.length > 0) {
                    searchServices = [];
                    $scope.servicesList.map(function (service) {
                        if (service.Letter) {
                            searchServices.push({
                                Letter: service.Letter
                            });
                        } else {
                            if (unitExist($scope.selectedUnitFilters, service.unit) !== -1) {
                                searchServices.push({
                                    originalTitle: service.originalTitle,
                                    title: service.title,
                                    category_id: service.category_id,
                                    unit: service.unit
                                });
                            }
                        }
                    });
                }

                $scope.servicesList = cleanList(searchServices);

                console.log('TCL: $scope.servicesList after search', $scope.servicesList);
                // $scope.$apply();
                $scope.limit = 0;
                $scope.loadMore();
            } else {
                $scope.servicesList = originalAZ;
                $scope.$apply();
            }
        };

        // Search by text
        $scope.searchAZ = _.debounce(function () {
            console.log("searchAZ");
            document.getElementById("servicesAZSearch").blur();
        }, 1200);


        $scope.limit = 0;
        $scope.loadMore = function () {
            $scope.weather = '';
            if ($scope.limit < $scope.servicesList.length) {
                console.log('Loading more services...');
                // LoadingService.show();
                //$scope.limit += 20;
                $scope.limit = $scope.servicesList.length;
                // LoadingService.hide();
                // $scope.$apply();
                // $scope.servicesList = originalAZ;
            }
        };

        $scope.loadMore();

    }])
    .filter('searchByTitle', function () {
        return function (currentList, searchText) {

            if (searchText) {
                var filtered = [];
                angular.forEach(currentList, function (item) {

                    if (item.title.toUpperCase().indexOf(searchText.toUpperCase()) !== -1)
                        filtered.push(item);
                });

                return filtered;
            }
            else {
                return currentList;
            }

        };
    });
