/*
 *  Licensed Materials - Property of IBM
 *  5725-I43 (C) Copyright IBM Corp. 2011, 2016. All Rights Reserved.
 *  US Government Users Restricted Rights - Use, duplication or
 *  disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */


// Staging
// var user = 'NEnMG6t9aYvq33btu88_fkGzaysa';
// var password = 'Ywf4lO9TnZBCh_Ax7ZOVVpKuf_ga';
// Production
var user = '6_ZklQFYJ8f0kEMbzyEfeBlSApsa';
var password = 'xa27J_chBcXf0_mBEUphjKjlWf4a';

function getToken(model) {
    WL.Logger.error("===> GET TOKEN input  <===");
    var responseToken = null;
    var tokenValue = null;

    try {
        var path = "oauth2/token?grant_type=password&username=" + model.username + "&password=" + model.password + "&scope=" + model.scope;
        var encoded = com.dm.common.utils.Base64.encodeString(user + ":" + password);
        // var encoded = "Nl9aa2xRRllKOGYwa0VNYnp5RWZlQmxTQXBzYTp4YTI3Sl9jaEJjWGYwX21CRVVwaGpLamxXZjRh";

        var input = {
            method: 'post',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'basic ' + encoded
            },
            path: path
        };

        responseToken = MFP.Server.invokeHttp(input);

        if( responseToken && responseToken.isSuccessful ){
            WL.Logger.warn("=====> valid_grant ");
            var tokenValue = {
                "expires_in": responseToken.expires_in,
                "refresh_token": responseToken.refresh_token,
                "access_token": responseToken.access_token
            };

        } else if(responseToken.error == 'invalid_grant'){
            WL.Logger.warn("=====> invalid_grant ");
            responseToken.isSuccessful = true;
            responseToken.token = null;
        }
        return responseToken;

    } catch (e) {
        return{
            isSuccessful: false,
            reference : e
        }
    }
}


function filterData(userIdentity) {
    return {
        "dob": userIdentity['http://myid.dubai.gov.ae/oidc/dob'],
        "photo": userIdentity['http://myid.dubai.gov.ae/oidc/photo'],
        "lastname": userIdentity['http://myid.dubai.gov.ae/oidc/lastname'],
        "fullnameAR": userIdentity['http://myid.dubai.gov.ae/oidc/fullnameAR'],
        "firstnameAR": userIdentity['http://myid.dubai.gov.ae/oidc/firstnameAR'],
        "username": userIdentity['http://myid.dubai.gov.ae/oidc/username'],
        "email": userIdentity['http://myid.dubai.gov.ae/oidc/email'],
        "gender": userIdentity['http://myid.dubai.gov.ae/oidc/gender'],
        "firstname": userIdentity['http://myid.dubai.gov.ae/oidc/firstname'],
        "maritalStatus": userIdentity['http://myid.dubai.gov.ae/oidc/maritalStatus'],
        "idcardnumber": userIdentity['http://myid.dubai.gov.ae/oidc/idcardnumber'],
        "lastnameAR": userIdentity['http://myid.dubai.gov.ae/oidc/lastnameAR'],
        "passportNo": userIdentity['http://myid.dubai.gov.ae/oidc/passportNo'],
        "accountLock": userIdentity['http://myid.dubai.gov.ae/oidc/accountLock'],
        "emailVerified": userIdentity['http://myid.dubai.gov.ae/oidc/emailVerified'],
        "idcardexpirydate": userIdentity['http://myid.dubai.gov.ae/oidc/idcardexpirydate'],
        "sponsorNo": userIdentity['http://myid.dubai.gov.ae/oidc/sponsorNo'],
        "userVerified": userIdentity['http://myid.dubai.gov.ae/oidc/userVerified'],
        "nationalityAR": userIdentity['http://myid.dubai.gov.ae/oidc/nationalityAR'],
        "sponsorType": userIdentity['http://myid.dubai.gov.ae/oidc/sponsorType'],
        "residencyExpiryDate": userIdentity['http://myid.dubai.gov.ae/oidc/residencyExpiryDate'],
        "residencyNo": userIdentity['http://myid.dubai.gov.ae/oidc/residencyNo'],
        "passportIssueDate": userIdentity['http://myid.dubai.gov.ae/oidc/passportIssueDate'],
        "mobile": userIdentity['http://myid.dubai.gov.ae/oidc/mobile'],
        "idcardissuedate": userIdentity['http://myid.dubai.gov.ae/oidc/idcardissuedate'],
        "passportCountry": userIdentity['http://myid.dubai.gov.ae/oidc/passportCountry'],
        "nationality": userIdentity['http://myid.dubai.gov.ae/oidc/nationality'],
        "idn": userIdentity['http://myid.dubai.gov.ae/oidc/idn'],
        "fullname": userIdentity['http://myid.dubai.gov.ae/oidc/fullname'],
        "passportExpiryDate": userIdentity['http://myid.dubai.gov.ae/oidc/passportExpiryDate']
    };
}

function fetchUserProfile(token) {
    var response = null;
    try {
        var path = 'oauth2/userinfo?schema=openid';
        var input = {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + token.access_token
            },
            path: path
        };
        response = MFP.Server.invokeHttp(input);
        WL.Logger.warn(JSON.stringify(response));
        if (response.isSuccessful && response.error != 'invalid_grant') {
            return filterData(response);
        }

    } catch (e) {
        return {
            isSuccessful: false,
            reference: e
        };
    }
    return response;
}

function authenticateMyID(username, password, isRememberMe) {
    var tokenResponse = null;
    try {
        var model = {
            "username": username,
            "password": password,
            "scope": "openid"
        };

        tokenResponse = getToken(model);
        var response = {
            'token': null,
            'userInfo': null,
        };

        if(tokenResponse && tokenResponse.isSuccessful){
            if (tokenResponse.error != null) {
                return response;
            } else {
                var userInfo = fetchUserProfile(tokenResponse);
                response.token = tokenResponse;
                response.userInfo = userInfo;
                return response;
            }

        } else {
            response.isSuccessful = false;
            return response;
        }
    }
    catch (e) {
        console.log("error ", e);
        return{
            isSuccessful: false,
            reference : e
        }
    }
}
