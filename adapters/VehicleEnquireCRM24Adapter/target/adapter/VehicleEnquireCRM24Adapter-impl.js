function vehicleEnquireService(vehicleID) {
    var rq ='<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/">'+
        '<soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">'+
        '<To>http://fts.dm.gov.ae:80/DTVehicleInquiryService/WebServices/DMFTSService.asmx</To>' +
        '<wsa:Action>http://tempuri.org/VehicleInquiryService</wsa:Action>' +
        '<tem:AuthHeader>'+
        '<tem:InterfaceUserName>DMadmin</tem:InterfaceUserName>'+
        '<tem:InterfacePassword>Dm@dmin!@#</tem:InterfacePassword>'+
        '<tem:InterfaceUniqueId>12</tem:InterfaceUniqueId>'+
        '</tem:AuthHeader>'+
        '</soap:Header>'+
        '<soap:Body>'+
        '<tem:VehicleInquiryService>'+
        '<tem:vehicleId>'+vehicleID+'</tem:vehicleId>'+
        '</tem:VehicleInquiryService>'+
        '</soap:Body>'+
        '</soap:Envelope>';

    var input = {
        method: 'post',
        returnedContentType: 'xml',
        path: getPath(),
        body: {
            contentType: 'application/soap+xml;charset=UTF-8',
            content: rq
        }

    };
    WL.Logger.error("vehicleEnquireService :: request : " + JSON.stringify(input));
    var response = WL.Server.invokeHttp(input);
    WL.Logger.error("vehicleEnquireService :: request : " + JSON.stringify(response));
    return response;
}

function getPath() {
    return '/DTVehicleInquiryService/WebServices/DMFTSService.asmx';
}
