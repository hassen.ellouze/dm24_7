iDubaiApp.config(["$urlRouterProvider", "$stateProvider",
    function ($urlRouterProvider, $stateProvider) {
        $stateProvider
            .state('tour', { // =======================================================================
                url: '/tour',
                hideBack: true,
                params: {
                    isFirst: false
                },
                views: {
                    "viewContent": {
                        templateUrl: 'views/tour.view.html',
                        controller: 'tourController'
                    }
                }
            })
            .state('dashboard', {
                url: '/dashboard',
                hideBack: true,
                params: {
                    isBack: false
                },
                views: {
                    "viewContent": {
                        templateUrl: 'views/dashboard.view.html',
                        controller: 'dashboardController'
                    }
                }
            })
            .state('personalDetails_1', {
                url: '/personalDetails_1',
                params: {
                    isBack: false,
                    from: null,
                    payload: null,
                },
                views: {
                    "viewContent": {
                        templateUrl: 'views/personalDetails/personalDetails_1.view.html',
                        controller: 'personalDetails_1Controller'
                    }
                }
            })
            .state('personalDetailsUpdate', {
                url: '/personalDetailsUpdate',
                params: {
                    isBack: false
                },
                views: {
                    "viewContent": {
                        templateUrl: 'views/personalDetails/personalDetailsUpdate.view.html',
                        controller: 'personalDetailsUpdateController'
                    }
                }
            })
            .state('chooseLanguage', {
                url: '/chooseLanguage',
                views: {
                    "viewContent": {
                        templateUrl: 'views/language.view.html'
                    }
                }
            })
            .state('services', {
                url: '/services',
                params: {
                    payload: null,
                    from: null
                },
                views: {
                    "viewContent": {
                        templateUrl: 'views/services.view.html',
                        controller: 'servicesController'
                    }
                }
            })
            .state('requestSummary', {
                url: '/requestSummary',
                params: {
                    requestDetails: null,
                    SRNumber: null,
                    SLA: null,
                    lat: null,
                    lng: null,
                    isBack: false,
                    from: null
                },
                views: {
                    "viewContent": {
                        templateUrl: 'views/request-summary.view.html',
                        controller: 'requestSummaryController'
                    }
                }
            })
            .state('map', {
                url: 'map',
                params: {
                    from: null,
                    payload: null,
                },
                views: {
                    "viewContent": {
                        templateUrl: 'views/map.view.html',
                        controller: 'mapController'
                    }
                }
            })
            .state('createRequest', {
                url: '/createRequest',
                params: {
                    payload: null
                },
                views: {
                    "viewContent": {
                        templateUrl: 'views/createRequest.view.html',
                        controller: 'createRequestController'
                    }
                }
            })
            .state('requestDone', {
                url: '/request/done',
                params: {
                    result: null
                },
                views: {
                    "viewContent": {
                        templateUrl: 'views/requestDone.view.html',
                        controller: 'createRequestController'
                    }
                }
            })
            .state('about', {//-----------------
                url: '/about',
                views: {
                    "viewContent": {
                        templateUrl: 'views/about.view.html',
                        controller: 'aboutController'
                    }
                }
            })
            .state('contactus', {
                url: '/contactus',
                views: {
                    "viewContent": {
                        templateUrl: 'views/contactus.view.html',
                        controller: 'contactUsController'
                    }
                }
            })
            .state('myRequests', {
                url: '/myRequests',
                views: {
                    "viewContent": {
                        templateUrl: 'views/my-requests.view.html',
                        controller: 'myRequestsController'
                    }
                }
            })
            .state('myfavoriteslist', {// -----------
                url: '/myfavoriteslist',
                views: {
                    "viewContent": {
                        templateUrl: 'views/favorites.view.html'
                    }
                }
            })
            .state('myLocations', {
                url: '/myLocations',
                views: {
                    "viewContent": {
                        templateUrl: 'views/locations.view.html',
                        controller: 'locationsController'
                    }
                }
            })
            .state('chat', {
                url: '/chat',
                views: {
                    'viewContent': {
                        templateUrl: "views/chat/chat.view.html",
                        controller: 'chatStartController'
                    }
                }
            })
            .state('servicesAZ', {
                url: '/servicesAZ',
                params: {
                    payload: null
                },
                views: {
                    "viewContent": {
                        templateUrl: 'views/servicesAZ.view.html',
                        controller: "servicesAZController"
                    }
                }
            })
            .state('serviceDetails', {
                url: '/serviceDetails',
                params: {
                    category: null,
                    service: null,
                    from: null,
                    payload: null
                },
                views: {
                    "viewContent": {
                        templateUrl: 'views/service-details.view.html',
                        controller: "servicesController"
                    }
                }
            })
            .state('onBoarding', {
                url: '/',
                params: {
                    category: null,
                    service: null
                },
                views: {
                    "viewContent": {
                        templateUrl: 'views/onboard.view.html',
                        controller: "onboardController"
                    }
                }
            })
            .state('comingSoon', {
                url: '/comingSoon',
                views: {
                    "viewContent": {
                        templateUrl: 'views/comingSoon.view.html',
                        controller: "comingSoonController"
                    }
                }
            })
            .state('settings', {
                url: '/settings',
                views: {
                    "viewContent": {
                        templateUrl: 'views/settings.view.html',
                        controller: "settingController"
                    }
                }
            })
            .state('contactUs', {
                url: '/contactUs',
                views: {
                    "viewContent": {
                        templateUrl: 'views/contactus.view.html',
                        controller: "contactUsController"
                    }
                }
            })
            .state('feedback', {
                url: '/feedback',
                views: {
                    "viewContent": {
                        templateUrl: 'views/feedback.view.html',
                        controller: "feedbackController"
                    }
                }
            })
            .state('faq', {
                url: '/faq',
                views: {
                    "viewContent": {
                        templateUrl: 'views/faq.view.html',
                        controller: 'faqController'
                    }
                }
            })
            .state('faq-questions', {
                url: '/faq-questions',
                params: {
                    questions: null
                },
                views: {
                    "viewContent": {
                        templateUrl: 'views/faq-questions.view.html',
                        controller: 'faqController'
                    }
                }
            })
            .state('faq-question-answer', {
                url: '/faq-question-answer',
                params: {
                    question: null,
                    forBack: null
                },
                views: {
                    "viewContent": {
                        templateUrl: 'views/faq-question-answer.view.html',
                        controller: 'faqController'
                    }
                }
            })
            .state('notification', {
                url: '/notification',
                views: {
                    "viewContent": {
                        templateUrl: 'views/notification.view.html',
                        controller: 'notificationController'
                    }
                }
            })
            .state('happiness', {
                url: '/happiness',
                views: {
                    "viewContent": {
                        templateUrl: 'views/happiness.view.html',
                        controller: 'happinessController'
                    }
                }
            })
            .state('smartRequest', {
                url: '/smartRequest',
                params: {
                    serviceInfo: null,
                    makani: null,
                    locationName: null,
                    formattedAddress: null,
                    location: null,
                    makaniInfo: null,
                    locationDetail: null,
                    DBMesure: null,
                    listServices: null,
                    requestText: null,
                    payload: null,
                    from: null
                },
                views: {
                    "viewContent": {
                        templateUrl: 'views/smartRequest.view.html',
                        controller: 'smartRequestController'
                    }
                }
            })
            .state('takeTour', {
                url: '/takeTour',
                views: {
                    "viewContent": {
                        templateUrl: 'views/takeTour.view.html',
                        controller: 'takeTourController'
                    }
                }
            })
            .state('favourites', {
            url: '/favourites',
            views: {
                "viewContent": {
                    templateUrl: 'views/favourites.view.html',
                    controller: 'favouritesController'
                }
            }
        });


        $urlRouterProvider.otherwise('/');
    }
]);

