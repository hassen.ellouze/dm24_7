
/* JavaScript content from js/utils/phoneUtils.js in folder common */
iDubaiApp.factory('PhoneUtils', [
        function () {

            var startWith = function(string, str) {
                return (string.substr(0, str.length) === str);
            };

            /**
             * Check is valid phone number format
             * @param phoneNumber
             * @returns {boolean}
             */
            function isValidPhoneNumber(phoneNumber) {
                if (phoneNumber.charAt(0) === '0') {
                    phoneNumber = phoneNumber.substr(1)
                }
                return ((phoneNumber.toString().length === 13 && startWith(phoneNumber, "+971") && !isNaN(phoneNumber.substring(4)))
                            || ( phoneNumber.toString().length === 12 && startWith(phoneNumber, "971") && !startWith(phoneNumber, "+")  && !isNaN(phoneNumber.substring(3)) )
                    || phoneNumber.toString().length === 9 && !startWith(phoneNumber, "+971") && !isNaN(phoneNumber.substring(0)))
            }

            /**
             * Check is not valid phone number format
             * @param phoneNumber
             * @returns {boolean}
             */
            function isNotValidPhoneNumber(phoneNumber) {
                return !isValidPhoneNumber(phoneNumber);
            }

            /**
             * format phone number
             * @param phoneNumber
             * @returns {*}
             */
            function formatPhoneNumber(phoneNumber) {
                var startphoneNumber = phoneNumber;
                if (phoneNumber.charAt(0) === '0') {
                    phoneNumber = phoneNumber.substr(1)
                }
                if (phoneNumber.toString().length === 9 && !startWith(phoneNumber, "+")
                    && !startWith(phoneNumber, "+971") &&!startWith(phoneNumber, "971")) {
                    return phoneNumber = "+971" + phoneNumber
                } else if (phoneNumber.toString().length === 12 && !startWith(phoneNumber, "+")) {
                    return phoneNumber = "+" + phoneNumber
                } else {
                    return startphoneNumber
                }
            }

            return {
                isValidPhoneNumber: isValidPhoneNumber,
                isNotValidPhoneNumber: isNotValidPhoneNumber,
                formatPhoneNumber: formatPhoneNumber
            };

        }
    ]
);
