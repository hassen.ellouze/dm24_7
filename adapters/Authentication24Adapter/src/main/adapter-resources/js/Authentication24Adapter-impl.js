/*
 *  Licensed Materials - Property of IBM
 *  5725-I43 (C) Copyright IBM Corp. 2011, 2016. All Rights Reserved.
 *  US Government Users Restricted Rights - Use, duplication or
 *  disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */


var publicKey = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCoQuXm00FV/i7ulPRmWbBovGM3Ytp/wk+tiYcDArgm6yppbttNB1FExSVefJtXtOaSeIHivX+t/h7dkoEt0uUO3a3VXkGILBdd7WttwLoP0+y9cKl+giFX4v5u5eQRPNBdyAPYajmrC4Hi3bNJolNe/s1Phm8fBFcwHyt4V9l+KwIDAQAB';
var privateKey = 'MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAKhC5ebTQVX+Lu6U9GZZsGi8Yzdi2n/CT62JhwMCuCbrKmlu200HUUTFJV58m1e05pJ4geK9f63+Ht2SgS3S5Q7drdVeQYgsF13ta23Aug/T7L1wqX6CIVfi/m7l5BE80F3IA9hqOasLgeLds0miU17+zU+Gbx8EVzAfK3hX2X4rAgMBAAECgYB2XlmNgKA99ordnDwypHXNuHmzxP8hUXjsBAvOumMCPf2gHjR4g+VQE8CuL8q4cLKj59K3x8fChzr/dMkbi3lHT66RrpZMGfgKZs4iw/Uybyc2Vmno4Mci1WrZ1GAqflQdPn0ZJOU9St4HRyLQySyfFSnbyrbKPo4WBrGtO7EbQQJBANj9axwOUJ1Nd9XumbW7ZMHA1s0xJ7ZyVGhxqk5rM9B8/e9lS6LShSwVaEeNqi/a7R702DigYABHwSSlgmuaBGUCQQDGgtTL815haVjWoSf80xmRKbGc2ZTK+Qs6Jt6geoPKD/ep62acJUJNQlsEyfAMtzXwrQ+kxSsE0EkPiUU6v+dPAkA6gSlN6TYnE2oKDkj3TM0/yESiEmzIrk6/0lW+yvyva+PzlfYssmIhnqv9Dt8iA/8G57Eco3D+4lnbDZz6cg5ZAkEAo5Y3NGc21MGSwzSFFnUEMbsRMfegBXAcD2zus4KizkZASxfBNFYEAFT5WsXq+x+T5OVI6fjNjabzPklZ8qMoQwJBAJK6uXcJv/haUEEbTeJsv3RpnAxZ+Nh0oa2unXd65n9HtOiJ5xd5d5BNRiw5/mhcZZVRWF0GIsQk2eyM5sbgIXE=';

function onAuthRequired(exception, errorMessage, response) {
    errorMessage = errorMessage ? errorMessage : null;
    if (response) {
        response.authRequired = true;
        response.errorMessage = errorMessage;
        response.authFailed = false;
        response.exception = exception;
        return response;
    }
    return {
        authRequired: true,
        errorMessage: errorMessage,
        authFailed: false,
        exception: exception
    };
}

function encryptUserIdentity(userIdentity) {
    return com.dm.common.utils.EncryptionUtil.encrypt(JSON.stringify(userIdentity), publicKey);
}


// Authentication
function authenticate(userId, password, isRememberMe) {
    WL.Logger.warn(" ==>  authenticate : ");
    try {
        var invocationData = {
            adapter: 'MyIDDubai24Adapter',
            procedure: 'authenticateMyID',
            parameters: [userId, password, isRememberMe]
        };
        var response = MFP.Server.invokeProcedure(invocationData);
        if (response.isSuccessful) {
            if (response.userInfo == null || response.token == null) {
                return onAuthRequired("User not found", "UM0001");
            } else if (response.isSuccessful && response.userInfo != null && response.token != null) {
                var anonToken = userId; // WL.Server.getActiveUser('wl_anonymousUserRealm').userId;
                var toEncrypt = {
                    u: userId,
                    p: password,
                    t: anonToken
                };
                WL.Logger.warn("toEncrypt");
                WL.Logger.warn(JSON.stringify(toEncrypt));
                var token = encryptUserIdentity(toEncrypt);
                var userIdentity = {
                    userId: "" + response.userInfo.username + "",
                    displayName: "" + response.userInfo.username + "",
                    attributes: {
                        userToken: token,
                        userInfo: response.userInfo,
                        token: token
                    },
                    isUserAuthenticated: 1
                };
                response.userIdentity = userIdentity;
                response.authRequired = false;

            }
        } else {
            return onAuthRequired("Server error", "UM005");
        }
        response.token = {};
        return response;
    }
    catch (e) {
        return{
            isSuccessful: false,
            reference : e
        }
    }
}