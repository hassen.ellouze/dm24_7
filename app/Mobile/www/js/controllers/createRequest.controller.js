iDubaiApp.controller('createRequestController',
    ['$state', '$scope', '$stateParams', '$filter', '$notification', 'DMService', '$procedures', '$auth', '$logger',
        '$rootScope', 'LoadingService', 'Makani', '$myLocations', 'PhoneUtils', 'GuestUtils', 'underscore', '$dmArea', 'DMCategories', '$DMLinks', 'AnalyticsService', 'Constants', 'SearchEngine', 'SharedService',
        /**
         *
         * @param $state
         * @param $scope
         * @param $stateParams
         * @param $filter
         * @param $notification
         * @param DMService
         * @param $procedures
         * @param $auth
         * @param $logger
         * @param $rootScope
         * @param LoadingService
         * @param Makani
         * @param $myLocations
         * @param PhoneUtils
         * @param GuestUtils
         * @param underscore
         * @param $dmArea
         * @param DMCategories
         * @param $DMLinks
         * @param AnalyticsService
         * @param Constants
         * @param SearchEngine
         */
        function ($state, $scope, $stateParams, $filter, $notification, DMService, $procedures, $auth, $logger,
                  $rootScope, LoadingService, Makani, $myLocations, PhoneUtils, GuestUtils, underscore, $dmArea, DMCategories, $DMLinks, AnalyticsService, Constants, SearchEngine, SharedService) {
            $scope.currentLang = localStorage.getItem('dm-lang');
            $scope.fromSearchByImage = false;
            var _ = underscore;

            $stateParams = SharedService.getStateHistory($state.current.name);


            function toDataUrl(url, callback) {
                var xhr = new XMLHttpRequest();
                xhr.onload = function () {
                    var reader = new FileReader();
                    reader.onloadend = function () {
                        callback(reader.result);
                    }
                    reader.readAsDataURL(xhr.response);
                };
                xhr.open('GET', url);
                xhr.responseType = 'blob';
                xhr.send();
            }

            if (localStorage.getItem('sourceType')) {
                $scope.fromSearchByImage = true;
            }
            if ($stateParams.result) {
                $scope.ExternalTranasactionId = $stateParams.result.ExternalTranasactionId;
                LoadingService.hide();
            } else {
                $scope.serviceInfo = $stateParams.payload.serviceInfo;
                $scope.makani = $stateParams.payload.makani;
                $scope.locationName = $stateParams.payload.locationName;
                $scope.formattedAddress = $stateParams.payload.formattedAddress;
                $scope.location = $stateParams.payload.location;
                $scope.locationDetail = $stateParams.payload.locationDetail;
                $scope.isNotUpload = false;
                $scope.additionalInfo = ($stateParams.payload.additionalInfo) ? $stateParams.payload.additionalInfo : '';
                $scope.isAnonymous = false;
                $scope.modal = {
                    toggle: false,
                    toggleTerms: false,
                    //toggleDelete: false
                };
                $scope.forDeleting = '';
                $scope.mediaUpload = [];
                $scope.isPlaying = false;
                $scope.recordedAudio = '';

                if (localStorage.getItem('uploadImage')) {


                    toDataUrl(localStorage.getItem('uploadImage'), function (myBase64) {
                        console.log("Converted Base 64 :: ", myBase64);
                        $scope.mediaUpload.push({
                            //src: 'data:image/jpg;base64,' + localStorage.getItem('uploadImage'),
                            src: myBase64,
                            id: $scope.mediaUpload.length
                        });
                    });

                }

                if($scope.location){

                    // $scope.location.lat = "25.204542";
                    // $scope.location.lng = "55.271738";
                    Makani.getNearestMakani($scope.location.lat, $scope.location.lng).then(function (results) {
                        try {
                            if (results.hasOwnProperty('DATA')) {
                                return;
                            } else {
                                var results = JSON.parse(results);
                                var makaniInfo = results.makaniInfo[0];
                                $scope.makani = makaniInfo.MAKANI;
                                $rootScope.makani = makaniInfo.MAKANI;
                                $rootScope.makaniInfo = makaniInfo.MAKANI_INFO[0];
                            }
                        }catch (e) {
                            console.log(e);
                        }

                    }, function (err) {
                        console.log('Error makani : ',err);
                    })
                }



                if(localStorage.getItem('recordedNoiseAudio')){
                    $scope.recordedNoiseAudio = localStorage.getItem('recordedNoiseAudio');
                }

                if(localStorage.getItem('recordedAudio')){
                    $scope.recordedAudio = localStorage.getItem('recordedAudio');
                }


                console.log($stateParams);

                $scope.imgSrc = encodeURI("https://maps.googleapis.com/maps/api/staticmap?" +
                    "center=" + $scope.location.lat  + ',' +  $scope.location.lng +
                    "&zoom=15" +
                    "&scale=false" +
                    "&size=600x300" +
                    "&maptype=satellite" +
                    "&key=AIzaSyDAyPe0GL66TrDBhkzQCF9-8pIWTLoxg7o" +
                    "&format=png" +
                    "&visual_refresh=true" +
                    "&markers=" + $scope.location.lat + ',' +  $scope.location.lng);
                //https://maps.googleapis.com/maps/api/staticmap?center=25.11724358770323,55.210145758231874&zoom=15&scale=false&size=600x600&maptype=roadmap&key=AIzaSyDAyPe0GL66TrDBhkzQCF9-8pIWTLoxg7o&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C25.11724358770323,55.210145758231874

                $scope.returnToMap = function () {
                    localStorage.setItem('recordedAudio', $scope.recordedAudio);
                    console.log('RETURN FROM MAP :: serviceInfo == ', $stateParams.payload.serviceInfo);
                    console.log('RETURN FROM MAP :: makani == ', $stateParams.payload.makani);
                    console.log('RETURN FROM MAP :: locationName == ', $stateParams.payload.locationName);
                    console.log('RETURN FROM MAP :: formattedAddress == ', $stateParams.payload.formattedAddress);
                    console.log('RETURN FROM MAP :: location == ', $stateParams.payload.location);

                     var payload = {
                         service: $scope.serviceInfo.service,
                         category: $scope.serviceInfo.category,
                         additionalInfo: $scope.additionalInfo,
                     };
                     SharedService.setStateHistory('map', {
                         payload: payload,
                         from: 'createRequest'
                     });
                    $state.go('map', {
                        payload: payload,
                        from: 'createRequest'
                    });
                };

                $scope.goBack = function () {
                   var prev=SharedService.getStateHistory($state.current.name).from;
                   $state.go(prev);
                };

                /*
               *  Fill Pets
               * */


                if ($scope.serviceInfo.service.serviceEN == "Stray Cats") {
                    DMCategories.getPets().then(function (result) {
                        result.map(function (item, index) {
                            if (item.pet_type_en.toUpperCase() == "CATS") {
                                $scope.pet_type_id = item.pet_type_id;
                            }
                        })
                    })

                } else if ($scope.serviceInfo.service.serviceEN == "Stray Dogs") {
                    // pet = dog
                    DMCategories.getPets().then(function (result) {
                        result.map(function (item, index) {
                            if (item.pet_type_en.toUpperCase() == "CATS") {
                                $scope.pet_type_id = item.pet_type_id;
                            }
                        })
                    })

                } else {
                    if ($scope.serviceInfo.service.pet_type == '1') {
                        DMCategories.getPets().then(function (result) {
                            $scope.pets = result;
                        })

                    }

                }


                /*if ($scope.serviceInfo.service.pet_type == '1' ) {

                    DMCategories.getPets().then(function (result) {
                        if( $scope.serviceInfo.service.serviceEN == "Stray Cats" ){
                            // pet = cat
                            result.map(function(item, index){
                                if (item.pet_type_en.toUpperCase() == "CATS") {
                                    $scope.pet_type_id = item.pet_type_id;
                                }
                            })
                        }else if( $scope.serviceInfo.service.serviceEN == "Stray Dogs" ){
                            // pet = dog
                            result.map(function(item, index){
                                if (item.pet_type_en.toUpperCase() == "CATS") {
                                    $scope.pet_type_id = item.pet_type_id;
                                }
                            })
                        } else{
                            $scope.pets = result;
                        }
                    });
                }*/

                /*
                *  Fill Insects
                * */
                if ($scope.serviceInfo.service.insect_type == '1') {
                    DMCategories.getInsects().then(function (result) {

                        $scope.insects = result


                    });
                }

                DMCategories.getServices().then(function (result) {
                    if (result.isSuccessful) {
                        if ($scope.serviceInfo.service.incident_id !== undefined) {
                            $scope.serviceName = underscore.find(result.categories, function (s) {
                                return s.incident_id == $scope.serviceInfo.service.incident_id;
                            })
                        } else {
                            $scope.serviceName = underscore.find(result.categories, function (s) {
                                return s.service_request_id == $scope.serviceInfo.service.service_request_id;
                            })
                        }
                    }
                })


            }

            /**
             * Get picture depend of source
             * @param source
             */
            $scope.openCamera = function (source) {


                try {
                    if (navigator.camera) {
                        var cameraOptions = {
                            quality: 50,
                            targetWidth: 720,
                            targetHeight: 600,
                            encodingType: Camera.EncodingType.JPEG,
                            destinationType: Camera.DestinationType.DATA_URL, //Camera.DestinationType.DATA_URL
                            correctOrientation: true,
                            sourceType: (source === 'camera') ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY
                        };
                        navigator.camera.getPicture(function (imageData) {
                            setTimeout(function () {
                                if (WL.Client.getEnvironment() == WL.Environment.ANDROID) {
                                    imageData = JSON.parse(imageData);
                                    $scope.mediaUpload.push({
                                        src: 'data:image/jpg;base64,' + imageData.filename,
                                        id: $scope.mediaUpload.length
                                    });
                                } else {

                                    $scope.mediaUpload.push({
                                        src: 'data:image/jpg;base64,' + imageData,
                                        id: $scope.mediaUpload.length
                                    });
                                }

                                //$scope.mediaUpload.push({src: imageData, id: $scope.mediaUpload.length});
                                $scope.$apply();
                            });
                        }, function (message) {
                        }, cameraOptions);
                    }
                } catch (e) {
                    catchError(e);
                }

            };

            /*
            * convert Image to base64
            */

            function getBase64Image(img) {
                var canvas = document.createElement("canvas");
                canvas.width = img.width;
                canvas.height = img.height;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0);
                var dataURL = canvas.toDataURL("image/png");
                return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
            }

            $scope.changeMe = function (id) {
                console.log('pressed');

                WL.SimpleDialog.show(
                    $filter('translate')('Confirm Delete Photo'),
                    $filter('translate')('Are you sure to delete this photo ?'), [{
                        text: $filter('translate')('Cancel'), handler: function () {
                            return;
                        }
                    },
                        {
                            text: $filter('translate')('Yes'), handler: function () {
                                $scope.deletePhoto(id)
                            }
                        }]);

            }

            /*
            *  Delete photo
            * */
            $scope.deletePhoto = function (id) {

                $scope.mediaUpload.splice(id, 1);
                $scope.$apply();
                console.log("Id to be deleted : ", id);
            };

            $scope.checkConnection = function (isAnonymous, pet, insect) {
                if ($auth.token || GuestUtils.getGuestPhoneNumber()) {
                    $scope.sendRequest(isAnonymous, pet, insect);
                } else {
                    WL.SimpleDialog.show(
                        $filter('translate')('Registration'),
                        $filter('translate')('Please register'), [{
                            text: $filter('translate')('I don\'t want to register'), handler: function () {
                                $scope.sendRequest(isAnonymous, pet, insect);
                                return;
                            }
                        },
                            {
                                text: $filter('translate')('Register now'), handler: function () {
                                    localStorage.setItem('recordedAudio', $scope.recordedAudio);

                                    var payload = {
                                        serviceInfo: $stateParams.payload.serviceInfo,
                                        makani: $stateParams.payload.makani,
                                        locationName: $stateParams.payload.locationName,
                                        formattedAddress: $stateParams.payload.formattedAddress,
                                        location: $stateParams.payload.location,
                                        additionalInfo: $scope.additionalInfo,
                                        recordedAudio: $scope.recordedAudio,
                                        locationDetail : $scope.locationDetail
                                    }

                                    SharedService.setStateHistory('personalDetails_1', {
                                        payload: payload,
                                        from: 'createRequest'
                                    });

                                    $state.go('personalDetails_1', {
                                        from: 'createRequest',
                                        payload: payload
                                    });
                                }
                            },
                            {
                                text: $filter('translate')('Cancel'), handler: function () {
                                    return;
                                }
                            }]);
                }

            }

            /*
            *  send Request
            * */

            $scope.sendRequest = function (isAnonymous, pet, insect) {
                try {
                    LoadingService.show();
                    moment.locale('en-gb'); // Local format (Grand Britain)

                    var mobileNumber = '';
                    var username = '';
                    var emailAddress = '';
                    var service = $scope.serviceInfo.service;
                    var category = $scope.serviceInfo.category;


                    var areaId = (!$rootScope.makaniInfo || !$rootScope.makaniInfo.COMMUNITY_NUM) ? '' : $rootScope.makaniInfo.COMMUNITY_NUM;

                    if ($auth.token) {
                        mobileNumber = $auth.dubaiUserProfile.mobile;
                        username = $auth.dubaiUserProfile.username;
                        emailAddress = ($auth.dubaiUserProfile.email) ? $auth.dubaiUserProfile.email : '';
                    } else if (!GuestUtils.isEmptyGuest()) {
                        var guestInfo = GuestUtils.getGuestInfo();
                        if (guestInfo != '') {
                            mobileNumber = (guestInfo.mobileNumber) ? guestInfo.mobileNumber : '';
                            username = (guestInfo.name) ? guestInfo.name : '';
                            emailAddress = (guestInfo.email) ? guestInfo.email : '';
                        }
                    } else {
                        mobileNumber = '+971000000000';
                        username = '';
                        emailAddress = '';
                    }

                    var showContactData = true;
                    if ($scope.serviceInfo.service.isAnonymous == true) {

                        if (isAnonymous.$viewValue == true) {
                            showContactData = false;
                        }
                    }
                    // todo add preferred time

                    var PreferredDate = '';
                    var PreferredTime = '';
                    if ($scope.serviceInfo.service.is_prefered_time && $scope.serviceInfo.service.is_prefered_time == 1) {
                        //PreferredDate= moment().format('L');
                        PreferredDate = new Date();
                        PreferredTime = moment().format('HH:mm');
                    }

                    var pet_type_id = '';
                    if ($scope.pet_type_id) {
                        pet_type_id = $scope.pet_type_id;
                    } else {
                        if (pet) {
                            pet_type_id = pet.$modelValue
                        }
                    }


                    /*var mobileNumber = ($auth.token) ? $auth.dubaiUserProfile.mobile : (GuestUtils.isGuest()) ? GuestUtils.getGuestPhoneNumber() : '';
                    var name = '';
                    var emailAdress = '';
                    if ($auth.token) {
                        if ($auth.dubaiUserProfile.email)
                        {
                            emailAdress = $auth.dubaiUserProfile.email
                        }
                    }
                    if ($auth.token) {
                        name = $auth.dubaiUserProfile.username;
                        // name = $scope.currentLang === 'en' ? $auth.dubaiUserProfile.fullname: $auth.dubaiUserProfile.fullnameAR ;
                    }*/

                    /*var oldReq = {
                        "dmArea": $scope.dmAreaEN, //AreaCode
                        "area": service.area.legacyArea,
                        "dmCustomerMobileNo": PhoneUtils.formatPhoneNumber(mobileNumber) + "",
                        "dmGISXCoordinates": $rootScope.location.lat + "",
                        "dmGISyCoordinates": $rootScope.location.lng + "",
                        "dmOrganizationUnit": service.area.ou,
                        "description": '[Name ' + name + ' @ Makani ' + $scope.makani + '] ' + $scope.request.description,
                        "dmStreet": $rootScope.location.address,
                        "subArea": service.service_title_en,
                        "dmFlatNum": ($scope.request.flatNumber) ? $scope.request.flatNumber + "" : "",
                        "dmFloorNum": ($scope.request.floorNumber) ? $scope.request.floorNumber + "" : "",
                        "base64FileByteArrayString": ($scope.photoBase64) ? $scope.photoBase64 : ""
                    };*/


                    /*if ($rootScope.makaniInfo.COMMUNITY_NUM == undefined) {
                        if($scope.request.dmAreaId != undefined) {
                            areaId= $scope.request.dmAreaId;
                        }

                    }else {
                        areaId=$rootScope.makaniInfo.COMMUNITY_NUM
                    }*/


                    var req = {
                        "ServiceTypeId": (service.incident_id !== undefined) ? 1 : 2,
                        "ServiceId": (service.incident_id !== undefined) ? service.incident_id : service.service_request_id,
                        "ServiceCategoryId": service.category_CRM_id,
                        "Street": $scope.locationName,
                        "Building": '',
                        "AreaId": areaId,
                        "Makani": $scope.makani,
                        "Floor": ($scope.floorNumber) ? $scope.floorNumber + '' : '',
                        "Flat": ($scope.flatNumber) ? $scope.flatNumber + '' : '',
                        "Location_X": $scope.location.lng + '',
                        "Location_Y": $scope.location.lat + '',
                        "AdditionalAddressDetails": '',
                        "SLA": service.sla,
                        "SourceOfCreation": 'Dubai 24/7',
                        "PetTypeId": pet_type_id,
                        "InsectTypeId": (!service.insect_type) ? '' : (service.insect_type == 1) ? insect.$modelValue : '',
                        "Description": ($stateParams.payload.DBMesure) ? '[Noise Mesure Average : ' + $stateParams.payload.DBMesure + '] ' + $scope.additionalInfo : $scope.additionalInfo,
                        "ContactFullName": username,
                        "ContactMobileNumber": mobileNumber,
                        "ContactEmail": emailAddress,
                        "ContactEmirateId": null,
                        "LicenseNo": '',
                        "TradeLicenseNameEN": '',
                        "TradeLicesnseNameAR": '',
                        "SmartCrmTransactionId": Date.now(),
                        "ShowContactData": showContactData,
                        "PreferredDate": PreferredDate,
                        "PreferredTime": PreferredTime,
                        "VeterniaryCardNumber": ''
                    };
                    // + ' \n ' + $scope.makaniAddress
                    console.log('==> REQUEST  ', req);

                    var params = [req.ServiceTypeId, req.ServiceId, req.ServiceCategoryId, req.Street,
                        req.Building, req.AreaId, req.Makani, req.Floor, req.Flat,
                        req.Location_X, req.Location_Y, req.AdditionalAddressDetails, req.SLA,
                        req.SourceOfCreation, req.PetTypeId, req.InsectTypeId, req.Description,
                        req.ContactFullName, req.ContactMobileNumber, req.ContactEmail, req.ContactEmirateId,
                        req.LicenseNo, req.TradeLicenseNameEN, req.TradeLicesnseNameAR, req.SmartCrmTransactionId,
                        req.ShowContactData, req.PreferredDate, req.PreferredTime, req.VeterniaryCardNumber
                    ];

                    if ($scope.mediaUpload.length == 0 && !$scope.recordedAudio && !$scope.recordedNoiseAudio) {
                        createNotification(params);
                    } else {
                        LoadingService.show('Uploading Media');
                        /*$scope.attachmentsToBeUploaded = [];*/
                        $scope.attachmentsToBeUploaded = {"imageList": [], "audioList": []};
                        $scope.mediaUpload.map(function (item, index) {

                            $scope.attachmentsToBeUploaded.imageList.push({
                                fileName: item.id + '',
                                fileContent: item.src.replace(/^data:image\/(png|jpg|jpeg);base64,/, "")
                            });
                        });


                        if ($scope.recordedAudio)
                        {
                            $scope.attachmentsToBeUploaded.audioList= [];
                            /*var xhr = new XMLHttpRequest();
                            xhr.open("GET", $scope.recordedAudio, true);
                            xhr.responseType = "blob";
                            xhr.onload = function (e) {
                                console.log(this.response);
                                var reader = new FileReader();
                                reader.onload = function (event) {
                                    var res = event.target.result;
                                    console.log('res:', res);
                                    console.log('File extension:', $scope.recordedAudio.substr($scope.recordedAudio.lastIndexOf('/')));
                                    $scope.attachmentsToBeUploaded.audioList.push({
                                        fileName:'additionalDetails',
                                        fileContent: res.split(",").pop(),
                                        fileExtension: $scope.recordedAudio.substr($scope.recordedAudio.lastIndexOf('.')+1)
                                    })
                                    addAttachments(params);
                                };
                                var file = this.response;
                                reader.readAsDataURL(file);
                            };
                            xhr.send();*/
                            convertAudioToBase64('GET', $scope.recordedAudio, function (err, data) {
                                if (err) { throw err; }
                                console.log(data);
                                $scope.attachmentsToBeUploaded.audioList.push({
                                    fileName:'additionalDetails',
                                    fileContent: data.res.split(",").pop(),
                                    fileExtension: $scope.recordedAudio.substr($scope.recordedAudio.lastIndexOf('.')+1)
                                });

                            });
                        }

                        if($scope.recordedNoiseAudio){
                            convertAudioToBase64('GET', $scope.recordedNoiseAudio, function (err, data) {
                                if (err) { throw err; }
                                console.log(data);
                                $scope.attachmentsToBeUploaded.audioList.push({
                                    fileName:'noiseAudio',
                                    fileContent: data.res.split(",").pop(),
                                    fileExtension: $scope.recordedNoiseAudio.substr($scope.recordedNoiseAudio.lastIndexOf('.')+1)
                                });

                            });
                        }

                        setTimeout(function () {
                            addAttachments(params);
                        }, 2000)

                    }


                } catch (e) {
                    catchError(e);
                }
            };

            function addAttachments(params) {
                DMService.addAttachment($scope.attachmentsToBeUploaded).then(function (result) {

                    if (result.isSuccessful) {
                        var attachment = [];
                        for (var i = 0; i < result.response.length; i++) {
                            if (result.response[i].Success == true) {
                                attachment.push({
                                    'FileName': result.response[i].OriginalFileName,
                                    'PhysicalFileName': result.response[i].PhysicalFileName
                                });
                            }
                        }


                        params.push(attachment);
                        createNotification(params);


                    } else {
                        LoadingService.hide();
                        $notification.alert($filter('translate')("Error while uploading photo, please try again later"));
                    }
                }, function (err) {
                    LoadingService.hide();
                    $notification.alert($filter('translate')("Error while uploading photo, please try again later"));
                    console.log(err);
                })
            }

            function isValidPlace(){
                return ($scope.locationName.toUpperCase().indexOf('DUBAI') >= 0 ||
                    $scope.locationName.indexOf('دبي') >= 0 ||
                    $scope.locationName.toUpperCase().indexOf('DUBAÏ') >= 0 ||
                    $scope.locationName.indexOf('دبى') >= 0) &&
                    ($scope.location.lat>=24.792136 && $scope.location.lat<=25.358561 &&
                        $scope.location.lng>=54.890454 && $scope.location.lng<=55.565039)
            }

            /*
            *  Create request
            * */
            function createNotification(params) {
//                if (Constants.IS_STAGING ||  $scope.locationName.toUpperCase().indexOf('DUBAI') >= 0 || $scope.locationName.indexOf('دبي') >= 0 || $scope.locationName.toUpperCase().indexOf('DUBAÏ') >= 0 || $scope.locationName.indexOf('دبى') >= 0) {
            	  if(isValidPlace()){
	                	DMService.createNotification(params).then(function (result) {
	                        $scope.serviceResponse = result;
	                        if (result.isSuccessful === true && result.ExternalTranasactionId != -1) {
	                            AnalyticsService.trackRoute(Constants.HIT_TYPE.EVENT, '', {
	                                     eventCategory: Constants.EVENTS.Services_Submitted,
	                                     eventAction: Constants.EVENTS.Services_Submitted,
	                                     eventLabel: $scope.serviceInfo.service.serviceEN
	                           });
	                            //save location in storage

	                            //get lat
	                            var lat = $scope.location.lat;
	                            //get lng
                                var lng =  $scope.location.lng;
	                            //get service title
                                var serviceTitleAr = $scope.serviceInfo.service.serviceAR;
                                var serviceTitleEn = $scope.serviceInfo.service.serviceEN;
	                            //get makani number
                                var makaniNumber =  $scope.makani;
	                            //get location by google maps
                                var locationName = $scope.locationName;
	                            //get location by user
                                var locationDetail = $scope.locationDetail;
	                            //get request number
                                var requestNumber = result.ExternalTranasactionId;

                                var isNewLocation = true;
	                            window.db.transaction(function (txDB) {
	                          		
	                            		txDB.executeSql('CREATE TABLE IF NOT EXISTS locationList (id, list)',[],function(tx, res){
	                            			var query = "SELECT list FROM locationList";
	                                		txDB.executeSql(query, [], function (tx, resultSet) {
	                                			if(resultSet.rows.length != 0 )
	                                			{
	                                            	locationList = JSON.parse(resultSet.rows.item(0).list);
	                                            	angular.forEach(locationList, function(locationItem, locationkey) {
	                                            		if(''+lat+'-'+lng == locationkey){
	                                            			//save the new request
	                                            			isNewLocation = false;

	                                            			locationItem.requestList.push({"serviceTitleAr": serviceTitleAr, "serviceTitleEn":serviceTitleEn,  "RequestNumber": requestNumber});
	                                            			txDB.executeSql('UPDATE locationList SET list = ?', [JSON.stringify(locationList)],function(tx, resultSet){
	                                            				//console.log("Update location list success:", JSON.stringify(tx), JSON.stringify(resultSet));
                                                                SharedService.setStateHistory('requestDone', {
                                                                    result: result
                                                                });

                                                                $state.go('requestDone', {
	                                                                result: result
	                                                            });
	                                            			},function (tx, error) {
	                                                			//console.log('Update location list error: ' + error.message);
	                                                		});
	                                            		}
	                                            	});
	                                            	if(isNewLocation){
	                                            		//add a new location in list
	                                            		newKey = ''+lat+'-'+lng;
	                                            		locationList[newKey] =  {
	                                            				"lat": lat,
	                                            				"lng": lng,
	                                            				"makaniNumber": makaniNumber,
	                                            				"location": locationName,
	                                            				"locationDetail": locationDetail,
	                                            				requestList: [{"serviceTitleAr": serviceTitleAr, "serviceTitleEn":serviceTitleEn,  "RequestNumber": requestNumber}]
	                                            		};
	                                        			txDB.executeSql('UPDATE locationList SET list = ?', [JSON.stringify(locationList)],function(tx, resultSet){
	                                        				//console.log("Update location list success:", JSON.stringify(tx), JSON.stringify(resultSet));
                                                            SharedService.setStateHistory('requestDone', {
                                                                result: result
                                                            });
                                                            $state.go('requestDone', {
	                                                            result: result
	                                                        });
	                                        			},function (tx, error) {
	                                            			//console.log('Update location list error: ' + error.message);
	                                            		});
	                                            	}
	                                			}
	                                			else{
	                                		      //cretae locationList and save the first data in storage
	                                				keyLocation = ''+lat+'-'+lng;
	                                				locationResult = {};
	                                				locationResult[keyLocation] = {
	                                                  "lat": lat,
	                                                  "lng": lng,
	                                                  "makaniNumber": makaniNumber,
	                                                  "location": locationName,
	                                                  "locationDetail": locationDetail,
	                                                   requestList: [{"serviceTitleAr": serviceTitleAr, "serviceTitleEn":serviceTitleEn,  "RequestNumber": requestNumber}]
	                                				};
	                                				txDB.executeSql('CREATE TABLE IF NOT EXISTS locationList (id, list)',[],function(tx1, res1){
	                                					//console.log('CREATE TABLE IF NOT EXISTS locationList', JSON.stringify(tx1), JSON.stringify(res1));
	                                					txDB.executeSql('INSERT INTO locationList VALUES (?,?)',['1', JSON.stringify(locationResult)],function(tx11, res11){
                                                            SharedService.setStateHistory('requestDone', {
                                                                result: result
                                                            });
	                                					    $state.go('requestDone', {
	                                                            result: result
	                                                        });
	                                					});
	                                				});
	                                			}
	                                		},function (tx, error) {
	                                			console.log('SELECT location error: ' + error.message);
	                                		});
	                            		},function (tx, error) {
	                            			console.log('failure to create table locationList: ' + error.message);
	                            		});
	                                	}, function () {
	                                         console.log('transaction ok');
	                                   });
	                    }
	                },function (error) {
	                    LoadingService.hide();
	                    $notification.alert($filter('translate')("Can't create request. Please try again later"));
	                });
                }else{
                	LoadingService.hide();
                    $notification.alert($filter('translate')("This location is not covered by the service"));
                }
            }

            /*
            * Catch Error
             */

            function catchError(e) {
                console.log('===> ERROR ', e);
                LoadingService.hide();
            }

            /**
             * Open makani website
             */
            $scope.openWebsite = function () {
                window.open($DMLinks.makani, "_blank", 'location=yes');
            };

            /**
             *  Gallery
             * */
            /*angular.element(document).ready(function () {
                setTimeout(function () {

                    $("#mediaGallery").lightGallery({
                        fullScreen: false
                    });


                }, 2000);

            });*/

            $scope.clearText = function(){
                $scope.additionalInfo = '';
                setTimeout(function () {
                    $('#additionalInfo').click();
                }, 100);
            }

            /*
           *  Speech To Text
           * */
            $scope.speechToText = function (searchRequest, mediaType) {
                var requestFrom = 'api'
                var platform = WL.Client.getEnvironment();
                var lang = localStorage.getItem('dm-lang') == 'ar' ? 'ar-AE' : 'en-US';

                SearchEngine.search(searchRequest, mediaType, requestFrom, platform, lang).then(
                    function (result) {
                        console.log('Speech:', result);
                        LoadingService.hide();
                        if (result) {
                            $scope.additionalInfo = $scope.additionalInfo + ' ' +result.requestText;
                        }else {
                            $scope.recordedAudio = '';
                        }
                    },
                    function (error) {
                        $scope.recordedAudio = '';
                        console.log(error);
                        LoadingService.hide();
                    }
                );

            };

            // record animation and events
            function pad(val) {
                var valString = val + "";
                if (valString.length < 2) {
                    return "0" + valString;
                } else {
                    return valString;
                }
            }

            var timer = null;

            function chrono() {
                var totalSeconds = 0;
                document.getElementById('heroInput').value = '    00:00';
                timer = setInterval(function () {
                    ++totalSeconds;
                    document.getElementById('heroInput').value = '    ' + pad(parseInt(totalSeconds / 60)) + ':' + pad(totalSeconds % 60);
                }, 1000);
            }

            /**
             *
             * @param method
             * @param url
             * @param done
             */
            function convertAudioToBase64 (method, url, done) {
                var xhr = new XMLHttpRequest();
                xhr.open(method, url, true);
                xhr.responseType = "blob";
                xhr.onload = function () {
                    var reader = new FileReader();
                    reader.onload = function (event) {
                        var res = event.target.result;
                        console.log('res:', res);
                        console.log('File extension:', url.substr(url.lastIndexOf('/')));
                        done(null, {res: res, url: url});
                    };
                    var file = this.response;
                    reader.readAsDataURL(file);

                };
                xhr.onerror = function () {
                    done(xhr.response);
                };
                xhr.send();
            }

            var longPress = false;

            $scope.recorder = new Object;

            $scope.recorder.stop = function () {

                if (longPress) {
                    LoadingService.show('Transforming your voice in text');
                    AnalyticsService.trackRoute(Constants.HIT_TYPE.EVENT, '', {
                        eventCategory: 'Search_By_Voice',
                        eventAction: 'Search By voice in create request'

                    });
                    window.plugins.audioRecorderAPI.stop(function (msg) {
                        // success
                        console.log('ok: ' + msg);

                        $scope.recordedAudio = msg;
                        /*var xhr = new XMLHttpRequest();
                        xhr.open("GET", msg, true);
                        xhr.responseType = "blob";
                        xhr.onload = function (e) {
                            console.log(this.response);
                            var reader = new FileReader();
                            reader.onload = function (event) {
                                var res = event.target.result;
                                console.log('res:', res);
                                console.log('File extension:', msg.substr(msg.lastIndexOf('/')));
                                $scope.speechToText(res.split(",").pop(), 'speech');
                            };
                            var file = this.response;
                            reader.readAsDataURL(file);
                        };
                        xhr.send();*/
                        convertAudioToBase64('GET', msg, function (err, data) {
                            if (err) { throw err; }
                            console.log(data);
                            $scope.speechToText(data.res.split(",").pop(), 'speech');
                        });
                        longPress = false;
                    }, function (msg) {
                        // failed
                        console.log('ERROR from recorder.stop: ' + msg);
                    });
                } else {
                    console.log('Shwoing tooltip...');
                    $('.recTooltip').css('visibility', 'visible');
                    $('.recTooltip').css('opacity', 1);
                    $('.recTooltiptext').css('visibility', 'visible');
                    $('.recTooltiptext').css('opacity', 1);

                    var hideTooltip = _.debounce(function (searchRequest, mediaType) {
                        $('.recTooltip').css('opacity', 0);
                        $('.recTooltiptext').css('opacity', 0);
                        $('.recTooltip').css('visibility', 'hidden');
                        $('.recTooltiptext').css('visibility', 'hidden');
                    }, 3000);

                    hideTooltip();

                }
            }

            $scope.recorder.record = function () {
                window.plugins.audioRecorderAPI.record(function (msg) {
                    // complete
                    console.log('ok: ' + msg);

                }, function (msg) {
                    // failed
                    console.log('ERROR from recorder.record: ' + msg);
                }, 30); // record 30 seconds
            }

            $scope.recorder.playback = function () {
                $scope.isPlaying = true;
                window.plugins.audioRecorderAPI.playback(function (msg) {
                    // complete
                    $scope.isPlaying = false;
                    $scope.$apply();
                    console.log('ok: ' + msg);
                }, function (msg) {
                    // failed
                    console.log('ERROR from recorder.playback : ' + msg);
                });
            };

            $scope.recorder.pausePlayback = function() {
                window.plugins.audioRecorderAPI.pausePlayback(function(msg) {
                    // complete
                    console.log('ok: ' + msg);
                }, function(msg) {
                    // failed
                    console.log('ko: ' + msg);
                });
            }

            $scope.recorder.stopPlayback = function() {

                window.plugins.audioRecorderAPI.stopPlayback(function(msg) {
                    $scope.isPlaying = false;
                    $scope.$apply();
                    // complete
                    console.log('ok: ' + msg);
                }, function(msg) {
                    // failed
                    console.log('ko: ' + msg);
                });
            }

            $scope.startRecording = function () {
                longPress = true;

                $('#rectButton').addClass("blueRec");
                $('.recTooltip .recTooltiptext').css({
                    'visibility': 'hidden',
                    'opacity': 0
                });

                // chrono();
                $scope.recorder.record();

                //callDb();
            }

            $scope.stopRecording = function () {
                $('#rectButton').removeClass("blueRec");
                //document.getElementById('heroInput').value = '';
                clearTimeout(timer);
                $scope.recorder.stop();
                /*navigator.DBMeter.stop(function () {
                    console.log("DBMeter well stopped");
                }, function (e) {
                    console.log('code: ' + e.code + ', message: ' + e.message);
                });*/

            }

            function nearestMakani(lat, lng) {
                try {
                    $rootScope.makaniInfo = '';
                    $rootScope.makani = '';
                    var deferred = $q.defer();
                    Makani.getNearestMakani(lat, lng).then(
                        function (data) {
                            console.log('===> getNearestMakani :: lat, lng ::  ', lat, lng);
                            console.log('===> getNearestMakani ::  ', data);
                            deferred.resolve(data);
                        }, function (error) {
                            $logger.log('ERROR GetNearestMakani : ', error)
                        }
                    );
                    return deferred.promise;
                } catch (e) {
                    console.log('===> ERROR ', e);
                }
            }

        }]
);