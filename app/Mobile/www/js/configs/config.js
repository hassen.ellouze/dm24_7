
/* JavaScript content from js/configs/config.js in folder common */
iDubaiApp.factory('$config', [
        function () {

            //PROD
            // var currentServerUrl = "https://mfpprod.dm.gov.ae";
            //STAG
            var currentServerUrl = "https://mfpstag.dm.gov.ae";

            try {
                currentServerUrl = WL.Client.getAppProperty(WL.AppProp.WORKLIGHT_ROOT_URL);
                if (currentServerUrl.indexOf("mfpprod.dm.gov.ae") > -1) {
                    currentServerUrl = "https://mfpprod.dm.gov.ae";
                } else if (currentServerUrl.indexOf("mfpstag.dm.gov.ae") > -1) {
                    currentServerUrl = "https://mfpstag.dm.gov.ae";
                } else {
                    currentServerUrl = "https://mfpstag.dm.gov.ae";
                }
            } catch (e) {
            }

            //for production
            var config = {
                enableLogger: true,
                logLevel: 0,
                server: currentServerUrl,
                env: "DEV"
            };

            config.cacheTimes = {
                wcmCacheTime: 60, //Every 60 min
                requestsCacheTime: 2, //Every 2 min
                faqCacheTime: (60 * 24 * 7), //Every week : 10080 min
                categoriesCacheTime: (60 * 24 * 7) //Every week : 10080 min
            };

            /* ngCash configuration */
            config.cache = {
                dbName: 'DM_Data_Storage',
                dbDesc: 'DM Data Storage',
                objectStore: 'DM_service_data',
                serviceID: 'DM',
                version: 1,
                bSize: 10 * 1024 * 1024,// big size
                mSize: 7 * 1024 * 1024, // medium size
                lSize: 5 * 1024 * 1024, // low size
                storageType: 'AUTO', // 'INDEXED', 'WEBSQL' , 'AUTO'
                clearDbCashe: false, //clear cache on start up
                expiredCasheDate: ''
            };

            // TODO change for PRODUCTION
            var urlProd = 'https://happinessmeter.dubai.gov.ae/HappinessMeter2/PostDataService';
            var urlStag = 'https://happinessmeterqa.dubai.gov.ae/HappinessMeter2/PostDataService';
            config.happiness = {
                applicationID: 'DM247APP',
                type: 'SMARTAPP',
                url: urlProd,
                notes: 'MobileSDK Vote',
                themeColor: '#4aadef    '
            };

            return config;
        }
    ]
);
