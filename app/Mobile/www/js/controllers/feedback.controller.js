iDubaiApp.controller('feedbackController',['$scope', 'LoadingService', 'helpdeskService', '$notification', '$filter', '$state' ,
    function ($scope, LoadingService, helpdeskService, $notification, $filter, $state) {
        $scope.currentLang = localStorage.getItem('dm-lang');
        $scope.feedback = 1;

    $scope.feedbackOptions = [
        {
            value: '1',
            label: 'Give feedback regarding services',
            labelAR: 'تقديم ملاحظات بخصوص الخدمات'
        },
        {
            value: '2',
            label: 'Report a bug in the app',
            labelAR: 'الإبلاغ عن خطأ في التطبيق'
        }
    ];
    $scope.feedback = $scope.feedbackOptions[0];

    $scope.submitForm = function(details, feedback) {

       //todo add optional email and name if user is connected
       var type = (feedback.value == '1') ? 'Feedback' : 'Technical issue';
       var options = {
           description : details,
           subject: feedback.label,
           osInfo: WL.Client.getEnvironment()
       };


       console.log('type : ', type);
       console.log('options : ', options);

        LoadingService.show();
        helpdeskService.senMail(type, options).then(function (success) {
            LoadingService.hide();
            try {
                WL.SimpleDialog.show($filter('translate')('Alert'), ( (feedback.value == '1') ? $filter('translate')("Feedback sent") : $filter('translate')("Email sent")), [{
                    text: $filter('translate')('OK'), handler: function () {
                        $state.go('dashboard');
                    }
                }]);
            } catch (e) {
                alert(message);
            }

        }, function (error) {
            LoadingService.hide();
            $notification.alert($filter('translate')("Failure - Retry"));
        });
    }
}]);
