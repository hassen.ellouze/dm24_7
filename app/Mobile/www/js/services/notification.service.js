
/* JavaScript content from js/services/notification.service.js in folder common */
iDubaiApp.factory("DMService", ['$q', '$DbStorageService', '$procedures', '$config', '$logger', 'underscore', 'CommonUtils', 'ConfigAdapters',
        function ($q, $DbStorageService, $procedures, $config, $logger, underscore, CommonUtils, ConfigAdapters) {
            var _ = underscore;

            /**
             * Create new request
             * @param params
             */
            function createNotification(params) {
                try {
                    var deferred = $q.defer();
/*                    var invocationData = {
                        adapter: 'NotificationsDM24Adapter',
                        procedure: 'createNotification',
                        parameters: params
                    };*/
                    CommonUtils.invokeGetResources({
                        path: ConfigAdapters.NotificationsDM24Adapter.createNotification,
                        params: [params],
                        showLoading: true
                    }).then(function (response) {
                        console.log('result ', response);
                        if( response && response.responseJSON && response.responseJSON.isSuccessful ){
                            deferred.resolve(response.responseJSON);
                        }
                    }, function (error) {
                        console.log('error ', error);
                        deferred.reject(error);
                    });
/*                    WL.Client.invokeProcedure(invocationData, {
                        onSuccess: $.proxy(function (data) {
                            $logger.log('## SUCCESS INVOKE', data);
                            deferred.resolve(data.invocationResult);
                        }, this),
                        onFailure: $.proxy(function (error) {
                            $logger.log('## FAIL INVOKE', error);
                            deferred.reject(error);
                        }, this)
                    });*/
                    return deferred.promise;
                } catch (e) {
                    console.log('===> ERROR ', e);
                }
            }

            /**
             * Add Photo
             * @param params
             */
            function addAttachment(params) {
                var defer = $q.defer();
                CommonUtils.invokeGetResources({
                    path: ConfigAdapters.AttachmentAdapter.addAttachment,
                    params: [params],
                    showLoading: true
                }).then(function (response) {
                    console.log('result ', response);
                    try {
                        if( response && response.responseJSON && response.responseJSON.isSuccessful ){
                            defer.resolve(response.responseJSON);
                        }
                        else{
                            defer.reject({});
                        }
                    }catch (e) {
                        console.log(e);
                        defer.reject(e);
                    }
                }, function (error) {
                    console.log('error ', error);
                    defer.reject(error);
                });
                /*
                var deferred = $q.defer();
                var invocationData = {
                    adapter: 'AttachmentAdapter',
                    procedure: 'addAttachment',
                    parameters: [params]
                };
                WL.Client.invokeProcedure(invocationData, {
                    onSuccess: $.proxy(function (data) {
                        $logger.log('## SUCCESS INVOKE ##', data);
                        deferred.resolve(data.invocationResult);
                    }, this),
                    onFailure: $.proxy(function (error) {
                        $logger.log('## FAIL INVOKE ##', error);
                        deferred.reject(error);
                    }, this)
                });
                */
            return defer.promise;
            }

            /**
             * Get latest services and notifications
             * @param phone
             */
            function getLatestServicesAndNotifications(phone) {
                try {
                    var defer = $q.defer();
                    var result = [];
                    CommonUtils.invokeGetResources({
                        path: ConfigAdapters.DMCRM24Adapter.getMyLatestNotifications,
                        params: [phone],
                        showLoading: true
                    }).then(function (response) {
                        console.log('result ', response);
                        try {
                            if( response && response.responseJSON && response.responseJSON.isSuccessful ){
                                result = getResponseArray(response.responseJSON);
                                defer.resolve(sortedResponseByDate(result));
                            }
                            else{
                                defer.reject({});
                            }
                        }catch (e) {
                            console.log(e);
                            defer.reject(e);
                        }
                    }, function (error) {
                        console.log('error ', error);
                        defer.resolve(sortedResponseByDate(result));
                    });
                    /*
                    $procedures.getMyLatestNotifications.send([phone], false).then(function (response) {
                        result = getResponseArray(response.invocationResult);
                        defer.resolve(sortedResponseByDate(result));
                    }, function (error) {
                        defer.resolve(sortedResponseByDate(result));
                        $logger.log("WS : Failed to get services data");
                    });
                    */
                    return defer.promise;
                } catch (e) {
                    console.log('===> ERROR ', e);
                }
            }

            /**
             * Sorted response by date
             * @param response
             * @returns {Array|*}
             */
            function sortedResponseByDate(response) {
                return _.sortBy(response, function (node) {
                    //return -(new Date(node.RequestStatusDate).getTime());
                    return - moment(node.RequestStatusDate, ['DD/MM/YYYY HH:mm', moment.ISO_8601]);
                });
            }

            /**
             * Get array of WS response
             * @param response
             * @returns {Array}
             */
            function getResponseArray(response) {
                /*var result = response.Envelope.Body.DMNotificationQuerySLA_Output.ListOfDmNotificationRequest_Io;
                var arr = [];
                if (result.hasOwnProperty('DmNotificationRequest')) {
                    arr = result.DmNotificationRequest;
                }
                return arr;*/
                if( response ){
                    return response.array;
                }
                return [];
            }

            /**
             * Get latest requests
             * @param phone
             * @param loader
             */
            function getMyLatestNotifications(phone, loader) {
                try {
                    var defer = $q.defer();
                    getLatestServicesAndNotifications(phone).then(function (result) {
                        if (result) {
                            defer.resolve(result);
                        }
                    }, function (error) {
                        defer.resolve([]);
                    });
                    return defer.promise;
                } catch (e) {
                    console.log('===> ERROR ', e);
                }
            }

            return {
                createNotification: createNotification,
                getMyLatestNotifications: getMyLatestNotifications,
                addAttachment: addAttachment
            };
        }
    ]
);
