
/* JavaScript content from js/services/faq.service.js in folder common */
iDubaiApp.factory('faqsService', ['$q', '$config', '$DbStorageService', '$logger', 'LoadingService', 'CommonUtils', 'ConfigAdapters',
        function ($q, $config, $DbStorageService, $logger, LoadingService, CommonUtils, ConfigAdapters) {

            function getFAQ() {
                var deferred = $q.defer();
                /*
                var invocationData = {
                    adapter: 'FAQ_Adapter',
                    procedure: 'getFAQ',
                    parameters: []
                };
                */
                $DbStorageService.get('FAQ_json_store').then(function (responseData) {
                    //Data exist
                    var data = JSON.parse(responseData.value);
                    deferred.resolve(data.faq);
                    var t = new Date().getTime();
                    var hourDiff = t - data.cacheTime;
                    var diffMins = (hourDiff / 60 / 1000);
                    if (data.cacheTime === null || (diffMins > $config.cacheTimes.faqCacheTime)) {

                        /*
                        CommonUtils.invokeGetResources({
                            path: ConfigAdapters.FaqAdapter.getFAQ
                        }).then(function (result) {
                            console.log("result ", result);
                            if (result.responseJSON.isSuccessful) {
                                $DbStorageService.remove('FAQ_json_store').then(function (resp) {
                                    $logger.info('FAQ Data is removed : ', resp);
                                    var temp = {
                                        faq: result.responseJSON.FAQ,
                                        cacheTime: t
                                    };
                                    $DbStorageService.add('FAQ_json_store', JSON.stringify(temp)).then(function (resp) {
                                        $logger.info("FAQ Data is saved");
                                    }, function (err) {
                                        $logger.log("Failed to save FAQ data");
                                    });
                                }, function (err) {
                                    $logger.log("Failed to remove FAQ data");
                                });
                            }
                        }, function (error) {

                        });
                        */
                        /*
                        WL.Client.invokeProcedure(invocationData, {
                            onSuccess: $.proxy(function (res) {
                                if (res.invocationResult.isSuccessful) {

                                    if (res.invocationResult.FAQ) {
                                        $DbStorageService.remove('FAQ_json_store').then(function (resp) {
                                            $logger.info('FAQ Data is removed : ', resp);
                                            var temp = {
                                                faq: res.invocationResult.FAQ,
                                                cacheTime: t
                                            };
                                            $DbStorageService.add('FAQ_json_store', JSON.stringify(temp)).then(function (resp) {
                                                $logger.info("FAQ Data is saved");
                                            }, function (err) {
                                                $logger.log("Failed to save FAQ data");
                                            });
                                        }, function (err) {
                                            $logger.log("Failed to remove FAQ data");
                                        });
                                    }

                                }
                            }, this),
                            onFailure: $.proxy(function (error) {
                                $logger.log("WS : Failed to get new FAQ data");
                            }, this)
                        });
                        */
                    }
                }, function (error) {
                    //Data does not exist
                    LoadingService.show();
                    CommonUtils.invokeGetResources({
                        path: ConfigAdapters.FaqAdapter.getFAQ
                    }).then(function (data) {
                        console.log("result ", data);
                        LoadingService.hide();
                        if (data.responseJSON.isSuccessful) {
                            deferred.resolve(data.responseJSON.FAQ);
                            var t = new Date().getTime();
                            var temp = {
                                faq: data.responseJSON.FAQ,
                                cacheTime: t
                            };
                            if (data.responseJSON.FAQ) {
                                $DbStorageService.add('FAQ_json_store', JSON.stringify(temp)).then(function (resp) {
                                    $logger.log("FAQ Data is saved");
                                }, function (err) {
                                    $logger.log("Failed to save FAQ data");
                                });
                            }
                        }
                    }, function (error) {

                    });
                    /*
                    WL.Client.invokeProcedure(invocationData, {
                        onSuccess: $.proxy(function (data) {
                            LoadingService.hide();
                            if (data.invocationResult.isSuccessful) {
                                deferred.resolve(data.invocationResult.FAQ);
                                var t = new Date().getTime();
                                var temp = {
                                    faq: data.invocationResult.FAQ,
                                    cacheTime: t
                                };
                                if (data.invocationResult.FAQ) {
                                    $DbStorageService.add('FAQ_json_store', JSON.stringify(temp)).then(function (resp) {
                                        $logger.log("FAQ Data is saved");
                                    }, function (err) {
                                        $logger.log("Failed to save FAQ data");
                                    });
                                }
                            }
                        }, this),
                        onFailure: $.proxy(function (error) {
                            LoadingService.hide();
                            $logger.log("WS : Failed to get new FAQ data");
                            deferred.reject({});
                        }, this)
                    });
                    */
                });
                return deferred.promise;
            }

            function getFAQ_old() {
                var deferred = $q.defer();
                var invocationData = {
                    adapter: 'FAQ_Adapter',
                    procedure: 'getFAQ',
                    parameters: []
                };

                WL.Client.invokeProcedure(invocationData, {
                    onSuccess: $.proxy(function (data) {
                        if (data.invocationResult.isSuccessful) {
                            deferred.resolve(data);
                        } else {
                            deferred.reject({});
                        }
                    }, this),
                    onFailure: $.proxy(function (error) {
                        deferred.reject(error);
                    }, this)
                });

                return deferred.promise;
            }

            return {
                getFAQ: getFAQ
            };
        }
    ]
);
