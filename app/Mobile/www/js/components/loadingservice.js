
/* JavaScript content from js/components/loadingservice.js in folder common */
"use strict";
iDubaiApp.factory("LoadingService", ["$rootScope", '$log', '$filter', '$logger',
    function ($rootScope, $log, $filter, $logger) {

        var service = {
            isOpen: false,
            show: function (text, canceler) {
                service.isOpen = true;
                if (!text) {
                    text = $filter("translate")('Please wait...');
                } else {
                    text = $filter("translate")(text);
                }

                /*setTimeout(function () {
                    canceler.resolve();
                },100)*/

                if (canceler) {

                    $("#resultLoading").on("click", function () {
                        canceler.resolve();
                    });
                }

                if (jQuery('body').find('#resultLoading').attr('id') != 'resultLoading') {
                    jQuery('body').append('<div id="resultLoading" style="display:none">' +
                        '<div class="spinnerAndroid">' +
                        '<svg class="circular" viewBox="25 25 50 50">' +
                        '<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>' +
                        '</svg> <div id="loadingText">' + text +
                        '</div>' +
                        '</div>' +
                        '<div class="bg"></div>' +
                        '</div>');
                } else {
                    if (canceler) {
                        $('#loadingText').html(text + '<br><a class="btn btn--goto skyBlue2 fz14-B mt-4" ' +
                            'style="height: 30px;padding: 5px;width: calc(100% - 80px);-webkit-box-shadow: 0 5px 12px rgba(144, 144, 144, 0.34); box-shadow: 0 5px 12px rgba(141, 141, 141, 0.34);">' + $filter("translate")("Cancel") + '</a>');
                    } else {
                        $('#loadingText').html(text);
                    }

                }

                jQuery('#resultLoading').css({
                    'width': '100%',
                    'height': '100%',
                    'position': 'fixed',
                    'z-index': '999999999999',
                    'top': '0',
                    'left': '0',
                    'right': '0',
                    'bottom': '0',
                    'margin': 'auto'
                });

                jQuery('#resultLoading .bg').css({
                    'background': '#000000',
                    'opacity': '0.7',
                    'width': '100%',
                    'height': '100%',
                    'position': 'absolute',
                    'top': '0'
                });

                jQuery('#resultLoading>div:first').css({
                    'width': '250px',
                    'height': '75px',
                    'text-align': 'center',
                    'position': 'fixed',
                    'top': '0',
                    'left': '0',
                    'right': '0',
                    'bottom': '0',
                    'margin': 'auto',
                    'font-size': '16px',
                    'z-index': '10',
                    'color': '#ffffff'

                });

                jQuery('#resultLoading .bg').height('100%');
                jQuery('#resultLoading').fadeIn(300);

            },
            hide: function () {
                if (service.isOpen) {
                    jQuery('#resultLoading .bg').height('100%');
                    jQuery('#resultLoading').fadeOut(300);
                    service.isOpen = false;
                }
            }
        };

        return service;
    }])

    .factory("LoadingService-rec", ["$rootScope", '$log', '$filter', '$logger',
        function ($rootScope, $log, $filter, $logger) {

            return {
                wl: null,
                layoutId: "loadfloat",
                defaultText: $filter('translate')("Please wait..."),
                count: 0,

                /**
                 * init Loading Service
                 */
                initLoadingService: function () {
                    var $self = this;
                    try {
                        $self.wl = new WL.BusyIndicator($self.layoutId, {
                            text: $filter('translate')($self.defaultText),
                            duration: 10,
                            minDuration: 3,
                            bounceAnimation: true,
                            textColor: "#333"
                        });
                    } catch (e) {
                        $log.log("pls use worklight");
                        return false;
                    }
                },

                /**
                 * show loader
                 */
                show: function () {
                    var $self = this;

                    window.count = $self.count;
                    window.wl = $self.wl;
                    try {

                        if (!$self.wl)
                            $self.initLoadingService();

                        $self.count++;

                        if (!$self.wl.isVisible()) {
                            $self.wl.show();
                            setTimeout(function () {
                                if ($self.wl.isVisible()) {
                                    $self.wl.hide();
                                    $self.count--;
                                }
                            }, 10000);
                        }

                    } catch (ex) {
                        $log.log(ex);
                        return false;
                    }
                },

                /**
                 * hide loader
                 */
                hide: function () {

                    var $self = this;
                    $self.count = $self.count > 0 ? $self.count - 1 : 0;

                    if ($self.wl && $self.wl.isVisible() && $self.count == 0) {
                        $self.wl.hide();
                    }
                }
            };

            /**
             * HTML5 Loader
             */

            var Class = {
                create: function () {
                    return function () {
                        this.initialize.apply(this, arguments);
                    };
                }
            };

            var Html5Loading = Class.create();
            Html5Loading.prototype = {
                initialize: function () {
                    this.radius = 26;
                    this.circleLineWidth = 6;
                    this.circleColor = '#3a444d';
                    this.moveArcColor = '#ffffff';

                    this.showQueue = [];
                    this.waterfall = this.createElement();
                    this.waterfall.appendTo("body");
                    this.canvas = this.waterfall.find("canvas:first")[0];
                    this.waterfall.hide();
                    this.openFlag = true;
                },
                showControl: function () {
                    var canvas = this.canvas;
                    if (!canvas.getContext) return;
                    if (canvas.__loading) return;
                    canvas.__loading = this;
                    this.waterfall.show();
                    var ctx = canvas.getContext('2d');
                    var radius = this.radius;
                    var me = this;
                    var rotatorAngle = Math.PI * 1.5;
                    var step = Math.PI / 6;
                    canvas.loadingInterval = setInterval(function () {
                        ctx.clearRect(0, 0, canvas.width, canvas.height);
                        var lineWidth = me.circleLineWidth;
                        var center = {x: canvas.width / 2, y: canvas.height / 2};
                        ctx.beginPath();
                        ctx.lineWidth = lineWidth;
                        ctx.strokeStyle = me.circleColor;
                        ctx.arc(center.x, center.y, radius, 0, Math.PI * 2);
                        ctx.closePath();
                        ctx.stroke();
                        ctx.beginPath();
                        ctx.strokeStyle = me.moveArcColor;
                        ctx.arc(center.x, center.y, radius, rotatorAngle, rotatorAngle + Math.PI * .45);
                        ctx.stroke();
                        rotatorAngle += step;
                    }, 50);
                },
                hideControl: function () {
                    var canvas = this.canvas;
                    canvas.__loading = false;
                    if (canvas.loadingInterval) {
                        window.clearInterval(canvas.loadingInterval);
                    }
                    var ctx = canvas.getContext('2d');
                    if (ctx) ctx.clearRect(0, 0, canvas.width, canvas.height);
                    this.waterfall.hide();
                    this.ForcedHideTimeOutID ? clearTimeout(this.ForcedHideTimeOutID) : null;
                },
                show: function () {
                    if (this.openFlag) {
                        this.addQueue("noID");
//    this.showControl();
                        this.waterfall.show();
                        this.timeOutForcedHide();
                    }
                },
                hide: function () {
                    if (this.removeQueue("noID")) {
                        this.isHide();
                    }
                },
                forcedHide: function () {
                    this.showQueue = [];
//         this.hideControl();
                    this.waterfall.hide();
                },
                uniqueShow: function () {
                    if (this.openFlag) {
                        var id = this.getUniqueID();
                        this.addQueue(id);
//     this.showControl();
                        this.waterfall.show();
                        this.timeOutForcedHide();
                        return id;
                    }
                },
                uniqueHide: function (id) {
                    if (this.removeQueue(id)) {
                        this.isHide();
                    }
                },
                isHide: function () {
                    if (this.showQueue.length < 1) {
//            this.hideControl();
                        this.waterfall.hide();
                    }
                },
                addQueue: function (id) {
                    this.showQueue.push(id);
                },
                removeQueue: function (id) {
                    for (var i in this.showQueue) {
                        if (this.showQueue[i] == id) {
                            this.showQueue.splice(i, 1);
                            return true;
                        }
                    }
                    return false;
                },
                getUniqueID: function () {
                    var tm = new Date();
                    var str = tm.getMilliseconds() + tm.getSeconds() * 60 + tm.getMinutes() * 3600 + tm.getHours() * 60 * 3600 + tm.getDay() * 3600 * 24 + tm.getMonth() * 3600 * 24 * 31 + tm.getYear() * 3600 * 24 * 31 * 12;
                    return str;
                },
                timeOutForcedHide: function () {
                    var _this = this;
                    this.ForcedHideTimeOutID ? clearTimeout(this.ForcedHideTimeOutID) : null;
                    this.ForcedHideTimeOutID = setTimeout(function () {
                        _this.forcedHide();
                    }, 30000);
                },
                createElement: function () {
                    var html = '<div id="divIsLoading" >'
                        + '<div style="position: fixed; top: 0px; right: 0px; bottom: 0px; left: 0px; z-index: 100040; display: table; width: 100%;height: 100%;">'
                        + '<div style="width: 100%;height: 100%;display: table-cell;vertical-align: middle;">'
                        //+ '<div style="margin: 0px auto;/* position: relative; */ /* width: 210px; *//* height: 150px; */" ></div>'
                        //+ '<div ><div ></div><div ></div></div>'
                        //      + '<div ><div ></div><div ></div></div>'
                        + '<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>'
                        + '</div></div></div>';
//    + '<div >'
//    + '<div style="width: 210px;height: 150px;background-color: #3a444d;border-radius: 4px;opacity: 0.75;"></div>'
//    + '<div style="width: 100%;height: 100%;position: absolute; left:0px; top:0px;">'
//+ '<canvas width="70px" height="70px" ></canvas>'
//     + '<div ></div>'
//     + '<div style="width:100%; font-size:15px; color:#ffffff; margin-top:5px; text-align: center;">Loading . . .</div>'
//     + '</div></div></div></div></div>';

                    return $(html);
                },
                isClose: function () {
                    this.openFlag = false;
                }
            };

            var load = new Html5Loading();

            var service = {};

            service.show = function () {
                load.show();
            };

            service.hide = function () {
                load.hide();
            };

            service.forcedHide = function () {
                load.forcedHide();
            };

            service.uniqueShow = function () {
                return load.uniqueShow();
            };

            service.uniqueHide = function (id) {
                load.uniqueHide(id);
            };

            service.close = function () {
                load.isClose();
            };

            $rootScope.$on("$locationChangeSuccess", function () {
                load.forcedHide();
            });

//return service;
        }])


//function Html5Loading(options) {
//    if (options) {
//        this.radius = options.radius || 12;
//        this.circleLineWidth = options.circleLineWidth || 4;
//        this.circleColor = options.circleColor || 'lightgray';
//        this.moveArcColor = options.moveArcColor || 'gray';
//        this.text = options.text || "Loading,Please wait...";
//    } else {
//        this.radius = 12;
//        this.circelLineWidth = 4;
//        this.circleColor = 'lightgray';
//        this.moveArcColor = 'gray';
//        this.text = "Loading,Please wait...";
//    }
//    this.showQueue = [];
//    this.waterfall = createElement(this.text);
//    this.waterfall.appendTo("body");
//    this.canvas = this.waterfall.find("canvas:first")[0];
//    //       this.waterfall.hide();
//};

//Html5Loading.prototype = {
//    showControl: function () {
//        var canvas = this.canvas;
//        if (!canvas.getContext) return;
//        if (canvas.__loading) return;
//        canvas.__loading = this;
//        this.waterfall.show();
//        var ctx = canvas.getContext('2d');
//        var radius = this.radius;
//        var me = this;
//        var rotatorAngle = Math.PI * 1.5;
//        var step = Math.PI / 6;
//        canvas.loadingInterval = setInterval(function () {
//            ctx.clearRect(0, 0, canvas.width, canvas.height);
//            var lineWidth = me.circleLineWidth;
//            var center = { x: canvas.width / 2, y: canvas.height / 2 };
//            ctx.beginPath();
//            ctx.lineWidth = lineWidth;
//            ctx.strokeStyle = me.circleColor;
//            ctx.arc(center.x, center.y, radius, 0, Math.PI * 2);
//            ctx.closePath();
//            ctx.stroke();
//            ctx.beginPath();
//            ctx.strokeStyle = me.moveArcColor;
//            ctx.arc(center.x, center.y, radius, rotatorAngle, rotatorAngle + Math.PI * .45);
//            ctx.stroke();
//            rotatorAngle += step;
//        }, 50);
//    },
//    hideControl: function () {
//        var canvas = this.canvas;
//        canvas.__loading = false;
//        if (canvas.loadingInterval) {
//            window.clearInterval(canvas.loadingInterval);
//        }
//        var ctx = canvas.getContext('2d');
//        if (ctx) ctx.clearRect(0, 0, canvas.width, canvas.height);
//        this.waterfall.hide();
//        this.ForcedHideTimeOutID ? clearTimeout(this.ForcedHideTimeOutID) : null;
//    },
//    show: function () {
//        this.addQueue("noID");
//        this.showControl();
//        this.timeOutForcedHide();
//    },
//    hide: function () {
//        if (this.removeQueue("noID")) {
//            this.isHide();
//        }
//    },
//    forcedHide: function () {
//        this.showQueue = [];
//        this.hideControl();
//    },
//    uniqueShow: function () {
//        var id = this.getUniqueID();
//        this.addQueue(id);
//        this.showControl();
//        this.timeOutForcedHide();
//        return id;
//    },
//    uniqueHide: function (id) {
//        if (this.removeQueue(id)) {
//            this.isHide();
//        }
//    },
//    isHide: function () {
//        if (this.showQueue.length < 1) {
//            this.hideControl();
//        }
//    },
//    addQueue: function (id) {
//        this.showQueue.push(id);
//    },
//    removeQueue: function (id) {
//        for (var i in this.showQueue) {
//            if (this.showQueue[i] == id) {
//                this.showQueue.splice(i, 1);
//                return true;
//            }
//        }
//        return false;
//    },
//    getUniqueID: function () {
//        var tm = new Date();
//        var str = tm.getMilliseconds() + tm.getSeconds() * 60 + tm.getMinutes() * 3600 + tm.getHours() * 60 * 3600 + tm.getDay() * 3600 * 24 + tm.getMonth() * 3600 * 24 * 31 + tm.getYear() * 3600 * 24 * 31 * 12;
//        return str;
//    },
//    timeOutForcedHide: function () {
//        var _this = this;
//        this.ForcedHideTimeOutID ? clearTimeout(this.ForcedHideTimeOutID) : null;
//        this.ForcedHideTimeOutID = setTimeout(function () {
//            _this.forcedHide();
//        }, 30000);
//    }
//};