function getBusinessEvents(){
	return getWCMFeed("wps/wcm/connect/IDUBAIContent/Events/Business/BusinessEvent1?srv=cmpnt&source=library&cmpntid=63aa3407-bd47-4d6a-a829-754e6849b04b&WCM_Page.ResetAll=TRUE&CACHE=NONE&CONTENTCACHE=NONE&CONNECTORCACHE=NONE");
}

function getFoodEvents(){
	return getWCMFeed("wps/wcm/connect/IDUBAIContent/Events/Food/FoodEvent1?srv=cmpnt&source=library&cmpntid=d31d0eca-faf6-4b18-8b6b-837400b36224&WCM_Page.ResetAll=TRUE&CACHE=NONE&CONTENTCACHE=NONE&CONNECTORCACHE=NONE");
}

function getBusinessNews(){
	return getWCMFeed("wps/wcm/connect/IDUBAIContent/Articles/BusinessNews/BusinessNewsArticle1?srv=cmpnt&source=library&cmpntid=ee482f34-4f04-4a10-b162-d4fc141fd66e&WCM_Page.ResetAll=TRUE&CACHE=NONE&CONTENTCACHE=NONE&CONNECTORCACHE=NONE");
}

function getIntlBusinessNews(){
	return getWCMFeed("wps/wcm/connect/IDUBAIContent/Articles/InternationalBusinessNews/IntlBusinessNewsArticle1?srv=cmpnt&source=library&cmpntid=ded66133-5edc-46f7-a6e2-5ceb832812b4&WCM_Page.ResetAll=TRUE&CACHE=NONE&CONTENTCACHE=NONE&CONNECTORCACHE=NONE");
}

function getDMProjects(){
	return getWCMFeed("wps/wcm/connect/IDUBAIContent/Articles/DMProjects/DMProjectsNewsArticle1?srv=cmpnt&source=library&cmpntid=b4b360d3-2c70-440a-a480-1a7cc6c89089&WCM_Page.ResetAll=TRUE&CACHE=NONE&CONTENTCACHE=NONE&CONNECTORCACHE=NONE");
}

function getNewToDubai(){
	return getWCMFeed("wps/wcm/connect/IDUBAIContent/Articles/NewToDubai/?srv=cmpnt&source=library&cmpntid=c79051c7-b448-42ba-8799-a1fe35ec8615&WCM_Page.ResetAll=TRUE&CACHE=NONE&CONTENTCACHE=NONE&CONNECTORCACHE=NONE");
}

function getHeadlines(){
	return getWCMFeed("wps/wcm/connect/IDUBAIContent/Articles/Headlines/HeadlinesArticle1?srv=cmpnt&source=library&cmpntid=13f1c149-6349-49de-ad08-998452cd0427&WCM_Page.ResetAll=TRUE&CACHE=NONE&CONTENTCACHE=NONE&CONNECTORCACHE=NONE");
}

function getStaticPages(){
	return getWCMFeed("wps/wcm/connect/IDUBAIContent/Articles/StaticPages/StaticPagesArticle1?srv=cmpnt&source=library&cmpntid=397785c6-f4dc-4079-943d-9d79913cd3b5&WCM_Page.ResetAll=TRUE&CACHE=NONE&CONTENTCACHE=NONE&CONNECTORCACHE=NONE");
}

function getSuccessStories(){
	return getWCMFeed("wps/wcm/connect/IDUBAIContent/SuccessStories/SuccessStories/SuccessStories1?srv=cmpnt&source=library&cmpntid=ad034ad7-5ec4-4d9c-98f1-b727c2b00fa7&WCM_Page.ResetAll=TRUE&CACHE=NONE&CONTENTCACHE=NONE&CONNECTORCACHE=NONE");
}

function getQuotes(){
	return getWCMFeed("wps/wcm/connect/IDUBAIContent/Quotes/Quotes/Quote1?srv=cmpnt&source=library&cmpntid=ef87d6f6-3b83-4cfb-9812-685a483e9c71&WCM_Page.ResetAll=TRUE&CACHE=NONE&CONTENTCACHE=NONE&CONNECTORCACHE=NONE");
}

function getWCMData(){
	return {
		wcm: {
			BusinessEvents: getBusinessEvents(),
			FoodEvents: getFoodEvents(),
			BusinessNews: getBusinessNews(),
			IntlBusinessNews: getIntlBusinessNews(),
			DMProjects: getDMProjects(),
			NewToDubai: getNewToDubai(),
			Headlines: getHeadlines(),
			StaticPages: getStaticPages(),
			SuccessStories: getSuccessStories(),
			Quotes: getQuotes(),
			NewsAr: getNews("ar"),
			NewsEn: getNews("en")
		}
	};
}

function getWCMFeed(feedPath){
	try {

        var input = {
            method : 'get',
            returnedContentType: 'html',
            returnedContentEncoding: "UTF-8",
            path : feedPath
        };

        var response = MFP.Server.invokeHttp(input);
        if(response.isSuccessful && response.html != undefined && response.html.body != undefined && response.html.body.p != undefined){
            var items = response.html.body.p.replace(/(\r\n|\n|\r)/gm,"");
            // items = items.substring(1,items.length-1);
            // items = JSON.parse("[" + items + "]");
            tems = "[" + items + "]";
            return {
                isSuccessful: response.isSuccessful,
                items: items
            };
        }
        else if( response.isSuccessful && response.html != undefined && response.html.body != undefined ){
            try {
                var items = response.html.body.replace(/(\r\n|\n|\r)/gm, "");
                items = items.substring(1, items.length - 1);
                // items = JSON.parse("[" + items + "]");
                items = "[" + items + "]";
            } catch (e) {
                return {
                    isSuccessful: false,
                    error: e
                }
            }
            return {
                isSuccessful: true,
                items: items
            };
        }else {
            return {
                isSuccessful: false
            };
        }
    }catch (e) {
        return {
            isSuccessful: false,
            error: e
        }
    }


	/*
	var obj = MFP.Server.invokeHttp(input);
	if(obj.isSuccessful && obj.html != undefined && obj.html.body != undefined && obj.html.body.p != undefined){
		var items = obj.html.body.p.replace(/(\r\n|\n|\r)/gm,"");
		items = items.substring(1,items.length-1);
		items = JSON.parse("[" + items + "]");
		return {
			isSuccessful: true,
			items: items
		};
	}else if(obj.isSuccessful && obj.html != undefined && obj.html.body != undefined){
		try {
			var items = obj.html.body.replace(/(\r\n|\n|\r)/gm, "");
			items = items.substring(1, items.length - 1);
			items = JSON.parse("[" + items + "]");
		} catch (e) {
			return {
				isSuccessful: false
			};
		}
		return {
			isSuccessful: true,
			items: items
		};
	}else {
		return {
			isSuccessful: false
		};
	}
	*/
}

function getNews(lang){
	var input = {
		method : 'get',
		path : "/wps/feeds/news/"+ lang + "/feed.xml",
		returnedContentEncoding: "UTF-8"
	};

	var obj = MFP.Server.invokeHttp(input);
	var result = [];
	if(obj && obj.rss && obj.rss.channel && obj.rss.channel.item){
		result = obj.rss.channel.item;
	}
	return {
		items: result
	};
}

function getEvents(){
	var input = {
		method : 'get',
		path : "/wps/feeds/events/" + lang + "/feed.xml"
	};

	var obj = MFP.Server.invokeHttp(input);

	return obj;
}
