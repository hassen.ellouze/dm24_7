/*
 *  Licensed Materials - Property of IBM
 *  5725-I43 (C) Copyright IBM Corp. 2011, 2013. All Rights Reserved.
 *  US Government Users Restricted Rights - Use, duplication or
 *  disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */

function searchRequest(searchRequest, mediaType, requestFrom, platform, lang) {
    var response = null;
    try {
        var input = {
            method: 'POST',
            // returnedContentType: 'json',
            path: "search",
            parameters: {
                searchRequest: searchRequest,
                mediaType: mediaType,
                requestFrom: requestFrom,
                platform: platform,
                lang: lang,
            },
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            }
        };

        WL.Logger.error("Search Engine :: request  : "  );
        response = MFP.Server.invokeHttp(input);
        WL.Logger.error("createNotification :: response : " );
        return response;
    }
    catch (e) {
        return{
            isSuccessful: false,
            reference : e
        }
    }

}