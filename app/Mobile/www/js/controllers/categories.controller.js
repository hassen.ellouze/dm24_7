iDubaiApp.controller('categoriesController',
    ['$scope', '$timeout', '$state', 'DMCategories',
        function ($scope, $timeout, $state, DMCategories) {

            try {
                $scope.currentLang = localStorage.getItem('dm-lang');
                $scope.categories = [];
                var deviceHeight = ($(window).height());

                $scope.categHeight = ((deviceHeight) / 5) + 5 ;
                if (WL.Client.getEnvironment() === WL.Environment.IPHONE) {
                    $scope.categHeight = (deviceHeight) / 5;
                }
                /**
                 * Get categories list
                 */
                DMCategories.getCategories(true).then(function (data) {
                    $scope.categories = data;
                });
            } catch (e) {
                console.log('===> ERROR ', e);
            }
            /**
             * Redirect to services page
             * @param category
             */
            $scope.onSelectedCategory = function(category){
                if (category.categoryId === "1") {
                    $state.go('commonServices');
                } else {
                    $state.go('services', {id: category.categoryId})
                }
            };

            /**
             * Name of corresponding language
             * @param category
             * @returns {name}
             */
            $scope.getName = function (category) {
                return (this.currentLang === 'ar') ? category.service_title_ar : category.service_title_en;
            };


            $scope.getImageName = function(index) {
                var names = ['common_services', 'maps', 'pest', 'cleanliness', 'health', 'infras', 'planing', 'environment'];
                return names[index];
            };
            /*$scope.getImageSize = function(index) {
                var size = ['15', 15, 15, 15, 25, 20, 25];
                return size[index];
            }*/
        }
    ]
);
