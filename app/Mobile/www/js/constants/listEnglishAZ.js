angular.module("iDubaiApp").factory("$listEnglishAZ", [
    function() {
      
      var listEnglishAZ =
      [
        {
          "id": 1,
          "originalTitle": "Report AC defect in DM facilities",
          "title": "Report AC defect in DM facilities",
          "category_id": 11,
          "unit": "Building & Facilities Maintenance"
        },
        {
          "id": 76,
          "originalTitle": "Request removal of accident waste",
          "title": "Request removal of accident waste",
          "category_id": 4,
          "unit": "Specialized Cleaning Section"
        },
        {
          "id": 47,
          "originalTitle": "Report Air Pollution",
          "title": "Report Air Pollution",
          "category_id": 6,
          "unit": "Environmental control Sec"
        },
        {
          "id": 1,
          "originalTitle": "Farm Animal Preventive Veterinary Care",
          "title": "Farm Animal Preventive Veterinary Care",
          "category_id": 2,
          "unit": "Veterinary Services Section"
        },
        {
          "id": 13,
          "originalTitle": "Report violations by pet owners in residential areas",
          "title": "Report violations by pet owners in residential areas",
          "category_id": 2,
          "unit": "Veterinary Services Section"
        },
        {
          "id": 2,
          "originalTitle": "Agriculture services for Hatta area",
          "title": "Agriculture services for Hatta area",
          "category_id": 6,
          "unit": "Hatta Center"
        },
        {
          "id": 3,
          "originalTitle": "Request Artificial Insemination for Cattle",
          "title": "Request Artificial Insemination for Cattle",
          "category_id": 2,
          "unit": "Veterinary Services Section"
        },
        {
          "id": 66,
          "originalTitle": "Bad smell from DM sewerage network",
          "title": "Bad smell from DM sewerage network",
          "category_id": 3,
          "unit": "Drainage Sys. Maintainance"
        },
        {
          "id": 48,
          "originalTitle": "Report violation on public beaches",
          "title": "Report violation on public beaches",
          "category_id": 6,
          "unit": "Coastal Zone & Water Mngmnt"
        },
        {
          "id": 24,
          "originalTitle": "Report of beggars or street vendors",
          "title": "Report of beggars or street vendors",
          "category_id": 5,
          "unit": "Markets Section"
        },
        {
          "id": 14,
          "originalTitle": "Request safe disposal of frozen poultry",
          "title": "Request safe disposal of frozen poultry",
          "category_id": 5,
          "unit": "Veterinary Services Section"
        },
        {
          "id": 82,
          "originalTitle": "Report construction site without information board",
          "title": "Report construction site without information board",
          "category_id": 7,
          "unit": "Building Supervision Section"
        },
        {
          "id": 73,
          "originalTitle": "Report blocking of roads by DM sewage project",
          "title": "Report blocking of roads by DM sewage project",
          "category_id": 3,
          "unit": "Drainage Projects Executive"
        },
        {
          "id": 83,
          "originalTitle": "Report collapse of building",
          "title": "Report collapse of building",
          "category_id": 7,
          "unit": "Building Supervision Section"
        },
        {
          "id": 84,
          "originalTitle": " Building distortion on public view",
          "title": " Building distortion on public view",
          "category_id": 7,
          "unit": "Building Inspection Section"
        },
        {
          "id": 85,
          "originalTitle": "Report building without permission",
          "title": "Report building without permission",
          "category_id": 7,
          "unit": "Building Inspection Section"
        },
        {
          "id": 18,
          "originalTitle": "Request Bulky Waste Removal Services",
          "title": "Request Bulky Waste Removal Services",
          "category_id": 4,
          "unit": "Specialized Cleaning Section"
        },
        {
          "id": 86,
          "originalTitle": "Report change in the building use without DM permission",
          "title": "Report change in the building use without DM permission",
          "category_id": 7,
          "unit": "Building Inspection Section"
        },
        {
          "id": 25,
          "originalTitle": "Report cheats in food specifications ",
          "title": "Report cheats in food specifications ",
          "category_id": 8,
          "unit": "Food Control Section"
        },
        {
          "id": 26,
          "originalTitle": "Report toys not approved by DM",
          "title": "Report toys not approved by DM",
          "category_id": 5,
          "unit": "Consumer Products Safety"
        },
        {
          "id": 27,
          "originalTitle": "Report food hygiene violation",
          "title": "Report food hygiene violation",
          "category_id": 8,
          "unit": "Food Control Section"
        },
        {
          "id": 77,
          "originalTitle": "Request Cleanliness of main streets",
          "title": "Request Cleanliness of main streets",
          "category_id": 4,
          "unit": "Specialized Cleaning Section"
        },
        {
          "id": 78,
          "originalTitle": "Report hygiene issues in DM public facilities",
          "title": "Report hygiene issues in DM public facilities",
          "category_id": 4,
          "unit": "Specialized Cleaning Section"
        },
        {
          "id": 49,
          "originalTitle": "Request Cleanliness of parks facilites",
          "title": "Request Cleanliness of parks facilites",
          "category_id": 6,
          "unit": "Parks and Recreation Section"
        },
        {
          "id": 96,
          "originalTitle": "Report bad smell from DM sewage treatment stations",
          "title": "Report bad smell from DM sewage treatment stations",
          "category_id": 3,
          "unit": "Sewage Treatment Plant Dept"
        },
        {
          "id": 28,
          "originalTitle": "Report health & Safety Violation at Labour Camps",
          "title": "Report health & Safety Violation at Labour Camps",
          "category_id": 5,
          "unit": "Health Control Section"
        },
        {
          "id": 51,
          "originalTitle": "Report trees blocking road visibility",
          "title": "Report trees blocking road visibility",
          "category_id": 6,
          "unit": "Horticulture Section"
        },
        {
          "id": 87,
          "originalTitle": "Report Construction Noise",
          "title": "Report Construction Noise",
          "category_id": 7,
          "unit": "Building Supervision Section"
        },
        {
          "id": 17,
          "originalTitle": "Request for agricultural pests control",
          "title": "Request for agricultural pests control",
          "category_id": 1,
          "unit": "Horticulture Services Section"
        },
        {
          "id": 95,
          "originalTitle": "report on construction waste ",
          "title": "report on construction waste ",
          "category_id": 7,
          "unit": "Building Supervision Section"
        },
        {
          "id": 52,
          "originalTitle": "Report damage on marine establishment",
          "title": "Report damage on marine establishment",
          "category_id": 6,
          "unit": "Coastal Zone & Water Mngmnt"
        },
        {
          "id": 88,
          "originalTitle": "Report damaged advertisement boards",
          "title": "Report damaged advertisement boards",
          "category_id": 7,
          "unit": "Advertisement Control Section"
        },
        {
          "id": 29,
          "originalTitle": "Report spoil Food",
          "title": "Report spoil Food",
          "category_id": 8,
          "unit": "Food Control Section"
        },
        {
          "id": 53,
          "originalTitle": "Report digging wells without permit ",
          "title": "Report digging wells without permit ",
          "category_id": 6,
          "unit": "Resources Protection Sec."
        },
        {
          "id": 54,
          "originalTitle": "Report misuesed landscaping and greenery",
          "title": "Report misuesed landscaping and greenery",
          "category_id": 6,
          "unit": "Horticulture Section"
        },
        {
          "id": 2,
          "originalTitle": "Request repair of door or window in DM facilities",
          "title": "Request repair of door or window in DM facilities",
          "category_id": 11,
          "unit": "Building & Facilities Maintenance"
        },
        {
          "id": 4,
          "originalTitle": "Request removal of accumulated rain water for Hatta",
          "title": "Request removal of accumulated rain water for Hatta",
          "category_id": 3,
          "unit": "Hatta Center"
        },
        {
          "id": 3,
          "originalTitle": "Report Electrical faults in DM Faciltiies",
          "title": "Report Electrical faults in DM Faciltiies",
          "category_id": 11,
          "unit": "Building & Facilities Maintenance"
        },
        {
          "id": 4,
          "originalTitle": "Report Electrical faults at DM residences",
          "title": "Report Electrical faults at DM residences",
          "category_id": 11,
          "unit": "Building & Facilities Maintenance"
        },
        {
          "id": 5,
          "originalTitle": "Report faulty electronics at DM Facilities",
          "title": "Report faulty electronics at DM Facilities",
          "category_id": 11,
          "unit": "Building & Facilities Maintenance"
        },
        {
          "id": 33,
          "originalTitle": "Report Expired Food violation",
          "title": "Report Expired Food violation",
          "category_id": 8,
          "unit": "Food Control Section"
        },
        {
          "id": 5,
          "originalTitle": "Request parasite control service for Farm Animals",
          "title": "Request parasite control service for Farm Animals",
          "category_id": 2,
          "unit": "Veterinary Services Section"
        },
        {
          "id": 55,
          "originalTitle": "Request fallen tree removal on main roads",
          "title": "Request fallen tree removal on main roads",
          "category_id": 6,
          "unit": "Horticulture Section"
        },
        {
          "id": 56,
          "originalTitle": "Request fallen tree removal on internal roads",
          "title": "Request fallen tree removal on internal roads",
          "category_id": 6,
          "unit": "Horticulture Services Section"
        },
        {
          "id": 89,
          "originalTitle": "Report falling construction items from a construction site",
          "title": "Report falling construction items from a construction site",
          "category_id": 7,
          "unit": "Building Supervision Section"
        },
        {
          "id": 90,
          "originalTitle": "Report inappropriate construction fence",
          "title": "Report inappropriate construction fence",
          "category_id": 7,
          "unit": "Building Supervision Section"
        },
        {
          "id": 6,
          "originalTitle": "Report Fire equipment defect",
          "title": "Report Fire equipment defect",
          "category_id": 11,
          "unit": "Building & Facilities Maintenance"
        },
        {
          "id": 57,
          "originalTitle": "Report Illegal Fishing Activity",
          "title": "Report Illegal Fishing Activity",
          "category_id": 6,
          "unit": "Resources Protection Sec."
        },
        {
          "id": 11,
          "originalTitle": "Send Comments about DM souqs",
          "title": "Send Comments about DM souqs",
          "category_id": 11,
          "unit": "Markets Section"
        },
        {
          "id": 81,
          "originalTitle": "Request General Waste Removal",
          "title": "Request General Waste Removal",
          "category_id": 4,
          "unit": "Operations & cleaning services"
        },
        {
          "id": 6,
          "originalTitle": "Request removal of sewage water for Hatta area",
          "title": "Request removal of sewage water for Hatta area",
          "category_id": 3,
          "unit": "Hatta Center"
        },
        {
          "id": 34,
          "originalTitle": "Report violations in health establishments ",
          "title": "Report violations in health establishments ",
          "category_id": 5,
          "unit": "Health Control Section"
        },
        {
          "id": 35,
          "originalTitle": "Report lack of hygiene in buildings",
          "title": "Report lack of hygiene in buildings",
          "category_id": 5,
          "unit": "Health Control Section"
        },
        {
          "id": 7,
          "originalTitle": "Report plumbing defects in DM Facilities",
          "title": "Report plumbing defects in DM Facilities",
          "category_id": 11,
          "unit": "Building & Facilities Maintenance"
        },
        {
          "id": 7,
          "originalTitle": "Requesting Heavy Machinery in Hatta area",
          "title": "Requesting Heavy Machinery in Hatta area",
          "category_id": 7,
          "unit": "Hatta Center"
        },
        {
          "id": 36,
          "originalTitle": "Report bad air quality or ventilation issues",
          "title": "Report bad air quality or ventilation issues",
          "category_id": 5,
          "unit": "Public Safety Section"
        },
        {
          "id": 79,
          "originalTitle": "Request removal of internal accident waste",
          "title": "Request removal of internal accident waste",
          "category_id": 4,
          "unit": "Operations & cleaning services"
        },
        {
          "id": 37,
          "originalTitle": "Report illegal labor accommodation in industrial entities",
          "title": "Report illegal labor accommodation in industrial entities",
          "category_id": 5,
          "unit": "Occupational Safety & Health"
        },
        {
          "id": 58,
          "originalTitle": "Report Land Pollution",
          "title": "Report Land Pollution",
          "category_id": 6,
          "unit": "Environmental control Sec"
        },
        {
          "id": 8,
          "originalTitle": "Report Elevator defects in DM Facilities",
          "title": "Report Elevator defects in DM Facilities",
          "category_id": 11,
          "unit": "Building & Facilities Maintenance"
        },
        {
          "id": 16,
          "originalTitle": "Request for lost pets",
          "title": "Request for lost pets",
          "category_id": 2,
          "unit": "Veterinary Services Section"
        },
        {
          "id": 67,
          "originalTitle": "Report Low irrigation water pressure",
          "title": "Report Low irrigation water pressure",
          "category_id": 3,
          "unit": "Irrigation Operation/Maintain"
        },
        {
          "id": 59,
          "originalTitle": "Request maintenance of DM playgrounds",
          "title": "Request maintenance of DM playgrounds",
          "category_id": 11,
          "unit": "Parks and Recreation Section"
        },
        {
          "id": 60,
          "originalTitle": "Request maintenance of DM play areas",
          "title": "Request maintenance of DM play areas",
          "category_id": 11,
          "unit": "Parks and Recreation Section"
        },
        {
          "id": 12,
          "originalTitle": "Request maintenance of park facilites",
          "title": "Request maintenance of park facilites",
          "category_id": 12,
          "unit": "Building & Facilities Maintenance"
        },
        {
          "id": 61,
          "originalTitle": "Request maintenance of Roads Greenery",
          "title": "Request maintenance of Roads Greenery",
          "category_id": 6,
          "unit": "Horticulture Section"
        },
        {
          "id": 62,
          "originalTitle": "Report marine pollution",
          "title": "Report marine pollution",
          "category_id": 6,
          "unit": "Coastal Zone & Water Mngmnt"
        },
        {
          "id": 91,
          "originalTitle": "Report improper storing of construction material",
          "title": "Report improper storing of construction material",
          "category_id": 7,
          "unit": "Building Supervision Section"
        },
        {
          "id": 63,
          "originalTitle": "report violations in Nature reserves",
          "title": "report violations in Nature reserves",
          "category_id": 6,
          "unit": "Resources Protection Sec."
        },
        {
          "id": 68,
          "originalTitle": "Report no irrigation water in grean areas",
          "title": "Report no irrigation water in grean areas",
          "category_id": 3,
          "unit": "Irrigation Operation/Maintain"
        },
        {
          "id": 69,
          "originalTitle": "Report sewage obstruction",
          "title": "Report sewage obstruction",
          "category_id": 3,
          "unit": "Drainage Sys. Maintainance"
        },
        {
          "id": 17,
          "originalTitle": "Report spread of insect in public area",
          "title": "Report spread of insect in public area",
          "category_id": 1,
          "unit": "Pest Control Section"
        },
        {
          "id": 80,
          "originalTitle": "Report container pile waste",
          "title": "Report container pile waste",
          "category_id": 4,
          "unit": "Operations & cleaning services"
        },
        {
          "id": 38,
          "originalTitle": "Report food poisining",
          "title": "Report food poisining",
          "category_id": 8,
          "unit": "Food Control Section"
        },
        {
          "id": 39,
          "originalTitle": "Report on work injuries ",
          "title": "Report on work injuries ",
          "category_id": 5,
          "unit": "Occupational Safety & Health"
        },
        {
          "id": 8,
          "originalTitle": "Request removal of agricultural waste",
          "title": "Request removal of agricultural waste",
          "category_id": 4,
          "unit": "Operations & cleaning services"
        },
        {
          "id": 1098,
          "originalTitle": "Request removal of old or abandoned cars",
          "title": "Request removal of old or abandoned cars",
          "category_id": 4,
          "unit": "Specialized Cleaning Section"
        },
        {
          "id": 10,
          "originalTitle": "Request for rain water removal",
          "title": "Request for rain water removal",
          "category_id": 3,
          "unit": "Technical and Support Admin"
        },
        {
          "id": 40,
          "originalTitle": "Report unsafe detergents and disinfecting",
          "title": "Report unsafe detergents and disinfecting",
          "category_id": 5,
          "unit": "Consumer Products Safety"
        },
        {
          "id": 41,
          "originalTitle": "Report unsafe health supplements",
          "title": "Report unsafe health supplements",
          "category_id": 5,
          "unit": "Consumer Products Safety"
        },
        {
          "id": 42,
          "originalTitle": "Report safety violations of swimmimg pools",
          "title": "Report safety violations of swimmimg pools",
          "category_id": 5,
          "unit": "Public Safety Section"
        },
        {
          "id": 70,
          "originalTitle": "Report of sand accumulation caused by sewage projects",
          "title": "Report of sand accumulation caused by sewage projects",
          "category_id": 3,
          "unit": "Drainage Projects Executive"
        },
        {
          "id": 9,
          "originalTitle": "Request security of DM facilities",
          "title": "Request security of DM facilities",
          "category_id": 11,
          "unit": "Security & Admin Services"
        },
        {
          "id": 11,
          "originalTitle": "Request test and checkups for farm animals",
          "title": "Request test and checkups for farm animals",
          "category_id": 2,
          "unit": "Veterinary Services Section"
        },
        {
          "id": 92,
          "originalTitle": "Report illegal multiple occupancy in one house",
          "title": "Report illegal multiple occupancy in one house",
          "category_id": 7,
          "unit": "Building Inspection Section"
        },
        {
          "id": 71,
          "originalTitle": "report Sewerage blockage in Hatta",
          "title": "report Sewerage blockage in Hatta",
          "category_id": 3,
          "unit": "Hatta Center"
        },
        {
          "id": 93,
          "originalTitle": "Report singles residing illegally in Villas",
          "title": "Report singles residing illegally in Villas",
          "category_id": 7,
          "unit": "Building Inspection Section"
        },
        {
          "id": 43,
          "originalTitle": "Report smoking violations in café shops",
          "title": "Report smoking violations in café shops",
          "category_id": 5,
          "unit": "Public Safety Section"
        },
        {
          "id": 44,
          "originalTitle": "Report smoking violations in public area",
          "title": "Report smoking violations in public area",
          "category_id": 5,
          "unit": "Health Control Section"
        },
        {
          "id": 12,
          "originalTitle": "Request home Pest Control Services",
          "title": "Request home Pest Control Services",
          "category_id": 1,
          "unit": "Pest Control Section"
        },
        {
          "id": 45,
          "originalTitle": "Report inappropraite storage of food",
          "title": "Report inappropraite storage of food",
          "category_id": 8,
          "unit": "Food Control Section"
        },
        {
          "id": 18,
          "originalTitle": "Stray animals in Hatta area ",
          "title": "Stray animals in Hatta area ",
          "category_id": 2,
          "unit": "Hatta Center"
        },
        {
          "id": 13,
          "originalTitle": "Report on stray cats",
          "title": "Report on stray cats",
          "category_id": 2,
          "unit": "Veterinary Services Section"
        },
        {
          "id": 19,
          "originalTitle": "Report stray dogs",
          "title": "Report stray dogs",
          "category_id": 2,
          "unit": "Veterinary Services Section"
        },
        {
          "id": 20,
          "originalTitle": "Report stray animals",
          "title": "Report stray animals",
          "category_id": 2,
          "unit": "Veterinary Services Section"
        },
        {
          "id": 72,
          "originalTitle": "Report damaged mainhole cover",
          "title": "Report damaged mainhole cover",
          "category_id": 3,
          "unit": "Drainage Sys. Maintainance"
        },
        {
          "id": 46,
          "originalTitle": "Report safety of cosmetic products",
          "title": "Report safety of cosmetic products",
          "category_id": 5,
          "unit": "Consumer Products Safety"
        },
        {
          "id": 15,
          "originalTitle": "Request treatment of sick animal in farms",
          "title": "Request treatment of sick animal in farms",
          "category_id": 2,
          "unit": "Veterinary Services Section"
        },
        {
          "id": 19,
          "originalTitle": "Trimming of house trees",
          "title": "Trimming of house trees",
          "category_id": 6,
          "unit": "Horticulture Services Section"
        },
        {
          "id": 74,
          "originalTitle": "unfix interlock after DM sewerage project",
          "title": "unfix interlock after DM sewerage project",
          "category_id": 3,
          "unit": "Drainage Projects Executive"
        },
        {
          "id": 21,
          "originalTitle": "Report violations by veterinary establishments",
          "title": "Report violations by veterinary establishments",
          "category_id": 2,
          "unit": "Veterinary Services Section"
        },
        {
          "id": 23,
          "originalTitle": "Report violation of animal welfare",
          "title": "Report violation of animal welfare",
          "category_id": 2,
          "unit": "Veterinary Services Section"
        },
        {
          "id": 22,
          "originalTitle": "Report violations of pet owners ",
          "title": "Report violations of pet owners ",
          "category_id": 2,
          "unit": "Veterinary Services Section"
        },
        {
          "id": 16,
          "originalTitle": "Request waste containers service",
          "title": "Request waste containers service",
          "category_id": 4,
          "unit": "Operations & cleaning services"
        },
        {
          "id": 75,
          "originalTitle": "Report water leakage on main road",
          "title": "Report water leakage on main road",
          "category_id": 3,
          "unit": "Irrigation Operation/Maintain"
        }
      ]
                    return listEnglishAZ;
                }
              ]);