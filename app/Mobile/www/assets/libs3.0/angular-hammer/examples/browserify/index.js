
/* JavaScript content from assets/libs3.0/angular-hammer/examples/browserify/index.js in folder common */
var angular = require('angular');

require('angular-hammer');

angular.module('hmTime', ['hmTouchEvents'])
  .controller('hmCtrl', ['$scope', function ($scope) {
    $scope.eventType = "No events yet";
    $scope.onHammer = function onHammer (event) {
      $scope.eventType = event.type;
    };
  }]);