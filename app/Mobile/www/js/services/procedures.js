'use strict';
iDubaiApp.value('$procedures', {
    getMakani: {adapter: "MakaniDM24Adapter"},
    checkMakani: {adapter: "MakaniDM24Adapter"},
    getBuildingInfo: {adapter: "MakaniDM24Adapter"},
    checkMakaniReturnBuildingAddress: {adapter: "MakaniDM24Adapter"},
    getNearestMakani: {adapter: "MakaniDM24Adapter"},
    getMakaniNumberDetails: {adapter: "MakaniDM24Adapter"},
    umUpdateProfileMyId: {adapter: "MyIDDubai24Adapter"},
    getCategories: {adapter: "ServicesDM24Adapter"},
    getRequests: {adapter: "ServicesDM24Adapter"},
	getIncidents: {adapter: "ServicesDM24Adapter"},
    getAreas: {adapter: "ServicesDM24Adapter"},
    insectTypes: {adapter: "ServicesDM24Adapter"},
    petTypes: {adapter: "ServicesDM24Adapter"},
    getCommonServices: {adapter: "CommonServicesDM24Adapter"},
    getHappinessMeterParams: {adapter: "HappinessDM24Adapter"},
    getMyLatestNotifications: {adapter: "DMCRM24Adapter"},
    enquireByServiceRequestNumber: {adapter: "EnquireCRM24Adapter"},
    vehicleEnquireService: {adapter: "VehicleEnquireCRM24Adapter"},
    searchRequest: {adapter: "SearchEngineAdapter"},
    requestOTP: {adapter: "OneTimePasswordAdapter"}

}).factory('execProcedure', ['$procedures', '$q', 'LoadingService', '$filter', '$rootScope', '$logger', 'errorHandler', 'CommonUtils', 'ConfigAdapters',
    function ($procedures, $q, LoadingService, $filter, $rootScope, $logger, errorHandler, CommonUtils, ConfigAdapters) {

        var send = function (params, showLoadingFlag) {
            var defer = $q.defer();

            var varThis = this;
            showLoadingFlag = showLoadingFlag == undefined ? true : showLoadingFlag;
            var adapterName = "";

            if (varThis.adapter != null) {
                adapterName = varThis.adapter;
            }

            try {
                var inputData = {
                    adapter: adapterName,
                    procedure: varThis.name,
                    parameters: params
                };
                if (showLoadingFlag == true) {
                    LoadingService.show();
                }

                var startTime = new Date().getTime();
                CommonUtils.invokeGetResources({
                    path: ConfigAdapters+'.'+inputData.adapter+'.'+inputData.procedure,
                    params: params
                }).then(function (result) {
                    console.log('result ', result);
                    LoadingService.hide();
                    defer.resolve(result);
                }, function (error) {
                    console.log('error ', error);
                    LoadingService.hide();
                    defer.resolve(error);
                });


            }catch (e) {
                console.log('error ', e);
            }

            /*
            var varThis = this;
            showLoadingFlag = showLoadingFlag == undefined ? true : showLoadingFlag;
            var adapterName = "";

            if (varThis.adapter != null) {
                adapterName = varThis.adapter;
            }

            var inputData = {
                adapter: adapterName,
                procedure: varThis.name,
                parameters: params
            };

            try {
                if (showLoadingFlag == true) {
                    LoadingService.show();
                }
                var startTime = new Date().getTime();
                WL.Client.invokeProcedure(inputData, {
                    onSuccess: function (data) {
                        console.log(inputData.procedure + 'Took ' + ( new Date().getTime() - startTime));
                        if (showLoadingFlag == true) {
                            LoadingService.hide();
                        }
                        $logger.log('## SUCCESS INVOKE', inputData, data);
                        defer.resolve(data);

                    },
                    onFailure: function (error) {
                        console.log(inputData.procedure + 'Took ' + ( new Date().getTime() - startTime));
                        try {
                            $logger.log('## FAIL INVOKE', inputData, error);
                        } catch (ex) {
                            $logger.log(ex);
                        }


                        if (showLoadingFlag == true) {
                            LoadingService.hide();
                        }

                        defer.reject(error);
                    },
                    timeout: 90000
                });
            } catch (e) {
                LoadingService.hide();
                $logger.log(e);
            }
            */
            return defer.promise;

        };

        var service = {
            init: function () {
                for (var i in $procedures) {
                    $procedures[i].send = send;
                    $procedures[i].name = i;
                }
            }
        };

        return service;
    }]);


iDubaiApp.factory("$WL", [function () {
    var client = null;
    var environment = null;
    try {
        client = WL.Client;
        environment = WL.Environment;
    } catch (ex) {

    }

    var wl = {
        Client: client,
        Environment: environment
    };

    return wl;
}]);
