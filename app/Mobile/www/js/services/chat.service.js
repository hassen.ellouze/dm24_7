
/* JavaScript content from js/services/chat.service.js in folder common */
iDubaiApp.factory("chatService",['$q', 'utils','$log','$rootScope', '$http',
                                          function ($q, utils,$log,$rootScope , $http) {
    return {
        startChat: startChat,
        setTypingState : setTypingState,
		serverConfiguration : serverConfiguration,
		poll : poll,
		sendMessage : sendMessage,
		exit : exit,
    };
    function serverUrl(){
    	return "https://chat.dm.gov.ae/I3root/Server1/websvcs/";
    	//return $rootScope.serverId == 1 ? "https://chat.dm.gov.ae/I3root/Server1/websvcs/" : "https://chat.dm.gov.ae/I3Root/Server2/websvcs/";
    }
    function startChat(username){
    	
    	var deferred = $q.defer();
    	var currentLang = localStorage.getItem('dm-lang') == "en" ? "en-us" : "ar";
    	var dataJson = '{"supportedContentTypes": "text/plain", "participant": {"name": "'+username+'", "credentials": ""}, "target": "inqchat", "targetType": "Workgroup", "language": "'+currentLang+'", "clientToken": "deprecated"}';
    	
    	serverConfiguration().then(function(res){
    		
    		console.log("SERVER CONF ::");
    		console.log(res);
    		
        	$http({
        			method : 'POST',
        			data : dataJson,
    				url : serverUrl()+"chat/start",
    				headers : {
    					'Content-Type' : "application/json; charset=utf-8"
    				}
        		}).then(function successCallback(response) {
    		    deferred.resolve(response);
    		  }, function errorCallback(response) {
    			  deferred.reject(response);
    		  });
        	
    	});
    	
    	return deferred.promise;
    }
	function serverConfiguration(){
		var deferred = $q.defer();
    	
    		$http({
    			method : 'GET',
				url : serverUrl()+"serverConfiguration"
    		}).then(function successCallback(response) {
		    deferred.resolve(response);
		  }, function errorCallback(response) {
			  $rootScope.serverId = 2;
				  $http({
		    			method : 'GET',
						url : serverUrl()+"serverConfiguration"
			    		}).then(function successCallback(response) {
					    deferred.resolve(response);
					  }, function errorCallback(response) {
						  deferred.reject(response);
					  });
		  });
    	
    	return deferred.promise;
	}
	function poll(id){
		var deferred = $q.defer();
    	$http({
    			method : 'GET',
				url : serverUrl()+"chat/poll/"+id,
    		}).then(function successCallback(response) {
		    deferred.resolve(response);
		  }, function errorCallback(response) {
			  deferred.reject(response);
		  });
    	return deferred.promise;
	}
	function setTypingState(id){
		var deferred = $q.defer();
    	var dataJson = '{ "typingIndicator": true}';
    	$http({
    			method : 'POST',
    			data : dataJson,
				url : serverUrl()+"chat/setTypingState/"+id,
				headers : {
					'Content-Type' : "application/json; charset=utf-8"
				}
    		}).then(function successCallback(response) {
		    deferred.resolve(response);
		  }, function errorCallback(response) {
			  deferred.reject(response);
		  });
    	return deferred.promise;
	}
	function sendMessage(id , message){
		var deferred = $q.defer();
    	var dataJson = '{"message" : "'+message+'", "contentType" : "text/plain"}';
    	$http({
    			method : 'POST',
    			data : dataJson,
				url : serverUrl()+"chat/sendMessage/"+id,
				headers : {
					'Content-Type' : "application/json; charset=utf-8"
				}
    		}).then(function successCallback(response) {
		    deferred.resolve(response);
		  }, function errorCallback(response) {
			  deferred.reject(response);
		  });
    	return deferred.promise;
	}
	function exit(id){
		var deferred = $q.defer();
    	$http({
    			method : 'POST',
				url : serverUrl()+"chat/exit/"+id
    		}).then(function successCallback(response) {
		    deferred.resolve(response);
		  }, function errorCallback(response) {
			  deferred.reject(response);
		  });
    	return deferred.promise;
	}
}]);