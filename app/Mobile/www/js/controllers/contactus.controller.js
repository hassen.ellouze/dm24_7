iDubaiApp.controller('contactUsController',
    ['$scope', '$state', '$filter', '$rootScope', '$DMLinks',
        function ($scope, $state, $filter, $rootScope, $DMLinks) {

            $scope.currentLang = localStorage.getItem('dm-lang');

            /**
             * Initialize map
             */
            $scope.initMap = function () {
                setTimeout(function () {
                    if (typeof(google) != "undefined") {
                        $scope.map = new google.maps.Map(document.getElementById('dm_centers'), {
                            center: {lat: 25.264326, lng: 55.311454},
                            zoom: 10
                        });
                        // DM position
                        $scope.infowindow = new google.maps.InfoWindow({
                            content: "DM Center!"
                        });
                        $scope.marker1 = new google.maps.Marker({
                            position: {lat: 25.264326, lng: 55.311454},
                            map: $scope.map
                        });
                        google.maps.event.addListener($scope.marker1, 'click', function () {
                            var infowindow = new google.maps.InfoWindow({
                                content: "Dubai Municipality Headquarter"
                            });
                            infowindow.open($scope.map, $scope.marker1);
                        });
                    }
                }, 1000);
            };
            $scope.initMap();

            /**
             * Open web site
             * @param site { Makani, DM }
             */
            $scope.openWebsite = function (site) {
                if (site === 1)
                    window.open($DMLinks.dmWebSite, "_blank", 'location=yes');
                else if (site === 2)
                    window.open($DMLinks.makani, "_blank", 'location=yes');
            };

            /**
             * Open google maps applications
             */
            $scope.onOpenMap = function () {
                if (WL.Client.getEnvironment() === WL.Environment.ANDROID) {
                    window.open('geo:?q=Municipalité+de+Dubaï,+Al+Rigga+-+Dubai+-+Émirats+arabes+unis', '_system');
                } else {
                    window.open('maps://?q=Municipalité+de+Dubaï,+Al+Rigga+-+Dubai+-+Émirats+arabes+unis', '_system');
                }
            }

        }]);