
/* JavaScript content from js/services/utils.js in folder common */
iDubaiApp.factory("utils",['$logger',function($logger) {
		return {
			isEmptyString: isEmptyString,
			writeTextFile: writeTextFile,
			readTextFile: readTextFile,
			deleteTextFile: deleteTextFile,
			readBase64Image: readBase64Image 
		};
		  
		/***************************   Image files   **************************/ 
		/* read image */
		function readBase64Image(fileName, onSuccess, onFailure) {
			 
			try{
				
				window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem; 
		        window.resolveLocalFileSystemURL(fileName, function(fileEntry) {
		        	           
		            // Get file system to copy ormoveimagefile to
		            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem){ 
		    			 
		            }, function(e) { 
			    		//$logger.log('fsFail error ' + e);
			    		onFailure(e);
			    	});
		            
		            // Get file Entry 
		        	 
		    		fileEntry.file(function(file) { 
		    			
		        		//$logger.log('gotFile ', file); 
		    	        var reader = new FileReader();
		    	        reader.onloadend = function (evt) {	    	        	
		    	        	// Base64 result :: evt.target.result 
		    	        	onSuccess(evt.target.result);
		    	        };
		    	        reader.readAsDataURL(file); 
		    	        //reader.readAsArrayBuffer(file);
		    	        
		        	},function(e) { 
			    		//$logger.log('fsFail error ' + e);
			    		onFailure(e);
			    	}); 
		    		
		        }, function(e) { 
		    		//$logger.log('fsFail error ' + e);
		    		onFailure(e);
		    	}); 
			}catch(ex){
				$logger.log("Can't read Base64Image ",ex);
				onFailure(ex);
			}
			
		} 
		 
		/***************************  End Image files   **************************/
		
		/***************************   Text files   **************************/ 
		function isEmptyString(str){
			return str == null || str.length < 1;
		}
		
		/* Read text file */
		function readTextFile(fileName, onSuccess, onFailure) {
			 
			window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
			window.requestFileSystem(window.PERSISTENT, 5 * 1024 * 1024, function(fs) {
				fs.root.getFile(fileName, {
					create : false
				}, function(fileEntry) {
					fileEntry.file(function(file) {
				       var reader = new FileReader();
				       reader.onloadend = function(e) {
				    	   onSuccess(this.result);
				       };
				       reader.readAsText(file);
				    }, function(e) {
						//$logger.log("Error createWriter", e);
						onFailure("Error createWriter", e);
					});
				}, function(e) {
					//$logger.log("Error get file", e);
					onFailure("Error get file", e);
				});
			}, function(e) {
				//var msg = getFileError(e); 
				//$logger.log('Error: ' + msg);
				onFailure("Error requestFileSystem", e);
			});
		}
		 
		/* Write text file */
		function writeTextFile(fileName, text, onSuccess, onFailure, create) {
			var options = { create: false };
			if(create){
				options = {
						create : true,
						exclusive : true
					};
			}
			
			window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
			window.requestFileSystem(window.PERSISTENT, 5 * 1024 * 1024, function(fs) {
				fs.root.getFile(fileName, options, function(fileEntry) {
					fileEntry.createWriter(function(fileWriter) {
						fileWriter.onwriteend = function(e) {
							//$logger.log('Write completed.');
							onSuccess('Write completed.');
						};
	
						fileWriter.onerror = function(e) {
							$logger.log("Error write file", e);
							onFailure("Error write file", e);
						};
	
						// Create a new Blob and write it to log.txt.
						var blob = new Blob([ text ], {
							type : 'text/plain'
						});
	
						fileWriter.write(blob);
					}, function(e) {
						//$logger.log("Error createWriter", e);
						onFailure("Error createWriter", e);
					});
				}, function(e) {
					//$logger.log("Error get file", e);
					onFailure("Error get file", e);
				});
			}, function(e) {
				//var msg = getFileError(e);
				//$logger.log('Error: ' + msg);
				onFailure("Error requestFileSystem", e);
			});
		}
		
		function deleteTextFile(fileName, onSuccess, onFailure){
			
			window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
			window.requestFileSystem(window.PERSISTENT, 5 * 1024 * 1024, function(fs) {
				  fs.root.getFile(fileName, {create: false}, function(fileEntry) {
				    fileEntry.remove(function() {
				       //$logger.log('File removed.');
				       onSuccess('File removed.');
				    }, function(e) {
						//$logger.log("Error deleteFile", e);
						onFailure("Error deleteFile", e);
					}); 
				  }, function(e) {
					    //$logger.log("Error get file", e);
						onFailure("Error get file", e);
					});
				}, function(e) {
					//var msg = getFileError(e); 
					//$logger.log('Error: ' + msg);
					onFailure("Error requestFileSystem", e);
				}); 
		} 
		
		function getFileError(e){
			var msg = '';
			switch (e.code) {
			case FileError.QUOTA_EXCEEDED_ERR:
				msg = 'QUOTA_EXCEEDED_ERR';
				break;
			case FileError.NOT_FOUND_ERR:
				msg = 'NOT_FOUND_ERR';
				break;
			case FileError.SECURITY_ERR:
				msg = 'SECURITY_ERR';
				break;
			case FileError.INVALID_MODIFICATION_ERR:
				msg = 'INVALID_MODIFICATION_ERR';
				break;
			case FileError.INVALID_STATE_ERR:
				msg = 'INVALID_STATE_ERR';
				break;
			default:
				msg = 'Unknown Error';
				break;
			};
			return msg;
		} 
		/***************************  End Text files   **************************/
}]);

iDubaiApp.factory("$FileIO",['$q', 'utils','$logger', function ($q, utils, $logger) {
    
	return {
        readFile: readFile,
        writeFile: writeFile,
        deleteFile: deleteFile,
        readBase64Image: readBase64Image 
    }; 
     
    function formatBase64Data(str){
    	//return str.replace(/^data:image\/(png|jpeg|jpg);base64,/, "");
    	return str.replace(/^data:.+\/(.+);base64,/, "");
    }; 
    
    function readBase64Image(_filePath){   
    	var deferred = $q.defer();    
		utils.readBase64Image(_filePath, function (base64Data) { 
			var result = formatBase64Data(base64Data);
			deferred.resolve(result); 
         }, function (error) {  
           deferred.reject(error); 
       });   
    	return deferred.promise; 
    } 
    
    
    function readFile(_filePath){   
    	
    	var deferred = $q.defer();    
		 utils.readTextFile(_filePath, function (data) { 
			 var wcmData = null;
			 try{
				 wcmData = JSON.parse(data);  
				 deferred.resolve(wcmData); 
			 }catch(ex){
				// $logger.error("ex : ",ex); 
				 deleteFile(_filePath).then(function(deleteSuccess){
            		$logger.log(deleteSuccess); 
            	 }, function(deleteError){
            		$logger.log(deleteError);
            	 });				 
				 deferred.reject(ex); 
			 }
			 
       }, function (error) {  
           deferred.reject(error); 
       });   
    	return deferred.promise; 
    } 
    
    function writeFile(_filePath, _wcmData, _create){   
    	
    	var deferred = $q.defer(); 
    	var t = new Date().getTime();
		var temp = {
           wcm: _wcmData,
           cacheTime: t
        }; 
		var wcmData = JSON.stringify(temp);  
		utils.writeTextFile(_filePath, wcmData, function (success) {
            //$logger.log("File saved");
            deferred.resolve(success);
        }, function (error) {
            //$logger.log("Failed to save file");
            deferred.reject(error);
        }, _create);  
    	return deferred.promise; 
    }
    
    function deleteFile(_filePath){   
    	
    	var deferred = $q.defer();    
		 utils.deleteTextFile(_filePath, function (success) {   
			 deferred.resolve(success); 
       }, function (error) {  
           deferred.reject(error); 
       });   
    	return deferred.promise; 
    }
    
}]); 