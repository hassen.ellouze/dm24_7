
/* JavaScript content from assets/libs/cordova/CameraUploader.js in folder common */

/* JavaScript content from js/libs/cordova/CameraUploader.js in folder common */
﻿ 
function CameraUploader() {
}

CameraUploader.prototype.decodeBase64 = function (base64Data, successCallback, errorCallback) {
    cordova.exec(successCallback, this._getErrorCallback(errorCallback, "decodeBase64"), "CameraUploader", "decodeBase64", [base64Data]);
};

CameraUploader.prototype.uploadDocument = function (gateway, bizServer, accessKey, byteCharacters, recordId, userToken, fileName, successCallback, errorCallback) {
    cordova.exec(successCallback, this._getErrorCallback(errorCallback, "uploadDocument"), "CameraUploader", "uploadDocument", [gateway, bizServer, accessKey, byteCharacters, recordId, userToken, fileName]);
};
    
CameraUploader.prototype._getErrorCallback = function (ecb, functionName) {
  if (typeof ecb === 'function') {
    return ecb;
  } else {
    return function (result) {
      console.log("The injected error callback of '" + functionName + "' received: " + JSON.stringify(result));
    }
  }
};

CameraUploader.install = function () {
  if (!window.plugins) {
    window.plugins = {};
  }

  window.plugins.cameraUploader = new CameraUploader();
  return window.plugins.cameraUploader;
};

cordova.addConstructor(CameraUploader.install);