
/* JavaScript content from js/constants/links.constant.js in folder common */
// Links constants
iDubaiApp.factory('$DMLinks', [
        function () {
            return {
                dmWebSite: "http://www.dm.gov.ae",
                makani: "http://www.makani.ae/",
                forgotPassword: "https://myid.dubai.gov.ae/ForgotPassword.aspx",
                registration: "https://myid.dubai.gov.ae/Registration.aspx",
                dmPlayStoreMarket: "market://details?id=com.iDubai24",
                dmItunesMarket: "https://itunes.apple.com/us/app/dm-24-7/id1246179225", //"itms-apps://itunes.apple.com/app/iDubai24",
                dmPlayStore: "https://play.google.com/store/apps/details?id=com.iDubai24",
                dmItunes: "https://itunes.apple.com/us/app/dm-24-7/id1246179225",
                dmPlayStoreRelatedApp: "https://play.google.com/store/apps/developer?id=Dubai+Municipality",
                dmItunesRelatedApp: "https://itunes.apple.com/us/developer/dubai-municipality/id427718653",
                termsEn: "http://www.dm.gov.ae/wps/portal/!ut/p/a1/nZFLj4IwFIX_ysyCpdzagjBLdOHoZGJGNEo3ppQCnVCKtHEev35A1_iYpmnS5Nxz850DFPZAa3aSBbNS16zq_3RyGC9CbzxdoeXqg_gowsHMn72uyTz2O0EyLEBzfN88GjgRujW_AwqU17axJSSZ4rq2or9FJU3poFIr4aAvkRppxVOjK8mlMA6yolWmE2eyBzW9ScNlBgnmKPWywB95Oe4eFI5HKSZ8hMmLz0Xuc8LwZekN7LPgCtcSaFHp9JxxEtUpCQugrchFK1q31MbC3gjW8tLNlFvok8sE7KZA346bdBtB0uUWDAaDCcQPMl03XHgPGy7vqF5-Ho806grsa_vukP_fYMxaaNR2q0IyWYc_pP7d5Eod3m0YPf8BICzalQ!!/dl5/d5/L0lHSkovd0RNQUhrQUVnQSEhLzRKU0UvZW4!/",
                termsAr: "http://www.dm.gov.ae/wps/portal/!ut/p/a1/nZFLb8IwEIT_SnvgSNbYCaTHwIFCVaHyEMQXlDibxFVsh9iij1_fBM68almWLM3O6psBDjvgOjnKInHS6KTq_ny4H8xCfzBekPnigwUkoqNJMHldsukqaAXxZQGZ0vvmyYUTkVvzW-DAhXa1KyHOlDDaYXeLStqyR0qjsEe-MLXS4VNtKikk2h5x2CjbijPZgdrOpBYyg5gKkvrZKOj7OW0fEg76KWWiT9lLIDAPBEvoeekN7JPgCtcceFGZ9JRxHOmUhQXwBnNssPFKYx3sLCaNKL1MeYU5egnCdgz87bBONxHEbW6ji8FQBqsHma4bzvyHDed3VC8_DwcetQV2tX23yP9vcIUaarXZqJANl-EP07_rXKn9uwuj5z9_p3Qh/dl5/d5/L0lHSkovd0RNQUhrQUVnQSEhLzRKU0UvYXI!/",
                privacyEn: "http://www.dm.gov.ae/wps/portal/!ut/p/a1/nZFLb4MwEIT_SnPIMayxIaFHmkOaSFXUpFGDLxGYBVxhm2Arffz6Qnomj1qWJUuzs_pmgMMeuE5PskydNDqt-z-fHvxlFPhPa7Jav7KQxHQ2D-fPG7bYhp0gGRaQBb1tngycmFybfwcOXGjXuAqSXAmjHfa3rKWtxqQyCsfkEzMrHT40ppZCoh0Th62ynTiXPajtTRohc0ioIFmQz8JJUNDuIZE_ySgTE8oeQ4FFKFhK_5ZewT4LLnCtgJe1yc4ZJ7HOWFQCb7HAFluvMtbB3mLaisrLlVeak5ciJF1Ys8E0KIPtnSCXDZfB3YarG_qWH8cjj7vW-q6-Os7_17ZFDY3a7VTEppvom_28FUodXlwUj0a_kOZivQ!!/?1dmy&current=true&urile=wcm%3apath%3a%2Fdmcontentenglish%2Fhome%2Fwebsite%2Bpolicies%2Fsecurityprivacypolicy",
                privacyAr: "http://www.dm.gov.ae/wps/portal/!ut/p/a1/pVHLboMwEPyV5pAj2NgkwJHmkCZSFTU0avAlMmYBVxgTcNPSry8kZ_JQV3tZ7eysZgYxtEes4ieZcyN1xcthZvODs_Jd53mD15s3OsMh8RazxcuWLqNZD4jHAXhJ7rvHIxXiW_cfiCEmKlObAsWpEroyMHReyraY4kIrmOJvSFpp4KnWpRQS2iluQXw10nR1I09cdOdFN1DVQqY9URAIEmSB5QtBLTdzwOIcexYIj2bYc0mQkMvrG-LPgCvq1ojlpU7OTsdhlVA_R6yBDBpo7EK3Bu1b4I0o7FTZuT7ZHFDcW-aNekIoih4Ucp1w5T5MuL4jdfl5PLKwz25I7KfX-d_wIqhQrXY75dP51u_o73um1OHV-OFk8gfcWAJA/dl5/d5/L0lHSkovd0RNQUhrQUVnQSEhLzRKU0UvYXI!",
            };
        }
    ]
);