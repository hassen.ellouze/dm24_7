iDubaiApp.controller('aboutController',
    ['$scope', '$state', 'WCMService', '$logger', '$timeout', '$filter', '$DMLinks',
        function ($scope, $state, WCMService, $logger, $timeout, $filter, $DMLinks) {
            $scope.news = [];
            $scope.newsLimit = 5;
            $scope.currentLang = localStorage.getItem('dm-lang');

            /**
             * Initialize news components
             */
            function initComponents() {
                $timeout(function () {
                    $scope.$apply();
                    initFlip();
                    initAccordion();
                    $timeout(function () {
                        initSameHeight();
                    }, 100);
                }, 50);
            }

            /**
             * Load dm news
             */
            function initData() {
                WCMService.getWCMData().then(function (data) {
                    if ($scope.currentLang === "ar") {
                        if (data.NewsAr !== null && data.NewsAr.items !== null) {
                            for (var i = 0; i < data.NewsAr.items.length; i++) {
                                var item = data.NewsAr.items[i];
                                if (item.title !== null && item.title.length > 1 && item.image !== null && item.image.length > 1
                                    && item.description !== null && item.description.length > 1) {
                                    $scope.news.push({
                                        title: item.title,
                                        largePic: navigator.onLine ? item.image : 'assets/img/images/no-img.jpg',
                                        fullDesc: item.description,
                                        order: i,
                                        isAboutView: true
                                    });
                                }
                            }
                        } else {
                            for (var i = 0; i < data.NewsEn.items.length; i++) {
                                var item = data.NewsEn.items[i];
                                if (item.title !== null && item.title.length > 1 && item.image !== null && item.image.length > 1
                                    && item.description !== null && item.description.length > 1) {
                                    $scope.news.push({
                                        title: item.title,
                                        largePic: navigator.onLine ? item.image : 'assets/img/images/no-img.jpg',
                                        fullDesc: item.description,
                                        order: i,
                                        isAboutView: true
                                    });
                                }
                            }
                        }
                    } else {
                        if (data.NewsEn !== null && data.NewsEn.items !== null) {
                            for (var i = 0; i < data.NewsEn.items.length; i++) {
                                var item = data.NewsEn.items[i];
                                if (item.title !== null && item.title.length > 1 && item.image !== null && item.image.length > 1
                                    && item.description !== null && item.description.length > 1) {
                                    $scope.news.push({
                                        title: item.title,
                                        largePic: navigator.onLine ? item.image : 'assets/img/images/no-img.jpg',
                                        fullDesc: item.description,
                                        order: i,
                                        isAboutView: true
                                    });
                                }
                            }
                        } else {
                            $scope.news = [];
                        }
                    }
                    initComponents();
                }, function (error) {
                    $scope.news = [];
                    initComponents();
                });
            }

            initData();

            /**
             * Load more news
             */
            $scope.loadMoreNews = function () {
                if ($scope.newsLimit < $scope.news.length) {
                    $scope.newsLimit++;
                    $timeout(function () {
                        $scope.$apply();
                    });
                }
            };

            /**
             * Open dm terms
             */
            $scope.goToTerms = function () {
                var currentLang = localStorage.getItem('dm-lang');
                var url = (currentLang === 'ar') ? $DMLinks.termsAr : $DMLinks.termsEn;
                window.open(url, '_blank', 'location=yes')
            };

            /**
             * Open dm privacy
             */
            $scope.goToPrivacy = function () {
                var currentLang = localStorage.getItem('dm-lang');
                var url = (currentLang === 'ar') ? $DMLinks.privacyAr : $DMLinks.privacyEn;
                window.open(url, '_blank', 'location=yes')
            };


            /**
             * Accordion
             */
            $('#accordion').children('li').first().children('a').addClass('active')
                .next().addClass('is-open').show();
            $('#accordion').on('click', 'li > a', function () {
                if (!$(this).hasClass('active')) {

                    $('#accordion .is-open').removeClass('is-open').hide();
                    $(this).next().toggleClass('is-open').toggle();

                    $('#accordion').find('.active').removeClass('active');
                    $(this).addClass('active');
                } else {
                    $('#accordion .is-open').removeClass('is-open').hide();
                    $(this).removeClass('active');
                }
            });
        }
    ]
);
