﻿iDubaiApp.controller('locationsController', ["$state", "$timeout", "$scope", '$dmServices', 'underscore', 'SharedService',
    function ($state, $timeout, $scope, $dmServices, underscore, SharedService) {

        $scope.currentLang = localStorage.getItem('dm-lang');
        var _ = underscore;
        var allServices = $dmServices;
        $scope.requestsByLocation = [];
        $scope.locations = {};
        $scope.sliders = [];
        var currentSelectedKey = null;
        $scope.isSelectedBtnMap = false;

        var map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(25.063890, 55.193373),
            zoom: 10,
            minZoom: 9,
            zoomControl: false,
            fullscreenControl: false,
            streetViewControl: false,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.BOTTOM_CENTER
            }
        });


        //var infowindow = new google.maps.InfoWindow();

        var marker, i;

        //get location from storage
        window.db.transaction(function (txDB) {
            var query = "SELECT list FROM locationList";
            txDB.executeSql(query, [], function (tx, resultSet) {
                if (!$scope.isEmpty(JSON.parse(resultSet.rows.item(0).list))) {
                    $scope.locations = JSON.parse(resultSet.rows.item(0).list);
                    angular.forEach($scope.locations, function (value, key) {
                        var staticMapsUri = "http://maps.google.com/maps/api/staticmap?center=" + value.lat + ',' + value.lng + "&markers=icon:/assets/img3.0/marker.png|" + value.lat + ',' + value.lng + "&zoom=13&size=700x700&sensor=false&key=AIzaSyDAyPe0GL66TrDBhkzQCF9-8pIWTLoxg7o";
                        value.imgMapLocation = encodeURI(staticMapsUri);
                    });

                    $scope.$apply();

                    //add marker in the map
                    angular.forEach($scope.locations, function (value, key) {


                        var markerIcon = './assets/img3.0/location-pin.png';
                        var markerIcon = {
                            url: './assets/img3.0/location-pin.svg',
                            scaledSize: new google.maps.Size(40, 40),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(32, 65),
                            labelOrigin: new google.maps.Point(40, 33)
                        };
                        var markerLabel = value.requestList.length;
                        marker = new MarkerWithLabel({
                            map: map,
                            animation: google.maps.Animation.DROP,
                            position: new google.maps.LatLng(value.lat, value.lng),
                            icon: markerIcon,
                            labelContent: markerLabel,
                            labelAnchor: new google.maps.Point(5, 70),
                            labelClass: "my-custom-class-for-label",
                            labelInBackground: true
                        });


                        google.maps.event.addListener(marker, 'click', (function (marker, key) {
                            return function () {
                                var isFirstClick = currentSelectedKey == null ? true : false;
                                var isAlreadySelected = false;
                                if (!isFirstClick) {
                                    isAlreadySelected = currentSelectedKey == key ? true : false;
                                }

                                if (isFirstClick || !isAlreadySelected) {
                                    if ($scope.sliders.length > 0) {
                                        for (i = 0; i <= $scope.sliders.length; i++) {
                                            $('.request-slider').slick('slickRemove', 0);
                                        }
                                    }
                                    $scope.slickConfigLoaded = false;
                                    $scope.sliders = $scope.locations[key].requestList;
                                    $scope.selectedLocation = $scope.locations[key];
                                    console.log("$scope.sliders", $scope.sliders);
                                    $scope.slickConfigLoaded = true;
                                    $scope.$apply();

                                    //infowindow.setContent( $scope.locations[key].locationDetail);
                                    //infowindow.open(map, marker);
                                    currentSelectedKey = key;
                                    if ($('.request-slider').hasClass("slick-initialized")) {
                                        console.log('unslicking');

                                        $('.request-slider').slick('unslick');
                                        $('.request-slider').slick($scope.slickConfig);
                                    }

                                    if (!$('#requestDetails').hasClass('isRequestDetails')) {
                                        $('#requestDetails').addClass('isRequestDetails');
                                    }

                                }
                                else {
                                    $('#requestDetails').toggleClass('isRequestDetails')
                                }

                            }
                        })(marker, key));


                    });
                    console.log("$scope.locations in", $scope.locations);
                }

            }, function (tx, error) {
                console.log('table location list not exist ' + error.message);
                $scope.$apply();
            });
        }, function () {
            console.log('transaction ok');
        });
        $scope.displayRequests = function () {
            if ($scope.slickConfigLoaded)
                $('#requestDetails').toggleClass('isRequestDetails');
        }
        var mapMode = angular.element(document.querySelector(".mapContainer"));
        console.log(mapMode.attr('class'));
        var navBar = angular.element(document.querySelector(".navbar"));
        console.log(navBar.attr('class'));
        $scope.hideNav = function () {

            if (mapMode.hasClass('mapToggle')) {
                navBar.removeClass('hideNav');
            } else {
                navBar.addClass('hideNav');
            }

        };

        $scope.goTo = function (request) {
            SharedService.setStateHistory('requestSummary', {
                requestDetails: request,
                from: 'myLocations'
            })
            $state.go('requestSummary', {
                requestDetails: request,
                from: 'myLocations'
            })
        }

        $scope.goToDashboard = function () {
            if ($(".listSelect").hasClass("showSelectList")) {
                $(".rowSelect").removeClass("rowToggle");
                $(".main-content").removeClass("mainPaddingTop");
                $(".main").removeClass("mainPaddingTop");
                $(".listSelect").removeClass("showSelectList");
                $(".widgetRequest").removeClass("translateList");
                $(".listItems").removeClass("translateList");
                $(".row").removeClass("hide");
            }
            else {
                if (navBar.hasClass('hideNav')) {
                    navBar.removeClass('hideNav');
                }
                $state.go('dashboard');
            }

        }

        $scope.deleteLocations = function () {
            console.log("checked locatin", $scope.formData.locationRadio);
        }

        $scope.deleteLocations = function () {
            var details = [];

            angular.forEach($scope.locations, function (value, key) {

                if ($scope.locations[key].checked) {
                    details.push(key);
                    delete $scope.locations[key];
                }
            });

            if (details.length > 0) {
                console.log('Selected Values: ' + details.toString());
                updateLocation($scope.locations);
                //$scope.$apply();
            }

            else {
                console.log('Please choose an option');
            }
        };

        function updateLocation(locationList) {
            window.db.transaction(function (txDB) {
                txDB.executeSql('UPDATE locationList SET list = ?', [JSON.stringify(locationList)], function (tx, resultSet) {
                    console.log("Update location list success:", JSON.stringify(tx), JSON.stringify(resultSet));
                }, function (tx, error) {
                    console.log('Update location list error: ' + error.message);
                });
            }, function () {
                console.log('transaction ok');
            });
        }

        $scope.changeAllSelection = function (isAllChecked) {
            if (isAllChecked) {
                //select All radio of location
                angular.forEach($scope.locations, function (value, key) {
                    $scope.locations[key].checked = true;
                });
            }
            else {
                //remove selection for all radio of location
                angular.forEach($scope.locations, function (value, key) {
                    $scope.locations[key].checked = false;
                });
            }
        }

        $scope.isEmpty = function (list) {
            for (var key in list) {
                if (list.hasOwnProperty(key))
                    return false;
            }
            return true;
        }

        $scope.slickConfig = {
            enabled: true,
            autoplay: false,
            infinite: false,
            draggable: true,
            autoplaySpeed: 3000,
            variableWidth: false,
            centerMode: true,
            arrows: false,
            rtl: $scope.currentLang == 'ar' ? true : false,
            dots: true,
            method: {},
            event: {
                beforeChange: function (event, slick, currentSlide, nextSlide) {
                },
                afterChange: function (event, slick, currentSlide, nextSlide) {
                }
            }
        };

        $scope.createRequest = function (location) {
            console.log('Location :: ', location);
            var payload = {
                makani: location.makaniNumber,
                location: {'lat': location.lat, 'lng': location.lng}
            }

            SharedService.setStateHistory('servicesAZ', {
                payload: payload,
                from: $state.current.name,
            });
            $state.go('servicesAZ', {
                payload: payload
            })
        }


    }]);
