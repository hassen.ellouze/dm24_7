/*
 *  Licensed Materials - Property of IBM
 *  5725-I43 (C) Copyright IBM Corp. 2011, 2016. All Rights Reserved.
 *  US Government Users Restricted Rights - Use, duplication or
 *  disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */

function _addAttachment(PhotoBase64) {

    WL.Logger.error(PhotoBase64);
    var _response=
        [
            {
                "Success": true,
                "ErrorMessage": null,
                "OriginalFileName": "plastic-landfills.jpg",
                "PhysicalFileName": "782137a1-ddb2-4dec-9209-8d236e09a343_plastic-landfills.jpg"
            }
        ];
    var response=
        [

        ];

    return {
        isSuccessful: true,
        response: response
    };
}

function addAttachment(AttachmentsList) {
    /*
    var ProdURL = "https://dubai247mobileapi.dm.gov.ae/STGDM360.dm.gov.ae/NotificationsSystem/api/Attachments/Add";
    var StageURL = "https://dubai247mobileapi.dm.gov.ae/STGDM360.dm.gov.ae/NotificationsSystem/api/Attachments/Add";
    */
    //AttachmentsList = [];

    var response = {
        isSuccessful: false,
        response: null
    };

    try {

        WL.Logger.error("AttachmentList :: " + JSON.stringify(AttachmentsList));
        var AttachmentData = {"imageList": [], "audioList":[]}

        for (var i = 0; i < AttachmentsList.imageList.length; i++) {
            AttachmentData.imageList.push({
                'fileName': AttachmentsList.imageList[i].fileName,
                'fileContent': AttachmentsList.imageList[i].fileContent.replace(/^data:image\/(png|jpg|jpeg);base64,/, "")
            })
        }
        for (var i = 0; i < AttachmentsList.audioList.length; i++) {
            AttachmentData.audioList.push({
                'fileName': AttachmentsList.audioList[i].fileName,
                'fileContent': AttachmentsList.audioList[i].fileContent.replace(/^data:audio\/(amr|wav);base64,/, ""),
                'fileExtension': AttachmentsList.audioList[i].fileExtension
            })
        }


        WL.Logger.error(" AttachmentData :: " + JSON.stringify(AttachmentData));
        var payload = com.dm.common.attachment.UploadAttachments.uploadAttachments(JSON.stringify(AttachmentData));

        WL.Logger.error("response from upload media :: " + JSON.stringify(payload));

        var pLoad = [];
        if(payload){
            pLoad = JSON.parse(payload);
        }

        if (pLoad.length > 0) {
            response.response = pLoad;
            response.isSuccessful = true;
        }

        WL.Logger.error("response upload Attachments: " + JSON.stringify(response));
    } catch (e) {
        return {
            isSuccessful: false,
            reference: e
        };
    }

    return response;
}