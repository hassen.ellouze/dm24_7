
/* JavaScript content from assets/libs/chat/resources/localization.en-us.js in folder common */
ININ.Web.Common.Resources.LocalizedStrings.set("LoginContainerHeaderText", "مرحبا بكم في المحادثة المباشرة مع بلدية دبي");
ININ.Web.Common.Resources.LocalizedStrings.set("MainPanelHeaderText", "مرحبا بكم في المحادثة المباشرة مع بلدية دبي");
ININ.Web.Common.Resources.LocalizedStrings.set("ClosePageWarning", "هل ترغب بإنهاء الدردشة الحالية؟ سيتم فقدان نسخة النص الخاصة بك");
ININ.Web.Common.Resources.LocalizedStrings.set("ExitChatWarning", "هل ترغب بإنهاء الدردشة الحالية؟ سيتم فقدان نسخة النص الخاصة بك");
ININ.Web.Common.Resources.LocalizedStrings.set("OutOfOrderMessage", "تم تسليم هذه الرسالة بشكل عشوائي:");
ININ.Web.Common.Resources.LocalizedStrings.set("StartChatButton", "ابدأالمحدثه");
ININ.Web.Common.Resources.LocalizedStrings.set("StartChatTab", "Chat");
ININ.Web.Common.Resources.LocalizedStrings.set("StartCallbackButton", "Start Callback");
ININ.Web.Common.Resources.LocalizedStrings.set("StartCallbackTab", "Callback");
ININ.Web.Common.Resources.LocalizedStrings.set("RegisterNewAccountTab", "Create an Account");
ININ.Web.Common.Resources.LocalizedStrings.set("Login", "Login");
ININ.Web.Common.Resources.LocalizedStrings.set("Register", "Register");
ININ.Web.Common.Resources.LocalizedStrings.set("IHaveAnAccount", "I have an account");
ININ.Web.Common.Resources.LocalizedStrings.set("IDontHaveAnAccount", "I don't have an account");
ININ.Web.Common.Resources.LocalizedStrings.set("CreateAnAccount", "Create an account");
ININ.Web.Common.Resources.LocalizedStrings.set("OptionalTag", "(Optional)");
ININ.Web.Common.Resources.LocalizedStrings.set("NameLabel", "Name:");
ININ.Web.Common.Resources.LocalizedStrings.set("DescriptionLabel", "Description:");
ININ.Web.Common.Resources.LocalizedStrings.set("TelephoneLabel", "Telephone:");
ININ.Web.Common.Resources.LocalizedStrings.set("PasswordLabel", "Password:");
ININ.Web.Common.Resources.LocalizedStrings.set("ConfirmPasswordLabel", "Confirm Password:");
ININ.Web.Common.Resources.LocalizedStrings.set("UserNameLabel", "Username:");
ININ.Web.Common.Resources.LocalizedStrings.set("lastNameLabel", "Last Name:");
ININ.Web.Common.Resources.LocalizedStrings.set("firstNameLabel", "First Name:");
ININ.Web.Common.Resources.LocalizedStrings.set("middleNameLabel", "Middle Name:");
ININ.Web.Common.Resources.LocalizedStrings.set("departmentLabel", "Department:");
ININ.Web.Common.Resources.LocalizedStrings.set("companyLabel", "Company:");
ININ.Web.Common.Resources.LocalizedStrings.set("jobTitleLabel", "Job Title:");
ININ.Web.Common.Resources.LocalizedStrings.set("assistantNameLabel", "Assistant Name:");
ININ.Web.Common.Resources.LocalizedStrings.set("webLoginLabel", "Web Login:");
ININ.Web.Common.Resources.LocalizedStrings.set("webPasswordLabel", "Web Password:");
ININ.Web.Common.Resources.LocalizedStrings.set("homeStreetAddressLabel", "Street Address:");
ININ.Web.Common.Resources.LocalizedStrings.set("homeCityLabel", "City:");
ININ.Web.Common.Resources.LocalizedStrings.set("homeStateLabel", "State:");
ININ.Web.Common.Resources.LocalizedStrings.set("homePostalCodeLabel", "Zip:");
ININ.Web.Common.Resources.LocalizedStrings.set("homeCountryLabel", "Country:");
ININ.Web.Common.Resources.LocalizedStrings.set("homeEmailLabel", "E-mail:");
ININ.Web.Common.Resources.LocalizedStrings.set("homePhoneLabel", "Phone:");
ININ.Web.Common.Resources.LocalizedStrings.set("homePhone2Label", "Additional Phone:");
ININ.Web.Common.Resources.LocalizedStrings.set("homeFaxLabel", "Fax:");

ININ.Web.Common.Resources.LocalizedStrings.set("homePagerLabel", "Pager:");
ININ.Web.Common.Resources.LocalizedStrings.set("homeMobileLabel", "Mobile:");
ININ.Web.Common.Resources.LocalizedStrings.set("homeUrlLabel", "URL:");

ININ.Web.Common.Resources.LocalizedStrings.set("businessStreetAddressLabel", "Street Address:");
ININ.Web.Common.Resources.LocalizedStrings.set("businessCityLabel", "City:");
ININ.Web.Common.Resources.LocalizedStrings.set("businessStateLabel", "State:");
ININ.Web.Common.Resources.LocalizedStrings.set("businessPostalCodeLabel", "Zip:");
ININ.Web.Common.Resources.LocalizedStrings.set("businessCountryLabel", "Country:");
ININ.Web.Common.Resources.LocalizedStrings.set("businessEmailLabel", "E-mail:");
ININ.Web.Common.Resources.LocalizedStrings.set("businessPhoneLabel", "Phone:");
ININ.Web.Common.Resources.LocalizedStrings.set("businessPhone2Label", "Additional Phone:");
ININ.Web.Common.Resources.LocalizedStrings.set("businessFaxLabel", "Fax:");
ININ.Web.Common.Resources.LocalizedStrings.set("businessPagerLabel", "Pager:");
ININ.Web.Common.Resources.LocalizedStrings.set("businessMobileLabel", "Mobile:");
ININ.Web.Common.Resources.LocalizedStrings.set("businessUrlLabel", "URL:");
ININ.Web.Common.Resources.LocalizedStrings.set("assistantPhoneLabel", "Assistant Phone:");
ININ.Web.Common.Resources.LocalizedStrings.set("remarksLabel", "Remarks:");
ININ.Web.Common.Resources.LocalizedStrings.set("LoginFailed", "Login failed");
ININ.Web.Common.Resources.LocalizedStrings.set("RegistrationSucceeded", "Registration succeeded");
ININ.Web.Common.Resources.LocalizedStrings.set("RegistrationFailed", "Registration failed");
ININ.Web.Common.Resources.LocalizedStrings.set("CallbackSucceeded", "An agent will call you back as soon as possible");
ININ.Web.Common.Resources.LocalizedStrings.set("CallbackFailed", "Callback failed");
ININ.Web.Common.Resources.LocalizedStrings.set("CreateCallback", "Submit");
ININ.Web.Common.Resources.LocalizedStrings.set("InProcess", "In Process");
ININ.Web.Common.Resources.LocalizedStrings.set("Waiting", "Waiting");

ININ.Web.Common.Resources.LocalizedStrings.set("CallbackCreatorNameLabel", "Requestor:");

ININ.Web.Common.Resources.LocalizedStrings.set("CallbackCreationDateTimeLabel", "Requested at:");
ININ.Web.Common.Resources.LocalizedStrings.set("AssignedAgentLabel", "Assigned Agent:");
ININ.Web.Common.Resources.LocalizedStrings.set("CallbackStateLabel", "State:");
ININ.Web.Common.Resources.LocalizedStrings.set("EstimatedCallbackTimeLabel", "Estimated callback time:");
ININ.Web.Common.Resources.LocalizedStrings.set("WaitTimeLabel", "Average wait time:");
ININ.Web.Common.Resources.LocalizedStrings.set("QueuePositionLabel", "Position in %0 queue:");
ININ.Web.Common.Resources.LocalizedStrings.set("LongestWaitTimeLabel", "Longest wait time:");
ININ.Web.Common.Resources.LocalizedStrings.set("TimeDuration_Minute", "About a minute");
ININ.Web.Common.Resources.LocalizedStrings.set("TimeDuration_Minutes", "About %0 minutes");
ININ.Web.Common.Resources.LocalizedStrings.set("TimeDuration_Minutes_AlternatePlural", "About %0 minutes");
ININ.Web.Common.Resources.LocalizedStrings.set("TimeDuration_Hour", "About an hour");
ININ.Web.Common.Resources.LocalizedStrings.set("TimeDuration_Hours", "About %0 hours");
ININ.Web.Common.Resources.LocalizedStrings.set("TimeDuration_Hours_AlternatePlural", "About %0 hours");
ININ.Web.Common.Resources.LocalizedStrings.set("TimeDuration_Day", "About a day");
ININ.Web.Common.Resources.LocalizedStrings.set("TimeDuration_Days", "About %0 days");
ININ.Web.Common.Resources.LocalizedStrings.set("TimeDuration_Days_AlternatePlural", "About %0 days");
ININ.Web.Common.Resources.LocalizedStrings.set("InteractionsWaitingCountLabel", "Interactions waiting:");
ININ.Web.Common.Resources.LocalizedStrings.set("LoggedInAgentsCountLabel", "Logged in agents:");
ININ.Web.Common.Resources.LocalizedStrings.set("AvailableAgentsCountLabel", "Available agents:");
ININ.Web.Common.Resources.LocalizedStrings.set("Callback", "Callback");

ININ.Web.Common.Resources.LocalizedStrings.set("CallbackStatusFailed", "Couldn't get the status of the callback.");
ININ.Web.Common.Resources.LocalizedStrings.set("UserIdLabel", "User ID:");
ININ.Web.Common.Resources.LocalizedStrings.set("DisconnectCallback", "Cancel");
ININ.Web.Common.Resources.LocalizedStrings.set("DisconnectCallbackSucceeded", "The callback request has been canceled.");

ININ.Web.Common.Resources.LocalizedStrings.set("DisconnectCallbackFailed", "Failed to cancel this callback request");
ININ.Web.Common.Resources.LocalizedStrings.set("EnterNameIfDesired", "Enter name if desired");
ININ.Web.Common.Resources.LocalizedStrings.set("AnonymousUser", "Anonymous User");
ININ.Web.Common.Resources.LocalizedStrings.set("Account", "Account");
ININ.Web.Common.Resources.LocalizedStrings.set("Name", "Name");
ININ.Web.Common.Resources.LocalizedStrings.set("Home", "Home");
ININ.Web.Common.Resources.LocalizedStrings.set("Business", "Business");
ININ.Web.Common.Resources.LocalizedStrings.set("OneErrorWithChatData", "There was one error with the chat information.");
ININ.Web.Common.Resources.LocalizedStrings.set("MultipleErrorsWithChatData", "There were %0 errors with the chat information.");
ININ.Web.Common.Resources.LocalizedStrings.set("OneErrorWithRegistrationData", "There was one error with the registration information.");
ININ.Web.Common.Resources.LocalizedStrings.set("MultipleErrorsWithRegistrationData", "There were %0 errors with the registration information.");
ININ.Web.Common.Resources.LocalizedStrings.set("OneErrorWithCallbackData", "There was one error with the callback information.");
ININ.Web.Common.Resources.LocalizedStrings.set("MultipleErrorsWithCallbackData", "There were %0 errors with the callback information.");
ININ.Web.Common.Resources.LocalizedStrings.set("FieldIsRequired", "Field is required");
ININ.Web.Common.Resources.LocalizedStrings.set("PasswordsDoNotMatch", "Passwords do not match");
ININ.Web.Common.Resources.LocalizedStrings.set("GeneralError", "General error");
ININ.Web.Common.Resources.LocalizedStrings.set("InvalidCharSetError", "Character set of content is invalid");
ININ.Web.Common.Resources.LocalizedStrings.set("InvalidContentTypeError", "Type of content is invalid");
ININ.Web.Common.Resources.LocalizedStrings.set("ContentError", "Content is invalid");
ININ.Web.Common.Resources.LocalizedStrings.set("MissingDataError", "Content is missing required data");
ININ.Web.Common.Resources.LocalizedStrings.set("UnknownEntityError", "Unknown entity");
ININ.Web.Common.Resources.LocalizedStrings.set("UnknownSessionError", "Unknown session");
ININ.Web.Common.Resources.LocalizedStrings.set("UnknownParticipantError", "Unknown participant");
ININ.Web.Common.Resources.LocalizedStrings.set("BadTargetError", "Unknown ACD target");
ININ.Web.Common.Resources.LocalizedStrings.set("UserDbError", "User account error");
ININ.Web.Common.Resources.LocalizedStrings.set("UserNotOnline", "User is not online");
ININ.Web.Common.Resources.LocalizedStrings.set("BadCredentialsError", "User name or password is incorrect");
ININ.Web.Common.Resources.LocalizedStrings.set("AccountExistsError", "Account already exists");
ININ.Web.Common.Resources.LocalizedStrings.set("FailedToSendMessage", "Failed to send message");
ININ.Web.Common.Resources.LocalizedStrings.set("ErrorConnectingToServer", "There was an error connecting to the server");
ININ.Web.Common.Resources.LocalizedStrings.set("Send", "أرسل");

ININ.Web.Common.Resources.LocalizedStrings.set("Exit", "Exit");
ININ.Web.Common.Resources.LocalizedStrings.set("DisconnectedFromChat", "You have been disconnected from the chat");
ININ.Web.Common.Resources.LocalizedStrings.set("ErrorConnectingServer", "Error connecting to the server. Please wait while we try to reconnect...");
ININ.Web.Common.Resources.LocalizedStrings.set("SuccessfullyReconnectedServer", "Successfully reconnected to the server.");
ININ.Web.Common.Resources.LocalizedStrings.set("CouldNotConnectServerRetry", "Could not connect to any servers. Will retry in a moment.");
ININ.Web.Common.Resources.LocalizedStrings.set("PleaseLeaveMessage", "Please leave a message for %0");
ININ.Web.Common.Resources.LocalizedStrings.set("PrintableChatHistory", "طباعة الدردشه");
ININ.Web.Common.Resources.LocalizedStrings.set("ChatHistory", "نص الدردشه");

ININ.Web.Common.Resources.LocalizedStrings.set("Print", "طباعه");
ININ.Web.Common.Resources.LocalizedStrings.set("LinkDisclaimer", "*Some links may not be valid after the chat ends.");
/* Note that the following line uses single quotes, to avoid having nested double quotes. */
ININ.Web.Common.Resources.LocalizedStrings.set("NeedPageRefresh_Format", 'Sorry for the inconvenience, but we have encountered a system failure that will require you to <a href="{0}">start a new chat</a>.');
ININ.Web.Common.Resources.LocalizedStrings.set("BrowserSecuritySettingsError", "Web browser security settings will not allow Interaction Web Tools to run.");
ININ.Web.Common.Resources.LocalizedStrings.set("FireFoxVersionError", "FireFox versions prior to 3.5 are not supported");
ININ.Web.Common.Resources.LocalizedStrings.set("IEVersionError", "Internet Explorer versions prior to 7 are not supported");
ININ.Web.Common.Resources.LocalizedStrings.set("ErrorOpeningWindow", "There was an error opening the window.");

ININ.Web.Common.Resources.LocalizedStrings.set("Typing", "(typing)");
ININ.Web.Common.Resources.LocalizedStrings.set("LastRespondedTime", "Last response sent at: %0");

ININ.Web.Common.Resources.LocalizedStrings.set("Display Name","أمر");
ININ.Web.Common.Resources.LocalizedStrings.set("Disclaimer", "Disclaimer: Subject of callbacks and content of chats might be monitored, recorded, or viewed by agents and supervisors for quality monitoring and routing and distribution purposes.");

ININ.Web.Common.Resources.LocalizedStrings.set("DayOfWeek0", "Sunday");
ININ.Web.Common.Resources.LocalizedStrings.set("DayOfWeek1", "Monday");

ININ.Web.Common.Resources.LocalizedStrings.set("DayOfWeek2", "Tuesday");
ININ.Web.Common.Resources.LocalizedStrings.set("DayOfWeek3", "Wednesday");
ININ.Web.Common.Resources.LocalizedStrings.set("DayOfWeek4", "Thursday");
ININ.Web.Common.Resources.LocalizedStrings.set("DayOfWeek5", "Friday");

ININ.Web.Common.Resources.LocalizedStrings.set("DayOfWeek6", "Saturday");
ININ.Web.Common.Resources.LocalizedStrings.set("AbbreviatedDayOfWeek0", "Sun");
ININ.Web.Common.Resources.LocalizedStrings.set("AbbreviatedDayOfWeek1", "Mon");
ININ.Web.Common.Resources.LocalizedStrings.set("AbbreviatedDayOfWeek2", "Tue");
ININ.Web.Common.Resources.LocalizedStrings.set("AbbreviatedDayOfWeek3", "Wed");
ININ.Web.Common.Resources.LocalizedStrings.set("AbbreviatedDayOfWeek4", "Thu");
ININ.Web.Common.Resources.LocalizedStrings.set("AbbreviatedDayOfWeek5", "Fri");
ININ.Web.Common.Resources.LocalizedStrings.set("AbbreviatedDayOfWeek6", "Sat");

ININ.Web.Common.Resources.LocalizedStrings.set("Month1", "January");
ININ.Web.Common.Resources.LocalizedStrings.set("Month2", "February");
ININ.Web.Common.Resources.LocalizedStrings.set("Month3", "March");
ININ.Web.Common.Resources.LocalizedStrings.set("Month4", "April");
ININ.Web.Common.Resources.LocalizedStrings.set("Month5", "May");
ININ.Web.Common.Resources.LocalizedStrings.set("Month6", "June");
ININ.Web.Common.Resources.LocalizedStrings.set("Month7", "July");
ININ.Web.Common.Resources.LocalizedStrings.set("Month8", "August");
ININ.Web.Common.Resources.LocalizedStrings.set("Month9", "September");
ININ.Web.Common.Resources.LocalizedStrings.set("Month10", "October");
ININ.Web.Common.Resources.LocalizedStrings.set("Month11", "November");
ININ.Web.Common.Resources.LocalizedStrings.set("Month12", "December");
ININ.Web.Common.Resources.LocalizedStrings.set("AbbreviatedMonth1", "Jan");
ININ.Web.Common.Resources.LocalizedStrings.set("AbbreviatedMonth2", "Feb");
ININ.Web.Common.Resources.LocalizedStrings.set("AbbreviatedMonth3", "Mar");
ININ.Web.Common.Resources.LocalizedStrings.set("AbbreviatedMonth4", "Apr");
ININ.Web.Common.Resources.LocalizedStrings.set("AbbreviatedMonth5", "May");
ININ.Web.Common.Resources.LocalizedStrings.set("AbbreviatedMonth6", "Jun");
ININ.Web.Common.Resources.LocalizedStrings.set("AbbreviatedMonth7", "Jul");
ININ.Web.Common.Resources.LocalizedStrings.set("AbbreviatedMonth8", "Aug");
ININ.Web.Common.Resources.LocalizedStrings.set("AbbreviatedMonth9", "Sep");
ININ.Web.Common.Resources.LocalizedStrings.set("AbbreviatedMonth10", "Oct");
ININ.Web.Common.Resources.LocalizedStrings.set("AbbreviatedMonth11", "Nov");
ININ.Web.Common.Resources.LocalizedStrings.set("AbbreviatedMonth12", "Dec");

ININ.Web.Common.Resources.LocalizedStrings.set("AM", "ص");

ININ.Web.Common.Resources.LocalizedStrings.set("PM", "م");

ININ.Web.Common.Resources.LocalizedStrings.set("AbbreviatedAM", "a");

ININ.Web.Common.Resources.LocalizedStrings.set("AbbreviatedPM", "p");


ININ.Web.Common.Resources.LocalizedStrings.set("Era", "CE");

ININ.Web.Common.Resources.LocalizedStrings.set("AbbreviatedEra", "CE");


ININ.Web.Common.Resources.LocalizedStrings.set("FallbackDateFormat", "d MMM yyyy");
ININ.Web.Common.Resources.LocalizedStrings.set("FallbackTimeFormat", "h:mm tt");

ININ.Web.Common.Resources.LocalizedStrings.set("OverrideDateTimeFormats", "0");


Bootloader.onLoadedLocalization("en-us");
