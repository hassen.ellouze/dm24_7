iDubaiApp.controller('requestSummaryController',
    ['$state', '$scope', '$auth', '$stateParams', 'LoadingService', '$notification', '$filter', '$q',
        'TrackingService', '$rootScope', 'underscore', '$compile', '$injector', '$dmServices', 'GuestUtils', 'helpdeskService', '$DMLinks', '$sce', 'Makani', 'ngDialog', 'SharedService',
        function ($state, $scope, $auth, $stateParams, LoadingService, $notification, $filter, $q,
                  TrackingService, $rootScope, underscore, $compile, $injector, $dmServices, GuestUtils, helpdeskService, $DMLinks, $sce, Makani, ngDialog, SharedService) {
            var _ = underscore;
            var dialog = null;
            $scope.isLiveTracker = false;
            $scope.currentLang = localStorage.getItem('dm-lang');
            /*if ($stateParams.isBack) {
                $stateParams = $state.current.params;
            }*/
            var myStateParams=SharedService.getStateHistory($state.current.name);
            $stateParams = _.clone(myStateParams);
            $scope.modal = {
                toggle: false
            }
            $scope.rating = "3";
            $scope.from = $stateParams.from; // a magic line

            if ($stateParams.requestDetails) {
                $scope.requestDetails = $stateParams.requestDetails;
                $scope.readyForRating = $scope.requestDetails.ServiceENHighlighted;
                var allServices = $dmServices;
                /*$scope.requestDetails.description =  _.find(allServices, function(item, index){
                 return $scope.requestDetails.ServiceEN === item.service_title_en
                 });*/


            }

            /*
             *  Button back
             * */

            $scope.goBack = function () {
                if ($scope.from == 'myRequests') {
                    $state.go('myRequests');
                }
                else if ($scope.from == 'myLocations') {
                    $state.go('myLocations');
                }
                else {
                    $state.go('dashboard');
                }
            }

            /**
             * Get vehicle data (Step Two)
             * @param vehicleID
             */
            $scope.vehicleEnquireService = function (vehicleID) {
                var dfd = $q.defer();
                TrackingService.vehicleEnquireService(vehicleID).then(function (data) {
                    dfd.resolve(data);
                }, function (error) {
                    dfd.reject(error);
                });
                return dfd.promise;
            };

            $scope.getRequestDetails = function () {
                var dfd = $q.defer();
                var requestDetails = null;
                var rn = $stateParams.requestDetails.RequestNumber;

                LoadingService.show();

                console.log('===> Service Request Number ', rn);
                TrackingService.enquireByServiceRequestNumber(rn).then(function (data) {
                    try {
                        console.log('==> data ', data)
                        requestDetails = data;
                        $scope.requestDetailsFromWS = data;

                        Makani.getNearestMakani($stateParams.requestDetails.Location_Y, $stateParams.requestDetails.Location_X).then(
                            function (result) {
                                if (result.hasOwnProperty('DATA')) {

                                } else {
                                    var makaniInfo = result.makaniInfo[0];
                                    $scope.requestDetailsFromWS.makani = makaniInfo.MAKANI;

                                }


                            }, function (err) {

                                console.log(err);
                            });


                        $scope.attachments = [];
                        $scope.attachmentsAudio = [];
                        if ($scope.requestDetailsFromWS.Attachments && $scope.requestDetailsFromWS.Attachments.length > 0) {
                            for (var i = 0; i < $scope.requestDetailsFromWS.Attachments.length; i++) {
                                var url = $scope.requestDetailsFromWS.Attachments[i].Url;
                                $scope.requestDetailsFromWS.Attachments[i].Url = $sce.trustAsResourceUrl(url);
                                if (['jpeg', 'jpg', 'png'].indexOf(url.substr(url.lastIndexOf('.') + 1)) > -1) {
                                    $scope.attachments.push($scope.requestDetailsFromWS.Attachments[i]);
                                } else {
                                    $scope.attachmentsAudio.push({
                                        src: $scope.requestDetailsFromWS.Attachments[i].Url
                                    })
                                }


                            }
                        }
                        requestDetails.DMGISXCoordinates = $stateParams.lng;
                        requestDetails.DMGISyCoordinates = $stateParams.lat;
                        $scope.trackingDetails = requestDetails;
                        if ($stateParams.from = 'myLocations') {
                            prepareRequest();
                        }
                        dfd.resolve(requestDetails);
                    }catch (e) {
                        console.log(e);
                    }
                }, function (error) {
                    dfd.reject(error);
                });
                return dfd.promise;


            };

            $scope.play = function (id) {
                $('#' + id)[0].play();
                $('#' + id + '_play').css('display', 'none');
                $('#' + id + '_stop').css('display', '');
                var aud = document.getElementById(id);
                aud.onended = function () {
                    $scope.stop(id);
                };


            }

            $scope.stop = function (id) {
                $('#' + id)[0].pause();
                $('#' + id)[0].currentTime = 0;
                $('#' + id + '_play').css('display', '');
                $('#' + id + '_stop').css('display', 'none');
            }

            /*
             * Cancel Rating
             */
            $scope.cancelRating = function () {
                dialog.close();
            }

            /*
             * Modal to Rate service
             */
            $scope.openModalToRateService = function () {
                SharedService.setStateHistory("backButton", {isActive: false});
                dialog = ngDialog.open({
                    template: 'rate-dialog',
                    className: 'ngdialog-theme-default',
                    scope: $scope,
                    trapFocus: false,
                    preCloseCallback: function(value) {
                        SharedService.setStateHistory("backButton", {isActive: true});
                    }
                });

            };

            /*
             * Rate service
             */
            $scope.rateService = function (rating, feedback) {
                console.log(rating);
                console.log(feedback);
                var mobileNumber = '';
                var username = '';
                var emailAddress = '';

                if ($auth.token) {
                    mobileNumber = $auth.dubaiUserProfile.mobile;
                    username = $auth.dubaiUserProfile.username;
                    emailAddress = ($auth.dubaiUserProfile.email) ? $auth.dubaiUserProfile.email : '';
                } else if (!GuestUtils.isEmptyGuest()) {
                    var guestInfo = GuestUtils.getGuestInfo();
                    if (guestInfo != '') {
                        mobileNumber = (guestInfo.mobileNumber) ? guestInfo.mobileNumber : '';
                        username = (guestInfo.name) ? guestInfo.name : '';
                        emailAddress = (guestInfo.email) ? guestInfo.email : '';
                    }
                }

                dialog.close();
                var options = {
                    description: feedback,
                    rate: rating,
                    osInfo: WL.Client.getEnvironment(),
                    mobileNumber: mobileNumber,
                    username: username,
                    emailAddress: emailAddress,
                };


                console.log('options : ', options);

                LoadingService.show();
                helpdeskService.senRateMail('0', options).then(function (success) {
                    LoadingService.hide();
                    WL.SimpleDialog.show($filter('translate')('Alert'), ($filter('translate')("Your rating has been successfully submitted")), [{
                        text: $filter('translate')('OK'), handler: function () {
                            $scope.readyForRating = 'False';
                            $scope.$apply();
                        }
                    }]);
                }, function (error) {
                    LoadingService.hide();
                    $notification.alert($filter('translate')("Failure - Retry"));
                });

            };

            /*$scope.getRequestDetails().then(
             function (data) {
             console.log('===> getRequestDetails :: ', data);
             if (data) {
             //$scope.loadStatus(data);
             //console.log('===> status dateRequest:: ', $scope.RequestStatus);
             if ($scope.status !== 'CLOSED' && data.VehicleNumber) {
             $scope.vehicleEnquireService(data.VehicleNumber).then(function (res) {
             console.log('===> vehicleEnquireService :: ', res);
             $scope.VehicleDataResponse = res.invocationResult.Envelope.Body.VehicleInquiryServiceResponse.VehicleInquiryServiceResult;
             if ($scope.VehicleDataResponse != undefined ) {
             if ($scope.VehicleDataResponse.Latitude != '0' && $scope.VehicleDataResponse.Longitude != '0') {
             $scope.isLiveTracker = true;
             }
             }
             //$scope.loadStatus(data);
             }, function (err) {
             //$notification.alert($filter('translate')("Can't fetch data. Please try again later"));
             });
             }

             } else {
             handleError();
             }
             }, function (error) {
             handleError();
             });*/

            /**
             *  Rate Service
             */
            $scope.modalToggle = function () {
                /*var bodyModal = angular.element(document.querySelector(".modalHidden"));
                 var body = angular.element(document.querySelector(".toShowMenu"));
                 if (!bodyModal.hasClass('modal-open')){
                 body.addClass('modal-open');
                 } else {
                 body.addClass('modal-open');
                 }*/

            };

            /**
             * Get address name by LatLng
             * @param latlng
             */
            $scope.geocodeLatLng = function (lat, lng) {
                var deferred = $q.defer();

                var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
                var geocoder = new google.maps.Geocoder()
                geocoder.geocode({'location': latlng}, function (results, status) {
                    if (status === 'OK') {
                        deferred.resolve(results[0].formatted_address);
                    } else {
                        deferred.resolve('');
                    }
                });
                return deferred.promise;
            };

            function prepareLocation(lat, lng) {
                $scope.geocodeLatLng(lat, lng).then(function (result) {
                    $scope.formattedAddress = result;
                    $scope.locationName = result;


                }, function (err) {
                    LoadingService.hide();
                    console.log(err);
                })
            }

            /*
             *  get Request Details (Step One)
             * */
            $scope.getRequestDetails().then(
                function (data) {
                    prepareLocation(data.Location_Y, data.Location_X);
                    $scope.imgSrc = encodeURI("https://maps.googleapis.com/maps/api/staticmap?" +
                        "center=" + data.Location_Y + ',' + data.Location_X +
                        "&zoom=15" +
                        "&scale=false" +
                        "&size=600x300" +
                        "&maptype=satellite" +
                        "&key=AIzaSyDAyPe0GL66TrDBhkzQCF9-8pIWTLoxg7o" +
                        "&format=png" +
                        "&visual_refresh=true" +
                        "&markers=" + data.Location_Y + ',' + data.Location_X);

                    LoadingService.hide();
                    if ($scope.requestDetails.ServiceENHighlighted != 'Completed') {
                        console.log('===> getRequestDetails :: ', data);
                        if (data) {
                            //console.log('===> data :: ', data);
                            if (data.VehicleNumber) {
                                $scope.VehicleNumber = data.VehicleNumber;
                                $scope.vehicleEnquireService(data.VehicleNumber).then(function (res) {
                                    console.log('===> vehicleEnquireService :: ', res);
                                    $scope.VehicleDataResponse = res.invocationResult.Envelope.Body.VehicleInquiryServiceResponse.VehicleInquiryServiceResult;
                                    if ($scope.VehicleDataResponse != undefined) {
                                        if ($scope.VehicleDataResponse.Latitude != '0' && $scope.VehicleDataResponse.Longitude != '0') {
                                            $scope.isLiveTracker = true;

                                        }
                                    }
                                    //$scope.loadStatus(data);
                                }, function (err) {
                                    //$notification.alert($filter('translate')("Can't fetch data. Please try again later"));
                                });
                            }

                        } else {
                            handleError();
                        }
                    }

                }, function (error) {
                    LoadingService.hide();
                    handleError();
                });

            /**
             *  hide navigation bar
             */

            $scope.hideNav = function () {
                var mapTracker = angular.element(document.querySelector(".mapTracker"));
                var navBar = angular.element(document.querySelector(".navbar"));
                SharedService.setStateHistory("backButton", {isActive: mapTracker.hasClass('showTrackerMap')});
                if (mapTracker.hasClass('showTrackerMap')) {
                    navBar.removeClass('hideNav');
                } else {
                    navBar.addClass('hideNav');

                }
            };

            /* /!**
             * Get request tracking details  by SRNumber
             *!/

             $scope.getTrackingDetails = function () {

             var dfd = $q.defer();
             var trackingDetails = null;
             var sr = $stateParams.SRNumber;
             console.log('===> SR Number ', sr);
             TrackingService.enquireByServiceRequestNumber(sr).then(function (data) {
             console.log('==> data ', data)
             trackingDetails = data;
             trackingDetails.DMGISXCoordinates = $stateParams.lng;
             trackingDetails.DMGISyCoordinates = $stateParams.lat;
             $scope.trackingDetails = trackingDetails;
             dfd.resolve(trackingDetails);
             }, function (error) {
             dfd.reject(error);
             });
             return dfd.promise;


             };

             /!**
             * Get vehicle data
             * @param vehicleID
             *!/
             $scope.vehicleEnquireService = function (vehicleID) {
             var dfd = $q.defer();
             TrackingService.vehicleEnquireService(vehicleID).then(function (data) {
             dfd.resolve(data);
             }, function (error) {
             dfd.reject(error);
             });
             return dfd.promise;
             };

             //$scope.vehicleData = null;
             $scope.getTrackingDetails().then(
             function (data) {
             console.log('===> getTrackingDetails :: ', data);
             if (data) {
             $scope.loadStatus(data);
             console.log('===> status dateRequest:: ', $scope.RequestStatus);
             if ($scope.status !== 'CLOSED' && (data.VehicleNumber !== '' && data.VehicleNumber !== null && data.VehicleNumber !== undefined)) {
             $scope.vehicleEnquireService(data.VehicleNumber).then(function (res) {
             console.log('===> vehicleEnquireService :: ', res);
             $scope.VehicleDataResponse = res.invocationResult.Envelope.Body.VehicleInquiryServiceResponse.VehicleInquiryServiceResult;
             $scope.loadStatus(data);
             }, function (err) {
             $notification.alert($filter('translate')("Can't fetch data. Please try again later"));
             });
             }

             } else {
             handleError();
             }
             }, function (error) {
             handleError();
             });*/

            /**
             * Show popup error and go back
             */
            function handleError() {
                WL.SimpleDialog.show(
                    $filter('translate')('Alert'),
                    $filter('translate')('Can\'t fetch data. Please try again later'), [{
                        text: $filter('translate')('OK'), handler: function () {
                            if ($rootScope.history[$rootScope.history.length - 2] === undefined) {
                                $state.go('dashboard');
                            } else {
                                $state.go($rootScope.history[$rootScope.history.length - 2].name, {
                                    isBack: true,
                                    params: $rootScope.history[$rootScope.history.length - 2].params
                                });
                            }
                        }
                    }]);
            }

            /////////////////   tracking Map ////////////////////
            // Tracking vehicle map
            var heightMaps = $(window).height();
            if (WL.Client.getEnvironment() == WL.Environment.IPHONE) {
                heightMaps = $(window).height() - 50;
            }
            jQuery('#mapTracker').css({'height': heightMaps + "px"});
            var map = null;
            var markers = [];
            var directionsDisplay;
            var directionsService;
            var requestMarker;
            var vehicleMarker;

            $scope._calcRoute = function () {

                function mapLocation() {
                    var directionsDisplay;
                    var directionsService = new google.maps.DirectionsService();
                    var map;

                    function initialize() {
                        directionsDisplay = new google.maps.DirectionsRenderer();
                        var chicago = new google.maps.LatLng(37.334818, -121.884886);
                        var mapOptions = {
                            zoom: 7,
                            center: chicago
                        };
                        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
                        directionsDisplay.setMap(map);
                        google.maps.event.addDomListener(document.getElementById('routebtn'), 'click', calcRoute);
                    }

                    function calcRoute() {
                        var start = new google.maps.LatLng(37.334818, -121.884886);
                        //var end = new google.maps.LatLng(38.334818, -181.884886);
                        var end = new google.maps.LatLng(37.441883, -122.143019);
                        /*
                         var startMarker = new google.maps.Marker({
                         position: start,
                         map: map,
                         draggable: true
                         });
                         var endMarker = new google.maps.Marker({
                         position: end,
                         map: map,
                         draggable: true
                         });
                         */
                        var bounds = new google.maps.LatLngBounds();
                        bounds.extend(start);
                        bounds.extend(end);
                        map.fitBounds(bounds);
                        var request = {
                            origin: start,
                            destination: end,
                            travelMode: google.maps.TravelMode.DRIVING
                        };
                        directionsService.route(request, function (response, status) {
                            if (status == google.maps.DirectionsStatus.OK) {
                                directionsDisplay.setDirections(response);
                                directionsDisplay.setMap(map);
                            } else {
                                alert("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
                            }
                        });
                    }

                    google.maps.event.addDomListener(window, 'load', initialize);
                }

                mapLocation();

            }

            $scope.calcRoute = function () {
                LoadingService.show();
                //$scope.requestDetailsFromWS.Location_Y


                vehicleEnquireService($scope.VehicleDataResponse.RegistrationNumber).then(function (vehicleData) {
                    LoadingService.hide();
                    if (typeof(google) !== "undefined") {

                        if (map) {

                            map.setCenter({
                                lat: parseFloat(vehicleData.Latitude),
                                lng: parseFloat(vehicleData.Longitude)
                            });



                        }else{
                            directionsService = new google.maps.DirectionsService();
                            directionsDisplay = new google.maps.DirectionsRenderer();
                            map = new google.maps.Map(document.getElementById('mapTracker'), {
                                center: {
                                    lat: parseFloat(vehicleData.Latitude),
                                    lng: parseFloat(vehicleData.Longitude)
                                },
                                zoom: 2,
                                zoomControl: false,
                                fullscreenControl: false,
                                streetViewControl: false
                            });

                            var controlReturnDIV = document.createElement('DIV');
                            //controlReturnDIV.className = 'btn-downHolder';
                            controlReturnDIV.style.left = '30px !important';

                            var controlReturnBtn = document.createElement('button');
                            controlReturnBtn.className = 'btn-down';

                            controlReturnBtn.setAttribute("ng-click", "hideNav(); trackerMap.toggleShow =! trackerMap.toggleShow");
                            compile(controlReturnBtn);

                            controlReturnDIV.appendChild(controlReturnBtn);

                            //map.controls[google.maps.ControlPosition.BOTTOM].push(controlCallDIV);
                            map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(controlReturnDIV);
                        }

                        //### Add call Technician on Google Maps ...
                        //var controlCallDIV = document.createElement('DIV');
                        //controlCallDIV.className = 'btn-callHolder';

                        /* var controlCallBtn = document.createElement('button');
                         controlCallBtn.className = 'btn-call';

                         controlCallBtn.setAttribute("ng-if", "vehicleData.DriverMobileNumber");
                         controlCallBtn.setAttribute("ng-click", "openDialer()");
                         compile(controlCallBtn);


                         controlCallDIV.appendChild(controlCallBtn); */

                        // Add Return button
                        /*<div class="btn-downHolder">
                         <button type="button" class="btn-down" ng-click="hideNav() ; trackerMap.toggleShow =! trackerMap.toggleShow"></button>
                         </div>*/




                    }


                    /*directionsDisplay.setMap(null);
                     for (var i = 0; i < markers.length; i++) {
                     markers[i].setMap(null);
                     }*/
                    /*if(!position){
                     var position = new google.maps.LatLng(vehicleData.Latitude, vehicleData.Longitude);
                     }*/


                    if (vehicleMarker) {
                        //vehicleMarker.setPosition(position);
                        //map.setZoom(17);
                    } else {
                        vehicleMarker = new google.maps.Marker({
                            position: new google.maps.LatLng(parseFloat($scope.requestDetailsFromWS.Location_Y), parseFloat($scope.requestDetailsFromWS.Location_X)),
                            map: map,
                            title: '',
                            icon: './assets/img3.0/truckingx32.png'
                        });

                        requestMarker = new google.maps.Marker({
                            position: new google.maps.LatLng(parseFloat(vehicleData.Latitude), parseFloat(vehicleData.Longitude)),
                            map: map,
                            title: '',
                            icon: './assets/img3.0/pin.png'
                        });



                    }

                    var vehiclePosition = new google.maps.LatLng(parseFloat($scope.requestDetailsFromWS.Location_Y), parseFloat($scope.requestDetailsFromWS.Location_X));
                    var requestPosition = new google.maps.LatLng(parseFloat(vehicleData.Latitude), parseFloat(vehicleData.Longitude));

                    var bounds = new google.maps.LatLngBounds();
                    bounds.extend(vehiclePosition);
                    bounds.extend(requestPosition);
                    map.fitBounds(bounds);

                    var request = {
                        origin: vehiclePosition,
                        destination: requestPosition,
                        travelMode: google.maps.DirectionsTravelMode.DRIVING,
                        provideRouteAlternatives: true
                    };
                    directionsService.route(request, function (response, status) {

                        if (status === google.maps.DirectionsStatus.OK) {
                            directionsDisplay = new google.maps.DirectionsRenderer({
                                map: map,
                                directions: response,
                                suppressMarkers: true,
                                suppressInfoWindows: false
                            });
                            //var leg = response.routes[0].legs[0];
                            //makeVehicleMarker(leg.start_location, icons.start, "title", map);
                            //makeRequestMarker(leg.end_location, icons.end, 'title', map);
                            directionsDisplay.setDirections(response);
                            directionsDisplay.setMap(map);

                        } else {
                            //vehicleMarker.setVisible(true);
                            //requestMarker.setVisible(true);
                        }
                    });

                    var icons = {
                        start: new google.maps.MarkerImage('./assets/img3.0/van.png'),
                        end: new google.maps.MarkerImage('./assets/img3.0/pin.png')
                    };

                    /*i = 0;
                     var position = new google.maps.LatLng($scope.vehicleData.Latitude, $scope.vehicleData.Longitude);
                     var image = './assets/img3.0/van.png';
                     vehicleMarker = new google.maps.Marker({
                     position: position,
                     map: map,
                     title: '',
                     icon: image
                     });
                     var vehicleData = $scope.vehicleData;
                     vehicleMarker.setVisible(false);
                     google.maps.event.addListener(vehicleMarker, 'click', (function (vehicleMarker, i, vehicleData) {
                     return function () {
                     if ($(".infoBox").hasClass('showInfo')) {
                     $(".infoBox").removeClass('showInfo');
                     } else {
                     $(".infoBox").addClass('infoBox showInfo');
                     }
                     };
                     })(vehicleMarker, i, vehicleData));
                     markers.push(vehicleMarker);
                     i++;
                     position = new google.maps.LatLng($scope.requestData.DMGISXCoordinates, $scope.requestData.DMGISyCoordinates);
                     image = './assets/img3.0/pin.png';
                     requestMarker = new google.maps.Marker({
                     position: position,
                     map: map,
                     title: '',
                     icon: image
                     });
                     var requestData = $scope.requestData;
                     requestMarker.setVisible(false);
                     markers.push(requestMarker);*/

                }, function (error) {

                });


            }

            /* $scope.vehicleData = {
             Latitude : '25.1162565',
             Longitude : '55.2112637'
             };

             $scope.requestData = {
             DMGISXCoordinates : '25.091853',
             DMGISyCoordinates : '55.189000'
             };*/

            function compile(element) {
                var el = angular.element(element);
                $injector.invoke(function ($compile) {
                    $compile(el)($scope)
                })
            }

            function loadLocationMap(step) {
                console.log('===> vehicleData ', $scope.vehicleData);
                console.log('===> requestData ', $scope.requestData);
                setTimeout(function () {
                    try {
                        if (typeof(google) !== "undefined") {

                            map = new google.maps.Map(document.getElementById('mapTracker'), {
                                center: {
                                    lat: parseFloat($scope.vehicleData.Latitude),
                                    lng: parseFloat($scope.vehicleData.Longitude)
                                },
                                zoom: 11,
                                zoomControl: false,
                                fullscreenControl: false,
                                streetViewControl: false
                            });

                            //### Add call Technician on Google Maps ...
                            //var controlCallDIV = document.createElement('DIV');
                            //controlCallDIV.className = 'btn-callHolder';

                            /* var controlCallBtn = document.createElement('button');
                             controlCallBtn.className = 'btn-call';

                             controlCallBtn.setAttribute("ng-if", "vehicleData.DriverMobileNumber");
                             controlCallBtn.setAttribute("ng-click", "openDialer()");
                             compile(controlCallBtn);


                             controlCallDIV.appendChild(controlCallBtn); */

                            // Add Return button
                            /*<div class="btn-downHolder">
                             <button type="button" class="btn-down" ng-click="hideNav() ; trackerMap.toggleShow =! trackerMap.toggleShow"></button>
                             </div>*/

                            var controlReturnDIV = document.createElement('DIV');
                            //controlReturnDIV.className = 'btn-downHolder';
                            controlReturnDIV.style.left = '30px !important';

                            var controlReturnBtn = document.createElement('button');
                            controlReturnBtn.className = 'btn-down';

                            controlReturnBtn.setAttribute("ng-click", "hideNav(); trackerMap.toggleShow =! trackerMap.toggleShow");
                            compile(controlReturnBtn);


                            controlReturnDIV.appendChild(controlReturnBtn);


                            //map.controls[google.maps.ControlPosition.BOTTOM].push(controlCallDIV);
                            map.controls[google.maps.ControlPosition.RIGHT_TOP].push(controlReturnDIV);


                        }


                    } catch (ex) {
                        console.log('===> EX ', ex)
                    }
                }, 1000);
            }

            //loadLocationMap(1);

            // Get vehicle data (Step Three)
            function vehicleEnquireService(registerNumber) {
                var defer = $q.defer();
                var invocationData = {
                    adapter: "VehicleEnquireCRM24Adapter",
                    procedure: "vehicleEnquireService",
                    parameters: [registerNumber] //$scope.vehicleData.RegistrationNumber
                    //parameters: ['R-36730']

                };
                WL.Client.invokeProcedure(invocationData, {
                    onSuccess: function (data) {
                        console.log('vehicleEnquireService ::', JSON.stringify(data));
                        defer.resolve(data.invocationResult.Envelope.Body.VehicleInquiryServiceResponse.VehicleInquiryServiceResult)
                        //$scope.vehicleData = data.invocationResult.Envelope.Body.VehicleInquiryServiceResponse.VehicleInquiryServiceResult;
                        /*
                         <Int64Result>0</Int64Result>
                         <Status>false</Status>
                         <RegistrationNumber>N-98959</RegistrationNumber>
                         <Latitude>25.23993</Latitude>
                         <Longitude>55.36315</Longitude>
                         <LocationEn>DM Garage</LocationEn>
                         <LocationAr>الكراج</LocationAr>
                         <LocationDateTime>9/16/2018 7:57:47 AM</LocationDateTime>
                         <GeofenceEn>DM Garage</GeofenceEn>
                         <GeofenceAr>الكراج</GeofenceAr>
                         <DriverId/>
                         <DriverName/>
                         <DriverMobileNumber/>
                         <VehicleStatusName>Stop</VehicleStatusName>
                         <Speed>0</Speed>
                         */
                        /*$scope.vehicleData={
                         "Status": "true",
                         "Speed": "20",
                         "StringResult": "Failed",
                         "ErrorMessage": "DT-CRM-VehicleInquiryService-ERROR-10005",
                         "Int64Result": "0",
                         "Latitude": "25.1442558",
                         "Longitude": "55.1984654"
                         }*/
                        //loadLocationMap(4);
                    },
                    onFailure: function (err) {
                        defer.reject(err);
                    }
                });

                return defer.promise;
            }

            // Reload
            /*var interval = setInterval(function () {
             if ( $scope.isLiveTracker ) {
             vehicleEnquireService();

             }
             }, 10000);*/

            /*$scope.$watch(function(){
             if ($scope.isLiveTracker){
             setInterval(function () {
             if ( $scope.isLiveTracker ) {
             vehicleEnquireService();
             }
             }, 10000);
             }
             })*/

            // Open dialer
            $scope.openDialer = function (mobileNumber) {
                console.log('===> ', mobileNumber);
                if (mobileNumber) {
                    document.location.href = "tel:" + mobileNumber;
                }
            }

            /**
             * Open makani website
             */
            $scope.openWebsite = function () {
                window.open($DMLinks.makani, "_blank", 'location=yes');
            };

            function prepareRequest() {

                var SLA = '';
                var srv = _.find(allServices, function (item, index) {
                    return item.serviceEN == $scope.trackingDetails.ServiceEN;
                });

                if (srv) {
                    SLA = ( (parseInt(srv.sla) % 7) > 0) ? (parseInt(srv.sla) / 7) + 1 : (parseInt(srv.sla) / 7);
                    $scope.trackingDetails.SLA = Math.round(SLA);
                    $scope.trackingDetails.SLA = Math.round(SLA);
                }


                var format="Do MMM [at] h:mm A";
                if($scope.currentLang==="ar"){
                    format="Do MMM [على الساعة] h:mm A";
                }
                $scope.trackingDetails.RequestCreatedDate = moment($scope.trackingDetails.RequestCreatedDate, ['DD/MM/YYYY HH:mm', moment.ISO_8601]).format(format);

                //var StatusDate = moment($scope.trackingDetails.RequestStatusDate, ['DD/MM/YYYY HH:mm', moment.ISO_8601]);
                // var today = moment(moment().format('DD/MM/YYYY HH:mm'),['DD/MM/YYYY HH:mm', moment.ISO_8601]);
                //var duration = moment.duration(StatusDate.diff(today)).asHours();
                //duration = Math.abs(duration);

                allServices.some(function (item, index) {
                    if (item.serviceEN == $scope.trackingDetails.ServiceEN) {
                        $scope.trackingDetails.service_title_en = item.service_title_en;
                        $scope.trackingDetails.service_title_ar = item.service_title_ar;
                        $scope.trackingDetails.description_ar = item.description_ar;
                        $scope.trackingDetails.description_en = item.description_en;
                        return;
                    }
                });

                switch ($scope.trackingDetails.RequestStatus) {
                    case 'Initiation' : {
                        $scope.trackingDetails.ServiceENHighlighted = 'Received';
                        $scope.trackingDetails.ServiceARHighlighted = 'تم الاستلام';

                        $scope.trackingDetails.ServiceENDetailed = 'Initiation';
                        $scope.trackingDetails.ServiceARDetailed = 'تم الاستلام';
                        break;
                    }
                    case 'Under Processing' : {
                        if ($scope.trackingDetails.RequestUserFullName == '' || ($scope.trackingDetails.RequestUserFullName && $scope.trackingDetails.RequestUserFullName.indexOf('Group:') >= 0)) {
                            $scope.trackingDetails.ServiceENHighlighted = 'Received';
                            $scope.trackingDetails.ServiceARHighlighted = 'تم الاستلام';

                            $scope.trackingDetails.ServiceENDetailed = 'Initiation';
                            $scope.trackingDetails.ServiceARDetailed = 'تم الاستلام';
                        } else {
                            $scope.trackingDetails.ServiceENHighlighted = 'In Progress';
                            $scope.trackingDetails.ServiceARHighlighted = 'في تَقَدم';

                            $scope.trackingDetails.ServiceENDetailed = 'Under Processing';
                            $scope.trackingDetails.ServiceARDetailed = 'قيد التنفيذ';
                        }

                        break;
                    }
                    case 'Canceled' : {
                        $scope.trackingDetails.ServiceENHighlighted = 'Completed';
                        $scope.trackingDetails.ServiceARHighlighted = 'مكتملة';

                        $scope.trackingDetails.ServiceENDetailed = 'Cancelled';
                        $scope.trackingDetails.ServiceARDetailed = 'ألغيت';
                        break;
                    }
                    case 'Rejected' : {
                        $scope.trackingDetails.ServiceENHighlighted = 'Completed';
                        $scope.trackingDetails.ServiceARHighlighted = 'مكتملة';

                        $scope.trackingDetails.ServiceENDetailed = 'Rejected';
                        $scope.trackingDetails.ServiceARDetailed = 'رفض';
                        break;
                    }
                    case 'Approved' : {
                        $scope.trackingDetails.ServiceENHighlighted = 'Completed';
                        $scope.trackingDetails.ServiceARHighlighted = 'مكتملة';

                        $scope.trackingDetails.ServiceENHighlightedView = 'Accomplished';
                        $scope.trackingDetails.ServiceARHighlightedView = 'مكتملة';

                        $scope.trackingDetails.ServiceENDetailed = 'Accomplished'; // or Unaccomplished
                        $scope.trackingDetails.ServiceARDetailed = 'مكتملة';
                        break;
                    }
                    case 'Closed' : {
                        $scope.trackingDetails.ServiceENHighlighted = 'Completed';
                        $scope.trackingDetails.ServiceARHighlighted = 'مكتملة';

                        $scope.trackingDetails.ServiceENHighlightedView = 'Unaccomplished';
                        $scope.trackingDetails.ServiceARHighlightedView = 'غير مكتملة';

                        $scope.trackingDetails.ServiceENDetailed = 'Unaccomplished';
                        $scope.trackingDetails.ServiceARDetailed = 'غير مكتملة';
                        break;
                    }
                }

                $scope.requestDetails = $scope.trackingDetails;

            }


            /////////////////   tracking Map ////////////////////

            /*angular.element(document).ready(function () {
             setTimeout(function () {

             $("#mediaGallery").lightGallery({
             fullScreen: false
             });


             }, 2000);

             });*/

        }
    ]
).directive('lightgallery', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            if (scope.$last) {
                element.parent().lightGallery();
            }
        }
    };
})
