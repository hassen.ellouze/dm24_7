
/* JavaScript content from js/services/auth.service.js in folder common */
iDubaiApp.factory('$auth',
    ['$q', '$rootScope', 'LoadingService', '$state', '$procedures', '$filter', '$logger', 'CommonUtils', 'ConfigAdapters',
        function ($q, $rootScope, LoadingService, $state, $procedures, $filter, $logger, CommonUtils, ConfigAdapters) {

            var securityCheckName = 'UserLogin',
                authInProgress = false,
                _$scope = null,
                challengeHandler = null,
                activeUser = null;

            var isRememberMe = false;
            var isSubmitted = false;
            var currentLang = localStorage.getItem('dm-lang');
            var lang = "";
            if (currentLang === "en") {
                lang = "en_US";
            } else {
                lang = "ar_AE";
            }

            function goHome() {
                $state.go('dashboard',{}, {reload: true});
            }

            function goToPersonalDetailSuccess(){
                $state.go('personalDetails_1',{
                    from :'dubaiIDSucess'
                })
            }

            function goLogin() {
                $rootScope.loginError = false;
                $rootScope.loginMessage = '';
            }

            function getMessage(codeError) {
                var message = $filter('translate')(codeError);
                return (message) ? message : 'Code error : ' + codeError;
            }

            function clearContext() {
                localStorage.removeItem("iDubaiAuthToken");
                localStorage.removeItem("encryptedUser");
                localStorage.removeItem("username");
                WL.Client.removeGlobalHeader('ENC-USER');
                service.token = null;
                service.needLogin = true;
                service.dubaiUserProfile = null;
                $rootScope.loginError = false;
                $rootScope.loginMessage = '';
                isSubmitted = false;
            }

            //Init context
            function initContext(response) {
                try {
                    LoadingService.hide();
                    localStorage.removeItem("isRememberMe");
                    service.token = response.responseJSON.userIdentity.attributes.token;
                    service.needLogin = false;
                    service.dubaiUserProfile = response.responseJSON.userIdentity.attributes.userInfo;
                    localStorage.setItem("iDubaiAuthToken", service.token);
                    /*
                    WL.Client.updateUserInfo({
                        onSuccess: function () {
                            LoadingService.hide();
                        }
                    });
                    */
                }catch (e) {
                    console.log(e);
                }

            }

            challengeHandler = WL.Client.createSecurityCheckChallengeHandler(securityCheckName);
            challengeHandler.securityCheckName = securityCheckName;
            challengeHandler.handleChallenge = function (challenge) {
                console.log("handleChallenge ", challenge);
                authInProgress = true;
                LoadingService.hide();
                if (challenge.errorMsg !== null && _$scope) {
                    _$scope.$emit('login-error', {
                        message: challenge.errorMsg
                    });
                } else {
                    // redirect to login page
                    $rootScope.$emit('login-challenge');
                }


            };

            $rootScope.currentUserConnected = null;
            challengeHandler.handleSuccess = function (data) {
                console.log("handleSuccess ", data);
                try {
                    authInProgress = false;

                    activeUser = $rootScope.currentUserConnected.responseJSON.userInfo;
                    // initContext(data);
                    initContext($rootScope.currentUserConnected);
                    goHome();
                }catch (e) {
                    console.log(e);
                }
                /*
                if(_$scope) {
                    _$scope.$emit('login-success', {
                        data: data
                    });
                }
                */
            };

            challengeHandler.handleFailure = function (error) {
                console.log("handleFailure ", error);
                authInProgress = false;

                /*
                var message = error.failure !== null ? error.failure : 'Failed to login.';

                if(_$scope) {
                    _$scope.$emit('login-error', {
                        message: message
                    });
                }
                */
            };
            /*
            var challengeHandler = WL.Client.createChallengeHandler('iDubaiRealm');
            challengeHandler.isCustomResponse = function (response) {
                if (!response || !response.responseJSON || response.responseText === null) {
                    return false;
                }
                if (typeof (response.responseJSON.authRequired) !== 'undefined') {
                    if (response.responseJSON.authRequired == true) {
                        challengeHandler.activeRequest = null;
                    }
                    return true;
                } else {
                    return false;
                }
            };

            challengeHandler.handleChallenge = function (response) {
                var hasGlobalHeader = (WL.Client.__getGlobalHeaders()['ENC-USER'] != undefined);
                var hasLocalStorage = (localStorage.encryptedUser != undefined );
                if (hasGlobalHeader) { //Re-login response
                    challengeHandler.handleRelogin(response);
                } else {
                    if (!hasLocalStorage) { // It's a login response
                        challengeHandler.handleLoginResponse(response);
                    } else {// it's bootstrap
                        challengeHandler.handleBootstrap(response);
                    }
                }

            };

            // Handle Bootstrap
            challengeHandler.handleBootstrap = function (response) {
                var authRequired = response.responseJSON.authRequired;
                var afterUserInfoRefresh = function () {
                    if (WL.Client.isUserAuthenticated('iDubaiRealm')) {
                        var attributes = WL.Client.getUserInfo('iDubaiRealm', "attributes");
                        WL.Client.addGlobalHeader('ENC-USER', attributes.userToken);
                        initContext(response);
                    } else {
                        var hasLocalStorage = (localStorage.encryptedUser != undefined );
                        if (hasLocalStorage) {
                            clearContext();
                        }
                    }
                };
                if (authRequired == true) {
                    WL.Client.updateUserInfo({
                        onSuccess: function (resp) {
                            afterUserInfoRefresh();
                        },
                        onFailure: function (resp) {
                            afterUserInfoRefresh();
                        }
                    });
                } else {
                    var attributes = WL.Client.getUserInfo('iDubaiRealm', "attributes");
                    WL.Client.addGlobalHeader('ENC-USER', attributes.userToken);
                    initContext(response);
                }
            };

            // Handle Login
            challengeHandler.handleLoginResponse = function (response) {
                var authRequired = response.responseJSON.authRequired;
                if (authRequired == true) {
                    if (response.responseJSON.errorMessage) {
                        $rootScope.loginError = true;
                        $rootScope.loginMessage = getMessage(response.responseJSON.errorMessage);
                    }
                    $rootScope.$apply();
                    LoadingService.hide();
                } else {
                    if (isRememberMe) {
                        localStorage.setItem('encryptedUser', response.responseJSON.userIdentity.attributes.userToken);
                        WL.Client.addGlobalHeader('ENC-USER', response.responseJSON.userIdentity.attributes.userToken);
                    }
                    initContext(response);
                    $rootScope.isLoggedIn = true;
                    //goHome();
                    goToPersonalDetailSuccess();
                }
            };

            // Handle Relogin
            challengeHandler.handleRelogin = function (response) {
                try {
                    var authRequired = response.responseJSON.authRequired;
                    if (authRequired == true) {
                        var input = {
                            adapter: "Authentication24Adapter",
                            procedure: "autoLogin",
                            parameters: [localStorage.getItem('encryptedUser'), lang]
                        };
                        WL.Client.invokeProcedure(input, {
                            onSuccess: function (response) {
                                def.resolve(response);
                            },
                            onFailure: function (err) {
                                def.reject(err);
                            }
                        });
                    } else {
                        initContext(response);
                        goHome();
                    }
                } catch (exception) {
                    $logger.log('Error in handleRelogin', exception);
                    LoadingService.hide();
                    initContext(response);
                    goHome();
                }
            };
            */

            var service = {
                token: null,
                needAuth: false,
                needLogin: true,
                dubaiUserProfile: null,
                showLoginError: false,
                loginMessage: '',

                loginOnStartup: function () {
                    console.log('==> loginOnStartup ');
                    var def = $q.defer();
                    var encryptedUser = localStorage.getItem('encryptedUser');
                    try {
                        if (encryptedUser) {
                            var afterConnect = function () {
                                var input = {
                                    adapter: "Authentication24Adapter",
                                    procedure: "autoLogin",
                                    parameters: [encryptedUser, lang]
                                };
                                WL.Client.invokeProcedure(input, {
                                    onSuccess: function (response) {
                                        def.resolve(response);
                                    },
                                    onFailure: function (err) {
                                        goLogin();
                                        def.reject(err);
                                    }
                                });
                            };
                            if (!WL.Client.isConnected()) {
                                WL.Client.connect({
                                    onSuccess: function (response) {
                                        afterConnect();
                                        $logger.log('Successfully', response);
                                    },
                                    onFailure: function (err) {
                                        afterConnect();
                                        $logger.log('Error connection to server',
                                            err);
                                    }
                                });
                            } else {
                                afterConnect();
                            }
                        } else {
                            def.reject(encryptedUser);
                        }
                    } catch (exc) {
                        $logger.log('Error in load in startup', exc);
                    }
                    return def.promise;
                },

                loginMyId: function (username, password, _isRememberMe) {
                    var logindef = $q.defer();
                    isRememberMe = _isRememberMe;
                    if (!isRememberMe && localStorage.getItem('encryptedUser')) {
                        localStorage.removeItem("encryptedUser");
                    }
                    CommonUtils.invokeGetResources({
                        path: ConfigAdapters.Authentication24Adapter.authenticate,
                        params: [username, password, _isRememberMe]
                    }).then(function (result) {
                        console.log('result ', result);
                        try {
                            $rootScope.currentUserConnected = result;
                            if( result && result.responseJSON && result.responseJSON.errorMessage){
                                LoadingService.hide();
                                logindef.resolve(result);
                            }else{
                                if( authInProgress ){
                                    LoadingService.hide();
                                    logindef.resolve(result);
                                    challengeHandler.submitChallengeAnswer({
                                        'username':username,
                                        'password':password,
                                        rememberMe: _isRememberMe
                                    });

                                } else {
                                    WLAuthorizationManager.login(securityCheckName,{'username':username, 'password':password, rememberMe: _isRememberMe}).then(
                                        function () {
                                            WL.Logger.debug("login onSuccess");
                                            LoadingService.hide();
                                            logindef.resolve({});
                                        },
                                        function (response) {
                                            console.log('erreur ', response);
                                            WL.Logger.debug("login onFailure: " + JSON.stringify(response));
                                            LoadingService.hide();
                                            logindef.resolve(response);
                                        });
                                }
                            }
                        }catch (e) {
                            console.log(e);
                        }
                    }, function (error) {
                        console.log("error ", error);
                        LoadingService.hide();
                        logindef.resolve("UM005");
                    })
                    /*
                    var input = {
                        parameters: [username, password, _isRememberMe],
                        adapter: "Authentication24Adapter",
                        procedure: "authenticate"
                    };
                    challengeHandler.submitAdapterAuthentication(input, {
                        onSuccess: function (resp) {
                            LoadingService.hide();
                            logindef.resolve(resp);
                        },
                        onFailure: function (err) {
                            LoadingService.hide();
                            logindef.resolve("UM005");
                        }
                    });
                    */
                    return logindef.promise;
                },

                loginByOTP: function (username, email, mobile, _isRememberMe) {
                    var logindef = $q.defer();
                    isRememberMe = _isRememberMe;
                    if (!isRememberMe && localStorage.getItem('encryptedUser')) {
                        localStorage.removeItem("encryptedUser");
                    }
                    var input = {
                        parameters: [username, email, mobile],
                        adapter: "Authentication24Adapter",
                        procedure: "authenticateByOTP"
                    };
                    challengeHandler.submitAdapterAuthentication(input, {
                        onSuccess: function (resp) {
                            LoadingService.hide();
                            logindef.resolve(resp);
                        },
                        onFailure: function (err) {
                            LoadingService.hide();
                            logindef.resolve("UM005");
                        }
                    });
                    return logindef.promise;
                },


                logout: function () {
                    if (WL.Client) {
                        WL.Client.logout('iDubaiRealm', {
                            onSuccess: function () {
                                clearContext();
                                WL.Client.updateUserInfo();
                            },
                            onFailure: function () {
                                WL.Client.updateUserInfo();
                            }
                        });
                    }
                    delete $rootScope.username;
                    clearContext();
                }
            };

            return service;
        }
    ]
);