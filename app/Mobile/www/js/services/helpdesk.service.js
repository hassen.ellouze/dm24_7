
/* JavaScript content from js/services/helpdesk.service.js in folder common */
iDubaiApp.factory('helpdeskService',
		['$q', '$rootScope','LoadingService','$state','$procedures', '$filter','$logger', 'CommonUtils','ConfigAdapters',
		function ($q, $rootScope,LoadingService,$state,$procedures, $filter, $logger, CommonUtils, ConfigAdapters) {
			
		var service = {
				senMail : senMail,
            senRateMail : senRateMail
		};
		
		function senMail( type, options){
			var def = $q.defer();
            CommonUtils.invokeGetResources({
                path: ConfigAdapters.EmailAdapter.sendEmail,
                params: [ type, options]
            }).then(function (result) {
                console.log("result ", result);
                def.resolve(result);
            }, function (error) {
                console.log("error ", error);
                def.reject(error);
            });
		  	/*
			var input = {
						adapter: "EmailAdapter",
						procedure: "sendEmail",
						parameters: [ type, options]
					};
		  	WL.Client.invokeProcedure(input, {
					onSuccess: function(response){
						def.resolve(response);
					},
					onFailure: function(err){
						def.reject(err);
					}
		   	});
		  	*/

		  	return def.promise;
		}

            function senRateMail( type, options){
                var def = $q.defer();
                CommonUtils.invokeGetResources({
                    path: ConfigAdapters.EmailAdapter.senRateMail,
                    params: [type, options]
                }).then(function (result) {
                    console.log("result ", result);
                    def.resolve(result);
                }, function (error) {
                    console.log("error ", error);
                    def.reject(error);
                });
                /*
                var input = {
                    adapter: "EmailAdapter",
                    procedure: "senRateMail",
                    parameters: [ type, options]
                };
                WL.Client.invokeProcedure(input, {
                    onSuccess: function(response){
                        def.resolve(response);
                    },
                    onFailure: function(err){
                        def.reject(err);
                    }
                });
                */
                return def.promise;
            }
		
		return service;
		
	}]);
