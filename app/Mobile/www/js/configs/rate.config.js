
/* JavaScript content from js/configs/rate.config.js in folder common */
iDubaiApp.config(['$cordovaAppRateProvider',
        function ($cordovaAppRateProvider) {
            var prefs = {
                language: localStorage.getItem('dm-lang'),
                appName: 'DM 247 App',
                usesUntilPrompt: 20,
                iosURL: '1234932966',
                androidURL: 'market://details?id=com.iDubai24'
            };

            document.addEventListener("deviceready", function () {
                if (localStorage.getItem('counter') == null) {
                    var counter = {
                        applicationVersion: void 0,
                        countdown: 0,
                        firstTime: false,
                        enabled: true
                    };
                    localStorage.setItem('counter', JSON.stringify(counter));
                }
                $cordovaAppRateProvider.setPreferences(prefs);
            }, false);
        }
    ]
);
