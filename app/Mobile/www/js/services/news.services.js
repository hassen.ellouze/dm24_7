
/* JavaScript content from js/services/news.services.js in folder common */
iDubaiApp.factory("WCMService",
    ['$q', 'utils', '$logger', 'LoadingService', '$FileIO', '$http', '$DbStorageService', '$config',
        function ($q, utils, $logger, LoadingService, $FileIO, $http, $DbStorageService, $config) {


            function getDMNews() {
                console.log('==> getDMNews');
                var currentLang = localStorage.getItem('dm-lang');
                var deferred = $q.defer();
                var invocationData = {
                    adapter: 'WCM_Adapter',
                    procedure: 'getNews',
                    parameters: [currentLang]
                };

                WL.Client.invokeProcedure(invocationData, {
                    onSuccess: $.proxy(function (data) {
                        if (data.invocationResult.isSuccessful) {
                            deferred.resolve(data.invocationResult.rss.channel.item);
                        } else {
                            deferred.reject({});
                        }
                    }, this),
                    onFailure: $.proxy(function (error) {
                        deferred.reject(error);
                        $logger.log("Failed to get new WCM data");
                    }, this)
                });

                return deferred.promise;
            }

            function getWCMData() {
                var deferred = $q.defer();
                var invocationData = {
                    adapter: 'WCM_Adapter',
                    procedure: 'getWCMData',
                    parameters: []
                };
                $DbStorageService.get('wcm_json_store').then(function (responseData) {
                    //Data exist
                    var data = JSON.parse(responseData.value);
                    deferred.resolve(data.wcm);
                    var t = new Date().getTime();
                    var hourDiff = t - data.cacheTime; //in ms
                    var diffMins = (hourDiff / 60 / 1000);
                    if (data.cacheTime === null || (diffMins > $config.cacheTimes.wcmCacheTime)) {
                        $logger.info("invoke : ");
                        //$('#ui-butterbar-id').removeClass('hide').addClass('active');
                        WL.Client.invokeProcedure(invocationData, {
                            onSuccess: $.proxy(function (res) {
                                if (res.invocationResult.isSuccessful) {
                                    $logger.info("invoke : isSuccessful ", res.invocationResult.isSuccessful);
                                    if (res.invocationResult.wcm) {
                                        $logger.info("invoke wcm : ", res.invocationResult.wcm);
                                        $DbStorageService.remove('wcm_json_store').then(function (resp) {
                                            $logger.info('Wcm Data is removed : ', resp);
                                            var temp = {
                                                wcm: res.invocationResult.wcm,
                                                cacheTime: t
                                            };
                                            $DbStorageService.add('wcm_json_store', JSON.stringify(temp)).then(function (resp) {
                                                $logger.info("Wcm Data is saved");
                                            }, function (err) {
                                                $logger.log("Failed to save wcm data");
                                            });
                                        }, function (err) {
                                            $logger.log("Failed to remove wcm data");
                                        });
                                    }
                                    //$('#ui-butterbar-id').addClass('hide').removeClass('active');
                                }
                            }, this),
                            onFailure: $.proxy(function (error) {
                                //$('#ui-butterbar-id').addClass('hide').removeClass('active');
                                $logger.log("WS : Failed to get new WCM data");
                            }, this)
                        });
                    }
                }, function (error) {
                    $logger.error("Data does not exist ");
                    //Data does not exist
                    WL.Client.invokeProcedure(invocationData, {
                        onSuccess: $.proxy(function (res) {
                            LoadingService.hide();
                            if (res.invocationResult.isSuccessful) {

                                deferred.resolve(res.invocationResult.wcm);
                                var t = new Date().getTime();
                                var temp = {
                                    wcm: res.invocationResult.wcm,
                                    cacheTime: t
                                };
                                if (res.invocationResult.wcm) {
                                    $DbStorageService.add('wcm_json_store', JSON.stringify(temp)).then(function (resp) {
                                        $logger.log("Wcm Data is saved");
                                    }, function (err) {
                                        $logger.log("Failed to save wcm data");
                                    });
                                }
                            }
                        }, this),
                        onFailure: $.proxy(function (error) {
                            $logger.log("WS : Failed to get new WCM data");
                            LoadingService.hide();
                            deferred.reject({});
                        }, this)
                    });
                });
                return deferred.promise;
            }

            return {
                getWCMData: getWCMData,
                getDMNews: getDMNews
            };
        }
    ]
);
