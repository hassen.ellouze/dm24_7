var username = 'dm360_sys';
var password = 'dUbaI_MunP@ss';

function queryMobileNotifications(phone) {
	try {
		// CRMQueryAPI
		//STGDM360.dm.gov.ae/NotificationsSystem/api/ STAGE
		//DM360.dm.gov.ae/NotificationsSystem/api  PROD
		var encoded = com.dm.common.utils.Base64.encodeString(username + ":" + password);
		var input = {
			method: 'GET',
			path: "STGDM360.dm.gov.ae/NotificationsSystem/api/CRMTicket/GetStatusByPhoneNumber?MobileNum=" + phone,
			headers: {
				'Authorization': 'basic ' + encoded,
			},
			returnedContentType: 'json',
			returnedContentEncoding: 'UTF-8',

		};

		MFP.Logger.error("queryMobileNotification :: request  : " + JSON.stringify(input));
		var x = MFP.Server.invokeHttp(input);
		MFP.Logger.error("queryMobileNotification :: response : " + JSON.stringify(x));
		return x ;


	} catch(e) {
        return {
            isSuccessful: false,
            reference: e
        };
	}
}

function getMyLatestNotifications(phone){
	return queryMobileNotifications(phone);
//    if ( result.statusCode == '200' && result.array.length >0 ) {
//    	result.array.filter(function(el){
//    		return el.RequestServiceCode === 'Notification';
//    	});
//    }
}

function getMyLatestServices(phone){
	return   queryMobileNotifications(phone);
}
