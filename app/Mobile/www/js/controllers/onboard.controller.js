iDubaiApp.controller('onboardController', ['$scope', '$state', '$translate', '$rootScope', 'GuestUtils', function ($scope, $state, $translate, $rootScope, GuestUtils) {
    $scope.isShowing = false;
    window.db = window.openDatabase('DM247', '1.0', 'todo list example db', 2 * 1024 * 1024);
    window.db.transaction(function (txDB) {
        var query = "SELECT name FROM sqlite_master WHERE type='table' AND name='language'";
        var query1 = "SELECT currentLang FROM language";

        txDB.executeSql("SELECT name FROM sqlite_master WHERE type='table' AND name='personalDetails'", [], function (txp, resp) {
            //console.log("if PersonalDetails exists", JSON.stringify(txp), JSON.stringify(resp));
            if (resp.rows.length > 0) {
                //console.log('personal Details exists');
                txDB.executeSql("SELECT * FROM personalDetails", [], function (txpp, respp) {
                    //console.log('selecting personal details', JSON.stringify(txpp), JSON.stringify(respp));
                    if (respp.rows.length > 0) {
                       /* console.log(JSON.stringify(respp.rows.item(0).mobileNumber));
                        console.log(JSON.stringify(respp.rows.item(0).name));
                        console.log(JSON.stringify(respp.rows.item(0).email));
                        console.log(JSON.stringify(respp.rows.item(0).country));
                        console.log(JSON.stringify(respp.rows.item(0).option1));
                        console.log(JSON.stringify(respp.rows.item(0).option2));
                        console.log(JSON.stringify(respp.rows.item(0).option3));
                        console.log(JSON.stringify(respp.rows.item(0).option4));
                        console.log(JSON.stringify(respp.rows.item(0).mobileArea));*/
                        GuestUtils.saveGuestInfo(respp.rows.item(0).mobileNumber,
                            respp.rows.item(0).name,
                            respp.rows.item(0).email,
                            respp.rows.item(0).country,
                            respp.rows.item(0).option1,
                            respp.rows.item(0).option2,
                            respp.rows.item(0).option3,
                            respp.rows.item(0).option4,
                            respp.rows.item(0).mobileArea)
                    }
                })
            }
        })

        txDB.executeSql(query, [], function (tx, resultSet) {
               /* console.log("if language exists", JSON.stringify(tx), JSON.stringify(resultSet));
                for (var x = 0; x < resultSet.rows.length; x++) {
                    console.log("CurrentLang " + resultSet.rows.item(x));
                }*/

                if (resultSet.rows.length == 0) {
                    //console.log('entering rows == 0');
                    txDB.executeSql('CREATE TABLE IF NOT EXISTS language (currentLang)', [], function (tx1, res1) {
                        //console.log('CREATE TABLE IF NOT EXISTS language', JSON.stringify(tx1), JSON.stringify(res1));

                        txDB.executeSql('INSERT INTO language VALUES (?)', [''], function (tx11, res11) {
                            //console.log('NSERT INTO language VALUES ', JSON.stringify(tx11), JSON.stringify(res11));

                            $scope.isShowing = true;
                            $scope.$apply();
                            WL.App.hideSplashScreen();

                        });

                    });


                    txDB.executeSql('CREATE TABLE IF NOT EXISTS personalDetails (mobileNumber, name, email, country, option1, option2, option3, option4, mobileArea )', [], function (tx1, res1) {
                        //console.log('CREATE TABLE IF NOT EXISTS personalDetails ', JSON.stringify(tx1), JSON.stringify(res1));
                    });
                } else {
                    //console.log('entering rows > 0');
                    txDB.executeSql('SELECT currentLang FROM language', [], function (tx2, res2) {
                        var lang = res2.rows.item(0).currentLang;
                        //localStorage.setItem('dm-lang', lang);
                        $("html").attr("lang", lang);
                        $translate.use(lang);
                        $rootScope.$currentLanguage = lang;
                        if (lang == 'en') {

                            $('html').css('direction', 'ltr');
                        } else {
                            $('html').css('direction', 'rtl')
                        }

                        //console.log('SELECT currentLang FROM language', JSON.stringify(tx2), JSON.stringify(res2));

                        $state.go('dashboard');
                        WL.App.hideSplashScreen();

                    });

                }


            },
            function (tx, error) {
                console.log('SELECT error: ' + error.message);


            });


        txDB.executeSql("SELECT name FROM sqlite_master WHERE type='table' AND name='mostPopular'", [], function (txPop, resPop) {
            if (resPop.rows.length > 0) {
            } else {
                txDB.executeSql('CREATE TABLE IF NOT EXISTS mostPopular (id)', [], function (txPop1, resPop1) {
                    //console.log('CREATE TABLE IF NOT EXISTS mostPopular', JSON.stringify(txPop1), JSON.stringify(resPop1));
                    txDB.executeSql('INSERT INTO mostPopular VALUES (?)', ['Construction Noise']);
                    txDB.executeSql('INSERT INTO mostPopular VALUES (?)', ['general waste removel ']);
                    txDB.executeSql('INSERT INTO mostPopular VALUES (?)', ['Bulky Waste Removal-household']);
                    txDB.executeSql('INSERT INTO mostPopular VALUES (?)', ['Stray Cats']);
                    txDB.executeSql('INSERT INTO mostPopular VALUES (?)', ['control agricultural leison']);
                    txDB.executeSql('INSERT INTO mostPopular VALUES (?)', ['Obstruction of Sewage network']);
                    txDB.executeSql('INSERT INTO mostPopular VALUES (?)', ['water Leakage']);
                    txDB.executeSql('INSERT INTO mostPopular VALUES (?)', ['Sprinkling Pests house Request']);
                    txDB.executeSql('INSERT INTO mostPopular VALUES (?)', ['Cleanliness of food company']);
                    txDB.executeSql('INSERT INTO mostPopular VALUES (?)', ['Treating sick animals on farm']);


                });
            }
        })
    }, function (error) {
        console.log('transaction error: ' + error.message);
    }, function () {
        console.log('transaction ok');
    });




    //$scope.currentLang= $('html').attr('lang')
    $scope.currentLang = localStorage.getItem('dm-lang');
    console.log($scope.currentLang);


    $scope.spotWith = document.querySelectorAll(".welcome-slider")[0].clientWidth;
    $scope.snapPos = [0, -($scope.spotWith), -($scope.spotWith * 2), -($scope.spotWith * 3), -($scope.spotWith * 4)];
    $scope.snapPos1 = [0, ($scope.spotWith), ($scope.spotWith * 2), ($scope.spotWith * 3), ($scope.spotWith * 4)];

    // $scope.snaps =$scope.currentLang == 'ar' ? $scope.snapPos1 : $scope.snapPos;

    $('.btnENG').on( "click", function() {
        $scope.currentLang = 'en';
        $scope.snaps = $scope.snapPos;
    });

    $('.btnAR').on( "click", function() {
        $scope.currentLang = 'ar';
        $scope.snaps = $scope.snapPos1;
    });
    $('.btnLan').click(function () {
        $('.welcomeMessage').addClass('langUp');
        $('.onboard-screens').addClass('isOnboardBottom');
        $('#Clouds').addClass('isOnboardTop');

    });


    $('.welcome-skips').click(function () {
        var onboardBG1 = document.getElementById("onboardBG1");
        var onboardBG2 = document.getElementById("onboardBG2");
        var onboardBG3 = document.getElementById("onboardBG3");
        var onboardBG4 = document.getElementById("onboardBG4");
        var spotHoler = document.getElementById("spot-holder");
        if ($scope.currentLang === 'ar') {
            console.log('its arab');
            onboardBG1.classList.add("animate2");
            onboardBG2.classList.add("animate2");
            onboardBG3.classList.add("animate2");
            onboardBG4.classList.add("animate2");
            spotHoler.classList.add("animate2");
            onboardBG1.style.webkitTransform = 'translate3d(' + (($scope.spotWith * 3.19)) + 'px, 0px, 0px)';
            onboardBG2.style.webkitTransform = 'translate3d(' + (($scope.spotWith * 3.25)) + 'px, 0px, 0px)';
            // clouds2.style.webkitTransform = 'translate3d(' + (restPosition *0.4) + 'px, 0px, 0px)';
            onboardBG3.style.webkitTransform = 'translate3d(' + (($scope.spotWith * 2.4)) + 'px, 0px, 0px)';
            onboardBG4.style.webkitTransform = 'translate3d(' + (($scope.spotWith * 2.1)) + 'px, 0px, 0px)';
            // clouds2.style.transform = 'translate3d(' + (restPosition *0.4) + 'px, 0px, 0px)';
            onboardBG1.style.transform = 'translate3d(' + (($scope.spotWith * 3.19)) + 'px, 0px, 0px)';
            onboardBG2.style.transform = 'translate3d(' + (($scope.spotWith * 3.25)) + 'px, 0px, 0px)';
            onboardBG3.style.transform = 'translate3d(' + (($scope.spotWith * 2.4)) + 'px, 0px, 0px)';
            onboardBG4.style.transform = 'translate3d(' + (($scope.spotWith * 2.1)) + 'px, 0px, 0px)';
            spotHoler.style.transform = 'translate3d(' + ($scope.spotWith * 4) + 'px, 0px, 0px)';
            // $timeout(function(){
            //     $location.path("/dashboard");
            //     $('')
            //
            // },1000);
            $('.dot-item').removeClass('active');
            $('.dot-item.lastDot').addClass('active')
        } else {
            onboardBG1.classList.add("animate2");
            onboardBG2.classList.add("animate2");
            onboardBG3.classList.add("animate2");
            onboardBG4.classList.add("animate2");
            spotHoler.classList.add("animate2");
            onboardBG1.style.webkitTransform = 'translate3d(' + ((-$scope.spotWith * 3.19)) + 'px, 0px, 0px)';
            onboardBG2.style.webkitTransform = 'translate3d(' + ((-$scope.spotWith * 3.25)) + 'px, 0px, 0px)';
            // clouds2.style.webkitTransform = 'translate3d(' + (restPosition *0.4) + 'px, 0px, 0px)';
            onboardBG3.style.webkitTransform = 'translate3d(' + ((-$scope.spotWith * 2.4)) + 'px, 0px, 0px)';
            onboardBG4.style.webkitTransform = 'translate3d(' + ((-$scope.spotWith * 2.1)) + 'px, 0px, 0px)';
            // clouds2.style.transform = 'translate3d(' + (restPosition *0.4) + 'px, 0px, 0px)';
            onboardBG1.style.transform = 'translate3d(' + ((-$scope.spotWith * 3.19)) + 'px, 0px, 0px)';
            onboardBG2.style.transform = 'translate3d(' + ((-$scope.spotWith * 3.25)) + 'px, 0px, 0px)';
            onboardBG3.style.transform = 'translate3d(' + ((-$scope.spotWith * 2.4)) + 'px, 0px, 0px)';
            onboardBG4.style.transform = 'translate3d(' + ((-$scope.spotWith * 2.1)) + 'px, 0px, 0px)';
            spotHoler.style.transform = 'translate3d(' + (-$scope.spotWith * 4) + 'px, 0px, 0px)';
            $('.dot-item').removeClass('active');
            $('.dot-item.lastDot').addClass('active')
        }

    });
}]);

angular.module("iDubaiApp").directive('swipeAndSnaps', function ($location, $timeout) {
    return {

        link: function (scope, element, attr) {
            // var clouds2= document.getElementById("clouds2");
            var onboardBG1 = document.getElementById("onboardBG1");
            var onboardBG2 = document.getElementById("onboardBG2");
            var onboardBG3 = document.getElementById("onboardBG3");
            var onboardBG4 = document.getElementById("onboardBG4");
            scope.spotWith = document.querySelectorAll(".welcome-slider")[0].clientWidth;
            var snapLocations = JSON.parse(attr.snapLocations),
                restPosition = 0, // Define the location to end.
                positionX = 0; // The current position.
            scope.bestSnap = 0;
            $('.btnENG').on("click", function () {
                console.log($('html').attr('lang'));
                var calculate_snap_location = function (position) {

                    // Used to store each difference between current position and each snap point.
                    var currentDiff;

                    // Used to store the current best difference.
                    var minimumDiff;

                    // User to store the best snap position.
                    var bestSnap;

                    // We're going to cycle through each snap location
                    // and work out which is closest to the current position.
                    for (var i = 0; i < snapLocations.length; i++) {

                        // Calculate the difference.
                        currentDiff = Math.abs(positionX - snapLocations[i]);

                        // Works out if this difference is the closest yet.
                        if (minimumDiff === undefined || currentDiff < minimumDiff) {
                            minimumDiff = currentDiff;
                            bestSnap = snapLocations[i];
                        }
                    }

                    scope.bestSnap = bestSnap;
                    return bestSnap;

                };

                /**
                 * Perform any setup for the drag actions.
                 */
                Hammer(element[0]).on("dragstart", function (ev) {

                    // We dont want an animation delay when dragging.
                    element.removeClass('animate');
                    // clouds2.classList.remove("animate");
                    onboardBG1.classList.remove("animate");
                    onboardBG2.classList.remove("animate");
                    onboardBG4.classList.remove("animate");
                    onboardBG3.classList.remove("animate");
                });

                /**
                 * Follow the drag position when the user is interacting.
                 */
                Hammer(element[0]).on("drag", function (ev) {
                    // Set the current position.
                    positionX = restPosition + parseInt(ev.gesture.deltaX);
                    onboardBG1.style.webkitTransform = 'translate3d(' + (positionX * 0.8) + 'px, 0px, 0px)';
                    onboardBG2.style.webkitTransform = 'translate3d(' + (positionX * 1.2) + 'px, 0px, 0px)';
                    // clouds2.style.webkitTransform = 'translate3d(' + (positionX*0.4) + 'px, 0px, 0px)';
                    onboardBG3.style.webkitTransform = 'translate3d(' + (positionX * 0.4) + 'px, 0px, 0px)';
                    onboardBG4.style.webkitTransform = 'translate3d(' + (positionX * 0.1) + 'px, 0px, 0px)';
                    // clouds2.style.transform = 'translate3d(' + (positionX*0.4) + 'px, 0px, 0px)';
                    onboardBG1.style.transform = 'translate3d(' + (positionX * 0.8) + 'px, 0px, 0px)';
                    onboardBG2.style.transform = 'translate3d(' + (positionX * 1.2) + 'px, 0px, 0px)';
                    onboardBG3.style.transform = 'translate3d(' + (positionX * 0.4) + 'px, 0px, 0px)';
                    onboardBG4.style.transform = 'translate3d(' + (positionX * 0.1) + 'px, 0px, 0px)';
                    element.css('-webkit-transform', 'translate3d(' + positionX + 'px,0px,0px)');
                    element.css('transform', 'translate3d(' + positionX + 'px,0px,0px)');
                    // if ($('.welcome-dots .dot-item.lastDot').hasClass('active')){
                    //     $('.welcome-dots .dot-item.lastDot').toggleClass('active');
                    // }
                    // if (!$('.welcome-dots .dot-item.firstDot').hasClass('active')){
                    //     $('.welcome-dots .dot-item.firstDot').toggleClass('active');
                    // }

                });

                /**
                 * The drag is finishing so we'll animate to a snap point.
                 */
                Hammer(element[0]).on("dragend", function (ev) {
                    element.addClass('animate');
                    // clouds2.classList.add("animate");
                    onboardBG1.classList.add("animate");
                    onboardBG2.classList.add("animate");
                    onboardBG3.classList.add("animate");
                    onboardBG4.classList.add("animate");
                    // Work out where we should "snap" to.
                    restPosition = calculate_snap_location(positionX);
                    scope.$apply(function () {
                        scope.bestSnap = restPosition;
                    });

                    onboardBG1.style.webkitTransform = 'translate3d(' + (restPosition * 0.8) + 'px, 0px, 0px)';
                    onboardBG2.style.webkitTransform = 'translate3d(' + (restPosition * 1.2) + 'px, 0px, 0px)';
                    // clouds2.style.webkitTransform = 'translate3d(' + (restPosition *0.4) + 'px, 0px, 0px)';
                    onboardBG3.style.webkitTransform = 'translate3d(' + (restPosition * 0.4) + 'px, 0px, 0px)';
                    onboardBG4.style.webkitTransform = 'translate3d(' + (restPosition * 0.1) + 'px, 0px, 0px)';
                    // clouds2.style.transform = 'translate3d(' + (restPosition *0.4) + 'px, 0px, 0px)';
                    onboardBG1.style.transform = 'translate3d(' + (restPosition * 0.8) + 'px, 0px, 0px)';
                    onboardBG2.style.transform = 'translate3d(' + (restPosition * 1.2) + 'px, 0px, 0px)';
                    onboardBG3.style.transform = 'translate3d(' + (restPosition * 0.4) + 'px, 0px, 0px)';
                    onboardBG4.style.transform = 'translate3d(' + (restPosition * 0.1) + 'px, 0px, 0px)';
                    element.css('-webkit-transform', 'translate3d(' + restPosition + 'px, 0px, 0px)');
                    element.css('transform', 'translate3d(' + restPosition + 'px, 0px, 0px)');
                });
            });
            $('.btnAR').on("click", function () {
                console.log($('html').attr('lang'));
                var arspotWitdh = document.querySelectorAll(".welcome-slider")[0].clientWidth;
                var calculate_snap_location = function (position) {

                    // Used to store each difference between current position and each snap point.
                    var currentDiff;

                    // Used to store the current best difference.
                    var minimumDiff;

                    // User to store the best snap position.
                    var bestSnap;

                    // We're going to cycle through each snap location
                    // and work out which is closest to the current position.
                    for (var i = 0; i < snapLocations.length; i++) {

                        // Calculate the difference.
                        currentDiff = Math.abs(positionX - snapLocations[i]);

                        // Works out if this difference is the closest yet.
                        if (minimumDiff === undefined || currentDiff < minimumDiff) {
                            minimumDiff = currentDiff;
                            bestSnap = snapLocations[i];
                        }
                    }

                    scope.bestSnap = bestSnap;
                    return bestSnap;

                };

                /**
                 * Perform any setup for the drag actions.
                 */
                Hammer(element[0]).on("dragstart", function (ev) {

                    // We dont want an animation delay when dragging.
                    element.removeClass('animate');
                    // clouds2.classList.remove("animate");
                    onboardBG1.classList.remove("animate");
                    onboardBG2.classList.remove("animate");
                    onboardBG4.classList.remove("animate");
                    onboardBG3.classList.remove("animate");
                });

                /**
                 * Follow the drag position when the user is interacting.
                 */
                Hammer(element[0]).on("drag", function (ev) {
                    // Set the current position.
                    positionX = -(restPosition + parseInt(ev.gesture.deltaX));
                    onboardBG1.style.webkitTransform = 'translate3d(' + (-positionX * 0.8) + 'px, 0px, 0px)';
                    onboardBG2.style.webkitTransform = 'translate3d(' + (-positionX * 1.2) + 'px, 0px, 0px)';
                    // clouds2.style.webkitTransform = 'translate3d(' + (positionX*0.4) + 'px, 0px, 0px)';
                    onboardBG3.style.webkitTransform = 'translate3d(' + (-positionX * 0.4) + 'px, 0px, 0px)';
                    onboardBG4.style.webkitTransform = 'translate3d(' + (-positionX * 0.1) + 'px, 0px, 0px)';
                    // clouds2.style.transform = 'translate3d(' + (positionX*0.4) + 'px, 0px, 0px)';
                    onboardBG1.style.transform = 'translate3d(' + (-positionX * 0.8) + 'px, 0px, 0px)';
                    onboardBG2.style.transform = 'translate3d(' + (-positionX * 1.2) + 'px, 0px, 0px)';
                    onboardBG3.style.transform = 'translate3d(' + (-positionX * 0.4) + 'px, 0px, 0px)';
                    onboardBG4.style.transform = 'translate3d(' + (-positionX * 0.1) + 'px, 0px, 0px)';
                    element.css('-webkit-transform', 'translate3d(' + (-positionX) + 'px,0px,0px)');
                    element.css('transform', 'translate3d(' + (-positionX) + 'px,0px,0px)');
                });

                /**
                 * The drag is finishing so we'll animate to a snap point.
                 */
                Hammer(element[0]).on("dragend", function (ev) {
                    element.addClass('animate');
                    // clouds2.classList.add("animate");
                    onboardBG1.classList.add("animate");
                    onboardBG2.classList.add("animate");
                    onboardBG3.classList.add("animate");
                    onboardBG4.classList.add("animate");
                    // Work out where we should "snap" to.
                    restPosition = -(calculate_snap_location(positionX));
                    console.log(restPosition);
                    scope.$apply(function () {
                        scope.bestSnap = restPosition;
                    });

                    onboardBG1.style.webkitTransform = 'translate3d(' + (restPosition * 0.8) + 'px, 0px, 0px)';
                    onboardBG2.style.webkitTransform = 'translate3d(' + (restPosition * 1.2) + 'px, 0px, 0px)';
                    // clouds2.style.webkitTransform = 'translate3d(' + (restPosition *0.4) + 'px, 0px, 0px)';
                    onboardBG3.style.webkitTransform = 'translate3d(' + (restPosition * 0.4) + 'px, 0px, 0px)';
                    onboardBG4.style.webkitTransform = 'translate3d(' + (restPosition * 0.1) + 'px, 0px, 0px)';
                    // clouds2.style.transform = 'translate3d(' + (restPosition *0.4) + 'px, 0px, 0px)';
                    onboardBG1.style.transform = 'translate3d(' + (restPosition * 0.8) + 'px, 0px, 0px)';
                    onboardBG2.style.transform = 'translate3d(' + (restPosition * 1.2) + 'px, 0px, 0px)';
                    onboardBG3.style.transform = 'translate3d(' + (restPosition * 0.4) + 'px, 0px, 0px)';
                    onboardBG4.style.transform = 'translate3d(' + (restPosition * 0.1) + 'px, 0px, 0px)';
                    element.css('-webkit-transform', 'translate3d(' + (restPosition) + 'px, 0px, 0px)');
                    element.css('transform', 'translate3d(' + (restPosition) + 'px, 0px, 0px)');
                });
            });
        }
    };

});
