iDubaiApp.controller('personalDetails_1Controller',
    ['$scope', 'OTP', 'PhoneUtils', '$filter', '$notification', '$timeout', 'GuestUtils', '$stateParams', '$state', '$auth', '$DMLinks', 'LoadingService', '$countries', 'SharedService', 'underscore',
        function ($scope, OTP, PhoneUtils, $filter, $notification, $timeout, GuestUtils, $stateParams, $state, $auth, $DMLinks, LoadingService, $countries, SharedService, underscore) {
            var _ = underscore;
            $stateParams = SharedService.getStateHistory($state.current.name);
            console.log('Personal Details 2');
            $scope.from = $stateParams.from;
            $scope.Payload = $stateParams.payload;
            $scope.loginMessage = "";

            /*{
             makani: $stateParams.fromRequestPayload.makani,
             locationName: $stateParams.fromRequestPayload.result,
             formattedAddress: $stateParams.fromRequestPayload.result,
             location: $stateParams.fromRequestPayload.location,
             makaniInfo: $stateParams.fromRequestPayload.makaniInfo,
             listServices: $stateParams.fromRequestPayload.listServices,
             requestText: $stateParams.fromRequestPayload.$scope.additionalInfo,
             };*/
            if ($stateParams.from && $stateParams.from === 'dashboard') {
                $scope.activePage = 6;
            } else if ($stateParams.from && $stateParams.from == 'dubaiIDSucess') {
                $scope.activePage = 5;
            }
            else {
                $scope.activePage = 1;
            }
            ;
            $scope.hideBtn = false;

            $scope.listCountries = $countries;

            $scope.currentLang = localStorage.getItem('dm-lang');
            $scope.delay = false;
            $scope.user = {
                name: '',
                country: 'AE',
                email: '',
                option1: false, // pet owner
                option2: false, // farm owner
                option3: false, // visitor
                option4: false, // employee of DM
                mobileArea: '+971',
                numOne: '',
                numTwo: '',
                numThree: '',
                numFour: ''
            };

            $scope.dubaiID = {
                username: '',
                password: ''
            };

            $scope.rememberLoginData = false;

            $scope.isValidNumber = function (mobileArea, mobileNumber) {
                console.log(mobileNumber);
                var valid = PhoneUtils.isValidPhoneNumber(mobileArea + mobileNumber);
                if (mobileNumber) {
                    return !valid;
                }
                // return true;
            }


            $scope.code = null;
            $scope.sendCode = function (user) {

                try {
                    if( user.mobileNumber ){
                        LoadingService.show();
                        $scope.mobile = user.mobileArea + '' + convertToDigit(user.mobileNumber);
                        $scope.transactionID = Math.floor(Math.random() * 900) + 1000;

                        // for testing //
                        /*$scope.activePage = 4;
                         $scope.code = $scope.transactionID;*/
                        // for testing //

                        console.log($scope.transactionID);

                        if (PhoneUtils.isValidPhoneNumber($scope.mobile)) {
                            if ($scope.mobile != "+971000000000") {
                                $scope.message = ($scope.currentLang == 'en') ? 'Your verification code for Dubai Municipality App is \n' + $scope.transactionID : 'رمز التحقق الخاص بك لتطبيق بلدية دبي هو \n' + $scope.transactionID;
                                console.log($scope.message);

                                $(document).unbind("onSMSArrive");
                                $(document).bind("onSMSArrive", function (e) {

                                    // {"address":"globfone","body":"Hello","date_sent":1544799027000,"date":1544799027380,"service_center":"+46731210226"}
                                    console.log('onSMSArrive : ', e);
                                    var IncomingSMS = e.originalEvent.data;
                                    console.log('sms.address:' + IncomingSMS.address);
                                    console.log('sms.body:' + IncomingSMS.body);
                                    console.log(JSON.stringify(IncomingSMS));

                                    var englishKeys = ["verification", "code", "app", "dubai", "municipality"];
                                    var arabicKeys = ["رمز", "التحقق", "لتطبيق", "دبي", "بلدية"];

                                    var engRes = _.filter(englishKeys, function (key) {
                                        return IncomingSMS.body.toLowerCase().indexOf(key) > -1;
                                    });
                                    var arRes = _.filter(arabicKeys, function (key) {
                                        return IncomingSMS.body.toLowerCase().indexOf(key) > -1;
                                    });

                                    if (engRes.length == englishKeys.length || arRes.length == arabicKeys.length) {

                                        var code = IncomingSMS.body.match(/\d+/);
                                        code = code ? code[0] : "";
                                        // var code = IncomingSMS.body.substring(IncomingSMS.body.length - 4);
                                        if (code.length == 4) {
                                            $scope.user.numOne = code[0];
                                            $scope.user.numTwo = code[1];
                                            $scope.user.numThree = code[2];
                                            $scope.user.numFour = code[3];
                                            $scope.checkCode();
                                            $scope.$apply();
                                        }
                                    }
                                });

                                if (WL.Client.getEnvironment() === WL.Environment.ANDROID) {
                                    window.plugins.smsReceive.startWatch(function () {
                                        console.log('smsreceive: watching started');
                                    }, function () {
                                        console.warn('smsreceive: failed to start watching');
                                    });
                                }

                                OTP.sendCode($scope.mobile, $scope.transactionID, $scope.message).then(function (result) {
                                    if (result) {
                                        $scope.code = result.TransactionID.CDATA;
                                        if ($scope.code) {
                                            $scope.activePage = 4;
                                        }
                                    }
                                    LoadingService.hide();
                                }, function (error) {
                                    LoadingService.hide();
                                    console.log('==> OTP : Something went wrong');
                                    $notification.alert($filter('translate')("Error in sending code, please try again"));
                                })
                            } else {
                                GuestUtils.saveGuestInfo($scope.mobile,
                                    $scope.user.name,
                                    ($scope.user.email) ? $scope.user.email : '',
                                    $scope.user.country,
                                    $scope.user.option1,
                                    $scope.user.option2,
                                    $scope.user.option3,
                                    $scope.user.option4,
                                    $scope.user.mobileArea);

                                $scope.activePage = 5;
                                LoadingService.hide();
                            }

                        } else {
                            LoadingService.hide();
                            $notification.alert($filter('translate')("invalidNumber"));

                        }
                    }
                }catch (e) {
                    console.log("error ", e);
                }

            };

            $scope.checkCode = function () {

                var enteredNum = $scope.user.numOne + $scope.user.numTwo + $scope.user.numThree + $scope.user.numFour;
                if (enteredNum == $scope.code) {
                    GuestUtils.saveGuestInfo($scope.mobile,
                        $scope.user.name,
                        ($scope.user.email) ? $scope.user.email : '',
                        $scope.user.country,
                        $scope.user.option1,
                        $scope.user.option2,
                        $scope.user.option3,
                        $scope.user.option4,
                        $scope.user.mobileArea);

                    $scope.activePage = 5;
                    setTimeout(function () {
                        LoadingService.hide();
                    }, 2000);


                    /*$auth.loginByOTP($scope.user.name, $scope.user.email, $scope.mobile, true).then(function (response) {
                     console.log('loginByOTP ==> '+response);
                     });*/

                    if (WL.Client.getEnvironment() === WL.Environment.ANDROID) {
                        window.plugins.smsReceive.stopWatch(function () {
                            console.log('smsreceive: watching stopped');
                        }, function () {
                            console.warn('smsreceive: failed to stop watching');
                        });
                    }
                } else {
                    LoadingService.hide();
                    $notification.alert($filter('translate')("Wrong code"));
                }
                console.log(enteredNum);
            };

            $scope.$watch(function () {
                if ($scope.activePage == 4) {
                    $timeout(function () {
                        $scope.delay = true;
                    }, 10000);
                }
            });

            function convertToDigit(string) {
                try {
                    if(string){
                        return string.replace(/[٠١٢٣٤٥٦٧٨٩]/g, function (c) {
                            return c.charCodeAt(0) - 1632;
                        }).replace(/[۰۱۲۳۴۵۶۷۸۹]/g, function (c) {
                            return c.charCodeAt(0) - 1776;
                        });
                    }
                    else{
                        return null;
                    }
                }catch (e) {

                }
            }

            $scope.goBack = function () {
                try {
                    if (WL.Client.getEnvironment() === WL.Environment.ANDROID) {
                        window.plugins.smsReceive.stopWatch(function () {
                            console.log('smsreceive: watching stopped');
                        }, function () {
                            console.warn('smsreceive: failed to stop watching');
                        });
                    }
                } catch (err) {
                    console.log(err);
                }
                if ($stateParams.from == 'smartRequest') {
                    $state.go($scope.from, {
                        payload: $scope.Payload,
                        from: 'personalDetails_1'
                    });
                } else if ($stateParams.from == 'createRequest') {
                    var payload = {
                        serviceInfo: $scope.Payload.serviceInfo,
                        makani: $scope.Payload.makani,
                        locationName: $scope.Payload.locationName,
                        formattedAddress: $scope.Payload.formattedAddress,
                        location: $scope.Payload.location,
                        additionalInfo: $scope.Payload.additionalInfo,
                        locationDetail: $scope.Payload.locationDetail,
                        from: 'personalDetails_1'
                    }
                    $state.go($scope.from, {
                        payload: payload
                    });
                } else if ($stateParams.from) {
                    $state.go($stateParams.from);
                } else {
                    $state.go('dashboard');
                }
            };


            $scope.goToDashboard = function () {
                if ($stateParams.from) {
                    $state.go($stateParams.from);
                } else {
                    $scope.activePage = 1;
                }
            };

            $scope.loginDubaiID = function (dubaiID) {
                LoadingService.show();
                var username = dubaiID.username.$modelValue;
                var password = dubaiID.password.$modelValue;

                $auth.loginMyId(username, password, $scope.rememberLoginData).then(function (response) {
                    if (response.responseJSON !== undefined && response.responseJSON.errorMessage !== undefined) {
                        $scope.loginMessage = response.responseJSON.errorMessage;
                    }
                }, function (error) {
                    LoadingService.hide();
                    $scope.loginMessage = error;
                });
            };

            // Registration
            $scope.userRegistration = function () {
                window.open($DMLinks.registration, '_system ', 'location=yes');
                return;
            };


            /**
             *  Hide Navigation
             */
            /*var navBar = angular.element(document.querySelector(".navbar"));
             navBar.addClass('hideNav');*/


            var i = 0;
            $('.confirmation-number').keyup(function () {
                if ($(this).val().length == $(this).attr("maxlength")) {
                    $(this).next().focus();
                }
            });
            $('.keyboard-num').on('click', function () {
                if (i < 0)
                    i = 0;
                var text = $('.comfirm-' + i);
                text.val($(this).text());
                switch (i) {
                    case 0 :
                        $scope.user.numOne = $(this).text();
                        break;
                    case 1 :
                        $scope.user.numTwo = $(this).text();
                        break;
                    case 2 :
                        $scope.user.numThree = $(this).text();
                        break;
                    case 3 : {
                        LoadingService.show();
                        $scope.user.numFour = $(this).text();
                        $scope.checkCode();
                        break;
                    }
                }
                i = i + 1;

            });
            $('.back-space').on('click', function () {
                i = i - 1;
                if (i > 3) {
                    i = 3;
                }

                var text = $('.comfirm-' + i);
                text.val('');
                switch (i) {
                    case 0 :
                        $scope.user.numOne = '';
                        break;
                    case 1 :
                        $scope.user.numTwo = '';
                        break;
                    case 2 :
                        $scope.user.numThree = '';
                        break;
                    case 3 :
                        $scope.user.numFour = '';
                        break;

                }


            });

        }]);
/*

 iDubaiApp.controller('personalDetails_2Controller', ['$scope' , function ($scope) {
 console.log('Personal Details 2');
 }]);*/
